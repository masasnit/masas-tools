MASAS Tools

For more information visit http://www.masas.ca

--
ems-icons.zip contains all of the EM symbology set icons 
http://emsymbology.org/EMS/index.html that are used by MASAS tools and
hosted at http://icon.masas-sics.ca

To use these icons locally instead, unzip, place in an accessible location, and
change to the new location in the included index.html


In order to to run Masas-tools you need to follow these steps:

**Serve Static Files**

index.html and login.html and the resources they call need to be served by a web server. This typically done with Apache but can be done with any web server.

**Running the Proxy**

The proxy is a python application that needs to be run along side the static files for the masas-tools. It serves as a proxy for the masas tools. To run the application you need to have python 2.7 installed.

Run the application using:

* python masas_proxy.py
* use the option --enableCORS if you need to enable cors for cross domain use
* use the option --serverPort <value> to server the port you which the proxy to run on

**Running the Tools Service**

The tools service is a python application that needs to be run with the masas-tools. It provides a number of features to the masas-tools. Ex: Address lookup.

Run the application using:

* python toolsService.py
* use the option --enableCORS if you need to enable cors for cross domain use
* use the option --serverPort <value> to server the port you which the tools service to run on

*NOTE:* If you are running at command line and not using a WSGI, you need to comment out some lines in the proxy.py page.
Comment out the following block:

```
    cherrypy.engine.start()
    cherrypy.engine.block()

# def application(environ,start_response):
#     cherrypy.config.update({'environment': 'embedded'})
#     cherrypy.tree.mount(RootPage(), "/proxy/")
#
#     # Enable Cross Origin Resource Sharing (aka Cross Domain requests)...
#     cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)
#     return cherrypy.tree(environ, start_response)

if __name__ == '__main__':
    # root = RootPage()
    # cherrypy.quickstart(root,"/",config = None)
    parser = argparse.ArgumentParser( description='MASAS Tools Proxy' )
    parser.add_argument( '--enableCORS', action='store_true', help='Enable CORS (default: disabled)' )
```


**CONFIGURATION**

Under /assets/config/application.json you will need to configure the following:

* Proxy URL: Set the url of the python proxy application. Requires /go/url= after. Ex: "url": "http://localhost:8080/go?url="
* Tools Service URL: Set the url of the python tools service application. Ex: "serviceTools": "http://localhost:8081"
* The other options can be changed to your preferences.

Under /assets/config/hubConfig.json add the hubs you wish to have access to log into:
   
{
        "name": "Hub Name",
        "url": "https://hub.url.com",
        "accessUrl": "https://access.url.com"
}