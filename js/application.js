/** @namespace Namespace for APP classes and functions. */
var mApp = mApp || {};

// Global Application variable.
var App = {};

// Application Class
mApp.Application = function() {
    // Constructor

    // Does the user need to login?
    var accessCode = getCookie("accesscode");
    if( !accessCode || accessCode == ""){
        window.location = "login.html";
    }

    // Load the version data...
    this._loadVersionData( "assets/buildInfo.json" );

    // Load the settings...
    this.settings = new mApp.SettingsManager( "assets/config/application.json", "assets/config/authorFilters.json" );

    // TODO: Fix this to be a cleaner solution...
    // Need to set this proxy also (for now)...
    // Need to remove the trailing ?url= for now...
    var parmLoc = this.settings.appSettings.proxyUrl.indexOf( "?url=" );
    VIEW.AJAX_PROXY_URL = decodeURI( this.settings.appSettings.proxyUrl.substring( 0, parmLoc ) );

    // Setup the theme...
    this._applyTheme();

    // Initialize a each toolkit as needed...
    this._initExt();
    this._initOpenLayers();
};

mApp.Application.prototype.run = function() {

    // Create the MASAS client...
    this.masasHub = new MASAS.Hub( this.settings.currentHub.url );

    // Get the current user...
    this.masasHub.RefreshUserData();

    // Create the MASAS Publisher...
    this.masasPublisher = new mApp.MASASPublisher();

    // Create the data stores...
    this.dataStoreMgr = new mApp.DataStoreManager();

    // Create the main view...
    this.mainView = new mApp.MainView();

    // Update the extent in the Main View...
    this.mainView.updateViewFilterExtent();

    // Load the data...
    this.dataStoreMgr.refreshAll();
};

mApp.Application.prototype._applyTheme = function() {
    var activeThemeName = 'default';

    switch( this.settings.currentHub.name )
    {
        case 'Sandbox':
            activeThemeName = 'default';
            break;
        case 'Common':
            activeThemeName = 'operational';
            break;
        case 'Training':
            activeThemeName = 'default';
            break;
        case 'Exercise':
            activeThemeName = 'exercise';
            break;
        default:
            activeThemeName = 'default';
            break;
    }

    this.setActiveStyleSheet( activeThemeName );
};

mApp.Application.prototype.setActiveStyleSheet = function( title ) {
    var links = document.getElementsByTagName("link");

    for( var i = 0; i < links.length; i++ )
    {
        var curLink = links[i];
        if( curLink.getAttribute("rel").indexOf("style") != -1 && curLink.getAttribute("title") )
        {
            curLink.disabled = true;
            if( curLink.getAttribute("title") == title ) {
                curLink.disabled = false;
            }
        }
    }
};

mApp.Application.prototype._initExt = function() {
    // TODO: Validate all of these init statements...

    // Path to the blank image should point to a valid location on your server
    Ext.BLANK_IMAGE_URL = 'libs/ExtJS-3.4.2/resources/images/default/s.gif';

    // Make components not stateful by default
    Ext.Component.prototype.stateful = false;

    // Assign a state provider
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
        // 2 years = 730 days
        expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 730))
        //, path: ''
    }));

    // Tooltips on
    Ext.QuickTips.init();
};

mApp.Application.prototype._initOpenLayers = function() {
    // TODO: Validate all of these init statements...

    // Path for OpenLayers images
    OpenLayers.ImgPath = 'libs/OpenLayers-2.13.1/img/';

    // Proxy is used for layer loading...
    OpenLayers.ProxyHost = this.settings.appSettings.proxyUrl;

    // Adding retries because default is 0
    OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;

    // TODO: Is this really needed?  Looks like it may be for an older version of IE(<9.0).
    if (!'console' in window || typeof console == 'undefined') {
        // when missing, use a mock console such as OpenLayers' default
        console = OpenLayers.Console;
    } else {
        // use the browser's console for logging including from OpenLayers
        OpenLayers.Console = console;
    }

};

/**
 * Loads the Application version data from a file
 * @param configFile The file to load the objects from.
 */
mApp.Application.prototype._loadVersionData = function( configFile ) {
    this.version = "0.0.0.0";

    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", configFile, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        var respJSON = JSON.parse( responseTxt );
        if( typeof respJSON.version !== "undefined" ) {
            this.version = respJSON.version;
        }
    }

};

mApp.Application.prototype.logout = function() {
    // Delete the Access Code cookie...
    deleteCookie( "accesscode" );

    // Delete the URI cookie...
    deleteCookie( "uri" );

    // Redirect to the login page...
    window.location = "login.html";
};

Ext.onReady( function () {
    // Trigger the app creation...
    App = new mApp.Application();
    App.run();
});