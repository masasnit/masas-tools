//functions to set and get cookies

//get a cookie for a given name
function getCookie(cname) {
//    var name = cname + "=";
//    var ca = document.cookie.split(';');
//    for(var i=0; i<ca.length; i++) {
//        var c = ca[i];
//        while (c.charAt(0)==' ') c = c.substring(1);
//        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
//    }
//    return "";
    return localStorage.getItem( cname );
}

//set a cookie with a name, value and duration
function setCookie(cname, cvalue, exdays) {
//    var d = new Date();
//    d.setTime(d.getTime() + (exdays*24*60*60*1000));
//    var expires = "expires="+d.toUTCString();
//    document.cookie = cname + "=" + cvalue + "; " + expires + ";  path=/";
    localStorage.setItem( cname, cvalue );
}

// Delete a Cookie...
function deleteCookie( cname ) {
    localStorage.removeItem( cname );
}

