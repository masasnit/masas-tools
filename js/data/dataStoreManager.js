// Data Store Manager Class
mApp.DataStoreManager = function() {
    // Constructor
    var lookupPath = 'assets/lookups/'

    this._enableDrafts = false;
    this._autoRefreshTask = null;

    // Load the Entry Templates...
    this.entryTemplateStore = new Ext.data.JsonStore({
        autoLoad: true,
        url: lookupPath + 'templates-entry.json',
        storeId: 'entryTemplates',
        root: 'entry',
        idProperty: 'name',
        fields: ['name'],
        listeners: {
            "load": this._onEntryTemplatesDataLoad
        }
    });

    // Load the Alert Templates...
    this.alertTemplateStore = new Ext.data.JsonStore({
        autoLoad: true,
        url: lookupPath + 'templates-alert.json',
        storeId: 'alertTemplates',
        root: 'alert',
        idProperty: 'name',
        fields: ['name'],
        listeners: {
            "load": this._onAlertTemplatesDataLoad
        }
    });

    // Load the Categories...
    this.categoryStore = new Ext.data.JsonStore({
        autoLoad: true,
        url: lookupPath + 'categories-entry.json',
        storeId: 'categoryLookups',
        root: 'categories',
        fields: ['e_name', 'e_tip', 'f_name', 'f_tip', 'value'],
        listeners: {
            "load": this._onCategoryLookupDataLoad
        }
    });

    // Load the Entry Symbols...
    this.entrySymbolStore = new mApp.SymbolEntryStore( lookupPath + 'symbols-entry.json' );

    // Load the Alert Symbols...
    this.alertSymbolStore = new mApp.SymbolEntryStore( lookupPath + 'symbols-alert.json' );

    // Load the Overlay Symbols...
    this.overlaySymbolStore = new Ext.data.JsonStore({
        url: lookupPath + 'symbols-overlay.json',
        storeId: 'overlaySymbols',
        fields: ['type', 'subType', 'valueEn', 'valueFr', 'symbolUrl'],
        listeners: {
            "load": this._onOverlaySymbolLookupDataLoad
        }
    });
    this.overlaySymbolStore.load();

    // TODO: REMOVE 'FEEDSOURCES'
    // create the feed source store via the constructor which then adds to
    // the Ext.StoreMgr this new storeId of 'FeedSources'
    var masasSettings = [{
        title: App.settings.currentHub.name,
        url: App.settings.currentHub.url + "/feed",
        secret: App.settings.currentHub.accessCode
    }];

    this.masas_store = new VIEW.FeedSourceStore( { data: masasSettings } );

    // Create the entries/alert data store...
    this.masasEntryStore = new Ext.data.Store({
        name: 'Entries',
        storeId: 'masasEntries',
        reader: new Ext.data.ArrayReader({
                idIndex: 0
            },
            Ext.data.Record.create([
                { name: 'identifier' },
                { name: 'entry' }
            ])
        )
    });

    // Create the entries/alert DRAFT data store...
    this.masasDraftStore = new Ext.data.Store({
        name: 'Drafts',
        storeId: 'masasDrafts',
        reader: new Ext.data.ArrayReader({
                idIndex: 0
            },
            Ext.data.Record.create([
                { name: 'identifier' },
                { name: 'entry' }
            ])
        )
    });

    // Create the overlay data store...
    this.masasOverlayStore = new Ext.data.Store({
        name: 'Overlays',
        storeId: 'masasOverlays',
        reader: new Ext.data.ArrayReader(
            {
                idIndex: 0
            },
            Ext.data.Record.create([
                { name: 'identifier' },
                { name: 'overlay' }
            ])
        )
    });

    // Enable the feed readers...
    this.masasFeed = new mApp.MASASFeedReader( App.settings.currentHub.url + "/feed", App.settings.currentHub.accessCode, this.masasEntryStore, App.settings.appSettings.proxyUrl );
    this.masasDraftFeed = new mApp.MASASDraftFeedReader( App.settings.currentHub.url + "/feed", App.settings.currentHub.accessCode, this.masasDraftStore, App.settings.appSettings.proxyUrl );
    this.masasOverlayFeed = new mApp.MASASOverlayReader( App.settings.currentHub.url + "/overlays", App.settings.currentHub.accessCode, this.masasOverlayStore, App.settings.appSettings.proxyUrl );

    // Enable the auto refresh if needed...
    this.updateAutoRefresh();
};

mApp.DataStoreManager.prototype._onEntryTemplatesDataLoad = function( records, options ) {
};

mApp.DataStoreManager.prototype._onAlertTemplatesDataLoad = function( records, options ) {
};

mApp.DataStoreManager.prototype._onCategoryLookupDataLoad = function( records, options ) {
};

mApp.DataStoreManager.prototype._onOverlaySymbolLookupDataLoad = function( records, options ) {
};

mApp.DataStoreManager.prototype.refreshAll = function() {
    App.mainView.mainStatusBar.showLoadingFeeds();

    this._loadingErrors = [];
    this._loadingInfos = [];
    this._loadingFeeds = [ this.masasFeed, this.masasOverlayFeed ];

    if( this._enableDrafts ) {
        this._loadingFeeds.push( this.masasDraftFeed );
    }

    this.masasEntryStore.removeAll();
    this.masasDraftStore.removeAll();
    this.masasOverlayStore.removeAll();

    this.masasFeed.getFeedData( this._onMasasFeedSuccess, this._onMasasFeedFail );
    this.masasOverlayFeed.getFeedData( this._onMasasOverlayFeedSuccess, this._onMasasFeedFail );

    if( this._enableDrafts ) {
        this.masasDraftFeed.getFeedData( this._onMasasDraftFeedSuccess, this._onMasasFeedFail );
    }
};

mApp.DataStoreManager.prototype._onMasasFeedSuccess = function( feed, itemCount ) {
    var dataStoreMgr = App.dataStoreMgr;

    var max_results = App.settings.userSettings.masasEntryLimit == 0 ? 1000 : App.settings.userSettings.masasEntryLimit;

    if( itemCount == 0 ) {
        dataStoreMgr._loadingInfos.push( 'No Entries were found for this view' );
    }
    else if( max_results != 0 && itemCount >= max_results )
    {
        // this max results warning will appear for the sum total of
        // entries for all Feeds as it not only warns about possible
        // truncation for an individual Feed, but browser performance
        // issues as well.
        dataStoreMgr._loadingInfos.push( 'Maximum Entries Exceeded',
            '<b>The maximum number of map Entries has been reached for' +
                ' this view.<br>' + 'Only the ' + max_results +
                ' most recent Entries may be displayed.</b><br><br>Please' +
                ' use the map to zoom in to an area of interest to reduce' +
                ' the number of Entries needing to be displayed on the map.' +
                ' <br><br>If necessary, you can adjust the Entry Display' +
                ' Limit in the Advanced settings panel.' );
    }

    dataStoreMgr._updateLoadingFeed( feed );
};

mApp.DataStoreManager.prototype._onMasasDraftFeedSuccess = function( feed, itemCount ) {
    App.dataStoreMgr._updateLoadingFeed( feed );
};

mApp.DataStoreManager.prototype._onMasasOverlayFeedSuccess = function( feed, itemCount ) {
    App.dataStoreMgr._updateLoadingFeed( feed );
};

mApp.DataStoreManager.prototype._onMasasFeedFail = function( feed, message ) {
    var dataStoreMgr = App.dataStoreMgr;

    dataStoreMgr._loadingErrors.push( feed.name + ": " + message );
    dataStoreMgr._updateLoadingFeed( feed );
};

mApp.DataStoreManager.prototype._updateLoadingFeed = function( feed ) {
    var foundIndex = -1;
    for( var i=0; i < this._loadingFeeds.length; i++ ) {
        if( this._loadingFeeds[i] == feed ) {
            foundIndex = i;
            break;
        }
    }
    if( foundIndex > -1 ) {
        this._loadingFeeds.splice( foundIndex, 1 );
    }

    if( this._loadingFeeds.length == 0 ) {
        App.mainView.mainStatusBar.doneLoadingFeeds();

        // If any errors, show the user...
        if( this._loadingErrors.length > 0 )
        {
            var errorMessage = "An error has been encountered:<br>" + this._loadingErrors.join( "<br>" );
            Ext.Msg.alert( "Loading Data Error", errorMessage );
        }

        // If any information, show the user...
        if( this._loadingInfos.length > 0 )
        {
            var infoMessage = this._loadingInfos.join( "<br>" );
            Ext.Msg.alert( "Loading Data Info", infoMessage );
        }

        this._loadingErrors = [];
        this._loadingInfos = [];

        // Update the geometry...
        VIEW.checkGeometry({onLoad: true});

        // Update the "Show All Geometry"...
        if( VIEW.mapLayers.MASASLayerGeometries.getVisibility() == true ) {
            VIEW.show_all_geometries();
        }
    }
};

mApp.DataStoreManager.prototype.updateAutoRefresh = function() {
    this.enableAutoRefresh( App.settings.userSettings.enableAutoRefresh, App.settings.userSettings.refreshRate );
};

mApp.DataStoreManager.prototype.enableAutoRefresh = function( enable, refreshRate ) {
    var dsMgr = this;
    var newInterval = parseInt( refreshRate ) * 60 * 1000;

    if( enable )
    {
        if( this._autoRefreshTask && this._autoRefreshTask.interval != newInterval )
        {
            Ext.TaskMgr.stop( this._autoRefreshTask );
            this._autoRefreshTask = null;
        }

        this._autoRefreshTask = Ext.TaskMgr.start( {
            run: dsMgr._onAutoRefresh,
            interval: newInterval,
            scope: dsMgr
        } );
    }
    else
    {
        if( this._autoRefreshTask ) {
            // Stop the task...
            Ext.TaskMgr.stop( this._autoRefreshTask );
            this._autoRefreshTask = null;
        }
    }

};

mApp.DataStoreManager.prototype._onAutoRefresh = function() {
    // Skip the first run..
    if( this._autoRefreshTask.taskRunCount > 1 ) {
        // Auto Refresh callback...
        this.refreshAll();
    }
};

mApp.DataStoreManager.prototype.enableDrafts = function( enable ) {
    this._enableDrafts = enable;
};

mApp.DataStoreManager.prototype.isDraftsEnable = function() {
    return this._enableDrafts;
};
