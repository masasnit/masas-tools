
mApp.MASASDraftFeedReader = function( url, token, store, proxyUrl ) {
    mApp.MASASReader.call( this, url, token, store, proxyUrl );
    this.name = "Drafts";
};

mApp.MASASDraftFeedReader.prototype = Object.create( mApp.MASASReader.prototype );
mApp.MASASDraftFeedReader.prototype.constructor = mApp.MASASDraftFeedReader;

mApp.MASASDraftFeedReader.prototype.getFeedData = function( callback_success, callback_fail ) {
    var params = {};

    // Add the "update_allow" filter...
    params['update_allow'] = App.masasHub.userData.id;

    // Force to get the non effective items also...
    var currentDate = new Date();
    currentDate.setMilliseconds( 0 );
    params['dtinstance'] = currentDate.toISOString().replace( ".000", "" );

    // cache buster will be filtered out by the proxy and not passed to hub
    params['_dc'] = new Date().getTime();

    this.readData( params, callback_success, callback_fail );
}

mApp.MASASDraftFeedReader.prototype._parseFeed = function( rawFeed ) {
    var entries = [];

    $feedEntries = $(rawFeed).find( "entry" ).has( "category[scheme='masas:category:status'][term='Draft']");

    for( var entryCtr = 0; entryCtr < $feedEntries.length; entryCtr++ )
    {
        try {
            var newEntry = new MASAS.Entry();
            newEntry.FromNode( $feedEntries[entryCtr] );

            entries.push( [
                newEntry.identifier,
                newEntry
            ]);
        }
        catch( ex ) {
        }
    }

    return entries;
};
