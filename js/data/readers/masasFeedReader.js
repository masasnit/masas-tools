
mApp.MASASFeedReader = function( url, token, store, proxyUrl ) {
    mApp.MASASReader.call( this, url, token, store, proxyUrl );
    this.name = "Entry";
};

mApp.MASASFeedReader.prototype = Object.create( mApp.MASASReader.prototype );
mApp.MASASFeedReader.prototype.constructor = mApp.MASASFeedReader;

mApp.MASASFeedReader.prototype.getFeedData = function( callback_success, callback_fail ) {
    var params = {};

    // add geospatial bbox query to Hub request
    params['bbox'] = App.mainView.getViewFilterExtent().toBBOX(4);

    params['lang'] = App.settings.userSettings.language.substr( 0, 2 );

    var timeFilter = App.settings.userSettings.timeFilter;
    switch( timeFilter.type ) {
        case 'current':
            // By default we want only to see the effective entries, the hub does this for us.
            // If the user has requested not to filter effective, then we need all the data from
            // the hub.  This can be done by asking for an "instance" at now.
            if( App.settings.userSettings.effectiveOnly == false ) {
                var timeNow = new Date();
                timeNow.setMilliseconds( 0 );
                params['dtinstance'] = timeNow.toISOString().replace( ".000", "" );
            }
            break;
        case 'instance':
            params['dtinstance'] = timeFilter.startDate.toISOString().replace( ".000", "" );
            break;
        case 'since':
            params['dtsince'] = timeFilter.startDate.toISOString().replace( ".000", "" );
            break;
        case 'range':
            params['dtstart'] = timeFilter.startDate.toISOString().replace( ".000", "" );
            params['dtend'] = timeFilter.endDate.toISOString().replace( ".000", "" );
            break;
    }

    // Filter Test Messages.  NOTE: Default is filtered!
    if( App.settings.userSettings.filterTestEntries == false ) {
        params['suppressTestMessages'] = 'False';
    }

    // Filter any US (author filters) account.
    if( App.settings.userSettings.filterUSAlerts )
    {
        var authorList = [];
        for( var authorCtr = 0; authorCtr < App.settings.userSettings.authorFilters.length; authorCtr++ )
        {
            var curAuthor = App.settings.userSettings.authorFilters[authorCtr];
            if( curAuthor.enable ) {
                authorList.push( curAuthor.accountURI );
            }
        }

        if( authorList.length > 0 ) {
            params['not_author'] = authorList.join();
        }
    }

    if( App.settings.userSettings.masasEntryLimit > 0 ) {
        params['limit'] = App.settings.userSettings.masasEntryLimit;
    }

    // cache buster will be filtered out by the proxy and not passed to hub
    params['_dc'] = new Date().getTime();

    this.readData( params, callback_success, callback_fail );
}

mApp.MASASFeedReader.prototype._parseFeed = function( rawFeed ) {
    var entries = [];

    $feedEntries = $(rawFeed).find( "entry" );

    for( var entryCtr = 0; entryCtr < $feedEntries.length; entryCtr++ )
    {
        var entryFiltered = false;
        var newEntry = new MASAS.Entry();

        try {
            newEntry.FromNode( $feedEntries[entryCtr] );
        }
        catch( ex ) {
            newEntry = null;
        }

        if( newEntry != null )
        {
            // Apply the effective filter if needed...
            // Only do this on an "Instance" and "Range" query;
            // "Current" handles this via the Hub.
            if( App.settings.userSettings.effectiveOnly == true && newEntry.effective )
            {
                switch( App.settings.userSettings.timeFilter.type ) {
                    case 'current':
                        // We don't need to do anything in this case...
                        break;
                    case 'instance':
                        var instance = App.settings.userSettings.timeFilter.startDate;
                        if( newEntry.effective > instance ) {
                            entryFiltered = true;
                        }
                        break;
                    case 'since':
                        var endTime = new Date();
                        var startTime = App.settings.userSettings.timeFilter.startDate;
                        if( newEntry.effective > endTime || newEntry.effective < startTime ) {
                            entryFiltered = true;
                        }
                        break;
                    case 'range':
                        var startTime = App.settings.userSettings.timeFilter.startDate;
                        var endTime = App.settings.userSettings.timeFilter.endDate;
                        if( newEntry.effective > endTime || newEntry.effective < startTime ) {
                            entryFiltered = true;
                        }
                        break;
                }
            }

            if( entryFiltered != true )
            {
                entries.push( [
                    newEntry.identifier,
                    newEntry
                ]);
            }
        }
    }

    return entries;
};
