
mApp.MASASOverlayReader = function( url, token, store, proxyUrl ) {
    mApp.MASASReader.call( this, url, token, store, proxyUrl );
    this.name = "Overlay";
    this.filterHazards = true;
    this.filterResources = true;
    this.filterInfra = true;
};

mApp.MASASOverlayReader.prototype = Object.create( mApp.MASASReader.prototype );
mApp.MASASOverlayReader.prototype.constructor = mApp.MASASOverlayReader;

mApp.MASASOverlayReader.prototype.getFeedData = function( callback_success, callback_fail ) {
    var params = {};

    // Active State...
    if( App.settings.userSettings.overlayActiveState !== "all" ) {
        params['activeState'] = App.settings.userSettings.overlayActiveState;
    }

    // Owner Id...
    if( App.settings.userSettings.overlayShowOnlyMine ) {
        params['ownerId'] = App.masasHub.userData.id;
    }

    // add geospatial boundingBox query to Hub request
    params['boundingBox'] = App.mainView.getViewFilterExtent().toBBOX(4);

    // cache buster will be filtered out by the proxy and not passed to hub
    params['_dc'] = new Date().getTime();

    this.readData( params, callback_success, callback_fail );
};

mApp.MASASOverlayReader.prototype._parseFeed = function( rawFeed ) {
    var overlays = [];

    var overlayObjs = rawFeed;

    for( var ctr = 0; ctr < overlayObjs.length; ctr++ )
    {
        var curOverlay = overlayObjs[ctr];

        var overlay = {
            id: curOverlay.id,
            title: curOverlay.title,
            description: curOverlay.description,
            type: curOverlay.type,
            subtype: curOverlay.subtype,
            expiresDTG: new Date( curOverlay.expiresDTG ),
            digest: curOverlay.digest,
            geometry: curOverlay.geometry,
            ownerid: curOverlay.ownerid,
            ownerName: curOverlay.ownerName,
            active: curOverlay.active,
            organizationid: curOverlay.organizationid,
            updateAllowed: curOverlay.updateAllowed,
            createdDTG: new Date( curOverlay.createdDTG ),
            links: []
        };

        // Get the links
        if( curOverlay.links !== undefined )
        {
            for( var linkCtr = 0; linkCtr < curOverlay.links.length; linkCtr++ )
            {
                var link = {
                    text: curOverlay.links[linkCtr].text,
                    url: curOverlay.links[linkCtr].url
                };

                overlay.links.push( link );
            }
        }

        var filter = ( (overlay.type == "hazard" && this.filterHazards ) ||
                       (overlay.type == "resource" && this.filterResources ) ||
                       (overlay.type == "infrastructure" && this.filterInfra ) )

        if( !filter ) {
            overlays.push( [overlay.id, overlay]);
        }
    }

    return overlays;
};
