
mApp.MASASReader = function( url, token, store, proxyUrl ) {
    this.name = "MASAS Feed";
    this._url = url;
    this._token = token;
    this._store = store;
    this._proxyUrl = proxyUrl;
};

mApp.MASASReader.prototype.readData = function( params, callback_success, callback_fail ) {
    var reader = this;
    this._getData( params,
        function( rawFeed ) {
            // Success - Parse data...
            var records = reader._parseFeed( rawFeed );

            // Add data to the store...
            reader._store.removeAll();
            reader._store.loadData( records );

            if( callback_success && typeof( callback_success ) === "function" ) {
                callback_success( reader, records.length );
            }
        },
        function( responseMsg ) {
            if( callback_fail && typeof( callback_fail ) === "function" ) {
                callback_fail( reader, responseMsg );
            }
        });
};

mApp.MASASReader.prototype._getData = function( params, callback_success, callback_fail ) {
    var fullUrl = "";

    var paramStr = "";
    if( params ) {
        paramStr = $.param( params );
    }

    fullUrl = this._url + "?secret=" + this._token;
    if( paramStr.length > 0 ) {
        fullUrl += "&" + paramStr;
    }

    // Create the access url...
    if( this._proxyUrl != undefined && this._proxyUrl != null ) {
        fullUrl = this._proxyUrl + encodeURIComponent( fullUrl );
    }

    var request = $.ajax({
        type: 'GET',
        url: fullUrl,
        timeout: 600000
    });

    request.done( function( responseMsg ) {
        if( callback_success && typeof( callback_success ) === "function" ) {
            callback_success( responseMsg );
        }
    });

    request.fail( function(jqXHR, textStatus) {
        var failureMsg = 'Failed to retrieve MASAS User data! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
        var status_num = parseInt( textStatus, 10 );
        if (status_num === 401) {
            failureMsg = 'Feed Error - Access Code is not valid or authorized.';
        }

        if( callback_fail && typeof( callback_fail ) === "function" ) {
            callback_fail( failureMsg );
        }

    });
};

mApp.MASASReader.prototype._parseFeed = function( rawFeed ) {

};

