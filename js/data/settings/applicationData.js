// Application Data Class
mApp.ApplicationData = function() {
    // Set the defaults...
    this.setDefaults();
};

mApp.ApplicationData.prototype.setDefaults = function() {
    this.proxyEnabled = false;
    this.proxyUrl = "";

    this.masasDefaultEnableAutoRefresh = true;
    this.masasDefaultRefreshRate = 5;

    this.serviceTools = "https://localhost:8081";
    this.serviceSymbols = "http://icon.masas-sics.ca";

    this.mapDefaultViewExtent = "-180.0,40.0,-20.0,70.0";

    // Generated from loaded settings...
    this.serviceTool_addressSearchUrl = "address_search";
    this.serviceTool_exportEntryUrl = "export_entry";
    this.serviceTool_favoriteEntryUrl = "favorite_entry";
    this.serviceTool_getAreasUrl = "get_areas";

    this.iconLocation = "img/";

    this.supportLink = "";
    this.masasSiteLink = "";
    this.masasSiteLabel = "";
};

mApp.ApplicationData.prototype.loadFromJsonObj = function( jsonObj ) {
    // Reset the data...
    this.setDefaults();

    // Get the proxy information...
    var proxyObj = this._getParameterFromObject( jsonObj, "proxy", null );

    if( proxyObj != null ) {
        this.proxyEnabled = this._getParameterFromObject( proxyObj, "enable", this.proxyEnabled );
        this.proxyUrl = this._getParameterFromObject( proxyObj, "url", this.proxyUrl );
    }

    // Get the MASAS information...
    var masasObj = this._getParameterFromObject( jsonObj, "MASAS", null );
    if( masasObj != null ) {
        this.masasDefaultEnableAutoRefresh = this._getParameterFromObject( masasObj, "defaultEnableAutoRefresh", this.masasDefaultEnableAutoRefresh );
        this.masasDefaultRefreshRate = this._getParameterFromObject( masasObj, "defaultRefreshRate", this.masasDefaultRefreshRate );

        this.masasAccess_searchUrl = this.masasAccessUrl + "/api/search";
    }

    // Services...
    this.serviceTools = this._getParameterFromObject( jsonObj, "serviceTools", this.serviceTools );
    this._generateServerToolsEndPoints();

    this.serviceSymbols = this._getParameterFromObject( jsonObj, "serviceSymbols", this.serviceSymbols );

    // Map configuration...
    var mapObj = this._getParameterFromObject( jsonObj, "map", null );
    if( mapObj != null ) {
        this.mapDefaultViewExtent = this._getParameterFromObject( mapObj, "defaultExtent", this.mapDefaultViewExtent );
    }

    // Support Link...
    this.supportLink = this._getParameterFromObject( jsonObj, "supportLink", this.supportLink );

    // MASAS Site Link...
    this.masasSiteLink = this._getParameterFromObject( jsonObj, "masasSiteLink", this.masasSiteLink );
    this.masasSiteLabel = this._getParameterFromObject( jsonObj, "masasSiteLabel", this.masasSiteLabel );
};

mApp.ApplicationData.prototype._getParameterFromObject = function( jsonObject, paramName, defaultValue ) {
    if( typeof jsonObject[paramName] !== "undefined" ) {
        return jsonObject[paramName];
    }

    return defaultValue;
};

mApp.ApplicationData.prototype._generateServerToolsEndPoints = function() {
    this.serviceTool_addressSearchUrl = this.serviceTools + "/" + this.serviceTool_addressSearchUrl;
    this.serviceTool_exportEntryUrl = this.serviceTools + "/" + this.serviceTool_exportEntryUrl;
    this.serviceTool_favoriteEntryUrl = this.serviceTools + "/" + this.serviceTool_favoriteEntryUrl;
    this.serviceTool_getAreasUrl = this.serviceTools + "/" + this.serviceTool_getAreasUrl;
};
