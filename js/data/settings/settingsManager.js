// Settings Manager Class
mApp.SettingsManager = function( configFileLocation, authorFilterLocation ) {
    // Constructor

    // Initialize internal data...
    this._cUserDataStorageKey = "userData";

    // Load the application settings...
    this._loadAppSettings( configFileLocation );

    // Load the user settings...
    this._loadUserSettings();

    // Load the author filters...
    this._loadAuthorFilters( authorFilterLocation );
};

/**
 * Loads the application settings from a file
 * @param configFile The file to load the objects from.
 * @returns {Array} The array of objects if exists/available, empty otherwise.
 */
mApp.SettingsManager.prototype._loadAppSettings = function( configFile ) {
    this.appSettings = new mApp.ApplicationData();

    this.currentHub = {
        name: getCookie( "hubName" ),
        url: getCookie( "hubUrl" ),
        accessUrl:  getCookie( "accessUrl" ),
        accessCode: getCookie( "accesscode" )
    };

    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", configFile, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        var respJson = JSON.parse( responseTxt );

        this.appSettings.loadFromJsonObj( respJson );
    }

};

mApp.SettingsManager.prototype._loadUserSettings = function() {
    this.userSettings = new mApp.UserData( this );

    var rawUserData = localStorage.getItem( this._cUserDataStorageKey );
    if( rawUserData != undefined )
    {
        var jsonUserData = JSON.parse( rawUserData );
        if( jsonUserData != undefined )
        {
            this.userSettings.loadFromJsonObj( jsonUserData );
        }
    }
};

mApp.SettingsManager.prototype._loadAuthorFilters = function( configFile ) {
    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", configFile, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        var respJson = JSON.parse( responseTxt );

        this.userSettings.authorFilters = respJson.authors;
    }
};

mApp.SettingsManager.prototype.saveUserSettings = function() {
    var jsonUserData = this.userSettings.toJsonObj();
    localStorage.setItem( this._cUserDataStorageKey, JSON.stringify( jsonUserData ) );
}
