// User Data Class
mApp.UserData = function( settingsManager ) {
    this._settingsMgr = settingsManager;

    // Set the defaults...
    this.setDefaults();
};

mApp.UserData.prototype.setDefaults = function() {

    // User Data that will not be saved...

    // Time filters...
    this.timeFilter = {
        type: "current", // current, instance, since, range
        startDate: new Date(),
        endDate: new Date()
    };

    // Filter to retrieve only effective only...
    this.effectiveOnly = true;

    // Test Entries/Alert filter...
    this.filterTestEntries = true;

    // User Data that will be saved...
    this.language = "en-ca";
    this.autoCenterPolygonMarkup = true;

    this.showDetailedPopup = true;
    this.masasEntryLimit = 400;

    this.enableAutoRefresh = this._settingsMgr.appSettings.masasDefaultEnableAutoRefresh;
    this.refreshRate = this._settingsMgr.appSettings.masasDefaultRefreshRate;

    this.defaultViewExtent = this._settingsMgr.appSettings.mapDefaultViewExtent;

    this.filterUSAlerts = true;

    this.overlayActiveState = "all"; // active, deactive,all
    this.overlayShowOnlyMine = false;

    // NOTE: This is currently populated by the Settings Manager...
    this.authorFilters = [];

};

mApp.UserData.prototype.loadFromJsonObj = function( jsonObject ) {
    this.language = this._getParameterFromObject( jsonObject, "language", this.language );
    this.autoCenterPolygonMarkup = this._getParameterFromObject( jsonObject, "autoCenterPolygonMarkup", this.autoCenterPolygonMarkup );
    this.showDetailedPopup = this._getParameterFromObject( jsonObject, "showDetailedPopup", this.showDetailedPopup );
    this.masasEntryLimit = this._getParameterFromObject( jsonObject, "masasEntryLimit", this.masasEntryLimit );
    this.enableAutoRefresh = this._getParameterFromObject( jsonObject, "enableAutoRefresh", this.enableAutoRefresh );
    this.refreshRate = this._getParameterFromObject( jsonObject, "refreshRate", this.refreshRate );
    this.defaultViewExtent = this._getParameterFromObject( jsonObject, "defaultViewExtent", this.defaultViewExtent );

    this.filterUSAlerts = this._getParameterFromObject( jsonObject, "filterUSAlerts", this.filterUSAlerts );

    this.overlayActiveState = this._getParameterFromObject( jsonObject, "overlayActiveState", this.overlayActiveState );
    this.overlayShowOnlyMine = this._getParameterFromObject( jsonObject, "overlayShowOnlyMine", this.overlayShowOnlyMine );
};

mApp.UserData.prototype.toJsonObj = function() {
    // Create the json object...
    var jsonUserData = {
        "language": this.language,
        "autoCenterPolygonMarkup": this.autoCenterPolygonMarkup,
        "showDetailedPopup": this.showDetailedPopup,
        "masasEntryLimit": this.masasEntryLimit,
        "enableAutoRefresh": this.enableAutoRefresh,
        "refreshRate": this.refreshRate,
        "defaultViewExtent": this.defaultViewExtent,
        "filterUSAlerts": this.filterUSAlerts,
        "overlayActiveState": this.overlayActiveState,
        "overlayShowOnlyMine": this.overlayShowOnlyMine
    }

    return jsonUserData;
};

mApp.UserData.prototype._getParameterFromObject = function( jsonObject, paramName, defaultValue ) {
    if( typeof jsonObject[paramName] !== "undefined" ) {
        return jsonObject[paramName];
    }

    return defaultValue;
};