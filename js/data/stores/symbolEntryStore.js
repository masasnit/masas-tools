// Symbol Entry Store Class
mApp.SymbolEntryStore = function( url ) {
    this.symbols = [];
    this._loadData( url );
};

mApp.SymbolEntryStore.prototype._loadData = function( url ) {
    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", url, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        this.symbols = JSON.parse( responseTxt );
    }
};

mApp.SymbolEntryStore.prototype.findNodeByTerm = function( term ) {
    var foundNode = this._findNodeByAttr( this.symbols, "term", term );
    return foundNode;
};

mApp.SymbolEntryStore.prototype.findNodeByAttr = function( attribute, value ) {
    var foundNode = this._findNodeByAttr( this.symbols, attribute, value );
    return foundNode;
};

mApp.SymbolEntryStore.prototype._findNodeByAttr = function( node, attr, attrValue ) {
    var symbolEntryStore = this;
    var foundNode = null;

    node.every( function( childNode ) {
        if( childNode.hasOwnProperty( "leaf" ) )
        {
            if( childNode[attr] === attrValue ) {
                foundNode = childNode;
                return false;
            }
        }
        else
        {
            foundNode = symbolEntryStore._findNodeByAttr( childNode.children, attr, attrValue );
            if( foundNode !== null ) {
                return false;
            }
        }

        return true;
    });

    return foundNode;
};