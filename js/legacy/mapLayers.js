/**
MASAS View Tool - Map Layers
Updated: Jul 15, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings for the top toolbar and
map layers.
*/

/*global VIEW,Ext,OpenLayers,GeoExt,google */

Ext.ns('VIEW.MapLayers');

/**
 * Setup map layers once application loaded and ready
 */
VIEW.MapLayers.initialize = function () {
    
    /* Base Layers */
    
    var base_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts'],
        ['OSM Tile Service', 'osm'],
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest'],
        ['XYZ Grid Map Service', 'xyz' ]
    ];
    
    var base_layer_fields = [
        // required fields for all layer types, the LayerRecord already has
        // defined 'layer' and 'title' with 'name' mapping to title.
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // optional fields
        // use "defaultValue" to set a (non-falsy) value for something thats missing
        {name: 'base_layer', type: 'boolean', defaultValue: true},
        // layers that can't be deleted, defaults for all users
        {name: 'permanent', type: 'boolean'},
        {name: 'map_type', type: 'string'},
        // allows overriding the map default of 16 depending on the layer
        {name: 'zoom_levels', type: 'integer'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'showOnToolbar', type: 'boolean'},
        // holds references to the toolbar button and layers menu item
        {name: 'button'},
        {name: 'menu'}
    ];
    
    VIEW.MapLayers.baseLayers = new VIEW.Layers.LayerStore({
        'storeId': 'baseLayers',
        'map': VIEW.map,
        'fields': base_layer_fields,
        'layer_types': base_layer_types,
        'base_layers': true
    });

    // Load the layers...
    VIEW.MapLayers.loadBaseLayers();

    // start out with the first available layer
    if( VIEW.MapLayers.baseLayers.getCount() > 0 ) {
        VIEW.map.setBaseLayer( VIEW.MapLayers.baseLayers.getAt(0).getLayer() );
    }
    
    /* Overlay Layers */
    
    var overlay_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts']
        ['ArcGIS REST', 'application/vnd.esri.arcgis.rest'],
        ['ArcGIS Feature Map Service REST', 'application/vnd.esri.arcgis.mapservice.rest'],
        ['KML', 'application/vnd.google-earth.kml+xml']
    ];
    
    var overlay_layer_fields = [
        {name: 'visibility', type: 'boolean'},
        //NOTE: HTTP Basic Auth urls should contain "https://username:password@server"
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        // in the store its a string, but is converted to float for layer
        {name: 'opacity', type: 'string'},
        {name: 'permanent', type: 'boolean'},
        {name: 'legend_url', type: 'string'},
        {name: 'attribution', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'showOnToolbar', type: 'boolean'},
        {name: 'button'},
        {name: 'menu'}
    ];
    
    VIEW.MapLayers.overlayLayers = new VIEW.Layers.LayerStore({
        'storeId': 'overlayLayers',
        'map': VIEW.map,
        'fields': overlay_layer_fields,
        'layer_types': overlay_layer_types
    });

    // Load the overlays...
    VIEW.MapLayers.loadOverlayLayers();

    /* Entry Layers */
    
    var entry_layer_types = [
        ['Web Map Service', 'application/vnd.ogc.wms'],
        ['Tile Map Service', 'application/vnd.osgeo.tms'],
        //['Web Map Tile Service', 'application/vnd.ogc.wmts'],
        ['Entry Geometry', 'entry']
    ];
    
    var entry_layer_fields = [
        {name: 'visibility', type: 'boolean'},
        {name: 'url', type: 'string'},
        {name: 'type', type: 'string'},
        {name: 'opacity', type: 'string'},
        // used for entry geometry data
        {name: 'geom_type', type: 'string'},
        {name: 'geom_val', type: 'string'}
    ];
    
    VIEW.MapLayers.entryLayers = new VIEW.Layers.LayerStore({
        'storeId': 'entryLayers',
        'map': VIEW.map,
        'fields': entry_layer_fields,
        'layer_types': entry_layer_types
    });

};

/**
 * Loads the base layers from a file.
 */
VIEW.MapLayers.loadBaseLayers = function() {
    var filteredLayers = [];

    var allowGoogleLayers = true;

    // Google Maps is normally the default due to being listed first,
    // however it may fail to load, which can happen with some networks with
    // restrictive firewalls.  In that case, OSM becomes the fallback default
    if( !( 'google' in window ) )
    {
        allowGoogleLayers = false;

        alert('A problem with your network prevented the Google Map from ' +
            'loading properly.  Open Street Map will be used instead.  If ' +
            'you still want to use the Google Map, you can reload this ' +
            'webpage to try again.');
    }

    // Load the layers...
    var layers = VIEW.MapLayers.loadLayersFromFile( "./assets/config/layers_base.json", "base_layers" );

    // Let's add the maps the the base_data structure...
    for( var baseLayerCtr = 0; baseLayerCtr < layers.length; baseLayerCtr++ )
    {
        var curLayer = layers[baseLayerCtr];
        var layerEnabled = true;

        // Check the Enabled property...
        if( curLayer.hasOwnProperty( "enabled") ) {
            layerEnabled = curLayer.enabled;
        }

        // If we have a "google" layer but with no support...
        if( curLayer.type == "google" && allowGoogleLayers == false )
        {
            // Don't add it!
            layerEnabled = false;
        }

        if( layerEnabled == true ) {
            filteredLayers.push( curLayer );
        }
    }

    if( filteredLayers.length == 0 )
    {
        alert( "A base map has not been properly configure: Please contact the Administrator. A default map will be used instead." )

        // We need a base map... otherwise it looks bad!
        filteredLayers.push( {
            "name": "OpenStreetMap",
            "url": "http://otile1.mqcdn.com/tiles/1.0.0/map/",
            "type": "osmm",
            "visibility": false,
            "base_layer": true,
            "permanent": true,
            "zoom_levels": 19,
            "attribution": "MapQuest-OSM",
            "description": "OpenStreetMap tiles from MapQuest",
            "showOnToolbar": true
        } );
    }

    VIEW.MapLayers.baseLayers.loadData( filteredLayers );
};

/**
 * Loads the overlay layers from a file.
 */
VIEW.MapLayers.loadOverlayLayers = function() {
    var filteredLayers = [];

    // Load the layers...
    var layers = VIEW.MapLayers.loadLayersFromFile( "./assets/config/layers_overlays.json", "overlay_layers" );

    // Let's add the maps the the base_data structure...
    for( var baseLayerCtr = 0; baseLayerCtr < layers.length; baseLayerCtr++ )
    {
        var curLayer = layers[baseLayerCtr];
        var layerEnabled = true;

        // Check the Enabled property...
        if( curLayer.hasOwnProperty( "enabled") ) {
            layerEnabled = curLayer.enabled;
        }

        if( layerEnabled == true ) {
            filteredLayers.push( curLayer );
        }
    }

    VIEW.MapLayers.overlayLayers.loadData( filteredLayers );
};

/**
 * Loads an array of objects from a file
 * @param fileName The file to load the objects from.
 * @param attribute The attribute name of the array to return
 * @returns {Array} The array of objects if exists/available, empty otherwise.
 */
VIEW.MapLayers.loadLayersFromFile = function( fileName, attribute ) {
    var layers = [];

    var httpReq = new XMLHttpRequest();

    // Open/read the file in sync mode...
    httpReq.open( "GET", fileName, false );
    httpReq.send();

    if( httpReq.status == 200 && httpReq.readyState == 4 )
    {
        var responseTxt = httpReq.responseText;
        var respJSON = JSON.parse( responseTxt );

        if( respJSON.hasOwnProperty( attribute ) ) {
            layers = respJSON[attribute];
        }
    }

    return layers;
};
