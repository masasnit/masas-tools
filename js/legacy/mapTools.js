/**
MASAS View Tool - Map Tools / Bottom Toolbar
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings and handlers needed for
the bottom toolbar and tools of the map.

@requires src/AddressSearch.js
*/

/*global VIEW,Ext,OpenLayers,GeoExt,google */

Ext.ns('VIEW.MapTools');

// stores any measuring Actions
VIEW.MapTools.Measurements = {};
// stores button to clear location if needed
VIEW.MapTools.locationClearBtn = null;


/**
 * Zooms the map to full extent of the load feed layer
 */
VIEW.MapTools.zoom_to_load_extent = function () {
    var load_extent = VIEW.mapLayers.loadFeed.getDataExtent();
    if (load_extent) {
        // reducing the load extent because otherwise the map will move to
        // the next highest zoom level when using the original value
        load_extent = load_extent.scale(0.9);
        VIEW.map.zoomToExtent(load_extent);
    }
};


/**
 * Uses the browser's geolocation functions to zoom the map to the user's
 * approximate location.  If GPS is available its fairly accurate, but if not
 * the browser may try Geo IP based methods which are pretty rough.
 */
VIEW.MapTools.zoom_to_geolocation = function () {
    if (!navigator.geolocation) {
        alert('Geolocation is not supported by your browser.');
        return;
    }
    if (Ext.Msg.isVisible()) {
        // since Ext.Msg is a singleton, will have to wait until another
        // operation such as a feed load is complete
        alert('Please wait until other loading operations are complete.');
        return;
    }
    Ext.Msg.show({
        title: null,
        msg: 'Attempting to locate your current position...',
        buttons: Ext.Msg.CANCEL,
        // since there is no cleanup or way to cancel a pending
        // getCurrentPosition call, cancels are very simple
        fn: function () {
            // Geolocate Cancel...
        },
        closable: false,
        wait: true,
        modal: true,
        minWidth: Ext.Msg.minProgressWidth,
        waitConfig: null
    });
    navigator.geolocation.getCurrentPosition(VIEW.MapTools.geolocation_result_success,
        VIEW.MapTools.geolocation_result_error, {
            //TODO: work out the best timeout value to use
            timeout: 30000,
            // allow a cached position if less than 1 minute old
            maximumAge: 60000
        }
    );
};


/**
 * Success handler for geolocation
 *
 * @param {Object} - the resulting position with values in .coords
 */
VIEW.MapTools.geolocation_result_success = function (position) {
    if (Ext.Msg.isVisible()) {
        Ext.Msg.hide();
        // use the location layer similar to address search
        var location_xy = new OpenLayers.LonLat(position.coords.longitude,
            position.coords.latitude);
        location_xy.transform(new OpenLayers.Projection('EPSG:4326'),
            VIEW.map.getProjectionObject());
        var location_point = new OpenLayers.Geometry.Point(location_xy.lon,
            location_xy.lat);
        var location_feature = new OpenLayers.Feature.Vector(location_point, null, {
            fillColor: 'red',
            fillOpacity: 1,
            strokeColor: 'red',
            strokeOpacity: 1,
            pointRadius: 6,
            //TODO: reverse geocode the location and add the address to this label
            //      and to the address search box?
            label: 'Your Location',
            labelYOffset: 15
        });
        VIEW.mapLayers.location.addFeatures([location_feature]);
        VIEW.map.setCenter(location_xy, 14);
        // enable the clear button on the toolbar
        if (VIEW.MapTools.locationClearBtn) {
            VIEW.MapTools.locationClearBtn.show();
        }
    }
    // if the message box wasn't visible, assume the user cancelled
    // the geolocation and do nothing
};


/**
 * Error handler for geolocation
 * 
 * @param {Object} - error results, usually a numeric value in .code
 */
VIEW.MapTools.geolocation_result_error = function (error) {
    if (Ext.Msg.isVisible()) {
        Ext.Msg.hide();
        var err_msg = 'Geolocation Error';
        // relying on codes vs error.message as it should be more cross-browser
        var err_codes = {
            1: 'Permission denied',
            2: 'Position unavailable',
            3: 'Request timeout'
        };
        if (error.code) {
            err_msg += ' - ' + err_codes[error.code];
        }
        alert(err_msg);
    }
};


/**
 * Hide or Show All Entry Geometry
 *
 * @param {Object} - the Ext.menu.checkItem component
 * @param {Boolean} - checked state
 */
VIEW.MapTools.all_geometry_toggle = function( cmp, checked ) {
    VIEW.mapLayers.MASASLayerGeometries.setVisibility( checked );
    if( checked ) {
        VIEW.show_all_geometries();
    }
    else {
        VIEW.mapLayers.MASASLayerGeometries.destroyFeatures();
    }
};


/**
 * Activate a measuring tool: Point, Line, or Area
 *
 * @param {Object} - the Ext.menu component
 */
VIEW.MapTools.measurement_toggle = function (cmp) {
    // one tool at a time should be active
    for (var k in VIEW.MapTools.Measurements) {
        if (VIEW.MapTools.Measurements.hasOwnProperty(k)) {
            VIEW.MapTools.Measurements[k].disable();
        }
    }
    // create on demand only
    if (!VIEW.MapTools.Measurements[cmp.measureType]) {
        VIEW.MapTools.Measurements[cmp.measureType] = new GeoExt.ux[cmp.measureType]({
            activateOnEnable: true,
            autoDeactivate: true,
            deactivateOnDisable: true,
            map: VIEW.map,
            controlOptions: {
                geodesic: true
            }
        });
    }
    VIEW.MapTools.Measurements[cmp.measureType].enable();
};


/**
 * Hide or Show the scale line
 *
 * @param {Object} - the Ext.menu.checkItem component
 * @param {Boolean} - checked state
 */
VIEW.MapTools.scale_line_toggle = function (cmp, checked) {
    if (checked) {
        // need to create/re-create each time its added
        VIEW.mapControls.scale = new OpenLayers.Control.ScaleLine();
        VIEW.map.addControl(VIEW.mapControls.scale);
    } else {
        VIEW.map.removeControl(VIEW.mapControls.scale);
        VIEW.mapControls.scale = null;
    }
};


/**
 * Hide or Show the overview map
 *
 * @param {Object} - the Ext.menu.checkItem component
 * @param {Boolean} - checked state
 */
VIEW.MapTools.overview_map_toggle = function (cmp, checked) {
    /* Create the overviewMapPanel control on demand.  It can't be
     * created as part of the other controls since the map needs
     * to be fully loaded first and adding later also removes a blue
     * border that appears inside the window otherwise.  On demand
     * means that an error only appears for those who use it, rather
     * than all users, because when closing in the destroy method for
     * OpenLayers doesn't know that the GeoExt version
     * has already destroyed the window.  Further work is needed
     * on the GeoExt extension to resolve this, although it does only apply
     * when exiting the view tool, usually on IE.
     */
    if (!VIEW.MapTools.overviewMapPanel) {
        // use the current default base layer, either google or OSM
        var overview_layer = VIEW.MapLayers.baseLayers.getAt(0).getLayer().clone();
        VIEW.MapTools.overviewMapPanel = new GeoExt.ux.OverviewMapPanel({
            map: VIEW.map,
            width: 200,
            height: 150,
            renderTo: Ext.getBody(),
            hidden: true,
            layers: [overview_layer],
            ctrlOptions: {
                minRatio: 5,
                maxRatio: 30,
                autoPan: true
            },
            border: true,
            cls: 'overviewPanel'
        });
        VIEW.mapControls.overviewMap = VIEW.MapTools.overviewMapPanel.control;   
    }
    if (VIEW.MapTools.overviewMapPanel) {
        if (checked) {
            VIEW.MapTools.overviewMapPanel.show();
        } else {
            VIEW.MapTools.overviewMapPanel.hide();
        }
    }
};


/**
 * Show the Google StreetView Panel
 */
VIEW.MapTools.streetview_activate = function () {
    if (!VIEW.mapControls.streetview) {
        // create on demand only
        VIEW.mapControls.streetview = new GeoExt.ux.GoogleStreetView();
        VIEW.map.addControl(VIEW.mapControls.streetview);
    }
    VIEW.mapControls.streetview.activate();
};
