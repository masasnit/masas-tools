/**
MASAS View Tool - AddressSearch
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file displays the address search box and the code.  Address search 
requires a proxy to be set.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * Address search box for remote geocode searches
 */
VIEW.AddressSearchCombo = Ext.extend(Ext.form.ComboBox, {
    /**
     * Config
     */
    width: 200,
    listWidth: 300,
    // minimum characters before search query is sent
    minChars: 6,
    // hide trigger of the combo
    hideTrigger: true,
    forceSelection: true,
    loadingText: 'Searching...',
    emptyText: 'Address Search',
    allowBlank: true,
    displayField: 'address',
    
    /**
     * Constructor
     *
     * @param {Object} - config
     */
    constructor: function (config) {
        if (config.url) {
            this.url = config.url;
        } else {
            throw new Error('Missing url');
        }
        if (config.layer) {
            this.layer = config.layer;
        } else {
            throw new Error('Missing layer');
        }
        if (config.clearBtn) {
            this.clearBtn = config.clearBtn;
        } else {
            throw new Error('Missing clear button');
        }
        
        VIEW.AddressSearchCombo.superclass.constructor.call(this, config);
    },
    
    /**
     * Initialize
     */
    initComponent: function () {
        var addrStore = new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({
                url: this.url,
                method: 'GET',
                disableCaching: false
            }),
            reader: new Ext.data.JsonReader({
                root: 'results',
                fields: ['address', 'lat', 'lon']
            })
        });
        
        Ext.applyIf(this, {
            store: addrStore,
            listeners: {
                select: this.handleSelect,
                scope: this
            }
        });
        
        VIEW.AddressSearchCombo.superclass.initComponent.call(this);
    },
    
    /**
     * Show a selected address on the location map
     */
    handleSelect: function (combo, record) {
        var locationMap = this.layer.map;
        var address_xy = new OpenLayers.LonLat(record.data.lon, record.data.lat);
        address_xy = address_xy.transform(new OpenLayers.Projection('EPSG:4326'),
            locationMap.getProjectionObject());
        var address_point = new OpenLayers.Geometry.Point(address_xy.lon, address_xy.lat);
        var address_feature = new OpenLayers.Feature.Vector(address_point, null, {
            fillColor: 'red',
            fillOpacity: 1,
            strokeColor: 'red',
            strokeOpacity: 1,
            pointRadius: 6,
            label: record.data.address,
            labelYOffset: 15
        });
        this.layer.addFeatures([address_feature]);
        // simple zoom to for now
        locationMap.setCenter(address_xy, 14);
        // enable the clear button on the toolbar
        if (this.clearBtn) {
            this.clearBtn.show();
        }
    }
});
