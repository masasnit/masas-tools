/**
MASAS View Tool - ContentDisplay
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file generates a tabbed popup panel that displays details about an entry.
The subpanels are defined in the file ContentPanelTypes.js
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * Content display window
 */
VIEW.ContentWindow = Ext.extend(Ext.Window, {
    /**
     * Config
     */
    updateAllowed: false,
    activeTab: 0,
    links: null,
//    feedInfo: null,
    modelStoreId: null,
    collapsible: true,
    minimizedHeight: 350,
    maximizedHeight: 600,
    shadow: false,

    /**
     * Initialize
     */
    initComponent: function () {
        var links = this.links;
//        var secret = (this.feedInfo) ? this.feedInfo.get('secret') : null;
        var secret = App.settings.currentHub.accessCode;
        var panels = {
            Atom: VIEW.ContentPanel.Atom({
                url: links.Atom.href,
                'secret': secret,
                // include the Show Revisions link in this template if there
                // are any, creating a shortcut to the revisions panel
                showRevisions: true,
                listeners: {
                    'contentloaded': function (panel) {
                        // the Atom content is async loaded, once its
                        // complete, load the resulting source panel
                        // and display geometry
                        panels.Source.Atom.cleanXml = panel.cleanXml;
                        panels.Source.Atom.loadContent();
                        VIEW.show_entry_geometry(panels.Atom.entryID);
                    }
                }
            }),
            Source: {
                Atom: VIEW.ContentPanel.Source({
                    title: 'Hub Entry Source'
                })
            }
        };
        //NOTE: only supporting a single CAP message associated with an Entry
        //      at this time.
        if (links.CAP) {
            //TODO: the links from the Entry that is present in the Feed are
            //      are used to load CAP messages, not the newly downloaded
            //      Entry used in the Atom content panel.  This means that if the
            //      Entry's CAP links have changed since the Feed was
            //      loaded, they will be incorrect.  In order to fix this,
            //      the latest Entry version would need to be parsed accordingly.
            panels.CAP = VIEW.ContentPanel.CAP({
                url: links.CAP.href,
                'secret': secret,
                listeners: {
                    'contentloaded': function (panel) {
                        panels.Source.CAP.cleanXml = panel.cleanXml;
                        panels.Source.CAP.loadContent();
                    }
                }
            });
            panels.Source.CAP = VIEW.ContentPanel.Source({
                title: 'CAP Message Source',
                listeners: {
                    // since the CAP message loads on demand, making sure its
                    // already loaded in the other panel before displaying the
                    // XML source
                    'show': function (panel) {
                        if (!panel.contentLoaded) {
                            panels.CAP.loadContent();
                        }
                    }
                }
            });
        }
        if (links.revisions) {
            panels.Revisions = VIEW.ContentPanel.Revisions({
                url: links.revisions.href,
                'secret': secret,
                listeners: {
                    'contentloaded': function (cmp) {
                        // add handlers to show popup windows with content
                        Ext.select('.CAPRevisionLink').on({
                            'click': VIEW.show_CAP_revision,
                            scope: cmp
                        });
                        Ext.select('.AttachmentRevisionLink').on({
                            'click': VIEW.show_attachment_revision,
                            scope: cmp
                        });
                        // add handlers to hide/show details div
                        Ext.select('.btnRevisionDetails', true).each(function (el) {
                            var idx = el.dom.id.split('-')[1];
                            var review_button = new Ext.Button({
                                text: 'Show Details',
                                renderTo: el.dom,
                                handler: function (btn) {
                                    Ext.select('#entryRev-' + idx).toggleClass('x-hide-display');
                                    if (btn.getText() === btn.initialConfig.text) {
                                        btn.setText('Hide Details');
                                    } else {
                                        btn.setText(btn.initialConfig.text);
                                    }
                                },
                                scope: cmp
                            });
                        }, cmp);
                        // add handlers to hide/show revision geometries
                        Ext.select('.btnRevisionGeometry', true).each(function (el) {
                            var idx = el.dom.id.split('-')[1];
                            var geometry_button = new Ext.Button({
                                text: 'Show Geometry',
                                renderTo: el.dom,
                                handler: function (btn) {
                                    var elem = Ext.get('btnRevGeometry-' + idx);
                                    var fid = elem.getAttribute('data-updated');
                                    var feature = VIEW.mapLayers.MASASLayerEntry.getFeatureByFid(fid);
                                    if (feature) {
                                        if (btn.getText() === btn.initialConfig.text) {
                                            btn.setText('Hide Geometry');
                                            feature.renderIntent = 'revision';
                                        } else {
                                            btn.setText(btn.initialConfig.text);
                                            feature.renderIntent = 'delete';
                                        }
                                        VIEW.mapLayers.MASASLayerEntry.redraw();
                                    }
                                },
                                scope: cmp
                            });
                        }, cmp);
                    }
                }
            });
        }
        if (links.attachments) {
            //TODO: the links from the Entry that is present in the Feed are
            //      are used to load attachments, not the newly downloaded
            //      Entry used in the Atom content panel.  This means that if the
            //      Entry's attachment links have changed since the Feed was
            //      loaded, they will be incorrect.  In order to fix this,
            //      the latest Entry version would need to be parsed accordingly.
            var attach = links.attachments;
            for (var i = 0; i < attach.length; i++) {
                // OpenLayers MASAS parser determined whether this attachment
                // is an image, which will display inline, or some other
                // type which needs an external application to view
                //TODO: add another type later for Map Layers
                var type = (attach[i].type === 'image') ? 'Image' : 'External';
                // create the panel type if needed, supporting multiple
                // attachments of the same type
                if (!panels[type]) {
                    panels[type] = [];
                }
                panels[type].push( VIEW.ContentPanel[type]({
                    'secret': secret,
                    'linkData': attach[i]
                }) );
            }
        }
        // access logs are only generated for content items like CAP and attachments
        if (links.CAP || links.attachments) {
            panels.Access = VIEW.ContentPanel.Access({
                url: links.Atom.href + '/content/log',
                'secret': secret
            });
        }
        
        this.panels = panels;
        var tabs = this.buildTabs(panels);

        // The apply() was added so not to modify the logic behind the applyIf().
        // This needs to be revisited!!!
        Ext.apply(this, {
            buttonAlign: 'left',
            buttons: [
                {
                    text: 'Edit...',
                    ref: '../btnEdit',
                    cls: 'x-btn-left',
                    ctCls: 'width_100',
                    iconCls: 'btnEditItemIcon',
                    hidden: true,
                    scope: this,
                    handler: this._onEditClick
                },
                {
                    xtype: 'displayfield',
                    ctCls: 'width_100'
                },
                {
                    text: "Close",
                    cls: 'x-btn-right',
                    scope: this,
                    handler: this._onCloseClick
                }
            ]
        });

        Ext.applyIf(this, {
            // for mobile clients the normal close button is very small
            // and hard to use.  To modify it to make it bigger takes
            // a lot of overriding, so this hackish method provides
            // an easier way for touch based clients to close windows
            title: 'Entry Content <a class="titleClose" href="#" ' +
                'onclick="VIEW.contentWindow.close(); return false;">Close</a>',
            closable: true,
            width: 700,
            height: this.minimizedHeight,
            border: false,
            layout: 'fit',
            renderTo: Ext.getBody(),
            listeners: {
                'close': VIEW.close_content_window,
                'show': this.alignToViewport,
                'render': this._onRender,
                scope: this
            },
            items: [{
                xtype: 'grouptabpanel',
                tabWidth: 130,
                ref: 'tabs',
                activeGroup: this.activeTab,
                items: tabs,
                deferredRender: false,
                listeners: {
                    // except for "Hub Entry Source", which is the already loaded
                    // Entry XML, will load on demand the appropriate data linked
                    // to each tab when that tab is clicked on the panel
                    'tabchange': function (gtpanel, tab) {
                        if (gtpanel.activeGroup.items.itemAt(0) == tab) {
                            gtpanel.activeGroup.setActiveTab(gtpanel.activeGroup.items.itemAt(1));
                        } else {
                            //NOTE: previously when changing between tabs, if you
                            //      went back to a tab, it reloaded that content again.
                            //      Added this check as it doesn't seem necessary
                            //      to reload again if previously successful
                            if (!tab.contentLoaded) {
                                if (tab.loadContent) {
                                    tab.loadContent();
                                }
                            }
                        }
                    },
                    // the close event iterates through each panel in the group
                    // closing it, and setting the next one in the group as
                    // active until none are left.  This resulted in loadContent
                    // calls loading links like the access log, history, etc
                    // when an entry content window was closed.  Removing any
                    // listener that would trigger loadContent.
                    'beforedestroy': function (gtpanel) {
                        // since tabchange and show are added as anonymous
                        // functions, the normal removeListener method can't
                        // be used
                        try {
                            gtpanel.events['tabchange'].clearListeners();
                            if (this.panels.Source.CAP) {
                                this.panels.Source.CAP.events['show'].clearListeners();
                            }
                        } catch (err) {
                        }
                    },
                    scope: this
                }
            }]
        });
        VIEW.ContentWindow.superclass.initComponent.call(this);
    },

    _onRender: function() {
        this.btnEdit.setVisible( this.updateAllowed );
        this.btnEdit.ownerCt.doLayout();
    },

    _onCloseClick: function() {
        this.close();
    },

    _onEditClick: function() {
        if( this.links.CAP ) {
            App.mainView.editAlert( this.links.Atom.href );
        }
        else {
            App.mainView.editEntry( this.links.Atom.href );
        }
    },

    /**
     * Overwrite the window's native toggleCollapse function
     */
    toggleCollapse: function () {
        /* in SVN Revision 190:191 mpriour committed the addition of this
        with comment "Adjust window resize logic" */
        var collapsed = this.getHeight() <= this.maximizedHeight * 0.95;
        var height = collapsed ? this.maximizedHeight : this.minimizedHeight;
        var currPoss = this.getPosition();
        var top = currPoss[1];
        var left = currPoss[0];
        var htdiff = this.maximizedHeight - this.minimizedHeight;
        var newTop = collapsed ? top - htdiff : top + htdiff;
        this.setPagePosition(left, newTop);
        this.setHeight(height);
        this.doLayout();
        this.alignToViewport(this);
        this.getEl().toggleClass('x-panel-collapsed');
    },
    
    /**
     * Align the window to the viewport corner
     */
    alignToViewport: function (cmp) {
        var el = cmp.getEl();
        el.anchorTo(Ext.getBody(), 'br-br', [-5, -5], false, true);
    },
    
    /**
     * Build the content display tabs
     */
    buildTabs: function (panels) {
        /*tabs are: 
         * [atom,cap?],
         * [attachments?,individual attachements?],
         * [advanced,Revisions?,atom Source,cap source?,Access?]
         */
        var groups = [];
        var i = 0;
        var messageItems = [{title: 'Messages', layout: 'fit', html: 'Messages'},
            panels.Atom]; 
        var attachmentItems = [];
        var advancedItems = [];
        if (panels.CAP) {
            messageItems.push(panels.CAP);
        }
        groups[i++] = {
            expanded: true,
            items: messageItems,
            autoScroll: true,
            ref: 'messages'
        };
        if (panels.Image || panels.External) {
            // start with the attachments section header Tab
            var attach_panels = [{
                xtype: 'panel',
                title: 'Attachments',
                layout: 'fit',
                html: 'Attachments'
            }];
            // add other panel Tabs accordingly
            if (panels.Image) {
                attach_panels.push.apply(attach_panels, panels.Image);
            }
            if (panels.External) {
                attach_panels.push.apply(attach_panels, panels.External);
            }
            groups[i++] = {
                expanded: true,
                autoScroll: true,
                ref: 'attachments',
                items: attach_panels
            };
        }
        groups[i] = {
            expanded: false,
            ref: 'advanced',
            items: [{title: 'Advanced', xtype: 'panel', layout: 'fit', html: 'Advanced'},
                panels.Source.Atom],
            autoScroll: true
        };
        if (panels.Source.CAP) {
            groups[i].items.push(panels.Source.CAP);
        }
        if (panels.Revisions) {
            groups[i].items.push(panels.Revisions);
        }
        if (panels.Access) {
            groups[i].items.push(panels.Access);
        }
        return groups;
    }

});

Ext.reg('mv_contentwindow', VIEW.ContentWindow);
