/**
MASAS View Tool - ContentPanelTypes
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines the various panel tabs that are shown in the content
display popup.  The panel types are defined and then are generated via the
various factories.
*/

/*global VIEW,Ext,OpenLayers,GeoExt,xmlJsonClass */

Ext.ns('VIEW');

/**
 * Extends a normal Panel to add MASAS content
 */
VIEW.ContentPanel = Ext.extend(Ext.Panel, {
    /**
     * Config
     */
    template: null,
    layout: 'fit',
    autoScroll: true,
    deferredLayout: false,
    html: '<div class="x-mask-loading"><div class="loading-indicator">Loading...</div></div>',
    
    /**
     * Initialize
     */
    initComponent: function () {
        VIEW.ContentPanel.superclass.initComponent.call(this);
        this.addEvents({'contentloaded': true});
    }
});


/**
 * Extends the base content panel for XML content like Entries and CAP
 */
VIEW.ContentPanel.XML = Ext.extend(VIEW.ContentPanel, {
    /**
     * Config
     */
    secret: null,
    url: null,
    ignoreNamespace: null,
    cleanXml: null,
    // indicates whether the content needs additional transformation and what type
    contentTransform: null,
    // indicates whether the Show Revisions link should be shown on an Atom
    // Template page.  Currently only the main Content window Panel.
    showRevisions: false,
    
    /**
     * Load the content data for the Content Panel
     */
    loadContent: function (secret, url) {
        secret = secret || this.secret;
        url = url || this.url;
        var params = {};
        if (secret && url) {
            var defaultOpts = {
                method: 'GET',
                scope: this,
                discardUrl: true,
                text: 'Loading...'
            };
            // 30 secs to download Entry, others like CAP or the special
            // feeds may take longer so allow up to 1 minute
            var time_out = 30;
            if (this.contentTransform) {
                if (this.contentTransform !== 'atom') {
                    time_out = 60;
                }
            }
            // use the proxy if available and this is an absolute URL versus
            // a local relative URL
            if (VIEW.AJAX_PROXY_URL && Ext.form.VTypes.url(url)) {
                params = {
                    'url': Ext.urlAppend(url, 'secret=' + secret),
                    'timeout': time_out
                };
                url = VIEW.AJAX_PROXY_URL;
            } else {
                params = {'secret': secret, 'timeout': time_out};
            }
            Ext.Ajax.request(Ext.apply(defaultOpts, {
                'url': url,
                'timeout': time_out * 1000,
                callback: this.handleContentData,
                'params': params
            }));
        } else {
            alert('Unable to Load Entry Content');
        }
    },
    
    /**
     * Callback for loading content that parses and formats it for display
     * using XTemplates.
     */
    handleContentData: function (req, success, response) {
        var display_xml = '';
        var display_html = '';
        if (success) {
            display_xml = response.responseText;
            if (this.ignoreNamespace) {
                var ignoreNS = this.ignoreNamespace;
                if (Ext.isString(ignoreNS)) {
                    ignoreNS = [ignoreNS];
                }
                Ext.each(ignoreNS, function (ns) {
                    if (display_xml.search('xmlns:' + ns) !== -1) {
                        display_xml = display_xml.replace('xmlns:' + ns, 'xmlns');
                        display_xml = display_xml.replace(/<(\/?)([^:>]*:)?([^>]+)>/g,
                            '<$1$3>');
                    }
                });
            }
            try {
                var data_json = xmlJsonClass.xml2json(this.parseXml(
                    display_xml.replace(/[\r\n]+/g, '')), '  ');
                var contentObj = Ext.decode(data_json);
            } catch (err) {
                display_html += '<div style="font-weight: bold; color: red">Data Error</div>';
            }
            // perform any required transformations for this type of content
            // such as converting dates to local time
            if (this.contentTransform) {
                if (this.contentTransform === 'atom') {
                    contentObj = this.transformAtom(contentObj);
                    // here is the only place where the entry ID can be found
                    // and set for access by the parent content window
                    this.entryID = contentObj.entry.id;
                } else if (this.contentTransform === 'cap') {
                    contentObj = this.transformCAP(contentObj);
                } else if (this.contentTransform === 'revisions') {
                    contentObj = this.transformRevisions(contentObj);
                } else if (this.contentTransform === 'access') {
                    contentObj = this.transformAccess(contentObj);
                }
            }
            try {
                // XTemplate from templates.js
                display_html += this.template.apply(contentObj);
                this.contentLoaded = true;
            } catch (err) {
                display_html += '<div style="font-weight: bold; color: red">Template Error</div>';
            }
        } else {
            display_html += '<div style="font-weight: bold; color: red">Load Error</div>';
        }
        display_xml = Ext.util.Format.nl2br(Ext.util.Format.htmlEncode(display_xml));
        this.cleanXml = display_xml;
        if (this.getEl()) {
            this.getEl().update(display_html);
        } else {
            this.html = display_html;
        }
        this.fireEvent('contentloaded', this, this.contentLoaded);
    },
    
    /**
     * Parse any XML content using the browser's builtin methods
     */
    parseXml: function (xml) {
        var dom = null;
        if (window.DOMParser) {
            try {
                dom = (new DOMParser()).parseFromString(xml, 'text/xml');
            } catch (err) {
                dom = null;
            }
        }
        else if (window.ActiveXObject) {
            try {
                dom = new ActiveXObject('Microsoft.XMLDOM');
                dom.async = false;
                if (!dom.loadXML(xml)) {
                    dom = null;
                }
            } catch (err) {
                dom = null;
            }
        }
        if (dom == null) {
            // Unable to parse XML...
        }
        return dom;
    },
    
    /**
     * Transform some of the Entry elements for better template presentation
     */
    transformAtom: function (content) {
        // the result may not be an entry (feed/error message/etc) so returning
        // with an id so that a template error will at least inform the user
        if (!content.entry) {
            content.entry = {id: 'Unknown'};
            return content;
        }
        // if a prefix is used it needs to be changed for the template
        // to work, : isn't allowed
        if (content.entry.hasOwnProperty('age:expires')) {
            content.entry.expires = content.entry['age:expires'];
        }
        if (content.entry.hasOwnProperty('met:effective')) {
            content.entry.effective = content.entry['met:effective'];
        }
        
        var offset = new Date().getTimezoneOffset() / -60;
        function to_local_time(dtime) {
            var dtime_adj = Date.parseDate(dtime, 'Y-m-d\\TH:i:s\\Z');
            dtime_adj = VIEW.adjust_time(dtime_adj, offset);
            return Ext.util.Format.date(dtime_adj, 'l, M j Y - H:i:s');
        }
        
        // convert display times to the browser's local timezone
        //TODO: add an advanced option to allow users to work in the original
        //      UTC values only, skipping this conversion to local time
        content.entry.published = to_local_time(content.entry.published);
        // save the original updated prior to transformation for use in history
        // as its needed to match with the Hub
        content.entry.original_updated = content.entry.updated;
        content.entry.updated = to_local_time(content.entry.updated);
        content.entry.expires = to_local_time(content.entry.expires);
        if (content.entry.effective) {
            content.entry.effective = to_local_time(content.entry.effective);
        }

        var lang = App.settings.userSettings.language;

        function find_text_by_lang(elem, target) {
            var result;
            try {
                if (elem['@type'] === 'xhtml') {
                    if (elem.div.div instanceof Array) {
                        // in case nothing matches language preference, starting
                        // out with a default, replaced when a match
                        result = elem.div.div[0]['#text'];
                        for (var i = 0; i < elem.div.div.length; i++) {
                            if (elem.div.div[i]['@xml:lang'] === target) {
                                result = elem.div.div[i]['#text'];
                                break;
                            }
                        }
                    } else {
                        // use whatever provided for a single language
                        result = elem.div.div['#text'];
                    }
                } else {
                    // default is xhtml, odd case it might be type text instead
                    result = elem['#text'];
                }
            } catch (err) {
            }
            return result;
        }
        
        // create alternate title and content with text values in the user's
        // selected language for presentation
        content.entry.title2 = find_text_by_lang(content.entry.title, lang);
        content.entry.content2 = find_text_by_lang(content.entry.content, lang);
        
        // should always be multiple links to check
        for (var i = 0; i < content.entry.link.length; i++) {
            if (content.entry.link[i]['@rel'] === 'edit') {
                // the direct url to this Entry, used for Entry options
                content.entry.entry_url = content.entry.link[i]['@href'];
            } else if (content.entry.link[i]['@rel'] === 'related') {
                // determine if this link is on the same Hub and should be
                // displayed using a template instead
                //TODO: assumes only Entries will be linked, however attachments
                //      could be as well requiring additional handling
                var hub_domain = this.url.replace('http://', '').replace(
                    'https://', '').split('/')[0];
                if (content.entry.link[i]['@href'].search(hub_domain) !== -1) {
                    content.entry.link[i].local = true;
                }
                // determine if this link is to a layer supported by the entry
                // layer store, which will then be displayed differently
                var layer_types = [];
                for (var j = 0; j < VIEW.MapLayers.entryLayers.layer_types.length; j++) {
                    layer_types.push(VIEW.MapLayers.entryLayers.layer_types[j][1]);
                }
                if (layer_types.indexOf(content.entry.link[i]['@type']) > -1) {
                    content.entry.link[i].layer = true;
                }
            }
        }
        // put the secret value at the entry. level so the template can access
        // it via parent. to build the actual URL
        content.entry.secret = this.secret;
        
        // controls the show revisions link in the template, currently only the
        // Atom template on the content window panel uses this
        if (this.showRevisions) {
            content.entry.show_revisions = true;
        }
        
        // make geometry type and value available to templates
        if (content.entry.hasOwnProperty('georss:point')) {
            content.entry.geom_type = 'point';
            content.entry.geom_val = content.entry['georss:point'];
        } else if (content.entry.hasOwnProperty('georss:line')) {
            content.entry.geom_type = 'line';
            content.entry.geom_val = content.entry['georss:line'];
        } else if (content.entry.hasOwnProperty('georss:polygon')) {
            content.entry.geom_type = 'polygon';
            content.entry.geom_val = content.entry['georss:polygon'];
        } else if (content.entry.hasOwnProperty('georss:box')) {
            content.entry.geom_type = 'box';
            content.entry.geom_val = content.entry['georss:box'];
        } else if (content.entry.hasOwnProperty('georss:circle')) {
            content.entry.geom_type = 'circle';
            content.entry.geom_val = content.entry['georss:circle'];
        }
        
        return content;
    },
    
    /**
     * Transform some of the CAP elements for better template presentation
     */
    transformCAP: function (content) {
        var layer_features = [];
        var newid, geometry, feature;
        var commonUtils = mApp.CommonUtils.getInstance();
        var user_lang = 'en';

        // used to show the info block areas in user's language
        var info_lang = -1;
        if (!(content.alert.info instanceof Array)) {
            // convert from a single object to an array for processing
            content.alert.info = [content.alert.info];
            // assume that single info alerts will show area regardless of
            // language match
            info_lang = 0;
        }
        for (var i = 0; i < content.alert.info.length; i++) {
            if (content.alert.info[i].language) {
                if (content.alert.info[i].language.search(user_lang) > -1) {
                    info_lang = i;
                }
            }
            // add ability to view geometry associated with an area block
            if (content.alert.info[i].area) {
                if (!(content.alert.info[i].area instanceof Array)) {
                    content.alert.info[i].area = [content.alert.info[i].area];
                }
                for (var j = 0; j < content.alert.info[i].area.length; j++) {
                    // each area block will have a unique ID, also between
                    // alerts so Revision area blocks can be viewed
                    newid = content.alert.identifier + '-Area-' + i + '-' + j;
                    content.alert.info[i].area[j].areaid = newid;
                    if (info_lang === i) {
                        // VIEW.show_CAP_area toggles the area on/off and is
                        // dependent on matching the areadisplay value
                        content.alert.info[i].area[j].areadisplay = '[Hide]';
                    } else {
                        content.alert.info[i].area[j].areadisplay = '[Show]';
                    }
                    // any polygon(s) and circle(s) associated with this
                    // area block will be added using the same area ID so that
                    // they will all be turned on at once
                    if (content.alert.info[i].area[j].polygon) {
                        if (!(content.alert.info[i].area[j].polygon instanceof Array)) {
                            content.alert.info[i].area[j].polygon = [content.alert.info[i].area[j].polygon];
                        }
                        for (var k = 0; k < content.alert.info[i].area[j].polygon.length; k++) {
                            try {
                                // convert from CAP "lat,lon lat,lon" to GeoRSS
                                geometry = commonUtils.parseSimplePolygon( content.alert.info[i].area[j].polygon[k].replace(/,/g, ' ') );
                                geometry.transform(new OpenLayers.Projection('EPSG:4326'),
                                    VIEW.map.getProjectionObject());
                                feature = new OpenLayers.Feature.Vector(geometry);
                                // the show/hide links will use this value to fetch the
                                // right feature
                                feature.attributes.areaid = newid;
                                // hidden is default, clean up is taken care of by
                                // the Entry content window removing all features
                                // from the layer when the window is closed
                                if (info_lang === i) {
                                    feature.renderIntent = 'revision';
                                } else {
                                    feature.renderIntent = 'delete';
                                }
                                layer_features.push(feature);
                            } catch (err) {
                            }
                        }
                    }
                    if (content.alert.info[i].area[j].circle) {
                        if (!(content.alert.info[i].area[j].circle instanceof Array)) {
                            content.alert.info[i].area[j].circle = [content.alert.info[i].area[j].circle];
                        }
                        for (var k = 0; k < content.alert.info[i].area[j].circle.length; k++) {
                            try {
                                // convert from CAP "lat,lon radius(km)" to GeoRSS
                                var circle_vals = content.alert.info[i].area[j].circle[k].replace(/,/g, ' ').split(' ');
                                circle_vals[2] = parseFloat(circle_vals[2]) * 1000;
                                geometry = commonUtils.parseSimpleCircle( circle_vals[0] + ' ' + circle_vals[1] + ' ' + circle_vals[2],
                                                                          40, VIEW.map.getProjectionObject(),
                                                                          new OpenLayers.Projection('EPSG:4326') );
                                feature = new OpenLayers.Feature.Vector(geometry);
                                feature.attributes.areaid = newid;
                                if (info_lang === i) {
                                    feature.renderIntent = 'revision';
                                } else {
                                    feature.renderIntent = 'delete';
                                }
                                layer_features.push(feature);
                            } catch (err) {
                            }
                        }
                    }
                }
            }
        }
        if (layer_features.length > 0) {
            VIEW.mapLayers.MASASLayerEntry.addFeatures(layer_features);
        }
        
        return content;
    },
    
    /**
     * Transforms Revision feeds for better template presentation
     */
    transformRevisions: function (content) {
        if (content.feed.entry) {
            if (!(content.feed.entry instanceof Array)) {
                // a single Entry is an object, conversion needed
                content.feed.entry = [content.feed.entry];
            }
            var updated_entries = [];
            var layer_features = [];
            var u_entry, geometry, transform, feature;
            var commonUtils = mApp.CommonUtils.getInstance();

            // top level template value
            content.summary = content.feed.entry.length + ' Revisions';
            // hub default is 10 + original
            if (content.feed.entry.length > 10) {
                content.summary = '10 Most Recent Revisions';
            }
            for (var i = 0; i < content.feed.entry.length; i++) {
                // common entry transforms like datetimes
                u_entry = this.transformAtom({'entry': content.feed.entry[i]});
                // revisions should use the revision history feed's URL to
                // generate a direct access URL for this revision on the Hub.
                //NOTE: the Hub doesn't add a trailing slash to the history url
                u_entry.entry.entry_url = this.url + "/" +
                    u_entry.entry.original_updated;
                try {
                    // adding the revision geometries to the Entry Layer
                    transform = true;
                    if (u_entry.entry['georss:point']) {
                        geometry = commonUtils.parseSimplePoint(u_entry.entry['georss:point']);
                    } else if (u_entry.entry['georss:line']) {
                        geometry = commonUtils.parseSimpleLine(u_entry.entry['georss:line']);
                    } else if (u_entry.entry['georss:polygon']) {
                        geometry = commonUtils.parseSimplePolygon(u_entry.entry['georss:polygon']);
                    } else if (u_entry.entry['georss:box']) {
                        geometry = commonUtils.parseSimpleBox(u_entry.entry['georss:box']);
                    } else if (u_entry.entry['georss:circle']) {
                        geometry = commonUtils.parseSimpleCircle(u_entry.entry['georss:circle'],
                            40, VIEW.map.getProjectionObject(),
                            new OpenLayers.Projection('EPSG:4326'));
                        transform = false;
                    } else {
                        throw new Error('Unknown GeoRSS type');
                    }
                    if (transform) {
                        geometry.transform(new OpenLayers.Projection('EPSG:4326'),
                            VIEW.map.getProjectionObject());
                    }
                    feature = new OpenLayers.Feature.Vector(geometry);
                    // the show/hide buttons will use this value to fetch the
                    // right feature
                    feature.fid = u_entry.entry.updated;
                    // should be hidden to start, clean up is taken care of by
                    // the Entry content window removing all features from the
                    // layer when the window is closed
                    feature.renderIntent = 'delete';
                    layer_features.push(feature);
                } catch (err) {
                }
                updated_entries.push(u_entry.entry);
            }
            content.feed.entry = updated_entries;
            if (layer_features.length > 0) {
                VIEW.mapLayers.MASASLayerEntry.addFeatures(layer_features);
            }
        }
        
        return content;
    },
    
    /**
     * Transforms Access Log feeds for better template presentation
     */
    transformAccess: function (content) {
        if (content.feed) {
            // the access log records the URLs only, so allow users to open
            // the URLs using the secret in a new window, need to put the
            // secret value at the feed level so the template can access it
            // via parent.
            content.feed.secret = this.secret;
        }
        
        return content;
    }
    
});


/**
 * Extends the base content panel to show XML source
 */
VIEW.ContentPanel.XMLSource = Ext.extend(VIEW.ContentPanel, {
    /**
     * The XML should already be loaded, this is just loading it as a string.
     */
    loadContent: function (xmlString) {
        xmlString = xmlString || this.cleanXml;
        if (this.getEl()) {
            this.getEl().update(xmlString);
        } else {
            this.html = xmlString;
        }
        this.contentLoaded = true;
    }
});


/**
 * Extends the base content panel to support attachments
 */
VIEW.ContentPanel.Attachment = Ext.extend(VIEW.ContentPanel, {
    /**
     * Config
     */
    secret: null,
    linkData: null,
    // thumbnail = 64px square, normal = 150px height, full = original size
    previewSize: 'thumbnail',
    previewClass: 'unknown',
    title: 'Attachment',
    // a title needs to be set for the content window tab menu to show
    // each attachment, but this also means that a header is added to the
    // panel.  This should hide the header keeping the title available.
    header: false,
    
    /**
     * Initialize
     */
    initComponent: function () {
        if (!this.template) {
            // default template for Attachments
            this.template = VIEW.Template.Attachment_Display_Template;
        }
        // new title needs to be set for the content window tab menu to show
        // each attachment before the content can be loaded
        if (this.linkData.type) {
            // the - is needed here to help capitalize the type, it won't
            // show up in the window tab
            this.title = OpenLayers.String.camelize('-' + this.linkData.type);
            this.previewClass = this.linkData.type;
        }
        VIEW.ContentPanel.Attachment.superclass.initComponent.call(this);
    },
    
    /**
     * Load the attachment content
     */
    loadContent: function (secret, linkData) {
        secret = secret || this.secret;
        linkData = linkData || this.linkData;
        var templateData = Ext.apply(linkData, {
            sizeType: this.previewSize,
            previewClass: this.previewClass
        });
        //NOTE: attachments are loaded direct, because they are in <img>
        //      or other elements not subject to Same Source restrictions.
        //      Therefore they don't go through the Proxy and won't have
        //      any timeouts in effect either.
        //TODO: should we test if this content is on any hub that we have
        //      configured and/or if its already secret protected?  Is there a
        //      good chance that cross hub content posting / hosting is going
        //      to occur?
        // if a secret was provided add it to the url, otherwise assume that
        // the url already contains a secret
        if (secret) {
            if (linkData.url.search('secret') < 0) {
                templateData.url = Ext.urlAppend(linkData.url, 'secret=' + secret);
            }
        }
        if (this.getEl()) {
            // used to populate the content window tab
            this.getEl().update(this.template.apply(templateData));
        } else {
            // used to populate a revision window
            this.html = this.template.apply(templateData);
        }
        this.contentLoaded = true;
        this.fireEvent('contentloaded', this, this.contentLoaded);
    }
});


/**
 * Factory to create an Atom content panel
 */
VIEW.ContentPanel.Atom = function (options) {
    return new VIEW.ContentPanel.XML(Ext.apply({
            template: VIEW.Template.Entry_Display_Template,
            title: 'Hub Entry',
            contentTransform: 'atom'
        }, options));
};


/**
 * Factory to create a CAP content panel
 */
VIEW.ContentPanel.CAP = function (options) {
    return new VIEW.ContentPanel.XML(Ext.apply({
            template: VIEW.Template.CAP_Display_Template,
            title: 'CAP Message',
            ignoreNamespace: ['xmlns:cap', 'ns0'],
            contentTransform: 'cap'
        }, options));
};


/**
 * Factory to create a revision content panel
 */
VIEW.ContentPanel.Revisions = function (options) {
    return new VIEW.ContentPanel.XML(Ext.apply({
            template: VIEW.Template.Revision_Display_Template,
            title: 'Revisions',
            contentTransform: 'revisions'
        }, options));
};


/**
 * Factory to create an access log content panel
 */
VIEW.ContentPanel.Access = function (options) {
    return new VIEW.ContentPanel.XML(Ext.apply({
            template: VIEW.Template.Access_Log_Template,
            title: 'Access Log',
            contentTransform: 'access'
        }, options));
};


/**
 * Factory to create an XML source panel
 */
VIEW.ContentPanel.Source = function (options) {
    return new VIEW.ContentPanel.XMLSource(Ext.apply({
            title: 'Source'
        }, options));
};


/**
 * Factory to create an image attachment content panel
 * NOTE: not directly referred to as ContentPanel.Image but ContentPanel['Image']
 */
VIEW.ContentPanel.Image = function (options) {
    return new VIEW.ContentPanel.Attachment(Ext.apply({
            previewSize: 'normal'
        }, options));
};


/**
 * Factory to create an attachment content panel for other types
 * NOTE: not directly referred to as ContentPanel.External but ContentPanel['External']
 */
VIEW.ContentPanel.External = function (options) {
    return new VIEW.ContentPanel.Attachment(Ext.apply({
            previewSize: 'thumbnail'
        }, options));
};
