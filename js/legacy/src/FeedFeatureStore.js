/**
MASAS View Tool - FeedFeatureStore
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This feature store maps the generic MASAS feed object to a feature store
through an HTTP connection and a feature filter. This object maps feed
elements to feature attributes.  Methods provided to apply user settings
(time zone offset and feed source).
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * Data Feed Feature Store Type
 */
VIEW.FeedFeatureStore = Ext.extend(GeoExt.data.FeatureStore, {
    /**
     * Config
     */
    defaultLocal: false,
    
    constructor: function (config) {
        /*
         * Get a map reference so that we can use the map's projection for the
         * format parser just using GeoExt's map panel guess method, configure
         * with a map reference if you want something else
         */
        var map = config.map || GeoExt.MapPanel.guess();
        map = map.map || map;
        Ext.applyIf(config || {}, {
            fields: [{
                name: 'id',
                type: 'string'
            }, {
                name: 'icon',
                type: 'string'
            }, {
                name: 'colour',
                type: 'string'
            }, {
                name: 'title',
                type: 'string'
            }, {
                name: 'author_name',
                type: 'string'
            }, {
                name: 'status',
                type: 'string'
            }, {
                name: 'severity',
                type: 'string'
            }, {
                name: 'category',
                type: 'string'
            }, {
                name: 'published',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'updated',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'effective',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'expires',
                type: 'date',
                dateFormat: 'Y-m-d\\TH:i:s\\Z'
            }, {
                name: 'point',
                type: 'string'
            }, {
                name: 'content',
                type: 'string'
            }, 'links', 'linksCombined', {
                name: 'CAP',
                type: 'string'
            }, {
                name: 'clusterId',
                type: 'string'
            }, {
                name: 'type', // type = entry, alert, overlay
                type: 'string'
            }, {
                name: 'modelStoreId',
                type: 'string'
            }, {
                name: 'canEdit',
                type: 'boolean'
            }],
            batch: true,
            // set any MASAS query parameters that will be common for ALL
            // requests and not dynamically change
            baseParams: {},
            // default sort on first load
            sortInfo: {
                field: 'updated',
                direction: "DESC"
            },
            idProperty: 'id',
            // filter that runs for every feature added to the store, ensures
            // there are no values missing "updated"
            featureFilter: new OpenLayers.Filter.Comparison({
                type: '!=',
                property: 'updated',
                value: null
            }),
            autoLoad: false,
            listeners: {
                'datachanged': this.filterFeatures
            }
        });
        VIEW.FeedFeatureStore.superclass.constructor.call(this, config);
        
        // add a cluster sync function if the layer has a cluster strategy
        var clusterStrategy = OpenLayers.Array.filter(this.layer.strategies,
            function (s) {
                return s instanceof OpenLayers.Strategy.Cluster;
            })[0];
        if (this.layer && this.layer.strategies && clusterStrategy) {
//            this.layer.events.register('featuresadded', this, this.updateClusters);
            this.clusterStrategy = clusterStrategy;                    
        }

        // Setup the data sources this feature store will be listening to...
        if( this.showDraftOnly )
        {
            App.dataStoreMgr.masasDraftStore.on( 'clear', this.onDraftModelStoreClear, this );
            App.dataStoreMgr.masasDraftStore.on( 'load', this.onEntryStoreLoad, this );
        }
        else
        {
            App.dataStoreMgr.masasEntryStore.on( 'clear', this.onModelStoreClear, this );
            App.dataStoreMgr.masasEntryStore.on( 'load', this.onEntryStoreLoad, this );

            App.dataStoreMgr.masasOverlayStore.on( 'clear', this.onModelStoreClear, this );
            App.dataStoreMgr.masasOverlayStore.on( 'load', this.onOverlayStoreLoad, this );
        }

    },
    
    
    /**
     * Add a 'clusterId' property to both the store record and the feature so that
     * we can keep track of which cluster the feature/record belongs to
     */
//    updateClusters: function (evt) {
//        var lyr = this.layer;
//        var feats = evt.features;
//        // clustering should be finished now
//        for (var i = 0, len = feats.length; i < len; i++) {
//            var feat = feats[i], clusterId = feat.id;
//            if( feat.cluster )
//            {
//                for (var j = 0, clen = feat.cluster.length; j < clen; j++) {
//                    var cfeat = feat.cluster[j];
//                    var rec = this.getByFeature(cfeat);
//                    if (rec) {
//                        rec.data['clusterId'] = cfeat.attributes['clusterId'] = clusterId;
//                    }
//                }
//            }
//        }
//    },

    /**
     * Filter the map features.  The Entry table has already been filtered
     * and now it needs to sync with the map, triggered by the datachanged
     * event which applies to both add/remove and filter/sort of the store.
     *
     * @param {Object} - Feed Store
     */
    filterFeatures: function (store) {
        /*NOTE: When a Feed load occurs, because refilterOnStoreUpdate is
         *      true for the FilterRow extension, this function will be called
         *      multiple times.  Its required because otherwise when the Feed
         *      is reloaded, any existing filtering/sorting won't be applied
         *      to the new Entries.
         */
        if (store.layer) {
            var lyr = store.layer;
            // when there are features to remove from the layer, the
            // clusterStrategy.cacheFeatures is called accordingly, however
            // that triggers the datachanged event for the store, resulting
            // in an in-direct recursive loop.  Prevent this from occuring.
            if (lyr.filterFeaturesRunning) {
                return;
            }
            lyr.filterFeaturesRunning = true;
            var records = store.data;
            // this filtering will happen several times during a Feed load,
            // including when the Feed has been cleared prior to reloading,
            // and for those cases where there are no records nor features,
            // then we can skip the rest
            if (!records.length && !lyr.features.length) {
                lyr.filterFeaturesRunning = false;
                return;
            }
            var toCluster = [];
            var featureIds = [];
            // build the set of records that passed the filter
            store.each(function (rec) {
                var feat = rec.get('feature');
                featureIds.push(feat.id);
                toCluster.push(feat);
            });
            // now modify the features accordingly
            for (var i = 0, len = lyr.features.length; i < len; i++) {
                var feat = lyr.features[i];
                if (feat.cluster) {
                    for (var j = 0, clen = feat.cluster.length; j < clen; j++) {
                        if (featureIds.indexOf(feat.cluster[j].id) > -1) {
                            feat.cluster[j].renderIntent = 'default';
                        } else {
                            feat.cluster[j].renderIntent = 'delete';
                        }
                    }
                } else {
                    if (featureIds.indexOf(feat.id) > -1) {
                        feat.renderIntent = 'default';
                    } else {
                        feat.renderIntent = 'delete';
                    }
                }
            }
            if (this.clusterStrategy  && !this.clusterStrategy.clustering) {
                this.clusterStrategy.features = [];
                this.clusterStrategy.cacheFeatures({features: toCluster});
            } else {
                lyr.redraw();
            }
            
            lyr.filterFeaturesRunning = false;
        }
    },

    onEntryStoreLoad: function( store, records, options ) {
        var features = [];

        for(var i = 0; i < records.length; i++ ) {
            var feature = this.parseFeature( store, records[i].data.entry )
            features.push( feature );
        }
        this.loadData( features, true );
    },

    onOverlayStoreLoad: function( store, records, options ) {
        var features = [];

        for(var i = 0; i < records.length; i++ ) {
            var feature = this.parseOverlayFeature( store, records[i].data.overlay )
            features.push( feature );
        }
        this.loadData( features, true );
    },

    onDraftModelStoreClear: function( store, records, options ) {
        this.removeAll();
        this.commitChanges();
        return true;
    },

    onModelStoreClear: function( store, records, options ) {
        // Clear the items related to this store...
        var recs = this.queryBy(function (rec) {
            return rec.data.modelStoreId == store.storeId;
        });
        this.remove(recs.items);
        this.commitChanges();
        return true;
    },

    /**
     * Method: parseFeature
     * Parse feature from an Atom entry node..
     *
     * Parameters:
     * node - {DOMElement} An Atom entry or feed node.
     *
     * Returns:
     * An <OpenLayers.Feature.Vector>.
     */
    parseFeature: function( store, entry ) {
        // some defaults
        var featureAttrib = {
            modelStoreId: store.storeId,
            links: '',
            CAP: 'N',
            status: 'Actual',
            type: 'entry',
            icon: App.settings.appSettings.serviceSymbols + '/ems/other/small.png',
            category: null,
            severity: '',
            certainty: '',
            canEdit: false
        };

        featureAttrib.canEdit = App.masasHub.CanPost() && entry.CanEdit( App.masasHub.userData );
        featureAttrib.id = entry.identifier;
        featureAttrib.published = entry.published;
        featureAttrib.updated = entry.updated;
        featureAttrib.expires = entry.expires;
        featureAttrib.effective = entry.effective;
        featureAttrib.author_name = entry.author.name;
        featureAttrib.author_uri = entry.author.uri;

        // Categories...
        if( entry.icon ) {
            featureAttrib.icon = App.settings.appSettings.serviceSymbols + "/" + entry.icon + '/small.png';
        }

        if( entry.status ) {
            featureAttrib.status = entry.status;
        }

        if( entry.severity ) {
            featureAttrib.severity = entry.severity;
        }

        if( entry.certainty ) {
            featureAttrib.certainty = entry.certainty;
        }
        if( entry.categories.length > 0 ) {
            featureAttrib.category = entry.categories.join( ',' );
        }

        if( entry.colour ) {
            featureAttrib.colour = entry.colour;
        }

        // Links...
        featureAttrib.links = {};

        var links = entry.GetLinks();

        for (var i = 0, linksObj = featureAttrib.links; i < links.length; i++)
        {
            try {
                var link = $(links)[i];
                var linkObj = {
                    href: $(link).attr('href'),
                    rel: $(link).attr('rel'),
                    type: $(link).attr('type') || "application/atom+xml",
                    hrefLang: $(link).attr('hreflang'),
                    length: $(link).attr('length')
                };
                switch (linkObj.rel) {
                    case 'edit':
                        linksObj.Atom = linkObj;
                        break;
                    case 'enclosure':
                        // quick short circuit for CAP
                        if (linkObj.type === 'application/common-alerting-protocol+xml') {
                            linksObj.CAP = linkObj;
                            featureAttrib.CAP = 'Y';
                            featureAttrib.type = 'alert';
                            // featureAttrib.enclosures.push({type:'cap',url:linkObj.href,mime:linkObj.type});
                        } else {
                            // look for known enclosure types
                            linksObj.attachments || (linksObj.attachments = []);
                            var known = false;
                            var title = $(link).attr('title') ||
                                OpenLayers.Util.createUrlObject(linkObj.href).pathname.split('/').pop();
                            var obj = {
                                url: linkObj.href,
                                mime: linkObj.type,
                                'title': title,
                                length: linkObj.length,
                                hrefLang: linkObj.hrefLang
                            };

                            var enclosureRegEx = mApp.CommonUtils.getInstance().EnclosureRegEx;
                            for (var type in enclosureRegEx) {
                                if (linkObj.type.search(enclosureRegEx[type]) >= 0) {
                                    known = true;
                                    obj.type = type;
                                    linksObj.attachments.push(obj);
                                    break;
                                }
                            }
                            if (!known) {
                                obj.type = "unknown";
                                linksObj.attachments.push(obj);
                            }
                        }
                        break;
                    case 'related':
                        linksObj.xlink = linkObj;
                        break;
                    case 'history':
                        linksObj.revisions = linkObj;
                        break;
                    // use the rel attribute value as the links object key if
                    // not a well known value & previously handled above
                    default:
                        linksObj[linkObj.rel] = linkObj;
                }
            } catch (e) {}
        }

        // Title, content...
        featureAttrib.title = entry.GetTitle();
        featureAttrib.content = entry.GetContent();

        // Geometry
        var geometry = this.createGeometry( entry.geometry[0] );
        var originalGeom = geometry;

        if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
            var centroid = geometry.getCentroid();
            geometry = centroid;
        }

        // create the OpenLayers Feature for this Atom entry
        var feature = new OpenLayers.Feature.Vector( geometry, featureAttrib );
        feature.fid = featureAttrib.id;
        feature.originalGeom = originalGeom;
        feature.layer = this.layer;
        feature.title = featureAttrib.title;

        return feature;
    },

    parseOverlayFeature: function( store, overlay ) {
        // some defaults
        var featureAttrib = {
            modelStoreId: store.storeId,
            links: '',
            CAP: 'N',
            status: '',
            type: 'overlay',
            icon: App.settings.appSettings.serviceSymbols + '/ems/other/small.png',
            category: null,
            severity: '',
            certainty: '',
            canEdit: false
        };

        featureAttrib.id = overlay.id;
        featureAttrib.published = overlay.createdDTG;
        featureAttrib.updated = overlay.createdDTG;
        featureAttrib.expires = overlay.expiresDTG;
        featureAttrib.author_name = overlay.ownerName;
        featureAttrib.author_uri = overlay.ownerid;

        // Title, content...
        featureAttrib.title = overlay.title;
        featureAttrib.content = overlay.description;

        // Symbol...
        var symbolRecId = App.dataStoreMgr.overlaySymbolStore.findBy( function( record, id ) {
            if( record.data.type === overlay.type && record.data.subType === overlay.subtype ) {
                return true;
            }
            return false;
        } );
        if( symbolRecId >= 0 ) {
            var symbolRec = App.dataStoreMgr.overlaySymbolStore.getAt( symbolRecId );
            featureAttrib.icon =  symbolRec.data.symbolUrl;
        }

        // Links...
        featureAttrib.links = {};
        for( var i = 0; i < overlay.links.length; i++ )
        {
            featureAttrib.links.xlink || ( featureAttrib.links.xlink = [] );
            var curLink = overlay.links[i];
            var linkObj = { href: curLink.url };
            featureAttrib.links.xlink.push( linkObj );
        }

        // Can we post?
        if( App.masasHub.CanPost() )
        {
            // Can this author edit this?
            if( overlay.ownerid == App.masasHub.userData.id ) {
                featureAttrib.canEdit = true;
            }
            else if( overlay.updateAllowed === "org" && overlay.organizationid == App.masasHub.userData.organizationID ) {
                featureAttrib.canEdit = true;
            }
            else if( overlay.updateAllowed === "any" ) {
                featureAttrib.canEdit = true;
            }
        }

        // Geometry
        var geoJsonFormat = new OpenLayers.Format.GeoJSON();
        var geometry = geoJsonFormat.read( overlay.geometry, "Geometry" );
        var originalGeom = geometry;

        // Convert the vertices...
        var vertices = geometry.getVertices();
        for( var i=0; i<vertices.length;i++ ) {
            vertices[i].transform( new OpenLayers.Projection('EPSG:4326'), App.mainView.mapPanel.map.getProjectionObject() )
        }

        if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
            var centroid = geometry.getCentroid();
            geometry = centroid;
        }

        // create the OpenLayers Feature for this Atom entry
        var feature = new OpenLayers.Feature.Vector( geometry, featureAttrib );
        feature.fid = featureAttrib.id;
        feature.originalGeom = originalGeom;
        feature.layer = this.layer;
        feature.title = featureAttrib.title;

        return feature;
    },

    createGeometry: function( entryGeo ) {
        var newGeometry = null;
        var map = App.mainView.mapPanel.map;

        switch( entryGeo.type )
        {
            case 'point':
                var values = entryGeo.data.split( ' ' );

                newGeometry = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject() );
                break;
            case 'line':
                var points = [];
                var values = entryGeo.data.split( ' ' );

                for( var iCtr = 0; iCtr < values.length; iCtr += 2 )
                {
                    var newPt = new OpenLayers.Geometry.Point( values[iCtr+1], values[iCtr] ).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject() );
                    points.push( newPt );
                }
                newGeometry = new OpenLayers.Geometry.LineString( points );
                break;
            case 'polygon':
                var points = [];
                var values = entryGeo.data.split( ' ' );

                for( var iCtr = 0; iCtr < values.length; iCtr += 2 )
                {
                    var newPt = new OpenLayers.Geometry.Point( values[iCtr+1], values[iCtr] ).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject() );
                    points.push( newPt );
                }
                var linRing = new OpenLayers.Geometry.LinearRing( points );
                newGeometry = new OpenLayers.Geometry.Polygon( [linRing] );
                break;
            case 'box':
                var values = entryGeo.data.split( ' ' );

                var geom = new OpenLayers.Bounds( values[1], values[0], values[3], values[2]).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject() );
                newGeometry = geom.toGeometry();
                break;
            case 'circle':
                var values = entryGeo.data.split( ' ' );
                var centerPt = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject() );

                newGeometry = new OpenLayers.Geometry.Polygon.createRegularPolygon( centerPt, values[2], 64 );
                break;
        }

        return newGeometry;
    }

});

Ext.reg('mv_feedfeaturestore', VIEW.FeedFeatureStore);
