/**
MASAS View Tool - FeedSettingsPanel
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines the panel, store, and form for specifying all the details about
a feed, including url, secret and name.  Each feed is available as a tab in 
the panel along with an 'add feed' tab.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW.Panel');

/**
 * A custom store for the feed sources, primarily accessed after being
 * established via the constructor through Ext.StoreMgr(storeId)
 */
VIEW.FeedSourceStore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (config) {
        config = config || {};
        var fields = [
        // required settings in VIEW.FEED_SETTINGS
        'title', 'url', 'secret', {
            // optional setting to render this feed disabled at load
            name: 'disabled',
            type: 'boolean',
            defaultValue: false
        }, {
            // optional setting to hide the user's secret value at load
            name: 'hideSecret',
            type: 'boolean',
            defaultValue: true
        }, {
            // optional setting to prevent the user from changing this feed
            name: 'locked',
            type: 'boolean',
            defaultValue: false
        }];
        config.fields = (config.fields instanceof Array) ? config.fields.concat(fields) : fields;
        Ext.apply(config, {
            storeId: 'FeedSources',
            idProperty: 'url'
        });
        VIEW.FeedSourceStore.superclass.constructor.call(this, config);
    }
});
