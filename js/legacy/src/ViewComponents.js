/**
MASAS View Tool - ViewComponents
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file defines a grab-bag of application handlers in the global 'VIEW'
namespace.  This includes feature select and hover handlers, which 
delegate to cluster or individual handlers depending on if the features
are in a cluster or not.  Some methods are defined for finding features and
removing features from a feed and cleaning up a map.  Methods are provided
to zooming the map to highlight features or clusters or saved views
controller methods defined to control loading of all or single feed.
Methods defiend to render feed content to popup windows.
*/

/*global VIEW,Ext,OpenLayers,GeoExt */

Ext.ns('VIEW');

/**
 * App variable initial config.  ViewComponents.js is loaded first and so sets
 * up some variables needed by other modules.
 */
Ext.apply(VIEW, {
    map: null,
    gridPanel: null,
    contentWindow: null,
    // to prevent zooming in too far to a single point, offset the zoom back
    // out this amount
    pointZoomOffset: 6,
    // max height for popups, will add scrollbars if more info
    popupMaxHeight: 200
});


/**
 * This is the main select feature handler, delegates to the appropriate handler
 */
VIEW.feature_selected = function (evt) {
    // normal screen select feature event
    if (!evt.targetFeature) {
        if (evt.feature.cluster) {
            if (evt.feature.cluster.length > 1) {
                VIEW.show_cluster_popup(evt.feature);
            } else {
                VIEW.show_feature_popup(evt.feature.cluster[0]);
            }
        } else {
            VIEW.show_feature_popup(evt.feature);
        }
    }
    // selected from the grid
    else {
        var feature = null;
        if (evt.targetFeature.cluster) {
            feature = evt.feature;
            VIEW.show_feature_popup(feature);
        } else {
            feature = evt.targetFeature;
            // selecting a single feature from inside a cluster
            VIEW.show_feature_popup(feature, evt.feature.layer, evt.feature);
        }
    }
};


/**
 * Feature unselect handler
 */
VIEW.feature_unselected = function (evt) {
    if (evt.targetFeature) {
        // needed for when switching between entries using the grid
        // to show popups
        //TODO: why is the generic cleanup method needed and the
        //      normal close_feature_popup not working?
        VIEW.clean_up_map();
    } else {
        // used with the popup hover in/out and close
        VIEW.close_feature_popup(evt);
    }
};


/**
 * Opens a popup for clustered features
 */
VIEW.show_cluster_popup = function (feature) {
    var map = feature.layer.map || VIEW.map;
    var popup = new GeoExt.Popup({
        html: VIEW.Template.Cluster_Tip_Template.apply(feature.cluster),
        title: VIEW.Template.Cluster_Title_Template.apply(feature),
        unpinnable: false,
        location: feature,
        closable: true,
        anchored: true,
        map: map,
        ref: 'popup',
        id: 'popup_' + feature.id,
        width: 350,
        minHeight: 100,
        autoScroll: true,
        panIn: false,
        listeners: {
            'close': function (cmp) {
                VIEW.close_feature_popup({'feature': feature});
            }
        }
    });
    App.mainView.mapPanel.add(popup);
    App.mainView.mapPanel.popup = popup;
    popup.show();
    if (popup.getHeight() > VIEW.popupMaxHeight) {
        popup.setHeight(VIEW.popupMaxHeight);
        popup.panIntoView();
    }
};


/**
 * Opens a popup to show feature summary
 */
VIEW.show_feature_popup = function (feature, layer, cluster) {
    var map = (layer && layer.map) || feature.layer.map || VIEW.map;
    var mapExtent = map.getExtent();
    var popup = null;
    var focusFeat = null;
    var detailed_popup = App.settings.userSettings.showDetailedPopup;
    // skipping popup for entry with already open content window
    if (VIEW.contentWindow) {
        if (feature.cluster) {
            // assuming single feature clusters are the norm for standalone
            // icons on the map, otherwise is an actual cluster
            if (feature.cluster.length === 1) {
                if (VIEW.contentWindow.panels.Atom.entryID === feature.cluster[0].data.id) {
                    return;
                }
            }
        } else {
            if (VIEW.contentWindow.panels.Atom.entryID === feature.data.id) {
                return;
            }
        }
    }
    if (feature && cluster) {
        // I think this is creating a new feature/icon that will appear
        // on top of the cluster to distinguish it and using that
        // new feature as the target for the popup
        var cloned = feature.clone();
        cloned.fid = feature.fid;
        cloned.cloned = true;
        cluster.layer.addFeatures([cloned], {silent: true});
        focusFeat = cloned;
    } else {
        focusFeat = feature.clone();
    }
    // attributes to use for generating the content templates
    var attrs = feature.attributes;
    // adding the original updated datetime in UTC as the normal updated value
    // has been converted to local
    attrs['original_updated'] = feature.data.updated;

    // apply popup content templates
    var content = null;

    if( feature.data.type == "overlay" ) {
        content = VIEW.Template.Overlay_Detailed_Popup_Template.apply( attrs );
    }
    else {
        content = (detailed_popup) ? VIEW.Template.Detailed_Popup_Template.apply( attrs ) : VIEW.Template.Simple_Popup_Template.apply( attrs );
    }

    var title = VIEW.Template.Popup_Title_Template.apply(attrs);
    if (feature.originalGeom) {
        var geom = feature.originalGeom.clone();
        var tempFeature = new OpenLayers.Feature.Vector(geom, Ext.apply({}, attrs));
        VIEW.mapLayers.MASASLayerHighlight.addFeatures([tempFeature]);
        /* NOTE: in SVN Revision 184:185 madir committed a change to stop the
        check below.  His comment was "re #3388: don't re-assign the feature
        location for the popup, features are already in adjusted locations."
        
        JW modified this check to move the icon, to which the popup will be
        attached, only when adjustIcons is off, accomodating large geoms
        where a part may be in view and able to be shown.
        */
        if( !App.settings.userSettings.autoCenterPolygonMarkup ) {
            // test if original geom is contained in the viewport
            var geomBounds = geom.getBounds();
            if (!mapExtent.containsBounds(geomBounds) &&
            !geomBounds.containsBounds(mapExtent)) {
                var nearest = VIEW.get_nearest_vertex(mapExtent.toGeometry().getCentroid(),
                    geom).clone();
                if (mapExtent.containsLonLat(new OpenLayers.LonLat(nearest.x,
                nearest.y))) {
                    focusFeat.geometry = nearest;
                }
                if (focusFeat.layer) {
                    focusFeat.layer.drawFeature(focusFeat);
                }
            }
        }
    }
    // if the intended popup location isn't able to be viewed, don't
    // show it all.  This resolves problems where the map would try
    // a significant pan to the feature location and the user would end up
    // with a blank screen.  Assumes .geometry is always a point.
    //NOTE: if this kind of check is needed after generating a GeoExt popup
    //      the insideViewport attribute can be used.
    if (!mapExtent.contains(focusFeat.geometry.x, focusFeat.geometry.y)) {
        //TODO: no cleanup right now of any features added above as they
        //      should be cleaned up with next popup load.  Make sure
        //      this is okay with further testing.
        return;
    }
    
    popup = new GeoExt.Popup({
        html: content,
        title: title,
        unpinnable: false,
        location: focusFeat,
        closable: true,
        anchored: true,
        map: map,
        ref: 'popup',
        id: 'popup_' + feature.id,
        width: (detailed_popup) ? 500 : 350,
        minHeight: 100,
        autoScroll: true,
        panIn: false,
        buttons: [
            {
                text: 'Edit...',
                ref: '../btnEdit',
                iconCls: 'btnEditItemIcon',
                hidden: !attrs.canEdit,
                scope: this,
                handler: function() { App.mainView.editMarkup( attrs.type, attrs.id ) }
            }
        ],
        listeners: {
            'close': function (cmp) {
                VIEW.close_feature_popup({'feature': focusFeat});
            }
        }
    });

    App.mainView.mapPanel.add(popup);
    App.mainView.mapPanel.popup = popup;
    popup.show();
    if (popup.getHeight() > VIEW.popupMaxHeight) {
        popup.setHeight(VIEW.popupMaxHeight);
        popup.panIntoView();
    }
    
    // highlight the grid record
    var gridPanel = null;

    if( feature.layer.name === "MASAS Drafts" ) {
        gridPanel = App.mainView.draftPanel;
    }
    else if ( feature.layer.name === "MASAS Feed" ) {
        gridPanel = App.mainView.entryPanel;
    }

    var rowId = gridPanel.getStore().findExact('feature', feature);
    var view = gridPanel.getView();

    if( rowId > -1 && ( view.mainBody !== undefined ) ) {
        view.focusRow(rowId);
        Ext.fly( view.getRow(rowId)).addClass( 'x-grid3-row-selected' );
    }

};


/**
 * Close a popup
 */
VIEW.close_feature_popup = function (evt) {
    var feature, popup, featureId;
    feature = evt.feature;
    // skipping popup for entry with already open content window
    if (VIEW.contentWindow) {
        if (feature.cluster) {
            // assuming single feature clusters are the norm for standalone
            // icons on the map, otherwise is an actual cluster
            if (feature.cluster.length === 1) {
                if (VIEW.contentWindow.panels.Atom.entryID === feature.cluster[0].data.id) {
                    return;
                }
            }
        } else {
            if (VIEW.contentWindow.panels.Atom.entryID === feature.data.id) {
                return;
            }
        }
    }
    popup = App.mainView.mapPanel.popup || Ext.getCmp('popup_' + feature.id);
    if (popup) {
        App.mainView.mapPanel.popup = null;
        popup.destroy();
        if (feature) {
            feature.popup = null;
            //TODO: not sure what feature.highlight does?
            feature.highlight = false;
            if (!feature.cluster && feature.cloned && feature.layer) {
                feature.layer.removeFeatures([feature]);
            }
        }
    }
    // ensure grid is unfocused
    /* NOTE: in SVN Revision 186:187 madair commited a change to prevent error
    messages about feature.cluster not existing when using the close box on
    a popup window.  His comment was "closes #409: add check for
    feature.cluster so the cose popup doesn't fail".
    
    JW added additional brackets around this cluster check as that was also
    failing.
    */
    featureId = feature.fid || ((feature.cluster) ? feature.cluster[0].fid : null);
    if( featureId && evt.object )
    {
        var gridPanel = null;

        if( evt.object.name === "MASAS Drafts" ) {
            gridPanel = App.mainView.draftPanel;
        }
        else if ( evt.object.name === "MASAS Feed" ) {
            gridPanel = App.mainView.entryPanel;
        }

        var row = gridPanel.getStore().find('fid', featureId);
        var view = gridPanel.getView()
        if( row > -1 && ( view.mainBody !== undefined ) ) {
            Ext.fly( gridPanel.getView().getRow(row)).removeClass( 'x-grid3-row-selected' );
        }
    }
    VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
};

/**
 * Locates the vertex closest to the reference point (both are geometries)
 */
VIEW.get_nearest_vertex = function (refPt, geometry) {
    var vertices = geometry.getVertices();
    var dist = Number.MAX_VALUE;
    var closest = null;
    var center = refPt.getCentroid();
    for (var v = 0; v < vertices.length; ++v) {
        var testDist = vertices[v].distanceTo(center);
        if (testDist < dist) {
            dist = testDist;
            closest = v;
        }
    }
    return vertices[closest];
};


/**
 * Cleanup the map.  Attached to the map's zoomend listener.
 */
VIEW.clean_up_map = function (evt) {
    // remove popups
    if (App.mainView && App.mainView.mapPanel.popup) {
        App.mainView.mapPanel.popup.destroy();
    }

    // deselect features as needed
    VIEW.mapControls.highlightControl.unselectAll();

    if (VIEW.mapLayers.MASASLayerHighlight.features.length) {
        VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
    }

    /** NOTE: I HAVEN'T SEEN THIS CODE RUN YET - MORE INVESTIGATION NEEDED **/

    // remove features not processed through cluster strategy
    // i.e. features we add progamatically from grid selection event
    var unclustered = VIEW.mapLayers.MASASLayer.getFeaturesByAttribute( 'count', undefined );
    if (unclustered.length) {
        // OK to use destroy here because we used cloned feature(s) and
        // didn't add it to the grid
        VIEW.mapLayers.MASASLayer.destroyFeatures(unclustered);
        // clustering can stop working properly after this destroy, so this
        // refilter should correct any clusters that aren't working
        App.mainView.entryPanel.featureStore.filterFeatures( App.mainView.entryPanel.featureStore );
    }

    var unclustered = VIEW.mapLayers.MASASLayerDraft.getFeaturesByAttribute( 'count', undefined );
    if (unclustered.length) {
        // OK to use destroy here because we used cloned feature(s) and
        // didn't add it to the grid
        VIEW.mapLayers.MASASLayerDraft.destroyFeatures(unclustered);
        // clustering can stop working properly after this destroy, so this
        // refilter should correct any clusters that aren't working
        App.mainView.draftPanel.featureStore.filterFeatures( App.mainView.draftPanel.featureStore );
    }

};

/**
 * Zoom to a feature
 */
VIEW.zoom_to_feature = function( feature ) {
    if( feature )
    {
        var geometry = feature.originalGeom;

        if( geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point' )
        {
            VIEW.map.zoomToExtent( geometry.getBounds() );
        }
        else
        {
            var zoom = VIEW.map.baseLayer.maxZoomLevel - this.pointZoomOffset;
            VIEW.map.setCenter( new OpenLayers.LonLat( geometry.x, geometry.y ), zoom );
        }
    }
    // the popup hasn't been created yet after this zoom so this doesn't
    // close the expected new popup, but does close any older ones.  So
    // that may be why its being called?  Unknown if actually needed.
    VIEW.close_feature_popup( { feature: feature } );
};


/**
 * Zoom to a cluster
 */
VIEW.zoom_to_cluster = function (id, layerId) {
    var map = App.mainView.mapPanel.map;
    var layer = map.getLayer( layerId );
    var mainFeature = layer.getFeatureById(id);
    var geometry = new OpenLayers.Geometry.Collection();
    for (var j = 0; j < mainFeature.cluster.length; ++j) {
        var feature = mainFeature.cluster[j];
        geometry.addComponents(feature.originalGeom.clone());
    }
    geometry.calculateBounds();
    map.zoomToExtent(geometry.getBounds());
    VIEW.close_feature_popup({feature: mainFeature});
};


/**
 * Select and show an individual feature out of a cluster listing
 */
VIEW.highlight_cluster_feature = function (id) {
    // Search the EntryViewStore...
    var store = Ext.StoreMgr.get('EntryViewStore');
    var entryNdx = store.find('id', id);

    if( entryNdx == -1 ) {
        // Search the DraftViewStore...
        store = Ext.StoreMgr.get('DraftViewStore');
        entryNdx = store.find('id', id);
    }

    if (entryNdx > -1) {
        var rec = store.getAt(entryNdx);
        if (rec) {
            var feat = rec.getFeature();
            feat.renderIntent = 'select';
            var cloned = feat.clone();
            cloned.cloned = true;
            feat.layer.addFeatures([cloned], {silent: true});
            VIEW.mapLayers.MASASLayerHighlight.addFeatures(
                new OpenLayers.Feature.Vector(feat.originalGeom.clone()));
            App.mainView.mapPanel.popup.destroy();
            App.mainView.mapPanel.popup = null;
            VIEW.show_content_window( rec.get('canEdit'), id, rec.get('modelStoreId') );
        }
    }
};

/**
 * Open a content window
 * 
 * @param {String} - the Atom <id> for an Entry
 * @param {String} - the ID for a source feed
 * @param {String} - the tab to open upon creation, ie Atom, CAP, Attachments
 */
VIEW.show_content_window = function( canEdit, id, modelStoreId, tab) {
    if( modelStoreId === "masasOverlays" )
    {
        App.mainView.showOverlayDetails( id, canEdit );
    }
    else
    {
        var store = null;
        if( modelStoreId === "masasDrafts" ) {
            store = Ext.StoreMgr.get( 'DraftViewStore' );
        }
        else if( modelStoreId === "masasEntries" ) {
            store = Ext.StoreMgr.get( 'EntryViewStore' );
        }

        if( store != null )
        {
            var entryNdx = store.find('id', id);
            if (entryNdx > -1) {
                VIEW.show_entry_content(store.getAt(entryNdx).get('links'), modelStoreId, tab, canEdit);
            }
        }
    }
};


/**
 * Populate a content window
 *
 * @param {String} - the links object for an Entry, used to load the data for
 *                   populating the content tabs
 * @param {String} - the ID for data store
 * @param {String} - the tab to open upon creation, ie Atom, CAP, Attachments
 */
VIEW.show_entry_content = function (links, modelStoreId, tab, canEdit) {
//    var feedRec = Ext.StoreMgr.get('FeedSources').getById(modelStoreId);
    if (VIEW.contentWindow && VIEW.contentWindow.links.Atom.href !== links.Atom.href) {
        // existing content window will be replaced with new info, clean up first
        VIEW.contentWindow.close();
    }
    if (!VIEW.contentWindow) {
        VIEW.contentWindow = new VIEW.ContentWindow({
            updateAllowed: canEdit,
            closeAction: 'close',
            'links': links,
//            feedInfo: feedRec,
            modelStoreId: modelStoreId,
            listeners: {
                destroy: function () {
                    // a handler attached to the close listener normally deals
                    // with cleanup for these windows, I think this was added
                    // for cases where close doesn't complete properly?
                    VIEW.contentWindow = null;
                }
            }
        });
    } else {
        // default back to Atom message if we already have an open window
        // and no special tab indicated
        tab = tab || 'messages|Atom';
    }
    // show window
    VIEW.contentWindow.show();
    // open to a specified tab and any tab specific instructions
    if (tab) {
        var parts = tab.split('|');
        var group = parts[0]; 
        tab = parts[1];
        var panel = VIEW.contentWindow.panels[tab] || VIEW.contentWindow.panels['Image'] ||
            VIEW.contentWindow.panels['External'];
        // handle dot delimited tab panels (Source.Atom,Image.1,External.0,etc...)
        if (tab && tab.indexOf('.') > -1) {
            parts = tab.split('.');
            panel = VIEW.contentWindow.panels[parts[0]];
            for (var i = 0; i < parts.length; i++) {
                panel = panel[parts[i]];
            }
        } else if (Ext.isArray(panel)) {
            panel = panel[0];
        }
        VIEW.contentWindow.tabs.setActiveGroup(VIEW.contentWindow.tabs[group]);
        VIEW.contentWindow.tabs.activeGroup.setActiveTab(panel);
    }
};


/**
 * After the content window is created and the Atom content is loaded, show the
 * Entry's Atom geometry on the map.
 *
 * NOTE: if the content fails to load properly, bad download/template then
 * this method won't be run and the popup will remain in place to show what
 * was attempted to be loaded
 *
 * @param {String} - the Atom <id> for an Entry
 */
VIEW.show_entry_geometry = function (entryID) {
    VIEW.mapLayers.MASASLayerEntry.destroyFeatures();

    // Search the EntryViewStore...
    var store = Ext.StoreMgr.get('EntryViewStore');
    var rec_idx = store.find('id', entryID);

    if( rec_idx == -1 ) {
        // Search the DraftViewStore...
        store = Ext.StoreMgr.get('DraftViewStore');
        rec_idx = store.find('id', entryID);
    }

    if (rec_idx > -1) {
        var entry_rec = store.getAt(rec_idx);
        // add the entry's geometry to the map for display
        if (entry_rec.data.feature.originalGeom) {
            var orig_geom = entry_rec.data.feature.originalGeom.clone();
            var geom_feature = new OpenLayers.Feature.Vector(orig_geom);
            // normally the style for geometry excludes points
            // because of hover/popup not wanting to see them, but
            // in this case we do
            if (orig_geom.CLASS_NAME === 'OpenLayers.Geometry.Point') {
                geom_feature.style = OpenLayers.Util.applyDefaults(
                    {pointRadius: 6},
                    VIEW.layerStyles.geometry_style.defaultStyle);
            }
            VIEW.mapLayers.MASASLayerEntry.addFeatures([geom_feature]);
        }

        // using a little delay before closing the popup to make it clearer
        // what feature this content window applies too
        setTimeout(function () {
            if (App.mainView.mapPanel.popup) {
                App.mainView.mapPanel.popup.destroy();
            }
            if (VIEW.mapLayers.MASASLayerHighlight.features.length) {
                VIEW.mapLayers.MASASLayerHighlight.destroyFeatures();
            }
        }, 1000);
    }
};


/**
 * Show or Hide any polygon(s) or circle(s) associated with a CAP area block
 * 
 * @param {String} - the areaid for a particular area block
 */
VIEW.show_CAP_area = function (areaid) {
    var elem = Ext.get(areaid);
    var elem_txt = elem.dom.innerHTML;
    var features = VIEW.mapLayers.MASASLayerEntry.getFeaturesByAttribute('areaid',
        areaid);
    if (features && features.length > 0) {
        if (elem_txt === '[Show]') {
            // there may be multiple polygon(s) and/or circle(s)
            for (var i = 0; i < features.length; i++) {
                features[i].renderIntent = 'revision';
            }
            elem.update('[Hide]');
        } else {
            for (var i = 0; i < features.length; i++) {
                features[i].renderIntent = 'delete';
            }
            elem.update('[Show]');
        }
        VIEW.mapLayers.MASASLayerEntry.redraw();
    }
};


/**
 * Clean up when closing a content window.
 */
VIEW.close_content_window = function () {
    VIEW.mapLayers.MASASLayerEntry.destroyFeatures();

    // remove the row highlighting from the grid
    // Search the EntryViewStore...
    var store = Ext.StoreMgr.get('EntryViewStore');
    var grid = App.mainView.entryPanel;
    var row = store.find('id', VIEW.contentWindow.panels.Atom.entryID);

    if( row == -1 ) {
        // Search the DraftViewStore...
        store = Ext.StoreMgr.get('DraftViewStore');
        grid = App.mainView.draftPanel;
        row = store.find('id', VIEW.contentWindow.panels.Atom.entryID);
    }

    var view = grid.getView();
    if( row > -1 && ( view.mainBody !== undefined ) ) {
        Ext.fly( view.getRow(row)).removeClass( 'x-grid3-row-selected' );
    }
    VIEW.contentWindow = null;
};


/**
 * Used for viewing a revision for an Entry, or opening an Entry in its own
 * window.
 */
VIEW.show_entry_revision = function (url, secret) {
    if (!url || !secret) {
        alert('Unable to load Entry');
        return;
    }
    var win_title = 'Entry';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 400,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel.Atom({
            template: VIEW.Template.Entry_Combined_Template,
            'secret': secret,
            'url': url,
            listeners: {
                'contentloaded': function (cmp) {
                    // add handlers to show popup windows with content
                    Ext.select('.CAPRevisionLink').on({
                        'click': VIEW.show_CAP_revision,
                        scope: cmp
                    });
                    Ext.select('.AttachmentRevisionLink').on({
                        'click': VIEW.show_attachment_revision,
                        scope: cmp
                    });
                }
            }
        })
    });
    win.items.itemAt(0).loadContent();
    // for mobile clients the normal close button is very small
    // and hard to use.  To modify it to make it bigger takes
    // a lot of overriding, so this hackish method provides
    // an easier way for touch based clients to close windows
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * When viewing the Revision log, opens a new window to display an associated
 * CAP message for that revision.
 */
VIEW.show_CAP_revision = function (evt) {
    var el = evt.target;
    var capUrl;
    // stop the browser from following the provided link in href
    evt.stopEvent();
    // because of href previously viewed content will appear as already
    // having been viewed based on browser history.  The actual URL is
    // stored between the <a></a> tags
    if (el.text) {
        capUrl = el.text;
    } else if (el.innerText) {
        // IE 8 support
        capUrl = el.innerText;
    } else {
        alert('Unable to load CAP message');
        return;
    }
    var win_title = 'CAP Message';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 325,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel.CAP({
            // secret is available because the scope was set to the
            // Revisions panel
            'secret': this.secret,
            'url': capUrl
        })
    });
    win.items.itemAt(0).loadContent();
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * When viewing the Revision log, opens a new window to display an associated
 * Attachment for that revision.
 */
VIEW.show_attachment_revision = function (evt) {
    var el = evt.target;
    var attachUrl;
    // stop the browser from following the provided link in href
    evt.stopEvent();
    // because of href previously viewed content will appear as already
    // having been viewed based on browser history.  The actual URL is
    // stored between the <a></a> tags
    if (el.text) {
        attachUrl = el.text;
    } else if (el.innerText) {
        // IE 8 support
        attachUrl = el.innerText;
    } else {
        alert('Unable to load Attachment');
        return;
    }
    var attachData = {
        'url': attachUrl,
        //TODO: language placeholder for now
        'hrefLang': null,
        'title': el.getAttribute('data-title'),
        'mime': el.getAttribute('data-mime'),
        'length': ((el.getAttribute('data-length')) ? el.getAttribute('data-length') : 'N/A')
    };
    // use the OpenLayers MASAS module's regular expressions to decode the
    // enclosure mime-type into a simplified one for attachment presentation
    var enclosureRegEx = mApp.CommonUtils.getInstance().EnclosureRegEx;
    for (var type in enclosureRegEx) {
        if (attachData.mime.search(enclosureRegEx[type]) >= 0) {
            attachData['type'] = type;
            break;
        }
    }
    if (!attachData['type']) {
        attachData['type'] = 'unknown';
    }
    // image shows a thumbnail preview while external only shows an icon
    var panel_type = (attachData['type'] === 'image') ? 'Image' : 'External';
    var win_title = 'Attachment';
    var win = new Ext.Window({
        title: win_title,
        layout: 'fit',
        width: 500,
        height: 325,
        autoScroll: true,
        collapsible: true,
        bodyStyle: 'background-color: white;',
        items: VIEW.ContentPanel[panel_type]({
            // secret is available because the scope was set to the
            // Revisions panel
            'secret': this.secret,
            'linkData': attachData
        })
    });
    win.items.itemAt(0).loadContent();
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Open a new window to show the Access Control details web page for an
 * author URI.
 */
VIEW.show_author = function (uri, name) {
    var secret = App.settings.currentHub.accessCode;
    var authUrl = Ext.urlAppend(uri, 'secret=' + secret);
    var win_title = 'Author Information';

    var win = new Ext.Window({
        title: win_title,
        'float': true,
        height: 500,
        width: 650,
        cls: 'AuthorInfo',
        renderTo: Ext.getBody(),
        layout: 'fit',
        html: '<iframe width="100%" height="100%" src="' + authUrl + '">'
    });
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};


/**
 * Used by AdvancedSettingsPanel.js  Allows the user to see all of the original geometry
 * for Entries.  For example, to show all road segments that may be closed.  
 */
VIEW.show_all_geometries = function () {
    VIEW.mapLayers.MASASLayerGeometries.destroyFeatures();

    // Get the features...
    var orig_features = VIEW.mapLayers.MASASLayer.features;
    orig_features = orig_features.concat( VIEW.mapLayers.MASASLayerDraft.features )

    var found_features = [];
    var orig_geom = null;
    var new_features = [];
    for (var i = 0; i < orig_features.length; i++) {
        // when clustering is turned on, most features end up as part
        // of a cluster, so look here first
        if (orig_features[i].cluster) {
            for (var j = 0; j < orig_features[i].cluster.length; j++) {
                found_features.push(orig_features[i].cluster[j]);
            }
        } else {
            found_features.push(orig_features[i]);
        }
    }
    for (var k = 0; k < found_features.length; k++) {
        if (found_features[k].originalGeom) {
            orig_geom = found_features[k].originalGeom.clone();
        } else {
            orig_geom = found_features[k].geometry.clone();
        }
        new_features.push(new OpenLayers.Feature.Vector(orig_geom));
    }
    VIEW.mapLayers.MASASLayerGeometries.addFeatures(new_features);
};


/**
 * Change the datetime values to account for timezones for display
 *
 * @param {DateTime} - a DateTime object to adjust
 * @param {Integer} - number of hours to adjust by, can accept minute
 *                    values such as 1.5  for 1 1/2 hours
 */
VIEW.adjust_time = function (date, offset) {
    var dt = date.clone();
    if (offset !== 0) {
        var hrOff = parseInt(offset, 10);
        var minOff = offset % hrOff * 60;
        //test for non-integer offsets
        if (minOff) {
            var min = dt.getMinutes() + minOff;
            dt.setMinutes(min);
        }
        var hr = dt.getHours() + hrOff;
        dt.setHours(hr);
    }
    return dt;
};


/**
 * Show Entry options window
 * 
 * @param {String} - URL for this Entry (required)
 * @param {String} - secret used to load this URL (required)
 */
VIEW.show_entry_options = function (url, secret, geom_type, geom_val) {
    if (!url || !secret) {
        return;
    }
    var option_buttons = [{
        xtype: 'button',
        text: 'Show in New Window',
        // width, height
        anchor: '100%, 20%',
        handler: function (btn) {
            // closing the options window first
            btn.ownerCt.close();
            VIEW.show_entry_revision(url, secret);
        }
    }, {
        xtype: 'button',
        text: 'Get Entry URL',
        anchor: '100%, 20%',
        handler: function (btn) {
            btn.ownerCt.close();
            var link_win_title = 'Entry URL';
            var link_win = new Ext.Window({
                title: link_win_title,
                layout: 'anchor',
                width: 500,
                height: 75,
                plain: true,
                items: [{
                    xtype: 'textfield',
                    height: 40,
                    anchor: '100%',
                    value: url,
                    listeners: {
                        afterrender: function () {
                            // select the text in this field after
                            // a 1/2 sec delay to allow it to display
                            this.focus(true, 500);
                        }
                    }
                }]
            });
            link_win_title += ' <a class="titleClose" href="#" ' +
                'onClick="Ext.getCmp(\'' + link_win.getId() +
                '\').close(); return false;">Close</a>';
            link_win.setTitle(link_win_title);
            link_win.show();
        }
    }];
    
    if (App.settings.appSettings.serviceTool_exportEntryUrl) {
        option_buttons.push({
            xtype: 'button',
            text: 'Save Entry',
            anchor: '100%, 20%',
            handler: function (btn) {
                btn.ownerCt.close();
                var save_win_title = 'Saving Entry...';
                var download_url = App.settings.appSettings.serviceTool_exportEntryUrl + '?url=' + url +
                    '&secret=' + secret;
                var save_win = new Ext.Window({
                    title: save_win_title,
                    layout: 'fit',
                    height: 250,
                    width: 350,
                    draggable: false,
                    resizable: false,
                    modal: true,
                    cls: 'EntrySaveWindow',
                    html: '<div class="EntrySaveDiv">The Entry is being' +
                        ' downloaded, archived into a ZIP file, and you will' +
                        ' be prompted when its ready to be saved. Attachments' +
                        ' will increase the download time.</div>' +
                        //NOTE: this iframe doesn't have an ExtJS timeout, it
                        //      will time out when the server times out
                        ' <iframe width="325" height="125" frameBorder="0"' +
                        ' src="' + download_url + '">'
                });
                save_win_title += ' <a class="titleClose" href="#" ' +
                    'onClick="Ext.getCmp(\'' + save_win.getId() +
                    '\').close(); return false;">Close</a>';
                save_win.setTitle(save_win_title);
                save_win.show();
            }
        });
    }
    
    if (geom_type && geom_val) {
        option_buttons.push({
            xtype: 'button',
            text: 'Add Geometry as Layer',
            anchor: '100%, 20%',
            handler: function (btn) {
                btn.ownerCt.close();
                Ext.Msg.prompt('Add Map Layer', 'Enter a name for new layer:',
                    function (btn, text) {
                        if (btn === 'ok') {
                            VIEW.Layers.add_entry_layer(text, 'entry', url,
                                geom_type, geom_val);
                        }
                    }, null, false, 'Entry');
            }
        });
    }
    
    if (App.settings.appSettings.serviceTool_favoriteEntryUrl) {
        option_buttons.push({
            xtype: 'button',
            text: 'Add to Favorites',
            anchor: '100%, 20%',
            handler: function (btn) {
                btn.ownerCt.close();
                Ext.Ajax.request({
                    url: App.settings.appSettings.serviceTool_favoriteEntryUrl,
                    params: { 'url': url, 'secret': secret },
                    success: function () {
                        Ext.Msg.alert('Success', 'Added to Favorites');
                    },
                    failure: function (response) {
                        Ext.Msg.alert('Error', 'Unable to add to Favorites');
                    }
                });
            }
        });
    }
    
    var win_title = 'Entry Options';
    var win = new Ext.Window({
        title: win_title,
        layout: 'anchor',
        width: 250,
        height: 250,
        plain: true,
        draggable: false,
        resizable: false,
        modal: true,
        items: option_buttons
    });
    win_title += ' <a class="titleClose" href="#" onclick="Ext.getCmp(\'' +
        win.getId() + '\').close(); return false;">Close</a>';
    win.setTitle(win_title);
    win.show();
};
