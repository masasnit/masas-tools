/**
MASAS View Tool - Map Setup
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

This file contains all of the configuration settings for the map.
*/

/*global VIEW,Ext,OpenLayers,GeoExt,google */

Ext.ns('VIEW');

/**
 * Default Map Options
 */
VIEW.mapOptions = {
    projection: "EPSG:3857",
    eventListeners: {
        'zoomend': VIEW.clean_up_map
    }
};

VIEW.map = new OpenLayers.Map(VIEW.mapOptions);

/**
 * Setup of the Layer Styles
 */
(function() {
    
    // same values as EntryBackground in CSS
    VIEW.layerColours = {
        'red': {'background': 'bg-red.png', 'colour': '#FF0000'},
        'yellow': {'background': 'bg-yellow.png', 'colour': '#FFFF00'},
        'green': {'background': 'bg-green.png', 'colour': '#008000'},
        'gray': {'background': 'bg-gray.png', 'colour': '#696969'},
        'black': {'background': 'bg-black.png', 'colour': '#000000'},
        'blue': {'background': 'bg-blue.png', 'colour': '#0000FF'},
        'purple': {'background': 'bg-purple.png', 'colour': '#800080'}
    };
    
    var ClusterOptions = {
        context: {
            mySymbol: function (feature) {
                if (feature.attributes.count > 1) {
                    // a clustered point
                    //var href = OpenLayers.Util.removeTail(window.location.href);
                    //return href.slice(0,href.lastIndexOf("/")-href.length+1) + 'img/cluster.png';
                    return App.settings.appSettings.iconLocation + 'cluster.png';
                } else if (feature.cluster) {
                    // all single points should go through this
                    return feature.cluster[0].attributes.icon;
                } else {
                    // last check
                    if (feature.attributes.icon) {
                        return feature.attributes.icon;
                    } else {
                        // default if nothing else
                        return App.settings.appSettings.serviceSymbols + '/ems/other/small.png';
                    }
                }
            },
            clusterSize: function (feature) {
                if (feature.attributes.count > 1) {
                    var size = 7 + feature.attributes.count;
                    // may need to adjust this for very large numbers
                    return (size > 20) ? 20 : size;
                } else {
                    // base symbol size, used for both cluster icons and
                    // entry icons, its a radius so results in a 30x30 square
                    return 15;
                }
            },
            myLabel: function (feature) {
                if (feature.attributes.count > 1) {
                    // clusters are labelled with the count
                    return feature.attributes.count;
                } else {
                    return '';
                }
            },
            myBackground: function (feature) {
                var colour = null;
                var background = '';
                if (feature.attributes.count > 1) {
                    // no background on clusters
                    return '';
                } else if (feature.cluster) {
                    // normal single point
                    if (feature.cluster[0].attributes.colour) {
                        colour = feature.cluster[0].attributes.colour.toLowerCase();
                    }
                } else {
                    // last check
                    if (feature.attributes.colour) {
                        colour = feature.attributes.colour.toLowerCase();
                    }
                }
                if (colour) {
                    if (VIEW.layerColours[colour]) {
                        background = App.settings.appSettings.iconLocation +
                            VIEW.layerColours[colour]['background'];
                    }
                }
                return background;
            }
        }
    };
    
    var GeometryOptions = {
        context: {
            myStrokeWidth: function (feature) {
                if (!feature.cluster && feature.geometry &&
                feature.geometry instanceof OpenLayers.Geometry.LineString) {
                    // make lines easier to see by being thicker
                    return 5;
                }
                // 1 is the default as it helps outline other shapes
                return 1;
            },
            myStrokeColor: function (feature) {
                if (!feature.cluster && feature.geometry &&
                feature.geometry instanceof OpenLayers.Geometry.LineString) {
                    // lines are shown in their respective colour
                    if (feature.attributes.colour) {
                        var colour = feature.attributes.colour.toLowerCase();
                        if (VIEW.layerColours[colour]) {
                            return VIEW.layerColours[colour]['colour'];
                        }
                    }
                }
                // black is the default for all other geometries as its
                // used to outline the fill colour
                return '#000000';
            },
            myFillColor: function (feature) {
                if (feature.attributes.colour) {
                    var colour = feature.attributes.colour.toLowerCase();
                    if (VIEW.layerColours[colour]) {
                        return VIEW.layerColours[colour]['colour'];
                    }
                }
                // green is the default
                return '#008000';
            }
        }
    };
    
    VIEW.layerStyles = {
        // normal display of a cluster
        'small_cluster_style': new OpenLayers.Style({
            graphicOpacity: 0.7,
            externalGraphic: '${mySymbol}',
            pointRadius: '${clusterSize}',
            label: '${myLabel}',
            labelAlign: 'lm',
            labelXOffset: -4,
            labelYOffset: 0,
            fontColor: 'black',
            fontSize: '12px',
            fontWeight: 'bold',
            fontFamily: 'Arial, Helvetica, sans-serif;',
            backgroundGraphic: '${myBackground}',
            // slightly larger than the 30x30 icon
            backgroundHeight: 32,
            backgroundWidth: 32
        }, ClusterOptions),
        // alter some values when hovering over a cluster to expand it
        'big_cluster_style': new OpenLayers.Style({
            pointRadius: 20,
            fillOpacity: 0.9,
            strokeColor: 'red',
            strokeOpacity: 0.9
        }),
        // Entry geometry layers
        'geometry_style': new OpenLayers.Style({
            fillOpacity: 0.5,
            fillColor: '${myFillColor}',
            strokeOpacity: 0.7,
            strokeColor: '${myStrokeColor}',
            strokeWidth: '${myStrokeWidth}'
        }, GeometryOptions)
    };
    
})();


/**
 * Permanent Map Layers
 */
VIEW.mapLayers = VIEW.mapLayers || {};

// User location
VIEW.mapLayers.location = new OpenLayers.Layer.Vector('Location', {
        displayInLayerSwitcher: false
    });

// this layer shows the actual icons and cluster symbols
VIEW.mapLayers.MASASLayer = new OpenLayers.Layer.Vector('MASAS Feed', {
        visibility: true,
        strategies: [new OpenLayers.Strategy.Cluster({distance: 20, addTo: true})],
        styleMap: new OpenLayers.StyleMap({
            "default": VIEW.layerStyles.small_cluster_style,
            "select": VIEW.layerStyles.big_cluster_style,
            "delete": {display: 'none'}
        }),
        eventListeners: {
            featureselected: VIEW.feature_selected,
            featureunselected: VIEW.feature_unselected
        }
    });

VIEW.mapLayers.MASASLayerDraft = new OpenLayers.Layer.Vector('MASAS Drafts', {
    visibility: true,
    strategies: [new OpenLayers.Strategy.Cluster({distance: 20, addTo: true})],
    styleMap: new OpenLayers.StyleMap({
        "default": VIEW.layerStyles.small_cluster_style,
        "select": VIEW.layerStyles.big_cluster_style,
        "delete": {display: 'none'}
    }),
    eventListeners: {
        featureselected: VIEW.feature_selected,
        featureunselected: VIEW.feature_unselected
    }
});


/**
 * Method: checkGeometry
 * Resets the centroid geometry for non-point geometries and adjusts the
 * point where the icon is displayed if it is partially off-screen.  Each
 * feature maintains an 'originalGeometry' property which holds the
 * geometry as coontained in the feed.
 *
 * Parameters:
 * node - {DOMElement} An Atom entry or feed node.
 *
 * Returns:
 * An Array of <OpenLayers.Feature.Vector>s.
 */
VIEW.checkGeometry = function( ev ) {
    if( !App.settings.userSettings.autoCenterPolygonMarkup && !ev.onLoad ) {
        return;
    }

    var features = VIEW.mapLayers.MASASLayer.features;
    features = features.concat( VIEW.mapLayers.MASASLayerDraft.features );

    var mapExtent = VIEW.map.getExtent();
    var offset = VIEW.map.getResolution() * 100;
    //TODO: showDetailedPopups isn't available now, is this difference
    //      in offset even required?
    var xfactor = (VIEW.showDetailedPopups) ? 5 : 4;
    var yfactor = (VIEW.showDetailedPopups) ? 3 : 2;
    if (features) {
        // loop through all features
        for (var i = 0; i < features.length; ++i) {
            if (features[i].cluster) {
                for (var j = 0, len = features[i].cluster.length; j < len; ++j) {
                    var geometry = features[i].cluster[j].originalGeom;
                    var centroid = geometry.getCentroid();
                    if (geometry && geometry.CLASS_NAME !== 'OpenLayers.Geometry.Point') {
                        var fBounds = geometry.getBounds();

                        // case where the feature geometry completely overlaps the viewport
                        // move the icon point to a fixed position offset from the
                        // top-right corner
                        if (fBounds.containsBounds(mapExtent)) {
                            var newGeom = new OpenLayers.Geometry.Point(mapExtent.right - xfactor * offset, mapExtent.top - yfactor * offset);
                            if (ev.zoomChanged) {
                                features[i].cluster[j].geometry = newGeom;
                                features[i].cluster[j].adjustedCentroid = true;
                            } else {
                                features[i].move(new OpenLayers.LonLat(newGeom.x, newGeom.y));
                                features[i].adjustedCentroid = true;
                                if (len === 1) {
                                    features[i].cluster[j].geometry = newGeom;
                                    features[i].cluster[j].adjustedCentroid = true;
                                }
                            }

                        }
                        // case where feature geometry just intersects the viewport
                        // move the icon point to the closest vertex that is within
                        // the viewport
                        else if ((!mapExtent.contains(centroid.x, centroid.y)) && (mapExtent.intersectsBounds(fBounds))) {
                            var vertices = geometry.getVertices();
                            var dist = Number.MAX_VALUE;
                            var closest = -1;
                            var mapCenter = mapExtent.toGeometry().getCentroid();
                            for (var v = 0; v < vertices.length; ++v) {
                                var testDist = vertices[v].distanceTo(mapCenter);
                                if (testDist < dist) {
                                    dist = testDist;
                                    closest = v;
                                }
                            }
                            if (closest > -1) {
                                var newGeom = vertices[closest].clone();
                                if (ev.zoomChanged) {
                                    features[i].cluster[j].geometry = newGeom;
                                    features[i].cluster[j].adjustedCentroid = true;
                                } else {
                                    features[i].move(new OpenLayers.LonLat(newGeom.x, newGeom.y));
                                    features[i].adjustedCentroid = true;
                                    if (len == 1) {
                                        features[i].cluster[j].geometry = newGeom;
                                        features[i].cluster[j].adjustedCentroid = true;
                                    }
                                }
                            }
                        }
                        // all other cases, just use the geometry as is
                        else {
                            var centroid = geometry.getCentroid();
                            if (features[i].cluster[j].adjustedCentroid) {
                                //reset the geometry to default centroid
                                features[i].cluster[j].geometry = centroid;
                                features[i].cluster[j].adjustedCentroid = false;
                            }
                            if (features[i].adjustedCentroid) {
                                //reset the geometry to default centroid
                                features[i].move(new OpenLayers.LonLat(centroid.x, centroid.y));
                                features[i].adjustedCentroid = false;
                            }
                        }
                    }
                }//j loop
            }
        }//i loop
    }
};

// display the geometry of an entry during a hover/popup
VIEW.mapLayers.MASASLayerHighlight = new OpenLayers.Layer.Vector('MASAS Highlight', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style
        })
    });

// display the geometry of an entry when the content window is open
VIEW.mapLayers.MASASLayerEntry = new OpenLayers.Layer.Vector('MASAS Entry', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style,
            // minor style change for revision geometries
            'revision': OpenLayers.Util.applyDefaults(
                {'fillColor': '#FF00FF', 'strokeColor': '#FF00FF'},
                VIEW.layerStyles.geometry_style)
        })
    });

// displaying all feature geometries with no icons or popups
VIEW.mapLayers.MASASLayerGeometries = new OpenLayers.Layer.Vector('MASAS Geometries', {
        visibility: false,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': VIEW.layerStyles.geometry_style
        })
    });

// displays a bounding box of the feed loading request area
VIEW.mapLayers.loadFeed = new OpenLayers.Layer.Vector('Load Feed', {
        visibility: true,
        displayInLayerSwitcher: false,
        styleMap: new OpenLayers.StyleMap({
            'default': new OpenLayers.Style({
                fill: false,
                strokeColor: 'red',
                strokeOpacity: 0.5,
                strokeDashstyle: 'dot'
            })
        })
    });


/**
 * Initial Map Controls
 */
VIEW.mapControls = VIEW.mapControls || {};

// created here in order to reference VIEW.mapLayers above
VIEW.mapControls.highlightControl = new OpenLayers.Control.SelectFeature(
    [VIEW.mapLayers.MASASLayer, VIEW.mapLayers.MASASLayerDraft], {
        hover: false,
        toggle: true,
        autoActivate: true
    });

VIEW.mapControls.loadingControl = new OpenLayers.Control.LoadingPanel();

VIEW.mapControls.navHistory = new OpenLayers.Control.NavigationHistory();

VIEW.mapControls.attribution = new OpenLayers.Control.Attribution();

//VIEW.mapControls.mousePos = new OpenLayers.Control.MousePosition({
//        prefix: '<a target="_blank" ' +
//            'href="http://spatialreference.org/ref/epsg/4326/">' +
//            'EPSG:4326</a> coordinates: '
//    }
//);


/**
 * Initialize remainder of map functionality once all files loaded and ready
 */
VIEW.initialize_map = function () {
    VIEW.map.addLayers(Ext.getValues(VIEW.mapLayers));
    VIEW.map.addControls(Ext.getValues(VIEW.mapControls));
    VIEW.MapLayers.initialize();

    VIEW.map.baseLayer.events.register("moveend", null, VIEW.checkGeometry);
};
