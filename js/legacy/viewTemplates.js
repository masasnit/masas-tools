/**
MASAS View Tool - Entry and CAP ExtJS XTemplates
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

Entry and CAP ExtJS XTemplates for the various information popups in
the application.
*/

/*global MASAS,VIEW,Ext,OpenLayers,GeoExt */
Ext.namespace('VIEW.Template');

/**
 * Ext XTemplate for clusters
 */
VIEW.Template.Cluster_Title_Template = new Ext.XTemplate(
'<h2 class="tooltipTitle">',
    '{values.cluster.length} Entries: ',
    '<a href="#cluster{[values.id.split(\'_\').pop()]}"',
        ' onClick="VIEW.zoom_to_cluster(\'{id}\',\'{values.layer.id}\')">',
        'Zoom To Entries',
    '</a>',
'</h2>');


/**
 * Ext XTemplate for clusters
 */
VIEW.Template.Cluster_Tip_Template = new Ext.XTemplate(
'<div class="popupBody">',
    '<table class="tooltipTable">',
        '<tbody>',
            '<tpl for=".">',
                '<tr class="{[xindex % 2 === 0 ? "even" : "odd"]}">',
                    // using feature's original data values and not transformed
                    // attributes
                    '<tpl for="data">',
                        '<td class="{[values.colour ? values.colour.toLowerCase() : "no"]}EntryBackground">',
                            '<img alt="Icon" height="20" src="{icon}" />',
                        '</td>',
                        '<td>',
                            // unique anchor using id and UTC time value for
                            // browser history tracking
                            '<a href="#{id},{updated}"',
                            ' onClick="VIEW.highlight_cluster_feature(\'{id}\')">{title}</a>',
                        '</td>',
                    '</tpl>',
                '</tr>',
            '</tpl>',
        '</tbody>',
    '</table>',
'</div>');


/**
 * Ext XTemplate for the Title portion of a popup, combines with simple or detailed.
 */
VIEW.Template.Popup_Title_Template = new Ext.XTemplate(
'<div class="popupTitle">',
        '<img class="entryIcon" alt="Event" height="20" src="{icon}">',
        // unique anchor using id and UTC time value for browser history tracking
        '<a href="#{id},{original_updated}"',
            ' onClick="VIEW.show_content_window({canEdit},\'{id}\',\'{modelStoreId}\')">',
        '&nbsp;{title}</a>',
        '<div class="contentLink">',
            '<tpl if="CAP==&quot;Y&quot;">',
                '<a title="Show CAP Message" class="btnAlertIcon"',
                    ' href="#{id},{original_updated}"',
                    ' onClick="VIEW.show_content_window({canEdit},\'{id}\',\'{modelStoreId}\',\'messages|CAP\')"></a>',
            '</tpl>',                
            '<tpl if="links && \'attachments\' in links && links.attachments.length &gt; 0">',
                '<a title="Show Attachment" class="btnAttachIcon"',
                    ' href="#{id},{original_updated}"',
                    ' onClick="VIEW.show_content_window({canEdit},\'{id}\',\'{modelStoreId}\',\'attachments\')"></a>',
            '</tpl>',
            '<tpl if="links && \'xlink\' in links">',
                '<a title="Show Related Link" class="btnLinkIcon"',
                    ' href="#{id},{original_updated}"',
                    ' onClick="VIEW.show_content_window({canEdit},\'{id}\',\'{modelStoreId}\')"></a>',
            '</tpl>',
        '</div>',
'</div>');


/**
 * Ext XTemplate for a simple popup
 */
VIEW.Template.Simple_Popup_Template = new Ext.XTemplate(
'<tpl if="status == \'Test\' || status == \'Draft\'">',
    '<div class="EntryStatusException">Status: {status}</div>',
'</tpl>',
'<div>Author: {author_name}</div>',
'<div class="popupInfo">Latest Update: {updated:date("M j H:i:s")}</div>',
'<div class="popupBody">{content}</div>');


/**
 * Ext XTemplate for a detailed popup
 */
VIEW.Template.Detailed_Popup_Template = new Ext.XTemplate(
'<div class="popupBody">',
    '<table>',
        '<tr><td>Author</td><td>{author_name}</td></tr>',
        '<tr><td>Status</td><td>{status}</td></tr>',
        '<tr><td>Severity</td><td>{severity}</td></tr>',
        '<tr><td>Category</td><td>{category}</td></tr>',
        '<tr><td>Updated</td><td>{updated:date("M j H:i:s")}</td></tr>',
        '<tr><td>Expires</td><td>{expires:date("M j H:i:s")}</td></tr>',
        '<tr><td>Content</td><td>{content}</td></tr>',
    '</table>',
'</div>');


/**
 * Ext XTemplate for a Overlay detailed popup
 */
VIEW.Template.Overlay_Detailed_Popup_Template = new Ext.XTemplate(
    '<div class="popupBody">',
    '<table>',
    '<tr><td>Author</td><td>{author_name}</td></tr>',
    '<tr><td>Updated</td><td>{updated:date("M j H:i:s")}</td></tr>',
    '<tr style="vertical-align:top;"><td>Description</td><td><textarea readonly rows=4 style="width:98%">{content}</textarea></td></tr>',
    '</table>',
    '</div>');

/**
 * Ext XTemplate that converts an Entry from XML to HTML for a normal
 * content window.
 */
VIEW.Template.Entry_Display_Template = new Ext.XTemplate(
'<tpl for="entry">',
    '<div class="TemplateDisplayBox">',
        '<div>',
            '<tpl if="typeof(entry_url) != \'undefined\' && typeof(secret) != \'undefined\'">',
                '<a href="#" onClick="VIEW.show_entry_options(\'{entry_url}\', \'{secret}\'',
                    '<tpl if="typeof(geom_type) != \'undefined\' && typeof(geom_val) != \'undefined\'">',
                        ', \'{geom_type}\', \'{geom_val}\'',
                    '</tpl>',
                    '); return false;">',
                    '<img alt="Options" title="Entry Options" src="img/wrench_orange.png"',
                        ' height="14" width="14"></a>',
            '</tpl>',
            '<span class="entryIdTitle">{id}</span>',
        '</div>',
        '<tpl for="author">',
            '<p><b>Author:</b> &nbsp;',
                '<a href="{uri}" onClick="VIEW.show_author(\'{uri}\',\'{name}\'); return false;">',
                    '{name}</a>',
            '</p>',
        '</tpl>',
        '<p><b>Latest Update:</b> {updated}',
            '<tpl if="typeof(show_revisions) != \'undefined\'">',
                '<tpl if="published != updated">',
                    '<span class="revisionsLink">',
                        '<a title="Show Revision History" href="#{id},revisions"',
                            ' onClick="VIEW.show_content_window(\'{id}\', \'\', \'advanced|Revisions\')">',
                                'Show Revisions</a>',
                    '</span>',
                '</tpl>',
            '</tpl>',
        '</p>',
        '<p><b>First Published:</b> {published} </p>',
        '<tpl if="typeof(effective) != \'undefined\'">',
            '<p><b>Effective:</b> {effective} </p>',
        '</tpl>',
        '<tpl if="typeof(expires) != \'undefined\'">',
            '<p><b>Expires:</b> {expires} </p>',
        '</tpl>',
    '</div>',
    '<div class="TemplateDisplayBox">',
        '<h3>Title</h3>',
        // specially formatted title
        '<tpl if="typeof(title2) != \'undefined\'">',
            '<p>{title2}</p>',
        '</tpl>',
        // fallback when not available, normally used by Revisions Template
        '<tpl if="typeof(title2) == \'undefined\'">',
            '<tpl if="title[\'@type\'] == \'xhtml\'">',
                '<tpl for="title.div.div">',
                    '<p><b>{[values["@xml:lang"]]}:</b> {[values["#text"]]} </p>',
                '</tpl>',
            '</tpl>',
            '<tpl if="title[\'@type\'] == \'text\'">',
                // accessing the title object here using the root values
                '<p> {[values.title["#text"]]} </p>',
            '</tpl>',
        '</tpl>',
    '</div>',
    '<div class="TemplateDisplayBox">',
        '<h3>Content</h3>',
        // specially formatted content
        '<tpl if="typeof(content2) != \'undefined\'">',
            '<p>{content2}</p>',
        '</tpl>',
        // fallback when not available, normally used by Revisions Template
        '<tpl if="typeof(content2) == \'undefined\'">',
            '<tpl if="content[\'@type\'] == \'xhtml\'">',
                '<tpl for="content.div.div">',
                    '<tpl if="typeof(values[\'#text\']) != \'undefined\'">',
                        '<p><b>{[values["@xml:lang"]]}:</b> {[values["#text"]]}</p>',
                    '</tpl>',
                '</tpl>',
            '</tpl>',
            '<tpl if="content[\'@type\'] == \'text\'">',
                '<tpl if="typeof(values.content[\'#text\']) != \'undefined\'">',
                    // accessing the content object here using the root values
                    '<p>{[values.content["#text"]]}</p>',
                '</tpl>',
            '</tpl>',
        '</tpl>',  
    '</div>',
    '<tpl for="link">',
        '<tpl if="values[\'@rel\'] == \'related\' && values[\'layer\']">',
            '<div class="TemplateDisplayBox">',
                '<h3>Map Layer</h3>',
                '<p>',
                    '<tpl if="values[\'@title\']">',
                        '{[values["@title"]]} - ',
                    '</tpl>',
                    '<a href="{[values[\'@href\']]}" onClick="VIEW.Layers.add_entry_layer(',
                        '\'{[values["@title"]]}\', \'{[values[\'@type\']]}\', ',
                        '\'{[values["@href"]]}\'); return false;"> Add to Map </a>',
                '</p>',
            '</div>',
        '</tpl>',
    '</tpl>',
    '<div class="TemplateDisplayBox">',
        '<h3>Categories</h3>',
        '<tpl for="category">',
            '<p><b>{[values["@label"]]}:</b> {[values["@term"]]}',
                '<tpl if="typeof(values[\'@mea:context\']) != \'undefined\'">',
                    ' - {[values["@mea:context"]]}',
                '</tpl>',
            '</p>',
        '</tpl>',
    '</div>',
    '<tpl for="link">',
        '<tpl if="values[\'@rel\'] == \'related\' && !values[\'layer\']">',
            '<div class="TemplateDisplayBox">',
                '<h3>Related Link</h3>',
                '<p>',
                    '<tpl if="values[\'@title\']">',
                        '{[values["@title"]]} - ',
                    '</tpl>',
                    // local related links versus external links
                    '<a href="{[values[\'@href\']]}"',
                        '<tpl if="values[\'local\']">',
                            ' onClick="VIEW.show_entry_revision(',
                                '\'{[values[\'@href\']]}\', \'{parent.secret}\'); return false;"',
                        '</tpl>',
                        '<tpl if="!values[\'local\']">',
                            ' target="_blank"',
                        '</tpl>',
                        '>{[values["@href"]]}</a>',
                '</p>',
            '</div>',
        '</tpl>',
    '</tpl>',
'</tpl>');


/**
 * Ext XTemplate to show an Entry's attachments
 */
VIEW.Template.Entry_Attachment_Template = new Ext.XTemplate(
'<tpl for="entry">',
    '<tpl for="link">',
        '<tpl if="values[\'@rel\'] == \'enclosure\'">',
            '<div class="TemplateDisplayBox">',
                '<tpl if="values[\'@type\'] == \'application/common-alerting-protocol+xml\'">',
                    '<h3>CAP Message</h3>',
                    '<p>',
                        //TODO: sync the URLs show for attachments so that
                        //      the browser history will show that its already
                        //      been viewed, old method used id,updated
                        //'<a class="CAPRevisionLink" href="#{parent.id},{parent.updated}">{[values["@href"]]}</a>',
                        '<a class="CAPRevisionLink" href="{[values["@href"]]}?secret={parent.secret}">{[values["@href"]]}</a>',
                    '</p>',
                '</tpl>',
                '<tpl if="values[\'@type\'] != \'application/common-alerting-protocol+xml\'">',
                    '<h3>Attachment</h3>',
                    '<p>',
                        '<tpl if="values[\'@title\']">',
                            '{[values["@title"]]} - ',
                        '</tpl>',
                        '<a class="AttachmentRevisionLink" href="{[values["@href"]]}?secret={parent.secret}"',
                            ' data-mime="{[values["@type"]]}"',
                            '<tpl if="values[\'@title\']">',
                                ' data-title="{[values["@title"]]}"',
                            '</tpl>',
                            '<tpl if="values[\'@length\']">',
                                ' data-length="{[values["@length"]]}"',
                            '</tpl>',
                        '>{[values["@href"]]}</a>',
                    '</p>',
                '</tpl>',
            '</div>',
        '</tpl>',
    '</tpl>',
'</tpl>');


/**
 * Ext XTemplate to combine Entry and Attachment templates
 */
VIEW.Template.Entry_Combined_Template = new Ext.XTemplate(
'<tpl for="entry">',
    '{[VIEW.Template.Entry_Display_Template.apply({entry: values})]}',
    '{[VIEW.Template.Entry_Attachment_Template.apply({entry: values})]}',
'</tpl>');


/**
 * Ext XTemplate that converts a CAP message from XML to HTML.
 */
VIEW.Template.CAP_Display_Template = new Ext.XTemplate(
'<tpl for="alert">',
    '<div class="TemplateDisplayBox">',
        '<h2>{scope} {status} {msgType}</h2>',
        '<p><b>Identifier:</b> {identifier} </p>',
        '<p><b>Sender:</b> {sender} </p>',
        '<p><b>Sent:</b> {sent} </p>',
    '</div>',
    '<tpl for="info">',
        '<div class="TemplateDisplayBox">',
            '<h2>{event}</h2>',
            '<tpl if="typeof(headline) != \'undefined\'">',
                '<p><b>Headline:</b> {headline} </p>',
            '</tpl>',
            '<tpl if="typeof(language) != \'undefined\'">',
                '<p><b>Language:</b>',
                    '<tpl if="language == \'en-CA\'"> English </tpl>',
                    '<tpl if="language == \'fr-CA\'"> French </tpl>',
                    '<tpl if="language != \'en-CA\' && language != \'fr-CA\'"> {language} </tpl>',
                '</p>',
            '</tpl>',
            '<p><b>Urgency:</b> {urgency} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                '<b>Severity:</b> {severity} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                '<b>Certainty:</b> {certainty} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>',
            '<tpl if="typeof(expires) != \'undefined\'">',
                '<p><b>Expires:</b> {expires} </p>',
            '</tpl>',
            '<tpl if="typeof(senderName) != \'undefined\'">',
                '<p><b>Sender Name:</b> {senderName} </p>',
            '</tpl>',
            '<tpl if="typeof(contact) != \'undefined\'">',
                '<p><b>Contact:</b> {contact} </p>',
            '</tpl>',
            '<tpl if="typeof(web) != \'undefined\'">',
                '<p><b>Web:</b> <a href="{web}" target="_blank">{web}</a> </p>',
            '</tpl>',
            '<tpl if="typeof(description) != \'undefined\'">',
                '<p><b>Description:</b> {description} </p>',
            '</tpl>',
            '<tpl if="typeof(instruction) != \'undefined\'">',
                '<p><b>Instruction:</b> {instruction} </p>',
            '</tpl>',
            '<tpl if="typeof(area) != \'undefined\'">',
                '<p><b>Area:</b>',
                    '<tpl for="area">',
                        ' {areaDesc} <a class="btnCAPArea" id="{areaid}" href="#"',
                            ' onClick="VIEW.show_CAP_area(\'{areaid}\'); return false;">',
                                '{areadisplay}</a>,',
                    '</tpl>',
                '</p>',
            '</tpl>',
        '</div>',
    '</tpl>',
'</tpl>');


/**
 * Ext XTemplate for attachments
 */
VIEW.Template.Attachment_Display_Template = new Ext.XTemplate(
'<tpl>',
    '<div class="TemplateDisplayBox">',
        '<h2>{title}</h2>',
        '<hr>',
        '<tpl if="sizeType == \'thumbnail\'">',
            '<div class="attachement-preview-wrapper">',
                '<div class="attachment-preview-icon attachment-preview-{previewClass}">',
                    '<a href="{url}" target="_blank"><img src="{[Ext.BLANK_IMAGE_URL]}"/></a>',
                '</div>',
            '</div>',
        '</tpl>',
        '<tpl if="sizeType != \'thumbnail\'">',
            '<div class="attachment-preview-image">',
                '<a href="{url}" target="_blank">',
                '<img {[(values.sizeType!=\'full\')?\'style="height:150px"\':\'style="height:100%"\']} src="{url}"/>',
                '</a>',
            '</div>',
        '</tpl>',
        '<div class="TemplateDisplayBox">',
            '<div class="attachment-link">',
                '<p>',
                    '<tpl if="mime == \'application/vnd.google-earth.kml+xml\'">',
                        '<a href="{url}" onClick="VIEW.Layers.add_entry_layer(',
                        // change mime to custom type
                            '\'{title}\', \'entry_kml\', \'{url}\'); return false;"> Add to Map </a>',
                        '&nbsp;&nbsp;&nbsp; or &nbsp;&nbsp;&nbsp;',
                    '</tpl>',
                    '<a href="{url}&download=yes" target="_blank">Download and Save</a>',
                '</p>',
            '</div>',
            '<p><b>Title: </b>{title}</p>',
            '<tpl if="hrefLang">',
                '<p><b>HrefLang:</b> {hrefLang} </p>',
            '</tpl>',
            '<tpl if="length && parseInt(length)">',
                '<p><b>Size:</b> {length:fileSize} </p>',
            '</tpl>',
            '<p><b>File Type: </b>{mime}</p>',
        '</div>',
    '</div>',
'</tpl>');


/**
 * Ext XTemplate for revisions
 */
VIEW.Template.Revision_Display_Template = new Ext.XTemplate(
'<div class="revisionsSummary">{summary}</div>',
'<tpl for="feed">',
    '<tpl for="entry">',
        '<div class="TemplateDisplayBox">',
            '<h2 class="RevisionHeader">Revision - {updated}</h2>',
            '<div class="btnRevisionDetails" id="btnRevDetails-{[xindex-1]}"></div>',
            '<div class="btnRevisionGeometry" id="btnRevGeometry-{[xindex-1]}"',
                ' data-updated="{updated}"></div>',
            '<div style="clear: left;"></div>',
            '<div class="entryRev x-hide-display" id="entryRev-{[xindex-1]}">',
                '{[VIEW.Template.Entry_Display_Template.apply({entry: values})]}',
                '{[VIEW.Template.Entry_Attachment_Template.apply({entry: values})]}',
            '</div>',
        '</div>',
    '</tpl>',
'</tpl>');


/**
 * Ext XTemplate for access logs
 */
VIEW.Template.Access_Log_Template = new Ext.XTemplate(
'<tpl if="typeof(feed) != \'undefined\'">',
    '<tpl for="feed">',
        '<tpl for="entry">',
            '<div class="TemplateDisplayBox">',
                '<tpl for="author">',
                    '<p>',
                        '<b>Account:&nbsp;',
                        '<a href="{uri}" onClick="VIEW.show_author(\'{uri}\'); return false;">{name}</a></b>',
                        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{uri}',
                    '</p>',
                '</tpl>',
                '<p>',
                    'Accessed <span class="viewedContentLink">',
                        '<a href="{[values.link[\'@href\']]}?secret={parent.secret}" target="_blank">',
                        '{[values.title[\'#text\']]}</a></span>',
                    ' on {updated}',
                '</p>',
            '</div>',
        '</tpl>',
    '</tpl>',
'</tpl>',
'<tpl if="typeof(feed) == \'undefined\'">',
    '<div class="TemplateDisplayBox"><h2>No Access Log for this Entry</h2></div>',
'</tpl>');


/**
 * Ext XTemplate for author settings lists
 */
VIEW.Template.Author_List_Template = new Ext.XTemplate(
'<tpl for=".">',
    '<div class="authorListItem">',
        '<div class="',
        '<tpl if="typeof(remove) == \'undefined\'">',
            'authorListAdd',
        '</tpl>',
        '<tpl if="typeof(remove) != \'undefined\'">',
            'authorListRemove',
        '</tpl>',
        '"></div>',
        '<div class="authorListInfo">',
            '<div class="authorListName">',
                '<a href="{uri}" onClick="VIEW.show_author(\'{uri}\'); return false;">{name}</a>',
            '</div>',
            '<p>',
                '<tpl if="typeof(org_name) != \'undefined\'">',
                    '{org_name}',
                '</tpl>',
            '</p>',
        '</div>',
        '<div style="clear: both;"></div>',
    '</div>',
'</tpl>');
