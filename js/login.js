/** @namespace Namespace for APP classes and functions. */
var mApp = mApp || {};

// Global Application variable.
var App = {};

// Start the app...
$( document ).ready(function() {
    App.loginMgr = new mApp.LoginManager();
});

// Login Manager...
mApp.LoginManager = function() {
    this.accessControlData = null;
    this.redirectURL = "./index.html";

    // Load the application settings...
    this._appSettings = this._loadAppSettings( "assets/config/application.json" );

    // Load the application settings...
    this._loadHubConfig( "assets/config/hubConfig.json" );
};

mApp.LoginManager.prototype._loadAppSettings = function( configFile ) {
    var jsonObj = this._loadDataFromFile( configFile );
    return jsonObj;
};

mApp.LoginManager.prototype._loadHubConfig = function( configFile ) {
    var jsonObj = this._loadDataFromFile( configFile );

    this._hubs = jsonObj.hubs;
    this._accessServices = jsonObj.accessServices;

    return;
};

mApp.LoginManager.prototype._loadDataFromFile = function( configFile ) {
    var jsonObj = null;

    var request = $.ajax({
        type: 'GET',
        async: false,
        url: configFile,
        timeout: 120000
    });

    if( request.status == 200 ) {
        var responseTxt = request.responseText;
        jsonObj = JSON.parse( responseTxt );
    }

    return jsonObj;
};

mApp.LoginManager.prototype.validateAccessCode = function() {
    // Get the access code...
    var accessCode = $('#textAccessCode').val()

    if( accessCode ) {
        this.checkUserCredentials( accessCode, this._loginSuccess, this._loginFailure );
    }
}

//on a successfull login store the access code and prompt user for hub
mApp.LoginManager.prototype._loginSuccess = function( code, accessControlData ) {
    $("#cboHubs").empty();
    // Load the hubs into the combo box...
    $.each( accessControlData, function( acCtr, acData ) {
        $.each(App.loginMgr._hubs, function(hubCtr, hub){
            if( hub.url == acData.url )
            {
                App.loginMgr.accessControlData[acCtr].hubName = hub.name;
                if( acData.post == "Y" )
                {
                    $( '#cboHubs' ).append( new Option( hub.name + ": Read/Write", acCtr ) );
                }
                else {
                    $( '#cboHubs' ).append( new Option( hub.name + ": Read", acCtr ) );
                }
            }
        });
    });

    $("#modalHubSelect").modal("show");
    setCookie( "accesscode", code, 1 );
};

//redirect a user upon selecting hub
mApp.LoginManager.prototype._hubSelect = function() {
    var acIndex = $('#cboHubs').val();
    var acData = App.loginMgr.accessControlData[acIndex];
    $("#modalHubSelect").modal("hide");
    setCookie( "hubUrl", acData.url, 1 );
    setCookie( "hubName", acData.hubName, 1 );
    setCookie( "accessUrl", acData.accessUrl, 1 );
    setCookie( "uri", acData.userUri, 1 );

    window.location.replace( App.loginMgr.redirectURL );
};


//tell the user he failed to login
mApp.LoginManager.prototype._loginFailure = function( failureMsg ) {
    $("#loginError").css( 'visibility', 'visible' );
};

//check user credentials using the provided access control api
mApp.LoginManager.prototype.checkUserCredentials = function( code, callback_success, callback_fail ) {

    App.loginMgr.accessControlData = [];

    // Let's hit all the access controls that are configured...
    for( var acCtr=0; acCtr < this._accessServices.length; acCtr++ )
    {
        var accessControlURL = this._accessServices[acCtr].url + '/api/check_access' + '?secret=' + code + '&query_secret=' + code;
        var url = this._appSettings.proxy.url + encodeURIComponent( accessControlURL );

        var request = $.ajax({
            type: 'GET',
            url: url,
            timeout: 120000,
            async: false
        });

        request.done( function( responseMsg ) {
            var userData = JSON.parse( responseMsg );

            if( userData.hubs !== undefined )
            {
                for( var hubCtr = 0; hubCtr < userData.hubs.length; hubCtr++ )
                {
                    App.loginMgr.accessControlData.push( {
                        'userName': userData.name,
                        'userId': userData.id,
                        'userUri': userData.uri,
                        'accessUrl': App.loginMgr._accessServices[acCtr].url,
                        'hubName': '',
                        'url': userData.hubs[hubCtr].url,
                        'post': userData.hubs[hubCtr].post

                    })
                }
            }

        });
    }

    if( App.loginMgr.accessControlData.length == 0 )
    {
        var failureMsg = 'Failed to retrieve MASAS User data!';

        if( callback_fail && typeof( callback_fail ) === "function" ) {
            callback_fail( failureMsg );
        }
    }
    else {
        if( callback_success && typeof( callback_success ) === "function" ) {
            callback_success( code, App.loginMgr.accessControlData );
        }
    }

};

