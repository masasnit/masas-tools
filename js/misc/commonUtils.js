
mApp.CommonUtils = (function () {

    // Instance stores a reference to the Singleton
    var instance;

    function init() {

        // Singleton

        // Private methods and variables
//        function privateMethod() {
//        }
//
//        var privateVariable = "Im also private";

        return {
            // Public methods and variables

            // used to convert from UTC to local time, ie for Eastern its -4
            UTC_Local_Offset: new Date().getTimezoneOffset() / -60,

            // used to convert from local time to UTC, ie for Eastern is +4
            Local_UTC_Offset: new Date().getTimezoneOffset() / 60,

            // regular expressions to decode the enclosure mime-types
            EnclosureRegEx: {
                image: /jpeg|gif|png|tiff|image/,
                video: /video/,
                text: /text\/plain/,
                spreadsheet: /text\/csv|excel/,
                word: /word|rtf/,
                pdf: /pdf/,
                kml: /kml\+xml/
            },

            /**
             Uses the browser's builtin parser for XML

             @param {String} - the plain text version of some XML
             @return {Object} - an XML DOM object
             */
            parseXML: function( xml ) {
                var doc = null;
                if (window.DOMParser) {
                    try {
                        doc = (new DOMParser()).parseFromString(xml, 'text/xml');
                    } catch (err) {
                        doc = null;
                    }
                } else if (window.ActiveXObject) {
                    try {
                        doc = new ActiveXObject('Microsoft.XMLDOM');
                        doc.async = false;
                        if (!doc.loadXML(xml)) {
                            doc = null;
                        }
                    } catch (err) {
                        doc = null;
                    }
                }
                return doc;
            },

            /**
             Encode any XML reserved characters in a block of text, such as
             <tag>here & there</tag>

             @param {String} - text to check for reserved characters
             @return {String} - text which has had entities substituted instead
             */
            encodeXML: function (text) {
                if (!text) {
                    // blank or missing element support
                    return '';
                }
                text = text.replace(/\&/g, '&amp;');
                text = text.replace(/</g, '&lt;');
                text = text.replace(/>/g, '&gt;');
                return text;
            },

            /**
             Decode any entities back into reserved XML characters

             @param {String} - text to check for entities
             @return {String} - text which has had entities replaced with characters
             */
            decodeXML: function (text) {
                if (!text) {
                    // blank or missing element support
                    return '';
                }
                text = text.replace(/&amp;/g, '&');
                text = text.replace(/&lt;/g, '<');
                text = text.replace(/&gt;/g, '>');
                return text;
            },

            /**
             Change a datetime value to account for a timezone offset.  Similar to view tool.

             @param {Date} - datetime object to change
             @param {Integer} - the timezone offset in +/- hours
             @return {Date} - adjusted datetime object
             */
            adjustTime: function (date, offset) {
                var dt = date.clone();
                if (offset !== 0) {
                    var hrOff = parseInt(offset, 10);
                    var minOff = offset % hrOff * 60;
                    //test for non-integer offsets
                    if (minOff) {
                        var min = dt.getMinutes() + minOff;
                        dt.setMinutes(min);
                    }
                    var hr = dt.getHours() + hrOff;
                    dt.setHours(hr);
                }
                return dt;
            },

            /**
             * Simple GeoRSS point, parses XML text input from parseLocations or other
             * inputs as a general function.
             *
             * @param {string} text "lat lon"
             * @return {OpenLayers.Geometry.Point}
             */
            parseSimplePoint: function (text) {
                var xy = OpenLayers.String.trim(text).split(/\s+/);
                if (xy.length !== 2) {
                    xy = OpenLayers.String.trim(text).split(/\s*,\s*/);
                }

                return new OpenLayers.Geometry.Point(parseFloat(xy[1]), parseFloat(xy[0]));
            },

            /**
             * Simple GeoRSS line
             *
             * @param {string} text "lat lon lat lon"
             * @return {OpenLayers.Geometry.LineString}
             */
            parseSimpleLine: function (text) {
                var coords = OpenLayers.String.trim(text).split(/\s+/);
                var points = [];
                var p;
                for (var i = 0, ii = coords.length; i < ii; i += 2) {
                    p = new OpenLayers.Geometry.Point(parseFloat(coords[i + 1]),
                        parseFloat(coords[i]));
                    points.push(p);
                }

                return new OpenLayers.Geometry.LineString(points);
            },

            /**
             * Simple GeoRSS polygon
             *
             * @param {string} text "lat lon lat lon lat lon lat lon"
             * @return {OpenLayers.Geometry.Polygon}
             */
            parseSimplePolygon: function (text) {
                var coords = OpenLayers.String.trim(text).split(/\s+/);
                var points = [];
                var p;
                for (var i = 0, ii = coords.length; i < ii; i += 2) {
                    p = new OpenLayers.Geometry.Point(parseFloat(coords[i + 1]),
                        parseFloat(coords[i]));
                    points.push(p);
                }

                return new OpenLayers.Geometry.Polygon(
                    [new OpenLayers.Geometry.LinearRing(points)]);
            },

            /**
             * Simple GeoRSS box
             *
             * @param {string} text "lat lon lat lon"
             * @return {OpenLayers.Geometry.Polygon}
             */
            parseSimpleBox: function (text) {
                var coords = OpenLayers.String.trim(text).split(/\s+/);
                var points = [];
                points.push(new OpenLayers.Geometry.Point(  //lower-left
                    parseFloat(coords[1]),
                    parseFloat(coords[0])
                ));
                points.push(new OpenLayers.Geometry.Point(  //upper-left
                    parseFloat(coords[1]),
                    parseFloat(coords[2])
                ));
                points.push(new OpenLayers.Geometry.Point(  //upper-right
                    parseFloat(coords[3]),
                    parseFloat(coords[2])
                ));
                points.push(new OpenLayers.Geometry.Point(  //lower-right
                    parseFloat(coords[3]),
                    parseFloat(coords[0])
                ));
                points.push(new OpenLayers.Geometry.Point(  //repeat first point
                    parseFloat(coords[1]),
                    parseFloat(coords[0])
                ));

                return new OpenLayers.Geometry.Polygon(
                    [new OpenLayers.Geometry.LinearRing(points)]);
            },

            /**
             * Simple GeoRSS circle
             *
             * @param {string} text "lat lon radius"
             * @return {OpenLayers.Geometry.Polygon}
             */
            parseSimpleCircle: function (text, sides, internalProj, externalProj) {
                var coords = OpenLayers.String.trim(text).split(/\s+/);
                if (!sides) {
                    // default is 40 sides for a circular polygon
                    sides = 40;
                }
                var origin = new OpenLayers.Geometry.Point(parseFloat(coords[1]),
                    parseFloat(coords[0]));
                if (internalProj && externalProj) {
                    origin = origin.transform(externalProj, internalProj);
                }
                // georss circle radius is in meters
                var radius = coords[2];

                var circle = OpenLayers.Geometry.Polygon.createRegularPolygon(origin,
                    radius, sides, 0);

                return circle;
            }

        };
    };

    return {
        // Get the Singleton instance if one exists
        // or create one if it doesn't
        getInstance: function () {
            if( !instance ) {
                instance = init();
            }
            return instance;
        }
    };

})();




