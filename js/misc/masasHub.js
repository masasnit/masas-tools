var MASAS = MASAS || {};

MASAS.Attachment = function()
{
    this.uri            = '';
    this.title          = '';
    this.contentType    = '';
    this.length         = 0;
    this.base64         = undefined;
    this.file           = null;
};

MASAS.Hub = function( hubUrl ) {

    /// Private Members
    var that = this;
    var _canPost = false;

    /// Public Members

    this.hubUrl = hubUrl;
    this.userData = undefined;

    /// Public Methods

    this.CanPost = function()
    {
        return _canPost;
    }

    this.CreateEntry = function( entry, callback_success, callback_fail )
    {
        var entryXML = xmlJsonClass.json2xml( { entry: entry.masasEntry }, '' );
        var postData = '<?xml version="1.0" encoding="UTF-8"?>\n' + entryXML;

        if( "attachments" in entry && entry.attachments.length > 0 )
        {
            PostNewEntryWithAttachments( postData, entry.attachments, callback_success, callback_fail );
        }
        else
        {
            PostNewEntry( postData, 'application/atom+xml', callback_success, callback_fail );
        }
    };

    this.UpdateEntry = function( entry, callback_success, callback_fail )
    {
        var masasHub = this;
        var entryXML = xmlJsonClass.json2xml( { entry: entry.masasEntry }, '' );
        var postData = '<?xml version="1.0" encoding="UTF-8"?>\n' + entryXML;

        if( "attachments" in entry && entry.attachments.length > 0 )
        {
            PutUpdateWithAttachments( entry.editUrl, postData, entry.attachments, callback_success, callback_fail );
        }
        else
        {
            // Update the entry
            PutUpdateEntry( entry.editUrl, postData,
                function( msg, entryMsg ) {
                    // Success

                    // Do we need to remove the attachments?
                    if( entry.deleteAttachments ) {
                        masasHub.DeleteEntryAttachments( entry.editUrl,
                            function( msg ) {
                                if( callback_success && typeof( callback_success ) === "function" )
                                {
                                    callback_success( msg, entryMsg );
                                }
                            }, callback_fail );
                    }
                    else
                    {
                        // Attachments don't need to be deleted, return success...
                        if( callback_success && typeof( callback_success ) === "function" )
                        {
                            callback_success( msg, entryMsg );
                        }
                    }

                } , callback_fail );

        }
    };

    this.DeleteEntryAttachments = function( editUrl, callback_success, callback_fail )
    {
        DeleteEntry( editUrl + "/content", "", "", callback_success, callback_fail )
    }

    this.CreateAlert = function( alertMsg, callback_success, callback_fail )
    {
        var alertXML = xmlJsonClass.json2xml( { alert: alertMsg.masasAlert }, '' );
        var postData = '<?xml version="1.0" encoding="UTF-8"?>\n' + alertXML;

        PostNewEntry( postData, 'application/common-alerting-protocol+xml', callback_success, callback_fail );
    };

    this.UpdateAlert = function( alertMsg, callback_success, callback_fail )
    {
        var alertXML = xmlJsonClass.json2xml( { alert: alertMsg.masasAlert }, '' );
        var postData = '<?xml version="1.0" encoding="UTF-8"?>\n' + alertXML;

        PostEntry( "PUT", alertMsg.editUrl + "/content", "application/common-alerting-protocol+xml", postData, callback_success, callback_fail )
    };

    this.CreateOverlay = function( overlayMsg, callback_success, callback_fail )
    {
        var overlayJson = JSON.stringify( overlayMsg.masasOverlay );
        PostOverlay( "POST", App.dataStoreMgr.masasOverlayFeed._url, 'application/json', overlayJson, callback_success, callback_fail)
    };

    this.UpdateOverlay = function( overlayMsg, callback_success, callback_fail )
    {
        var overlayJson = JSON.stringify( overlayMsg.masasOverlay );
        PostOverlay( "PUT", overlayMsg.editUrl, 'application/json', overlayJson, callback_success, callback_fail)
    };

    this.DeleteOverlay = function( overlayUrl, callback_success, callback_fail )
    {
        PostOverlay( "DELETE", overlayUrl, 'application/json', null, callback_success, callback_fail)
    };

    this.RefreshUserData = function( async, callback_success, callback_fail )
    {
        var queryAsync = false;

        if( async !== undefined && async != null ) {
            queryAsync = async;
        }

        var masasObj = this;

        // Create the access url...
        var url = App.settings.currentHub.accessUrl + '/api/check_access/?query_secret=' + App.settings.currentHub.accessCode;
        var proxiedUrl = App.settings.appSettings.proxyUrl + encodeURIComponent( url + "&secret=" + App.settings.currentHub.accessCode );

        var request = $.ajax({
            type: 'GET',
            crossDomain: true,
            async: queryAsync,
            url: proxiedUrl,
            timeout: 300000
        });

        request.done( function( responseMsg ) {
            masasObj.userData = JSON.parse( responseMsg );

            // Update the Permissions lookup...
            updateCanPost();

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( masasObj.userData );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            var failureMsg = 'Failed to retrieve MASAS User data! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            // Update the Permissions lookup...
            updateCanPost();

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
    };

    /// Private methods

    var PostNewEntry = function( entryData, contentType, callback_success, callback_fail )
    {
        PostEntry( "POST", App.settings.currentHub.url + '/feed', contentType, entryData, callback_success, callback_fail)
    };

    var PostNewEntryWithAttachments = function( entryData, attachments, callback_success, callback_fail )
    {
        var newData = '';
        newData  = '--0.a.unique.value.0\r\n';
        newData += 'Content-Disposition: attachment; name="entry"; filename="entry.xml"\r\n';
        newData += 'Content-Type: application/atom+xml\r\n';
        newData += '\r\n';
        newData += entryData;
        newData += '\r\n';

        for( var i=0; i<attachments.length; i++ )
        {
            newData += '--0.a.unique.value.0\r\n';
            newData += 'Content-Disposition: attachment; name="attachment"; filename="' + attachments[i].title + '"\r\n';
            newData += 'Content-Type: ' + attachments[i].contentType + '\r\n';
            newData += 'Content-Description: ' + attachments[i].title + '\r\n';
            newData += 'Content-Transfer-Encoding: base64\r\n';
            newData += '\r\n';
            newData += attachments[i].base64;
            newData += '\r\n';
        }
        newData += '--0.a.unique.value.0--\r\n';

        PostNewEntry( newData, 'multipart/related; boundary=0.a.unique.value.0', callback_success, callback_fail );
    };

    var PutUpdateEntry = function( editUrl, postData, callback_success, callback_fail )
    {
        PostEntry( "PUT", editUrl, "application/atom+xml;type=entry", postData, callback_success, callback_fail )
    };

    var PutUpdateWithAttachments = function( editUrl, entryData, attachments, callback_success, callback_fail )
    {
        var attachmentData = '';
        var header = {};

        // Create the attachment payload...
        for( var i=0; i<attachments.length; i++ )
        {
            attachmentData += '--0.a.unique.value.0\r\n';
            attachmentData += 'Content-Disposition: attachment; name="attachment"; filename="' + attachments[i].title + '"\r\n';
            attachmentData += 'Content-Type: ' + attachments[i].contentType + '\r\n';
            attachmentData += 'Content-Description: ' + attachments[i].title + '\r\n';
            attachmentData += 'Content-Transfer-Encoding: base64\r\n';
            attachmentData += '\r\n';
            attachmentData += attachments[i].base64;
            attachmentData += '\r\n';
        }
        attachmentData += '--0.a.unique.value.0--\r\n';
        header = {
            'Content-Type': 'multipart/related; boundary=0.a.unique.value.0'
        }

        // Need to update the entry first...
        PutUpdateEntry( editUrl, entryData,
            function( msg, entry ) {
                // The entry was updated, now we need to update the attachments...
                PostEntry( "PUT", editUrl + "/content", header, attachmentData,
                    function( msg, attachmentEntry ) {
                        if( callback_success && typeof( callback_success ) === "function" )
                        {
                            callback_success( msg, attachmentEntry );
                        }
                    },
                    function( failureMsg) {
                        // The entry update failed... stop here...
                        if( callback_fail && typeof( callback_fail ) === "function" )
                        {
                            callback_fail( failureMsg );
                        }
                    }
                );
            },
            function( failureMsg ) {
                // The entry update failed... stop here...
                if( callback_fail && typeof( callback_fail ) === "function" )
                {
                    callback_fail( failureMsg );
                }
            }
        );
    };

    var PostEntry = function( type, url, contentType, entryData, callback_success, callback_fail )
    {
        var newUrl = App.settings.appSettings.proxyUrl + encodeURIComponent( url + "?secret=" + App.settings.currentHub.accessCode  );

        var header = {};

        if( typeof contentType === 'object' ) {
            header = contentType;
        }
        else {
            header = { 'Content-type' : contentType };
        }

        var request = $.ajax({
            type: type,
            crossDomain: true,
            url: newUrl,
            headers: header,
            data: entryData,
            processData: false,
            timeout: 300000
        });

        request.done( function(msg) {
            if( callback_success && typeof( callback_success ) === "function" )
            {
                var entryObject = {};
//                try {
//                    var jsonStr = xmlJsonClass.xml2json( mApp.CommonUtils.getInstance().parseXML( msg ), '  ' );
//                    entryObject = JSON.parse( jsonStr );
//                }
//                catch( e ) {
//                    entryObject.error = e;
//                }

                callback_success( msg, entryObject );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });

    };

    var PostOverlay = function( type, url, contentType, entryData, callback_success, callback_fail )
    {
        var newUrl = App.settings.appSettings.proxyUrl + encodeURIComponent( url + "?secret=" + App.settings.currentHub.accessCode  );

        var header = {};

        if( typeof contentType === 'object' ) {
            header = contentType;
        }
        else {
            header = { 'Content-type' : contentType };
        }

        var request = $.ajax({
            type: type,
            crossDomain: true,
            dataType: "text",
            url: newUrl,
            headers: header,
            data: entryData,
            timeout: 300000
        });

        request.done( function(msg) {
            if( callback_success && typeof( callback_success ) === "function" )
            {
                var overlayObject = {};
                try {
                    var jsonStr = msg;
                    overlayObject = JSON.parse( jsonStr );
                }
                catch( e ) {
                    overlayObject.error = e;
                }

                callback_success( msg, overlayObject );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });

    };

    var DeleteEntry = function( url, contentType, entryData, callback_success, callback_fail )
    {
        var newUrl = App.settings.appSettings.proxyUrl + encodeURIComponent( url + "?secret=" + App.settings.currentHub.accessCode  );

        var header = {};

        if( typeof contentType === 'object' ) {
            header = contentType;
        }
        else {
            header = { 'Content-type' : contentType };
        }

        var request = $.ajax({
            type: 'DELETE',
            dataType: "text",
            url: newUrl,
            headers: header,
            crossDomain: true,
            data: entryData,
            timeout: 300000
        });

        request.done( function(msg) {
            if( callback_success && typeof( callback_success ) === "function" )
            {
                var entryObject = {};
//                try {
//                    var jsonStr = xmlJsonClass.xml2json( mApp.CommonUtils.getInstance().parseXML( msg ), '  ' );
//                    entryObject = JSON.parse( jsonStr );
//                }
//                catch( e ) {
//                    entryObject.error = e;
//                }

                callback_success( msg, entryObject );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });

    };

    /// Private Methods
    var updateCanPost = function()
    {
        _canPost = false;

        if( that.userData.hubs !== undefined )
        {
            for( var hubCtr = 0; hubCtr < that.userData.hubs.length; hubCtr++ )
            {
                if( that.hubUrl === that.userData.hubs[hubCtr].url && that.userData.hubs[hubCtr].post === "Y" ) {
                    _canPost = true;
                    break;
                }
            }
        }
    }

};
