
mApp.MASASPublisher = function() {

    this.publishEntry = function( entryModel, callback_success, callback_failure )
    {
        var peData = {
            entry: entryModel,
            callback_entryPublished: callback_success,
            callback_entryFailure: callback_failure,
            attachmentStatus: []
        };

        // Load the attachments as base64.  On success, the entry will be sent.
        loadAttachmentsAsBASE64( peData, publishEntry_LAB64_Success, publishEntry_LAB64_Failure );
    };

    this.cancelEntry = function( entryModel, callback_success, callback_failure ) {
        // Set the expires to now!
        entryModel.masasEntry['age:expires'] = new Date();

        // Update the entry....
        App.masasHub.UpdateEntry( entryModel.masasEntry, callback_success, callback_failure );
    };

    this.publishAlert = function( alertModel, callback_success, callback_failure )
    {
        // Send the alert to MASAS...
        if( alertModel.state === 'new' ) {
            App.masasHub.CreateAlert( alertModel, callback_success, callback_failure );
        }
        else {
            App.masasHub.UpdateAlert( alertModel, callback_success, callback_failure );
        }
    };

    this.publishOverlay = function( overlayModel, callback_success, callback_failure )
    {
        // Send the overlay to the Hub...
        if( overlayModel.state === 'new' ) {
            App.masasHub.CreateOverlay( overlayModel, callback_success, callback_failure );
        }
        else {
            App.masasHub.UpdateOverlay( overlayModel, callback_success, callback_failure );
        }
    };

    this.deleteOverlay = function( overlayId, callback_success, callback_failure )
    {
        App.masasHub.DeleteOverlay(  App.dataStoreMgr.masasOverlayFeed._url + "/" + overlayId, callback_success, callback_failure );
    };

    /// Private methods....

    var publishEntry_LAB64_Success = function( result ) {
        // Send the entry to MASAS...
        if( result.entry.state === 'new' ) {
            App.masasHub.CreateEntry( result.entry, result.callback_entryPublished, result.callback_entryFailure );
        }
        else {
            App.masasHub.UpdateEntry( result.entry, result.callback_entryPublished, result.callback_entryFailure );
        }
    };

    var publishEntry_LAB64_Failure = function( result, errorMsg ) {
        result.callback_entryFailure( errorMsg );
    };

    // Convert each attachment to BASE64
    var loadAttachmentsAsBASE64 = function( data, callback_success, callback_failure )
    {
        var attachments = data.entry.attachments;

        if( attachments.length > 0 )
        {
            var i = 0;
            for( i = 0; i < attachments.length; i++ ) {
                // Setup the init values for the status codes...
                data.attachmentStatus.push( { statusCode: 0, statusMsg: '' } );
            }

            for( i = 0; i < attachments.length; i++ ) {
                // Load the Attachment as base64
                loadAttachmentAsBASE64( i, data, callback_success, callback_failure );
            }
        }
        else
        {
            // No attachments, we are done...
            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( data );
            }
        }
    };

    // LoadAttachmentAsBASE64
    var loadAttachmentAsBASE64 = function( index, data, callback_success, callback_failure )
    {
        var attachment = data.entry.attachments[index];
        var attStatus = data.attachmentStatus[index];

        // Create a FileReader...
        var reader = new FileReader();

        reader.onloadend = function( evt ) {
            // We only need the actual base64 data, so remove the added text before the data...
            var startPos = (evt.target.result).indexOf( ',' );

            attachment.base64 = (evt.target.result).substr( startPos + 1 );
            attStatus.statusCode = 1;
            attStatus.statusMsg = "File loaded as BASE64.";

            checkIfAllAttachmentsLoaded( data, callback_success, callback_failure );
        };

        reader.onerror = function( evt ) {
            // Failed!
            attStatus.statusCode = -1;
            attStatus.statusMsg = "Failed: Could not resolve the File.";

            CheckIfAllAttachmentsLoaded( data, callback_success, callback_failure );
        }

        // Read the file as BASE64...
        reader.readAsDataURL( attachment.file );

    };

    var checkIfAllAttachmentsLoaded = function( data, callback_success, callback_failure )
    {
        var attachments = data.entry.attachments;
        var attachmentStatus = data.attachmentStatus;

        var count = 0;
        var failed = false;
        var errorMsg = '';

        for( var i=0; i < attachmentStatus.length; i++ )
        {
            if( attachmentStatus[i].statusCode == -1 )
            {
                count++;
                failed = true;
                // TODO: this needs work!
                errorMsg += attachmentStatus[i].statusMsg + ' ';
            }
            else if( attachmentStatus[i].statusCode == 1 ) {
                count++;
            }
        }

        if( count == attachments.length )
        {
            // We are done...
            if( failed )
            {
                if( callback_failure && typeof( callback_failure ) === "function" ) {
                    callback_failure( data, errorMsg );
                }
            }
            else
            {
                if( callback_success && typeof( callback_success ) === "function" ) {
                    callback_success( data );
                }
            }
        }
    };

};
