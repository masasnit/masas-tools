/**
MASAS View Tool - Main View

Independent Joint Copyright (c) 2011-2015 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

mApp.MainView = Ext.extend( Ext.Viewport , {

    initComponent: function() {

        this.settingsWnd = null;
        this.timeFilterWnd = null;
        this.viewOverlayWnd = null;

        this._viewState = "MapView"; // MapView, SplitView, GridView
        this._viewFilterExtent = null;

        // Create the Map Panel...
        VIEW.initialize_map();

        this.mainToolBar = new mApp.MainToolBar( { mainView: this } );
        this.mapPanel = new mApp.MapPanel();

        // create the view components
        this.entryPanel = new VIEW.FeedGrid( {
            layer: VIEW.mapLayers.MASASLayer,
            storeId: 'EntryViewStore',
            title: 'Active'
        });

        this.draftPanel = new VIEW.FeedGrid( {
            layer: VIEW.mapLayers.MASASLayerDraft,
            title: 'Drafts',
            storeId: 'DraftViewStore',
            showDraftOnly: true
        });

        this.mainStatusBar = new mApp.MainStatusBar( { mainView: this } );

        this.tabsDataView = new Ext.TabPanel({
            mainView: this,
            deferredRender : false,
            tabPosition: 'bottom',
            activeTab: 0,
            items: [
                this.entryPanel,
                this.draftPanel
            ],

            listeners: {
                scope: this,
                'render': this._onTabsDataViewRender
            }

    });

        this.layout = 'border';
        this.items = [
            {
                region: "north",
                tbar: this.mainToolBar
            },
            this.mapPanel,
            {
                ref: 'pnlBottom',
                region: "south",
                layout: 'fit',
                items: [
                    this.tabsDataView
                ],
                bbar: this.mainStatusBar
            }
        ];

        mApp.MainView.superclass.initComponent.call( this );
    }

});

mApp.MainView.prototype._onTabsDataViewRender = function() {
    if( !App.dataStoreMgr.isDraftsEnable() )
    {
        this.tabsDataView.hideTabStripItem( this.draftPanel );
        this.tabsDataView.setActiveTab( this.entryPanel );
    }
};

mApp.MainView.prototype.getViewFilterExtent = function() {
    return this._viewFilterExtent;
};

mApp.MainView.prototype.updateViewFilterExtent = function() {

    // Only change the View Filter if the Map is visible...
    if( this._viewState != "GridView" )
    {
        var map = this.mapPanel.map;

        // Get the current extent...
        var currentExtent = map.getExtent();

        // Update the view filter geometry on the map...
        VIEW.mapLayers.loadFeed.removeAllFeatures();
        var viewFilterGeometry = currentExtent.toGeometry();
        var viewFilterFeature = new OpenLayers.Feature.Vector( viewFilterGeometry );
        VIEW.mapLayers.loadFeed.addFeatures( [viewFilterFeature] );

        // add geospatial bbox query to Hub request
        currentExtent.transform( map.getProjectionObject(), new OpenLayers.Projection('EPSG:4326') );
        this._viewFilterExtent = currentExtent;
    }
};

mApp.MainView.prototype.toggleMapView = function() {
    App.mainView.mapPanel.show();
    App.mainView.pnlBottom.setHeight(0);
    App.mainView.pnlBottom.doLayout();

    App.mainView.doLayout();
    this._viewState = "MapView";
};

mApp.MainView.prototype.toggleSplitView = function() {
    App.mainView.mapPanel.show();
    App.mainView.pnlBottom.setHeight(225);
    App.mainView.pnlBottom.doLayout();

    App.mainView.doLayout();
    this._viewState = "SplitView";
};

mApp.MainView.prototype.toggleGridView = function() {
    // NOTE: The _onSplitView() is called to "fix" an unknown issue;
    //       The grid takes over the whole screen!
    App.mainView.toggleSplitView();

    var view_height = App.mainView.getHeight() - App.mainView.mainToolBar.getHeight();

    App.mainView.mapPanel.hide();
    App.mainView.pnlBottom.setHeight( view_height );
    App.mainView.pnlBottom.doLayout();

    App.mainView.doLayout();
    this._viewState = "GridView";
};

mApp.MainView.prototype.openSettingsWindow = function() {
    if( this.settingsWnd == null  )
    {
        this.settingsWnd = new mApp.SettingsWindow();
        this.settingsWnd.on( 'close', this._onSettingsWindowClose, this );
        this.settingsWnd.show( this );
    }
};

mApp.MainView.prototype._onSettingsWindowClose = function( panel ) {
    this.settingsWnd.destroy();
    this.settingsWnd = null;
};

mApp.MainView.prototype.openTimeFilterWindow = function() {
    if( this.timeFilterWnd == null  )
    {
        this.timeFilterWnd = new mApp.TimeFilterWindow();
        this.timeFilterWnd.on( 'close', this._onTimeFilterWindowClose, this );
        this.timeFilterWnd.show( this );
    }
};

mApp.MainView.prototype._onTimeFilterWindowClose = function( panel ) {
    this.timeFilterWnd.destroy();
    this.timeFilterWnd = null;
};

mApp.MainView.prototype.editMarkup = function( markupType, markupId ) {
    var cleanId = markupId;
    var delimiterPos = markupId.lastIndexOf( ":" );
    if( delimiterPos > -1 ) {
        cleanId = markupId.substr( delimiterPos + 1 );
    }

    if( markupType === "entry" ) {
        var entryLink = App.dataStoreMgr.masasFeed._url + "/" + cleanId;
        this.editEntry( entryLink );
    }
    else if( markupType === "alert" ) {
        var entryLink = App.dataStoreMgr.masasFeed._url + "/" + cleanId;
        this.editAlert( entryLink );
    }
    else if( markupType === "overlay" ) {
        this.editOverlay( cleanId );
    }

};

mApp.MainView.prototype.editEntry = function( entryLink ) {
    var editEntryWnd = new mApp.EntryWindow( { entryLink: entryLink } );
    editEntryWnd.show();
};

mApp.MainView.prototype.editAlert = function( entryLink ) {
    var editAlertWnd = new mApp.AlertWindow( { entryLink: entryLink } );
    editAlertWnd.show();
};

mApp.MainView.prototype.editOverlay = function( overlayId ) {

    var editOverlayWnd = new mApp.OverlayWindow( { overlayId: overlayId } );
    editOverlayWnd.show();
};

mApp.MainView.prototype.showOverlayDetails = function( overlayId, updateAllowed ) {
    // NOTE: Only show one View Overlay window, and
    // make sure it's the latest requested overlay.
    if( this.viewOverlayWnd != null  )
    {
        this.viewOverlayWnd.loadOverlay( overlayId, updateAllowed );
    }
    else
    {
        this.viewOverlayWnd = new mApp.OverlayViewWindow( { overlayId: overlayId, updateAllowed: updateAllowed } );
        this.viewOverlayWnd.on( 'close', this._onViewOverlayWindowClose, this );
        this.viewOverlayWnd.show( this );
    }
};

mApp.MainView.prototype._onViewOverlayWindowClose = function( panel ) {
    this.viewOverlayWnd.destroy();
    this.viewOverlayWnd = null;
};

mApp.MainView.prototype.setDraftVisibility = function( visible ) {
    // Enable the data source...
    App.dataStoreMgr.enableDrafts( visible );

    // Show/Hide the panel...
    if( !visible ) {
        this.tabsDataView.hideTabStripItem( this.draftPanel );
        this.tabsDataView.setActiveTab( this.entryPanel );
    }
    else {
        this.tabsDataView.footer.removeClass( "x-tab-hide" );
        this.tabsDataView.unhideTabStripItem( this.draftPanel );
    }

    // Refresh...
    App.dataStoreMgr.refreshAll();
};
