mApp.MapPanel = Ext.extend( GeoExt.MapPanel, {

    initComponent: function() {

        // initializes the map, layers, controls, and toolbars
        //VIEW.initialize_map();

        var defaultViewExtent = OpenLayers.Bounds.fromString( App.settings.userSettings.defaultViewExtent );
        defaultViewExtent.transform( 'EPSG:4326', VIEW.mapOptions.projection );

        this.region = 'center';
        this.layout = "absolute";
        this.map = VIEW.map;
        this.extent = defaultViewExtent;

        mApp.MapPanel.superclass.initComponent.call( this );
    }

});

mApp.MapPanel.prototype.zoomToDefaultExtent = function() {
    var defaultViewExtent = OpenLayers.Bounds.fromString( App.settings.userSettings.defaultViewExtent );
    defaultViewExtent.transform( 'EPSG:4326', VIEW.mapOptions.projection );
    this.map.zoomToExtent( defaultViewExtent );
};
