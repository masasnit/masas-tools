mApp.AddStartPanel = Ext.extend( Ext.Container , {

    initComponent: function() {

        Ext.apply( this, {
            layout: {
                type: 'vbox',
                align: 'center'
            },
            defaults: {
                xtype: 'container',
                height: 120,
                width: '100%',
                layout: {
                    type: 'hbox',
                    padding: '10',
                    align: 'middle'
                }
            },
            items: [
                {
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'btnNewEntryWizardIcon'
                        },
                        {
                            xtype: 'container',
                            height: 80,
                            flex: 1,
                            html: '<H1>Create a new Entry</H1><br>' +
                                  '<p>Enter a little description of what this will do.</p>',
                            margins: '10'
                        }
                    ]
                },
                {
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'btnNewAlertWizardIcon'
                        },
                        {
                            xtype: 'container',
                            height: 80,
                            flex: 1,
                            html: '<H1>Create a new Alert (CAP-CP)</H1><br>' +
                                  '<p>Enter a little description of what this will do.</p>',
                            margins: '10'
                        }
                    ]
                },
                {
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'btnNewOverlayWizardIcon'
                        },
                        {
                            xtype: 'container',
                            height: 80,
                            flex: 1,
                            html: '<H1>Create a new Overlay</H1><br>' +
                                  '<p>Enter a little description of what this will do.</p>',
                            margins: '10'
                        }
                    ]
                }
            ],
            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            }
        });

        mApp.AddStartPanel.superclass.initComponent.call( this );
    }

});

mApp.AddStartPanel.prototype._populateForm = function() {

};

mApp.AddStartPanel.prototype._saveData = function() {

};

mApp.AddStartPanel.prototype._beforeShow = function() {
    this._populateForm();
};

mApp.AddStartPanel.prototype._beforeClose = function() {
};