mApp.AddWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        var startPanel = new mApp.AddStartPanel();

        Ext.apply( this, {
            title: "Add a new item",
            closeAction: 'close',
            plain: true,
            collapsible: true,
            maximizable: true,

            layout: 'card',
            activeItem: 0,
            width: 500,
            height: 500,

            items: [ startPanel ],

            buttons: [
                {
                    text: "Previous",
                    hidden: true,
                    scope: this,
                    handler: this._onPreviousClick
                },
                {
                    text: "Next",
                    hidden: true,
                    scope: this,
                    handler: this._onNextClick
                },
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                }
            ],

            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            }
        });

        mApp.AddWindow.superclass.initComponent.call( this );
    }

});

mApp.AddWindow.prototype._beforeShow = function() {
};

mApp.AddWindow.prototype._beforeClose = function() {
};

mApp.AddWindow.prototype._onPreviousClick = function() {

};

mApp.AddWindow.prototype._onNextClick = function() {

};

mApp.AddWindow.prototype._onCancelClick = function() {
    this.close();
};
