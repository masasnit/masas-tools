mApp.AlertGeneralPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        this._seviceSymbols = App.settings.appSettings.serviceSymbols;
        this._htmlPreviewTemplate = '<div class="symbolPreviewBox_48"><img class="symbolPreviewImage" src="{0}" alt="Symbol not available."></div>';
        var htmlPreview = this._htmlPreviewTemplate.replace( '{0}', this._seviceSymbols + "/other/large.png" );
        this._entrySymbolEn = "Other";
        this._entrySymbolFr = "Autres alertes";
        this._entrySymbolCat = "Other";
        this._entrySymbolVal = "other"


        Ext.apply( this, {
            title: 'General',
            padding: 10,
            autoScroll: true,
            border:false,
            defaultType: 'textfield',

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 85,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },

            listeners: {
                'beforeadd': this._onValidateRequired
            },

            items: [
                {
                    xtype: 'button',
                    ref: 'btnToggleFrench',
                    text: 'Enable FR-CA',
                    enableToggle: true,
                    style: '{ position: absolute; right: 10px; top: 6px; z-index:10; }',
                    scope: this,
                    handler: this._onToggleFrenchTab
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Status',
                    ref: 'cboStatus',
                    allowBlank: false,
                    editable: false,
                    width: 108,
                    forceSelection: true,
                    triggerAction: 'all',
                    mode: 'local',
                    displayField: 'name',
                    valueField: 'name',
                    value: 'Actual',
                    tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
                    store: new Ext.data.ArrayStore({
                        fields: ['name', 'tip'],
                        data: [
                            ['Actual', 'Actual Entries']
                        ]
                    })
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Event',
                    height: 56,
                    items: [
                        {
                            xtype: 'button',
                            text: 'Select...',
                            width: 56,
                            height: '100%',
                            scope: this,
                            handler: this._onIconSelectClicked
                        },
                        {
                            xtype: 'displayfield',
                            ref: '../imagePreview',
                            height: 56,
                            html: htmlPreview
                        },
                        {
                            xtype: 'container',
                            height: 48,
                            flex: 1,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    ref: '../../../lblSymbolDesc',
                                    value: 'Other'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Urgency',
                    ref: 'cboUrgency',
                    allowBlank: false,
                    width: '95%',
                    items: [
                        { boxLabel: 'Unknown', name: 'entry-urgency', inputValue: 'Unknown' },
                        { boxLabel: 'Past', name: 'entry-urgency', inputValue: 'Past', checked: true },
                        { boxLabel: 'Future', name: 'entry-urgency', inputValue: 'Future' },
                        { boxLabel: 'Expected', name: 'entry-urgency', inputValue: 'Expected' },
                        { boxLabel: 'Immediate', name: 'entry-urgency', inputValue: 'Immediate' }
                    ]
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Severity',
                    ref: 'cboSeverity',
                    allowBlank: false,
                    width: '95%',
                    items: [
                        { boxLabel: 'Unknown', name: 'entry-severity', inputValue: 'Unknown' },
                        { boxLabel: 'Minor', name: 'entry-severity', inputValue: 'Minor', checked: true },
                        { boxLabel: 'Moderate', name: 'entry-severity', inputValue: 'Moderate' },
                        { boxLabel: 'Severe', name: 'entry-severity', inputValue: 'Severe' },
                        { boxLabel: 'Extreme', name: 'entry-severity', inputValue: 'Extreme' }
                    ]
                },
                {
                    xtype: 'radiogroup',
                    fieldLabel: 'Certainty',
                    ref: 'cboCertainty',
                    allowBlank: false,
                    items: [
                        {boxLabel: 'Unknown', name: 'entry-certainty', inputValue: 'Unknown'},
                        {boxLabel: 'Unlikely', name: 'entry-certainty', inputValue: 'Unlikely', checked: true},
                        {boxLabel: 'Possible', name: 'entry-certainty', inputValue: 'Possible'},
                        {boxLabel: 'Likely', name: 'entry-certainty', inputValue: 'Likely'},
                        {boxLabel: 'Observed', name: 'entry-certainty', inputValue: 'Observed'}
                    ]
                },
                {
                    xtype: 'tabpanel',
                    ref: 'tabsLangContent',
                    plain: true,
                    activeTab: 0,
                    deferredRender: false,
                    style: '{margin-bottom:10px;}',
                    defaults:{
                        bodyStyle:'padding:10px',
                        autoHeight: true
                    },
                    items: [
                        {
                            title:'EN-CA',
                            ref: 'tabEngInfo',
                            layout:'form',
                            defaultType: 'textfield',

                            labelAlign: 'right',
                            labelWidth: 75,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },

                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },

                            items: [
                                {
                                    fieldLabel: 'Headline',
                                    ref: '../../txtHeadlineEn',
                                    width: '95%',
                                    minLengthText: 'A good headline should say what and where',
                                    maxLength: 160,
                                    maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
                                    blankText: 'MASAS requires a headline value',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Description',
                                    ref: '../../txtDescriptionEn',
                                    height: 80,
                                    width: '95%',
                                    allowBlank: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Instruction',
                                    ref: '../../txtInstructionEn',
                                    height: 80,
                                    allowBlank: true,
                                    width: '95%'
                                },
                                {
                                    fieldLabel: 'Contact',
                                    ref: '../../txtContactEn',
                                    allowBlank: true,
                                    width: '95%'
                                },
                                {
                                    fieldLabel: 'Web Link',
                                    ref: '../../txtWebLinkEn',
                                    width: '95%',
                                    allowBlank: true,
                                    vtype: 'url'
                                }
                            ]
                        },
                        {
                            title:'FR-CA',
                            ref: 'tabFrenchInfo',
                            layout:'form',
                            defaultType: 'textfield',
                            disabled: true,

                            labelAlign: 'right',
                            labelWidth: 75,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },

                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },

                            items: [
                                {
                                    fieldLabel: 'Headline',
                                    ref: '../../txtHeadlineFr',
                                    width: '95%',
                                    minLengthText: 'A good headline should say what and where',
                                    maxLength: 160,
                                    maxLengthText: 'CAP Headlines cannot be longer than 160 characters',
                                    blankText: 'MASAS requires a headline value',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Description',
                                    ref: '../../txtDescriptionFr',
                                    height: 80,
                                    width: '95%',
                                    allowBlank: true,
                                    flex: 1
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Instruction',
                                    ref: '../../txtInstructionFr',
                                    height: 80,
                                    allowBlank: true,
                                    width: '95%'
                                },
                                {
                                    fieldLabel: 'Contact',
                                    ref: '../../txtContactFr',
                                    allowBlank: true,
                                    width: '95%'
                                },
                                {
                                    fieldLabel: 'Web Link',
                                    ref: '../../txtWebLinkFr',
                                    width: '95%',
                                    allowBlank: true,
                                    vtype: 'url'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Effective',
                    height: 22,
                    anchor: '80%',
                    items: [
                        {
                            xtype: 'radio',
                            ref: '../rdoEffectiveNow',
                            itemId: 'rdoEffectiveNow',
                            boxLabel: 'Immediately',
                            checked: true,
                            name: 'frmEntryGeneral_grpEffective',
                            width: 114,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onEffectiveChanged
                            }
                        },
                        {
                            xtype: 'radio',
                            ref: '../rdoEffectiveAt',
                            itemId: 'rdoEffectiveAt',
                            boxLabel: 'At',
                            name: 'frmEntryGeneral_grpEffective',
                            width: 32,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onEffectiveChanged
                            }
                        },
                        {
                            xtype: 'datefield',
                            ref: '../dateEffective',
                            disabled: true,
                            width: 95,
                            format: 'Y-m-d',
                            minValue: new Date(),
                            pnlEntryGeneral: this,
                            validator: this._onValidateEffectiveDT
                        },
                        {
                            xtype: 'timefield',
                            ref: '../timeEffective',
                            disabled: true,
                            width: 70,
                            format: 'H:i',
                            increment: 60,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            validator: this._onValidateEffectiveDT
                        }
                    ]
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Expires',
                    height: 22,
                    anchor: '80%',
                    items: [
                        {
                            xtype: 'radio',
                            ref: '../rdoExpiresIn',
                            itemId: 'rdoExpiresIn',
                            boxLabel: 'In',
                            checked: true,
                            name: 'frmEntryGeneral_grpExpires',
                            width: 28,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onExpiresChanged
                            }
                        },
                        {
                            xtype: 'combo',
                            ref: '../cboExpiresInt',
                            allowBlank: true,
                            editable: false,
                            width: 75,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            value: 1,
                            valueField: 'interval',
                            tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
                            store: new Ext.data.ArrayStore({
                                fields: ['interval', 'name', 'tip'],
                                data: [ [null, 'N/A', 'Do not set expires'],
                                    [1, '1 Hour', 'Expires in 1 hour'],
                                    [6, '6 Hours', 'Expires in 6 hours'],
                                    [12, '12 Hours', 'Expires in 12 hours'],
                                    [24, '24 Hours', 'Expires in 1 day'],
                                    [48, '48 Hours', 'Expires in 2 days'] ]
                            })
                        },
                        {
                            xtype: 'radio',
                            ref: '../rdoExpiresOn',
                            itemId: 'rdoExpiresOn',
                            boxLabel: 'On',
                            name: 'frmEntryGeneral_grpExpires',
                            width: 38,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onExpiresChanged
                            }
                        },
                        {
                            xtype: 'datefield',
                            ref: '../dateExpires',
                            disabled: true,
                            width: 95,
                            format: 'Y-m-d',
                            minValue: new Date(),
                            pnlEntryGeneral: this,
                            validator: this._onValidateExpiresDT
                        },
                        {
                            xtype: 'timefield',
                            ref: '../timeExpires',
                            disabled: true,
                            width: 70,
                            format: 'H:i',
                            increment: 60,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            validator: this._onValidateExpiresDT
                        }
                    ]
                }
            ]
        });

        mApp.AlertGeneralPanel.superclass.initComponent.call( this );
    }

});

mApp.AlertGeneralPanel.prototype.resetPanel = function() {
    var form = this.getForm();
    form.reset();
};

mApp.AlertGeneralPanel.prototype._onIconSelectClicked = function() {
    this.selectWnd = new mApp.SymbolSelectWindow( { symbolLibrary: 'alerts' });
    this.selectWnd.curSelectionSymbolValue = this._entrySymbolVal;
    this.selectWnd.on( 'close', this._onSymbolSelectWindowClose, this );

    this.selectWnd.show();
};

mApp.AlertGeneralPanel.prototype._onSymbolSelectWindowClose = function( panel ) {
    if( panel.curSelectionNode !== undefined &&  panel.curSelectionSymbol !== '' ) {
        this._setSymbolByNode( panel.curSelectionNode );

        if( panel.curSelectionNode.attributes.text ) {
            this.txtHeadlineEn.setValue( panel.curSelectionNode.attributes.text );
        }

        if( panel.curSelectionNode.attributes.f_text ) {
            this.txtHeadlineFr.setValue( panel.curSelectionNode.attributes.f_text );
        }
    }

    this.selectWnd.destroy();
    this.selectWnd = null;
};

mApp.AlertGeneralPanel.prototype._setSymbolByNode = function( node ) {
    this._entrySymbolVal = node.attributes.value;
    var symbol_url = this._seviceSymbols + "/" + node.attributes.term + "/large.png";
    this.imagePreview.setValue( this._htmlPreviewTemplate.replace( '{0}', symbol_url ) );

    this._entrySymbolEn = node.attributes.text;
    this._entrySymbolFr = node.attributes.f_text;

    this.lblSymbolDesc.setValue( this._entrySymbolEn );
};

mApp.AlertGeneralPanel.prototype._setSymbolByValue = function( value ) {

    var term = "other";
    foundNode = App.dataStoreMgr.alertSymbolStore.findNodeByAttr( 'value', value );
    if( foundNode !== undefined && foundNode != null ) {
        this._entrySymbol = foundNode.term;
        this._entrySymbolEn = foundNode.text;
        this._entrySymbolFr = foundNode.f_text;
        this._entrySymbolCat = foundNode.category;
        this._entrySymbolVal = foundNode.value;
    }

    var symbol_url = this._seviceSymbols + "/" + term + "/large.png";
    this.imagePreview.setValue( this._htmlPreviewTemplate.replace( '{0}', symbol_url ) );

    this.lblSymbolDesc.setValue( this._entrySymbolEn );
};

mApp.AlertGeneralPanel.prototype._onEffectiveChanged = function( object, checked ) {
    if( checked )
    {
        var effectiveDT = this.pnlEntryGeneral.dateEffective;
        var effectiveTM = this.pnlEntryGeneral.timeEffective;

        if( object.itemId === "rdoEffectiveNow" ) {
            effectiveDT.setDisabled( true );
            effectiveTM.setDisabled( true );

            effectiveDT.clearInvalid();
            effectiveTM.clearInvalid();
        }
        else {
            effectiveDT.setDisabled( false );
            effectiveTM.setDisabled( false );
        }
    }
};

mApp.AlertGeneralPanel.prototype._onValidateEffectiveDT = function( val ) {
    if (!val || val.length === 0) {
        return false;
    }

    var effectiveDT = this.pnlEntryGeneral.dateEffective;
    var effectiveTM = this.pnlEntryGeneral.timeEffective;

    var isValid = this.pnlEntryGeneral._check_future_time( effectiveDT.getValue(), effectiveTM.getValue() );
    if( isValid ) {
        effectiveDT.clearInvalid();
        effectiveTM.clearInvalid();
    }

    return isValid;
};

mApp.AlertGeneralPanel.prototype._onExpiresChanged = function( object, checked ) {
    if( checked )
    {
        var expiresDT = this.pnlEntryGeneral.dateExpires;
        var expiresTM = this.pnlEntryGeneral.timeExpires;
        var expiresInt = this.pnlEntryGeneral.cboExpiresInt;

        if( object.itemId === "rdoExpiresIn" ) {
            expiresDT.setDisabled( true );
            expiresTM.setDisabled( true );
            expiresInt.setDisabled( false );

            expiresDT.clearInvalid();
            expiresTM.clearInvalid();
        }
        else {
            expiresDT.setDisabled( false );
            expiresTM.setDisabled( false );
            expiresInt.setDisabled( true );
        }
    }
};

mApp.AlertGeneralPanel.prototype._onValidateExpiresDT = function( val ) {
    if (!val || val.length === 0) {
        return false;
    }

    var expiresDT = this.pnlEntryGeneral.dateExpires;
    var expiresTM = this.pnlEntryGeneral.timeExpires;

    var isValid = this.pnlEntryGeneral._check_future_time( expiresDT.getValue(), expiresTM.getValue() );
    if( isValid ) {
        expiresDT.clearInvalid();
        expiresTM.clearInvalid();
    }

    return isValid;
};

mApp.AlertGeneralPanel.prototype._onColourComboSelectionChanged = function( combo, record, index ) {
    if( record.data.colour )
    {
        this.pnlEntryGeneral.txtColourContext.setValue( record.data.context );
        // set colour for location feature drawing
        // TODO: Change the colour of the point markup....
    }
    else
    {
        this.pnlEntryGeneral.txtColourContext.reset();
        // TODO: Change the colour of the point markup....
    }
};

mApp.AlertGeneralPanel.prototype._onToggleFrenchTab = function() {
    this.tabsLangContent.tabFrenchInfo.setDisabled( !this.btnToggleFrench.pressed );
    this.txtHeadlineFr.setDisabled( !this.btnToggleFrench.pressed );
    this.txtDescriptionFr.setDisabled( !this.btnToggleFrench.pressed );
    this.txtInstructionFr.setDisabled( !this.btnToggleFrench.pressed );
    this.txtContactFr.setDisabled( !this.btnToggleFrench.pressed );
    this.txtWebLinkFr.setDisabled( !this.btnToggleFrench.pressed );

    if( this.tabsLangContent.tabFrenchInfo.disabled ) {
        this.tabsLangContent.setActiveTab( this.tabsLangContent.tabEngInfo );
    }
};

mApp.AlertGeneralPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.AlertGeneralPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    this.cascade( function( item ) {
        if( item.isFormField && !item.disabled && !item.ownerCt.disabled )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

/**
 Validates that a time value is in the future, the format of the time should
 already have been checked using the existing Ext validator.  Assuming local
 time for comparison.

 @param {String} - a date value 'YYYY-MM-DD', optional
 @param {String} - a time value 'H:S'
 @return {Boolean} - {Boolean} for a valid time
 */
mApp.AlertGeneralPanel.prototype._check_future_time = function (d_val, t_val) {
    var current = new Date();
    // assumes that minValue set for the date input field has already validated
    // that the date is either at least for today or in the future.  Is optional
    // because the user may start entering the time first.
    if (d_val) {
        var c_date = current.format('Y-m-d');
        if (d_val !== c_date) {
            return true;
        }
    }
    // ignore empty values
    if (!t_val || t_val.length === 0) {
        return true;
    }
    // compare to current
    var c_hour = parseInt(current.format('H'), 10);
    var c_min = parseInt(current.format('i'), 10);
    var t_vals = t_val.split(':');
    var t_hour = parseInt(t_vals[0], 10);
    var t_min = parseInt(t_vals[1], 10);
    if (t_hour < c_hour) {
        return 'Must be a time value that is in the future';
    } else if (t_hour === c_hour) {
        if (t_min <= c_min) {
            return 'Must be a time value that is in the future';
        }
    }

    return true;
};

mApp.AlertGeneralPanel.prototype._populateForm = function( alert, infoEn, infoFr ) {
    // Utils object...
    var commonUtils = mApp.CommonUtils.getInstance();

    if( infoFr )
    {
        this.btnToggleFrench.toggle( true );
        this._onToggleFrenchTab();
    }

    // Status...
    this.cboStatus.setValue( alert.status );

    // Symbol...
    var symbolValue = "other";
    if( Array.isArray( infoEn.eventCode ) )
    {
        for( var i=0; i<infoEn.eventCode.length; i++ )
        {
            if( infoEn.eventCode[i].valueName.search( "profile:CAP-CP:Event" ) != -1 ) {
                symbolValue = infoEn.eventCode[i].value;
            }
        }
    }
    else {
        if( infoEn.eventCode.valueName.search( "profile:CAP-CP:Event" ) != -1 ) {
            symbolValue = infoEn.eventCode.value;
        }
    }
    this._setSymbolByValue( symbolValue );


    this.cboUrgency.setValue( infoEn.urgency );
    this.cboSeverity.setValue( infoEn.severity );
    this.cboCertainty.setValue( infoEn.certainty );

    var nowEpoch = new Date().getTime();

    // Effective Date/Time...
    if( infoEn.hasOwnProperty( 'effective' ) )
    {
        var effectiveDate = new Date( Date.parse( infoEn.effective ) );

        // if the entry has already become effective, don't fill in the previous
        // date as any update should need a new effective time
        var effectiveEpoch = effectiveDate.format('U') * 1000;
        if( effectiveEpoch >= nowEpoch )
        {
            this.rdoEffectiveAt.setValue( true );
            this.dateEffective.setValue( effectiveDate );
            this.timeEffective.setValue( effectiveDate );
        }
    }

    // expires Date/Time...
    if( infoEn.hasOwnProperty( 'expires' ) )
    {
        var expiresDate = new Date( Date.parse( infoEn.expires ) );

        var expiresEpoch = expiresDate.format('U') * 1000;
        if( expiresEpoch >= nowEpoch )
        {
            this.rdoExpiresOn.setValue( true );
            this.dateExpires.setValue( expiresDate );
            this.timeExpires.setValue( expiresDate );
        }
    }

    // Headline...
    this.txtHeadlineEn.setValue( commonUtils.decodeXML( infoEn.headline ) );
    if( infoFr ) {
        this.txtHeadlineFr.setValue( commonUtils.decodeXML( infoFr.headline ) );
    }

    // Description...
    if( infoEn.hasOwnProperty( 'description' ) ) {
        this.txtDescriptionEn.setValue( commonUtils.decodeXML( infoEn.description ) );
    }
    if( infoFr && infoFr.hasOwnProperty( 'description' ) ) {
        this.txtDescriptionFr.setValue( commonUtils.decodeXML( infoFr.description ) );
    }

    // Instruction...
    if( infoEn.hasOwnProperty( 'instruction' ) ) {
        this.txtInstructionEn.setValue( commonUtils.decodeXML( infoEn.instruction ) );
    }
    if( infoFr && infoFr.hasOwnProperty( 'instruction' ) ) {
        this.txtInstructionFr.setValue( commonUtils.decodeXML( infoFr.instruction ) );
    }

    // Contact...
    if( infoEn.hasOwnProperty( 'contact' ) ) {
        this.txtContactEn.setValue( commonUtils.decodeXML( infoEn.contact ) );
    }
    if( infoFr && infoFr.hasOwnProperty( 'contact' ) ) {
        this.txtContactFr.setValue( commonUtils.decodeXML( infoFr.contact ) );
    }

    // Web...
    if( infoEn.hasOwnProperty( 'web' ) ) {
        this.txtWebLinkEn.setValue( commonUtils.decodeXML( infoEn.web ) );
    }
    if( infoFr && infoFr.hasOwnProperty( 'web' ) ) {
        this.txtWebLinkEn.setValue( commonUtils.decodeXML( infoFr.web ) );
    }

};

