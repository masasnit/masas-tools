mApp.AlertLocationPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        var locationPanel = this;

        Ext.apply( this, {
            title: 'Location',
            padding: 10,
            autoScroll: true,
            border:false,

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 90,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },

            scope: this,
            listeners: {
                'beforeadd': this._onValidateRequired
            },

            items: [
                {
                    xtype: 'panel',
                    border: false,
                    autoHeight: true,
                    style: '{ margin-bottom: 10px; }',
                    items: [
                        {
                            xtype: 'displayfield',
                            bodyStyle: 'padding: 10px',
                            value: 'Describe the area of the alert.  E.g. North west areas of the Town of ________'
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    fieldLabel: 'Description',
                    ref: 'txtAreaDescription',
                    anchor: '95%',
                    allowBlank: false
                },
                {
                    xtype: 'panel',
                    border: false,
                    autoHeight: true,
                    style: '{ margin-bottom: 10px; }',
                    items: [
                        {
                            xtype: 'displayfield',
                            bodyStyle: 'padding: 10px',
                            value: 'Draw the polygon or circle of the area the alert pertains to.'
                        }
                    ]
                },
                {
                    xtype: 'MASAS_locationPanel',
                    ref: "locationArea",
                    title: 'Alert Area',
                    header: true,
                    padding: 10,
                    style: '{ margin-bottom: 10px; }',
                    collapsible: true,
                    map: App.mainView.mapPanel.map,
                    mainWindow: this.alertWindow,
                    geometryTitle: 'Alert Geometry',
                    geometry: [ 'Polygon', 'Circle' ],
                    fillColor: '#ff0000'
                },
                {
                    xtype: 'panel',
                    border: false,
                    autoHeight: true,
                    style: '{ margin-bottom: 10px; }',
                    items: [
                        {
                            xtype: 'displayfield',
                            bodyStyle: 'padding: 10px',
                            value: 'Use this feature to include the location of the incident / event, if the location is not the same as the alert area. </br>' +
                                   'E.g. Location of an industrial fire, within the large alert area.'
                        }
                    ]
                },
                {
                    xtype: 'MASAS_locationPanel',
                    ref: "locationEvent",
                    title: 'Incident/Event Location (OPTIONAL)',
                    padding: 10,
                    header: true,
                    collapsible: true,
                    collapsed: true,
                    style: '{ margin-bottom: 10px; }',
                    map: App.mainView.mapPanel.map,
                    mainWindow: this.alertWindow,
                    geometryTitle: 'Incident / Event Geometry',
                    geometry: [ 'None', 'Point', 'Line', 'Polygon', 'Circle' ],
                    fillColor: '#000000'
                }
            ]
        });

        mApp.AlertLocationPanel.superclass.initComponent.call( this );
    }

});

mApp.AlertLocationPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.AlertLocationPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    if( !this.txtAreaDescription.validate() ) {
        foundInvalidItem = true;
        messages.push( title + " - " + this.txtAreaDescription.fieldLabel );
    }

    // Validate the area location...
    this.locationArea.pnlGeometry.cascade( function( item ) {
        if( item.isFormField  && !item.hidden && !item.ownerCt.hidden )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    // Validate the event location...
    this.locationEvent.pnlGeometry.cascade( function( item ) {
        if( item.isFormField  && !item.hidden && !item.ownerCt.hidden )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

mApp.AlertLocationPanel.prototype._populateForm = function( alert, infoEn, infoFr ) {
    var iCtr = 0;
    var points = [];
    var values = null;
    var pairedValues = null;
    var newPt = null;
    var geoType = "none";
    var geoData = undefined;

    // Update the Incident/Event Location...
    if( infoEn.hasOwnProperty( 'parameter' ) )
    {
        // Just grab the first CAPAN Event Locoation
        var eventLocation = null;
        if( Array.isArray( infoEn.parameter ) ) {
            for( var i = 0; i < infoEn.parameter.length; i++ ) {
                if( infoEn.parameter[i].valueName.search( "layer:CAPAN:eventLocation" ) != -1 ) {
                    eventLocation = infoEn.parameter[i];
                    break;
                }
            }
        }
        else {
            if( infoEn.parameter.valueName.search( "layer:CAPAN:eventLocation" ) != -1 ) {
                eventLocation = infoEn.parameter;
            }
        }

        geoData = eventLocation.value;
        if( eventLocation.valueName === "layer:CAPAN:eventLocation:point" ) {
            geoType = "point";
        }
        else if( eventLocation.valueName === "layer:CAPAN:eventLocation:line" ) {
            geoType = "line";
        }
        else if( eventLocation.valueName === "layer:CAPAN:eventLocation:polygon" ) {
            geoType = "polygon";
        }
        else if( eventLocation.valueName === "layer:CAPAN:eventLocation:circle" )
        {
            pairedValues = eventLocation.value.split( ' ' );
            var radius = parseFloat( pairedValues[1] ) * 1000; // NOTE: ALERT/CAPAN Circle radius is in KM

            geoData = pairedValues[0] + radius.toString();
            geoType = "circle";
        }
    }
    // Set the location event if one is available...
    this.locationEvent.setFromWGS84( geoType, geoData );

    // Update the Alert Area...
    var firstArea = null;
    if( Array.isArray( infoEn.area ) ) {
        // Grab the first one only...
        firstArea = infoEn.area[0];
    }
    else {
        firstArea = infoEn.area;
    }
    this.txtAreaDescription.setValue( firstArea.areaDesc );

    if( firstArea.hasOwnProperty( "polygon" ) ) {
        geoType = "polygon";
        geoData = firstArea.polygon;
    }
    else if( firstArea.hasOwnProperty( "circle" ) ) {
        geoType = "circle";
        geoData = firstArea.circle;
    }

    this.locationArea.setFromWGS84( geoType, geoData );

    this.doLayout();
};

mApp.AlertLocationPanel.prototype.populateAreas = function( alert ) {
    // Grab the info blocks....
    var infoEng = alert.info[0];
    var infoFra = alert.info[1];

    // Add the CAPAN Event Location information...
    var geometry = this.locationEvent.getGeometry( { meters: false } );
    if( geometry.type !== "none" )
    {
        infoEng.parameter.valueName = 'layer:CAPAN:eventLocation:' + geometry.type;
        infoFra.parameter.valueName = 'layer:CAPAN:eventLocation:' + geometry.type;
        infoEng.parameter.value = geometry.location;
        infoFra.parameter.value = geometry.location;
    }
    else
    {
        delete infoEng.parameter;
        delete infoFra.parameter;
    }

    // Area blocks...
    var en_area_blocks = [];
    var fr_area_blocks = [];

    // Get the alert area...
    geometry = this.locationArea.getGeometry( { meters: false } );

    if( geometry.type === "polygon" )
    {
        en_area_blocks.push({
            areaDesc: this.txtAreaDescription.getValue(),
            polygon: geometry.location
        });
    }
    else if( geometry.type === "circle" )
    {
        en_area_blocks.push({
            areaDesc: this.txtAreaDescription.getValue(),
            circle: geometry.location
        });
    }

    // check for single areas which don't get set as an array of objects
    if( en_area_blocks.length === 1 ) {
        infoEng.area = en_area_blocks[0];
    }
    else {
        infoEng.area = en_area_blocks;
    }
    if( fr_area_blocks.length === 1 ) {
        infoFra.area = fr_area_blocks[0];
    } else {
        infoFra.area = fr_area_blocks;
    }

};