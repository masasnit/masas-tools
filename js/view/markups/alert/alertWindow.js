mApp.AlertWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        // Map accessor...
        this.map = App.mainView.mapPanel.map;

        // Original Entry...
        this._origEntry = null;
        // Original Alert...
        this._origAlert = null;

        this.generalPanel = new mApp.AlertGeneralPanel( { title: 'General', alertWindow: this } );
        this.locationPanel = new mApp.AlertLocationPanel( { title: 'Location', alertWindow: this } );

        Ext.apply( this, {

            title: "Create a new Alert",
            layout: 'fit',
            width: 600,
            height: 500,
            minWidth: 300,
            minHeight: 200,
            closeAction: 'close',
            plain: true,

            collapsible: true,
            maximizable: true,

            items: {
                xtype: 'tabpanel',
                activeTab: 0,
                deferredRender: false,

                items: [
                    this.generalPanel,
                    this.locationPanel
                ]
            },

            buttons: [
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                },
                {
                    text: "Submit",
                    disabled: false,
                    scope: this,
                    handler: this._onSendClick
                }
            ],

            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            }
        });

        mApp.AlertWindow.superclass.initComponent.call( this );
    }

});

mApp.AlertWindow.prototype._beforeShow = function() {
    if( 'entryLink' in this )
    {
        var alertLink = undefined;
        this._origEntry = this._loadEntryFromHub( this.entryLink );

        // Get the alert link...
        for( var i = 0; i < this._origEntry.link.length; i++ )
        {
            var curLink = this._origEntry.link[i];
            if( curLink['@rel'] === 'enclosure' && curLink['@type'] === 'application/common-alerting-protocol+xml' )
            {
                alertLink = curLink['@href'];
                break;
            }
        }

        if( alertLink === undefined )
        {
            alert( 'The selected Alert could not be found!' );
            this.close();
        }
        else {
            this._origAlert = this._loadAlertFromHub( alertLink );
            this.setTitle( "Update Alert: " + this._origAlert.identifier );
            this._populateForm( this._origAlert );
        }
    }
};

mApp.AlertWindow.prototype._beforeClose = function() {
};

mApp.AlertWindow.prototype._onSendClick = function() {
    var generalValid = this.generalPanel.validate();
    var locationValid = this.locationPanel.validate();

    if( generalValid.valid && locationValid.valid )
    {
        var alertWindow = this;
        this.myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});

        this.disable();

        this.myMask.show();

        // Gather the data...
        var capAlert = this._generateAlert();

        var alertMsg = {
            masasAlert: capAlert,
            state: 'new',
            editUrl: ''
        };

        if( this._origEntry != null ) {
            alertMsg.state = 'update';
            alertMsg.editUrl = this.entryLink;
        }

        // Publish...
        App.masasPublisher.publishAlert(
            alertMsg,
            function( msg, entry ) {
                // success...
                alertWindow.myMask.hide();
                alertWindow.close();
            },
            function( failureMsg ) {
                alertWindow.enable();
                // Failed...
                alertWindow.myMask.hide();
                Ext.Msg.alert( 'Publish failed', 'Publish failed: <br>' +  failureMsg );
            }
        );
    }
    else
    {
        var strMessages = generalValid.messages.join( '</br>' );
        strMessages += '</br>' + locationValid.messages.join( '</br>' );

        var message = "Some required fields are missing:</br></br>" + strMessages + "</br>";

        Ext.Msg.alert( 'Error', message );
    }
};

mApp.AlertWindow.prototype._onCancelClick = function() {
    this.close();
};

mApp.AlertWindow.prototype._populateForm = function( alert ) {

    // Populate the info blocks....
    var infoEn = null;
    var infoFr = null;

    if( Array.isArray( alert.info ) )
    {
        for( var i = 0; i< alert.info.length; i++ )
        {
            if( alert.info[i].language.search( "en" ) !== -1 ) {
                infoEn = alert.info[i];
            }

            if( alert.info[i].language.search( "fr" ) !== -1 ) {
                infoFr = alert.info[i];
            }
        }
    }
    else {
        infoEn = alert.info;
    }

    this.generalPanel._populateForm( alert, infoEn, infoFr );
    this.locationPanel._populateForm( alert, infoEn, infoFr );
};

mApp.AlertWindow.prototype._getBasePubAlert = function() {
    var basePubAlert = {
        '@xmlns': 'urn:oasis:names:tc:emergency:cap:1.1',
        // customize identifier for each new alert
        identifier: 'TEST-1',
        // customize the sender for this user
        sender: 'user@example.com',
        sent: 'sent',
        status: 'status',
        msgType: 'Alert',
        scope: 'Public',
        code: 'profile:CAP-CP:0.3',
        info:[{
            language: 'en-CA',
            category: 'category',
            event: 'event',
            urgency: 'urgency',
            severity: 'severity',
            certainty: 'certainty',
            eventCode: {valueName: 'profile:CAP-CP:Event:0.3', value: 'value'},
            effective: 'effective',
            expires: 'expires',
            // customize the senderName for this user
            senderName: 'Example User',
            headline: 'headline',
            description: 'description',
            instruction: 'instruction',
            web: 'web',
            contact: 'contact',
            parameter: {},
            area: null
        }, {
            language: 'fr-CA',
            category: 'category',
            event: 'event',
            urgency: 'urgency',
            severity: 'severity',
            certainty: 'certainty',
            eventCode: {valueName: 'profile:CAP-CP:Event:0.3', value: 'value'},
            effective: 'effective',
            expires: 'expires',
            // customize the senderName for this user
            senderName: 'Example User',
            headline: 'headline',
            description: 'description',
            instruction: 'instruction',
            web: 'web',
            contact: 'contact',
            parameter: {},
            area: null
        }]
    };

    return basePubAlert;
}

mApp.AlertWindow.prototype._generateAlert = function () {
    // Utils object...
    var commonUtils = mApp.CommonUtils.getInstance();

    // initial Alert data...
    var alert = this._getBasePubAlert();

    if( this._origAlert != null ) {
        // We have an update...
        alert.msgType = 'Update';

        var newReference = this._origAlert.sender + ',' + this._origAlert.identifier + ',' + this._origAlert.sent;

        if( this._origAlert.references ) {
            alert.references = this._origAlert.references + ' ' + newReference;
        }
        else {
            alert.references = newReference;
        }
    }

    // Identifier:
    // Use the current time as the identifier; this value doesn't have to be unique.
    // An alert is identified by {sender,identifier,sent} as per the CAP spec.
    alert.identifier = "MASAS-" + new Date().getTime();

    // Sender...
    alert.sender = App.masasHub.userData.uri;

    // Sent time...
    var sentTimeStr = new Date().toISOString();
    var dotIndex = sentTimeStr.lastIndexOf( '.' );
    alert.sent = sentTimeStr.substr( 0, dotIndex ) + '-00:00';

    // Status...
    var catStatus = this.generalPanel.cboStatus.getValue();
    alert.status = catStatus;

    // Populate the info blocks....
    var infoEng = alert.info[0];
    var infoFra = alert.info[1];

    // Sender Name...
    infoEng.senderName = App.masasHub.userData.name;
    infoFra.senderName = App.masasHub.userData.name;

    infoEng.event = this.generalPanel._entrySymbolEn;
    infoFra.event = this.generalPanel._entrySymbolFr

    infoEng.category = this.generalPanel._entrySymbolCat;
    infoFra.category = this.generalPanel._entrySymbolCat;
    infoEng.eventCode.value = this.generalPanel._entrySymbolVal;
    infoFra.eventCode.value = this.generalPanel._entrySymbolVal;

    infoEng.urgency =  this.generalPanel.cboUrgency.getValue().getGroupValue();
    infoEng.severity = this.generalPanel.cboSeverity.getValue().getGroupValue();
    infoEng.certainty = this.generalPanel.cboCertainty.getValue().getGroupValue();
    infoFra.urgency = this.generalPanel.cboUrgency.getValue().getGroupValue();
    infoFra.severity = this.generalPanel.cboSeverity.getValue().getGroupValue();
    infoFra.certainty = this.generalPanel.cboCertainty.getValue().getGroupValue();

    // Effective Date/Time...
    if( this.generalPanel.rdoEffectiveNow.checked == false )
    {
        var effectiveDT = this.generalPanel.dateEffective.getValue();
        var effectiveTM = this.generalPanel.timeEffective.getValue();

        var strTime = effectiveTM.split(":");
        var effectiveDate = new Date( effectiveDT.getFullYear(), effectiveDT.getMonth(), effectiveDT.getDate(), strTime[0], strTime[1] );
        var effectiveDateStr = effectiveDate.toISOString();
        var dotIndex = effectiveDateStr.lastIndexOf( '.' );
        effectiveDateStr = effectiveDateStr.substr( 0, dotIndex ) + '-00:00';

        infoEng.effective = effectiveDateStr;
        infoFra.effective = effectiveDateStr;
    }
    else
    {
        // effective not in use, remove it...
        delete infoEng.effective;
        delete infoFra.effective;
    }

    // Expires Date/Time...
    var expiresDate = null;

    if( this.generalPanel.rdoExpiresIn.checked )
    {
        var expiresInt = this.generalPanel.cboExpiresInt.getValue();
        expiresDate = new Date();
        expiresDate.setHours( expiresDate.getHours() + expiresInt );
    }
    else
    {
        var expiresDT = this.generalPanel.dateExpires.getValue();
        var expiresTM = this.generalPanel.timeExpires.getValue();

        var strTime = expiresTM.split(":");
        expiresDate = new Date( expiresDT.getFullYear(), expiresDT.getMonth(), expiresDT.getDate(), strTime[0], strTime[1] );
    }

    var expiresStr = expiresDate.toISOString();
    var dotIndex = expiresStr.lastIndexOf( '.' );
    expiresStr = expiresStr.substr( 0, dotIndex ) + '-00:00';
    infoEng.expires = expiresStr;
    infoFra.expires = expiresStr;

    // Headline...
    infoEng.headline = commonUtils.encodeXML( this.generalPanel.txtHeadlineEn.getValue() );
    infoFra.headline = commonUtils.encodeXML( this.generalPanel.txtHeadlineFr.getValue() );

    // Description...
    this._helperAssignOrRemove( this.generalPanel.txtDescriptionEn.getValue(), infoEng, 'description' );
    this._helperAssignOrRemove( this.generalPanel.txtDescriptionFr.getValue(), infoFra, 'description' );

    // Instruction...
    this._helperAssignOrRemove( this.generalPanel.txtInstructionEn.getValue(), infoEng, 'instruction' );
    this._helperAssignOrRemove( this.generalPanel.txtInstructionFr.getValue(), infoFra, 'instruction' );

    // Contact...
    this._helperAssignOrRemove( this.generalPanel.txtContactEn.getValue(), infoEng, 'contact' );
    this._helperAssignOrRemove( this.generalPanel.txtContactFr.getValue(), infoFra, 'contact' );

    // Web...
    this._helperAssignOrRemove( this.generalPanel.txtWebLinkEn.getValue(), infoEng, 'web' );
    this._helperAssignOrRemove( this.generalPanel.txtWebLinkFr.getValue(), infoFra, 'web' );

    // Areas...
    this.locationPanel.populateAreas( alert );

    // Remove the french info block if it's not needed...
    if( !this.generalPanel.btnToggleFrench.pressed ) {
        alert.info = infoEng;
    }

    return alert;
};

mApp.AlertWindow.prototype._helperAssignOrRemove = function( value, target, field ) {
    if( value ) {
        target[field] = mApp.CommonUtils.getInstance().encodeXML( value );
    }
    else {
        delete target[field];
    }
};

mApp.AlertWindow.prototype._loadEntryFromHub = function( entryLink ) {
    var entry = undefined;

    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();

    // sync loading these urls
    var entryRequest = new OpenLayers.Request.GET({
        url: entryLink,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': App.settings.currentHub.accessCode,
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: App.settings.appSettings.proxyUrl
    });

    if (entryRequest.status !== 200) {
        mask.hide();
        alert('Unable to load Entry.');
        return entry;
    }
    var xml_doc = mApp.CommonUtils.getInstance().parseXML(entryRequest.responseText);
    if (!xml_doc) {
        mask.hide();
        alert('Unable to parse Entry.');
        return entry;
    }

    var jsonEntry = xmlJsonClass.xml2json(xml_doc, '  ');
    try {
        var jsonObj = JSON.parse( jsonEntry );
        entry = jsonObj.entry;
    } catch (err) {
        mask.hide();
        alert('Unable to parse Atom Entry.');
        return entry;
    }

    // advance to the next card
    mask.hide();

    return entry;
};

mApp.AlertWindow.prototype._loadAlertFromHub = function( alertLink ) {
    var alert = undefined;
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();

    // sync loading these urls
    var alertRequest = new OpenLayers.Request.GET({
        url: alertLink,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': App.settings.currentHub.accessCode,
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: App.settings.appSettings.proxyUrl
    });

    if (alertRequest.status !== 200) {
        mask.hide();
        alert('Unable to load Alert.');
        return alert;
    }
    var xml_doc = mApp.CommonUtils.getInstance().parseXML(alertRequest.responseText);
    if (!xml_doc) {
        mask.hide();
        alert('Unable to parse Alert.');
        return alert;
    }

    var jsonAlert = xmlJsonClass.xml2json(xml_doc, '  ');
    try {
        var jsonObj = JSON.parse( jsonAlert );
        alert = jsonObj.alert;
    } catch (err) {
        mask.hide();
        alert('Unable to parse Alert.');
        return alert;
    }

    // Done...
    mask.hide();

    return alert;
};
