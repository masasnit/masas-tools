mApp.LocationPanel = Ext.extend( Ext.Panel , {

    initComponent: function() {

        var locationPanel = this;
        this._activeControl = null;
        this._currentFeature = null;

        // Our drawing components...
        this.drawLayer = null;

        // Create the Map Layer we will need to use...
        this._setupMapLayers();

        // Create and setup the Map Controllers we will need...
        this._setupMapControllers();

        this.drawLayer.events.on( {
            'featuremodified': function( object ) { locationPanel._onFeatureModified( locationPanel, object ); },
            'afterfeaturemodified': function( object ) { locationPanel._onAfterFeatureModified( locationPanel, object ); }
        });

        this.geometryStore = new Ext.data.ArrayStore({
            fields: ['type', 'name', 'tip'],
            idIndex: 0 // id for each record will be the first element
        });

        Ext.applyIf( this, {
            autoScroll: true,

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 90,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },
            geometryTitle: 'Geometry',
            geometry: [ 'Point', 'Line', 'Polygon', 'Circle' ],
            fillColor: '#ffffff'
        });

        this.updateGeometryStore( this.geometry, this.geometryStore );

        Ext.apply( this, {
            layout: 'form',

            listeners: {
                scope: this,
                'beforeadd': this._onValidateRequired,
                show: this._onShow,
                destroy: this._onDestroy
            },

            items: [
                {
                    xtype: 'panel',
                    border: false,
                    autoHeight: true,
                    style: '{ margin-bottom: 10px; }',
                    items: [
                        {
                            xtype: 'displayfield',
                            ref: '../txtInstructionField',
                            bodyStyle: 'padding: 10px',
                            value: ''
                        }
                    ]
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Geometry',
                    ref: 'pnlButtons',
                    anchor: '100%',
                    style: '{ margin-bottom: 10px; }',
                    plain: true,
                    border: false,
                    allowBlank: true,
                    items: [
                        {
                            xtype: 'combo',
                            ref: '../cboGeometry',
                            width: 150,
                            editable: false,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            valueField: 'type',
                            tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
                            store: this.geometryStore,
                            locationPanel: this,
                            listeners : {
                                'render': this._onCboRender,
                                'select': this._onCboGeometrySelect
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'New Drawing',
                            ref: '../btnStartDrawing',
                            scope: this,
                            handler: this._onBtnStartDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Stop Drawing',
                            ref: '../btnStopDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnStopDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Edit Drawing',
                            ref: '../btnEditDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnEditDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Clear Drawing',
                            ref: '../btnClearDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnClearDrawingClick
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    layout: 'card',
                    ref: 'pnlGeometry',
                    title: this.geometryTitle,
                    defaults: {
                        bodyStyle: 'padding:10px'
                    },
                    items: [
                        {
                            xtype: 'panel',
                            ref: 'pnlNone',
                            border: false,
                            layout: 'form',
                            autoHeight: true,
                            instructions: {
                                'new': 'Select a geometry to enable an Incident / Event Location.',
                                'edit': 'N/A'
                            }
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlPoint',
                            border: false,
                            layout: 'form',
                            autoHeight: true,
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to draw a new point.',
                                'edit': 'Click and move the selected point the the new location.'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtPointLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtPointLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlLine',
                            border: false,
                            layout: 'form',
                            autoHeight: true,
                            anchor: '100% 100%',
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to start and add additional points.  Double-click the last point to stop drawing.',
                                'edit': 'Click and move the selected vertices to their new location.'
                            },
                            items: [
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Line',
                                    ref: 'txtLineGeometry',
                                    height: 150,
                                    anchor: '95%',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlPolygon',
                            border: false,
                            layout: 'form',
                            autoHeight: true,
                            anchor: '100% 100%',
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to start and add additional points.  Double-click the last point to stop drawing.',
                                'edit': 'Click and move the selected vertices to their new location.'
                            },
                            items: [
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Polygon',
                                    ref: 'txtPolygonGeometry',
                                    height: 150,
                                    anchor: '95%',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlCircle',
                            border: false,
                            layout: 'form',
                            autoHeight: true,
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map and drag the mouse to create the circle.  The location of the click will be the center of the circle.',
                                'edit': 'Click and move the center to change to location.  Click and move the Radius handle to change the size.'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtCircleLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtCircleLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Radius (km)',
                                    ref: 'txtCircleRadius',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: '0.0',
                                    regex: /^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        mApp.LocationPanel.superclass.initComponent.call( this );

        this._updateInstructions( 'clear' );
    },

    updateGeometryStore: function( geoTypes, dataStore ) {
        var lookup = {
            'none': [ 'none', 'None', 'Disable this geometry' ],
            'point': [ 'point', 'Point', 'Draw a point' ],
            'line': [ 'line', 'Line', 'Draw a line' ],
            'polygon': [ 'polygon', 'Polygon', 'Draw a polygon' ],
            'circle': [ 'circle', 'Circle', 'Draw a circle' ]
        };

        var geoData = { type: '', name: '', tip: '' };
        var newData = [];
        var geoCtr;
        var geoType;

        for( geoCtr = 0; geoCtr < geoTypes.length; geoCtr++ )
        {
            geoType = geoTypes[geoCtr].toLowerCase();
            if( geoType in lookup ) {
                geoData = lookup[geoType];
                newData.push( geoData );
            }
        }

        dataStore.loadData( newData );
    }

});

mApp.LocationPanel.prototype._onShow = function() {
};

mApp.LocationPanel.prototype._onDestroy = function() {
    this.cleanUp();
};

mApp.LocationPanel.prototype._setupMapLayers = function() {
    var defaultMapStyle = new OpenLayers.Style({
        pointRadius: 8,
        fillOpacity: 0.25,
        fillColor: this.fillColor,
        strokeOpacity: 0.8,
        strokeColor: this.fillColor,
        strokeWidth: 2
    });

    // Create our drawing layer...
    this.drawLayer = new OpenLayers.Layer.Vector( 'DrawGeometry', {
        isBaseLayer: false,
        visibility: true,
        styleMap: new OpenLayers.StyleMap( { 'default': defaultMapStyle } )
    });

    // Add the layers...
    this.map.addLayer( this.drawLayer );
};

mApp.LocationPanel.prototype._removeMapLayers = function() {
    if( this.drawLayer != null ) {
        this.drawLayer.destroy();
    }
};

mApp.LocationPanel.prototype._setupMapControllers = function() {

    // Draw Point Controller...
    this.drawPointController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Point,
        {
            geoType: 'point',
            locationPanel: this,
            featureAdded: this._onNewFeature
        }
    );

    // Draw Line Controller...
    this.drawLineController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Path,
        {
            geoType: 'line',
            locationPanel: this,
            featureAdded: this._onNewFeature
        }
    );

    // Draw Polygon Controller...
    this.drawPolygonController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Polygon,
        {
            geoType: 'polygon',
            locationPanel: this,
            featureAdded: this._onNewFeature
        }
    );

    this.drawCircleController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.RegularPolygon,
        {
            geoType: 'circle',
            locationPanel: this,
            featureAdded: this._onNewFeature,
            handlerOptions: { snapAngle: 5.625, sides: 64, irregular: false }
        }
    );

    // Modify Feature Controller...
    this.modifyFeatureController = new OpenLayers.Control.ModifyFeature( this.drawLayer );

    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.modifyFeatureController
    ];

    this.map.addControls( controls );
};

mApp.LocationPanel.prototype._removeMapControllers = function() {
    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.modifyFeatureController
    ];

    for( var index = 0; index < controls.length; index++ )
    {
        var curControl = controls[index];
        if( curControl.active ) {
            curControl.deactivate();
        }

        this.map.removeControl( curControl );
    }
};

mApp.LocationPanel.prototype.cleanUp = function() {
    this._removeMapControllers();
    this._removeMapLayers();
};

mApp.LocationPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.LocationPanel.prototype._onValidateLatitude = function( value ) {
    if( value === 'N/A' ) {
        return false;
    }

    return true;
};

mApp.LocationPanel.prototype._onValidateLongitude = function( value ) {
    if( value === 'N/A' ) {
        return false;
    }

    return true;
};

mApp.LocationPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    // Validate the event location...
    this.pnlGeometry.cascade( function( item ) {
        if( item.isFormField  && !item.hidden && !item.ownerCt.hidden )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

mApp.LocationPanel.prototype._updateInstructions = function( action ) {
    var drawDoneInstructions = "Click 'Edit Drawing' to modify the geometry, or 'Clear Drawing' to start over.";
    var instructions = "Select a drawing tool to draw a new geometry. Click 'New Drawing' to begin.";

    switch( action ) {
        case 'geometry':
            break;
        case 'start':
            instructions = this.pnlGeometry.layout.activeItem.instructions.new;
            break;
        case 'edit':
            instructions = this.pnlGeometry.layout.activeItem.instructions.edit;
            break;
        case 'stop':
            if( this._currentFeature != null )
            {
                instructions = drawDoneInstructions;
            }
            else {
                instructions = this.pnlGeometry.layout.activeItem.instructions.new;
            }
            break;
        case 'clear:':
            break;
        case 'drawDone':
            instructions = drawDoneInstructions;
            break;
    }

    this.txtInstructionField.setValue( instructions );
};

mApp.LocationPanel.prototype._onCboRender = function() {
    // Set the default value to the first item in the store...
    var value = this.store.getAt(0).get( 'type' )
    this.setValue( value );

    var pnlGeometry = this.locationPanel.pnlGeometry;
    pnlGeometry.setVisible( true );
    this.locationPanel.btnStartDrawing.setVisible( true );

    switch( value )
    {
        case 'none':
            pnlGeometry.activeItem = pnlGeometry.pnlNone;
            pnlGeometry.setVisible( false );
            this.locationPanel.btnStartDrawing.setVisible( false );
            break;
        case 'point':
            pnlGeometry.activeItem = pnlGeometry.pnlPoint;
            break;
        case 'line':
            pnlGeometry.activeItem = pnlGeometry.pnlLine;
            break;
        case 'polygon':
            pnlGeometry.activeItem = pnlGeometry.pnlPolygon;
            break;
        case 'circle':
            pnlGeometry.activeItem = pnlGeometry.pnlCircle;
            break;
    }

    mApp.LocationPanel.prototype._updateInstructions.call( this.locationPanel, 'geometry' );
};

mApp.LocationPanel.prototype._onCboGeometrySelect = function( combo, record, index ) {
    var pnlGeometry = combo.locationPanel.pnlGeometry;
    pnlGeometry.setVisible( true );
    combo.locationPanel.btnStartDrawing.setVisible( true );

    switch( combo.getValue() )
    {
        case 'none':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlNone );
            pnlGeometry.setVisible( false );
            combo.locationPanel.btnStartDrawing.setVisible( false );
            break;
        case 'point':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPoint );
            break;
        case 'line':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlLine );
            break;
        case 'polygon':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPolygon );
            break;
        case 'circle':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlCircle );
            break;
    }

    combo.locationPanel._updateInstructions( 'geometry' );
    combo.locationPanel.doLayout();
};

mApp.LocationPanel.prototype._onBtnStartDrawingClick = function() {
    this.btnStartDrawing.setVisible( false );
    this.btnStopDrawing.setVisible( true );
    this.btnClearDrawing.setVisible( true );
    this.cboGeometry.setDisabled( true );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'start' );

    this._clearFeature();

    switch( this.cboGeometry.getValue() )
    {
        case 'point':
            this._activateController( this.drawPointController );
            break;
        case 'line':
            this._activateController( this.drawLineController );
            break;
        case 'polygon':
            this._activateController( this.drawPolygonController );
            break;
        case 'circle':
            this._activateController( this.drawCircleController );
            break;
    }

    this.mainWindow.collapse();
};

mApp.LocationPanel.prototype._onBtnStopDrawingClick = function() {
    this.btnStopDrawing.setVisible( false );
    this.btnEditDrawing.setVisible( this._currentFeature != null );
    this.btnStartDrawing.setVisible( this._currentFeature == null );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'stop' );

    this._activateController( null );
};

mApp.LocationPanel.prototype._onBtnEditDrawingClick = function() {
    this.btnStopDrawing.setVisible( true );
    this.btnEditDrawing.setVisible( false );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'edit' );

    this._activateController( null );

    if( this._currentFeature != null )
    {

        var controller = this.modifyFeatureController
        controller.standalone = true;
        controller.clickout = false;

        switch( this._currentFeature.geoType )
        {
            case 'point':
                controller.createVertices = false;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                controller.mode |= OpenLayers.Control.ModifyFeature.DRAG;
                break;
            case 'line':
                controller.createVertices = true;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                break;
            case 'polygon':
                controller.createVertices = true;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                break;
            case 'circle':
                controller.createVertices = false;
                controller.mode = OpenLayers.Control.ModifyFeature.RESIZE;
                controller.mode |= OpenLayers.Control.ModifyFeature.DRAG;
                break;
        }

        this._activateController( this.modifyFeatureController );
        controller.selectFeature( this._currentFeature );
    }
};

mApp.LocationPanel.prototype._onBtnClearDrawingClick = function() {
    this._activateController( null );

    this._clearFeature();

    this.btnStartDrawing.setVisible( true );
    this.btnStopDrawing.setVisible( false );
    this.btnEditDrawing.setVisible( false );
    this.btnClearDrawing.setVisible( false );
    this.cboGeometry.setDisabled( false );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'clear' );
};

mApp.LocationPanel.prototype._clearFeature = function() {

    if( this._currentFeature != null ) {
        this.drawLayer.destroyFeatures( [ this._currentFeature ] );
        this._currentFeature = null;
    }

    this._resetDrawingControls();
};


mApp.LocationPanel.prototype._activateController = function( controller ) {
    var oldController = this._activeControl;
    this._activeControl = null;

    if( oldController != null ) {
        oldController.deactivate();
    }

    if( controller != null ) {
        controller.activate();
        this._activeControl = controller;
    }
};

mApp.LocationPanel.prototype._onNewFeature = function( feature ) {

    var locPanel = this.locationPanel;

    // Stop the controller...
    if( locPanel._activeControl != null ) {
        locPanel._activeControl.deactivate();
    }

    if( locPanel.mainWindow.collapsed ) {
        locPanel.mainWindow.toggleCollapse();
    }

    if( locPanel._currentFeature != null ) {
        this.layer.destroyFeatures( [ locPanel._currentFeature ] );
    }

    if( this.geoType === 'circle' )
    {
        // Figure out the radius...
        locPanel._updateRadius( feature );
    }

    feature.geoType = this.geoType;

    locPanel._currentFeature = feature;

    // Update the panel with the new data...
    mApp.LocationPanel.prototype._updatePanel.call( locPanel, locPanel._currentFeature );

    locPanel.btnStopDrawing.setVisible( false );
    locPanel.btnEditDrawing.setVisible( true );
    locPanel.btnClearDrawing.setVisible( true );
    locPanel.pnlButtons.doLayout();

    mApp.LocationPanel.prototype._updateInstructions.call( locPanel, 'drawDone' );
};

mApp.LocationPanel.prototype._onFeatureModified = function( panel, event ) {
    panel._updateRadius( event.feature );
    mApp.LocationPanel.prototype._updatePanel.call( panel, event.feature );
};

mApp.LocationPanel.prototype._onAfterFeatureModified = function( panel, event ) {
    panel._updateRadius( event.feature );
    mApp.LocationPanel.prototype._updatePanel.call( panel, event.feature );
};

mApp.LocationPanel.prototype._updateRadius = function( feature ) {
    // Figure out the radius...
    var radius = Math.abs( (feature.geometry.bounds.left - feature.geometry.bounds.right) / 2 );
    feature.radius = radius;
};

mApp.LocationPanel.prototype._updatePanel = function( feature ) {
    if( this.hasOwnProperty( 'pnlGeometry' ) )
    {
        this._resetDrawingControls();

        switch( feature.geoType )
        {
            case 'point':
                var latLonPt = new OpenLayers.LonLat( feature.geometry.x, feature.geometry.y );
                latLonPt = latLonPt.transform( feature.layer.map.projection, 'EPSG:4326' );
                this.pnlGeometry.pnlPoint.txtPointLatitude.setValue( latLonPt.lat );
                this.pnlGeometry.pnlPoint.txtPointLongitude.setValue( latLonPt.lon );
                break;
            case 'line':
                var strPoints = this._pointArrayToString( feature.geometry.getVertices(), false );
                this.pnlGeometry.pnlLine.txtLineGeometry.setValue( strPoints );
                break;
            case 'polygon':
                var strPoints = this._pointArrayToString( feature.geometry.getVertices(), true );
                this.pnlGeometry.pnlPolygon.txtPolygonGeometry.setValue( strPoints );
                break;
            case 'circle':
                var centerLL = feature.geometry.bounds.getCenterLonLat();
                var latLonPt = new OpenLayers.LonLat( centerLL.lon, centerLL.lat );
                latLonPt = latLonPt.transform( feature.layer.map.projection, 'EPSG:4326' );
                this.pnlGeometry.pnlCircle.txtCircleLatitude.setValue( latLonPt.lat );
                this.pnlGeometry.pnlCircle.txtCircleLongitude.setValue( latLonPt.lon );
                this.pnlGeometry.pnlCircle.txtCircleRadius.setValue( feature.radius / 1000 );
                break;
        }
    }
};

mApp.LocationPanel.prototype._pointArrayToString = function( pointsArray, repeatFirstPoint ) {
    var strPoints = '';
    var curLatLonPoint = null;
    var firstPointStr = null;

    for( var index = 0; index < pointsArray.length; index++ )
    {
        if( index > 0 ) {
            strPoints += ' ';
        }

        curLatLonPoint = new OpenLayers.LonLat( pointsArray[index].x, pointsArray[index].y );
        curLatLonPoint = curLatLonPoint.transform( this.drawLayer.map.projection, 'EPSG:4326' );
        if( index == 0 ) {
            firstPointStr = curLatLonPoint.lat + ',' + curLatLonPoint.lon;
        }

        strPoints += curLatLonPoint.lat + ',' + curLatLonPoint.lon;
    }

    if( repeatFirstPoint ) {
        strPoints += ' ' + firstPointStr;
    }

    return strPoints;
};

mApp.LocationPanel.prototype._resetDrawingControls = function() {
    if( this.hasOwnProperty( 'pnlGeometry' ) )
    {
        // Reset the point panel...
        this.pnlGeometry.pnlPoint.txtPointLatitude.setValue( 'N/A' );
        this.pnlGeometry.pnlPoint.txtPointLongitude.setValue( 'N/A' );

        // Reset the circle panel...
        this.pnlGeometry.pnlCircle.txtCircleLatitude.setValue( 'N/A' );
        this.pnlGeometry.pnlCircle.txtCircleLongitude.setValue( 'N/A' );
        this.pnlGeometry.pnlCircle.txtCircleRadius.setValue( 0.0 );

        // Reset the line panel...
        this.pnlGeometry.pnlLine.txtLineGeometry.setValue( 'N/A' );

        // Reset the polygon panel...
        this.pnlGeometry.pnlPolygon.txtPolygonGeometry.setValue( 'N/A' );
    }
};

mApp.LocationPanel.prototype.resetPanel = function() {
    this.getForm().reset();
    this.pnlGeometry.layout.setActiveItem( this.pnlGeometry.pnlPoint );
    this._updateInstructions( 'clear' );

    this.doLayout();
};

mApp.LocationPanel.prototype.getGeometry = function( parameters ) {
    var location = undefined;
    var pnlGeo = this.pnlGeometry;
    var geoType = 'none';
    var meters = false;

    if( this._currentFeature !== null )
    {
        geoType = this._currentFeature.geoType.toLowerCase();

        if( parameters.hasOwnProperty( 'meters' ) ) {
            meters = parameters.meters;
        }

        switch( this._currentFeature.geoType )
        {
            case 'point':
                location = pnlGeo.pnlPoint.txtPointLatitude.getValue() + ',' + pnlGeo.pnlPoint.txtPointLongitude.getValue();
                break;
            case 'line':
                location = pnlGeo.pnlLine.txtLineGeometry.getValue();
                break;
            case 'polygon':
                location = pnlGeo.pnlPolygon.txtPolygonGeometry.getValue();
                break;
            case 'circle':
                var radius = parseFloat( pnlGeo.pnlCircle.txtCircleRadius.getValue() );
                if( meters === true ) {
                    radius = radius / 1000;
                }
                location = pnlGeo.pnlCircle.txtCircleLatitude.getValue() + ',' + pnlGeo.pnlCircle.txtCircleLongitude.getValue() + ' ' + radius.toString();
                break;
            default:
                geoType = "none";
                location = undefined;
                break;
        }
    }

    return { type: geoType, location: location };
};

mApp.LocationPanel.prototype._setNewFeature = function( feature ) {
    if( feature )
    {
        // Add the feature...
        this.drawLayer.addFeatures( [feature] );
        this._currentFeature = feature;

        // Set the combo value...
        this.cboGeometry.setValue( this._currentFeature.geoType );

        // Make sure the panels are visible..
        this.pnlGeometry.setVisible( true );
        this.btnStartDrawing.setVisible( true );

        // Set the active panel...
        switch( feature.geoType )
        {
            case 'point':
                this.pnlGeometry.layout.setActiveItem( this.pnlGeometry.pnlPoint );
                break;
            case 'line':
                this.pnlGeometry.layout.setActiveItem( this.pnlGeometry.pnlLine );
                break;
            case 'polygon':
                this.pnlGeometry.layout.setActiveItem( this.pnlGeometry.pnlPolygon );
                break;
            case 'circle':
                this.pnlGeometry.layout.setActiveItem( this.pnlGeometry.pnlCircle );
                break;
        }

        // Update the panel with the new data...
        this._updatePanel( this._currentFeature );

        // Show the Edit button...
        this.btnEditDrawing.setVisible( true );
        this.btnStartDrawing.setVisible( false );
        this.btnClearDrawing.setVisible( true );
        this.pnlButtons.doLayout();

        this.cboGeometry.setDisabled( true );

        this._updateInstructions( 'drawDone' );
    }

    this.doLayout();
};

mApp.LocationPanel.prototype.setFromWGS84 = function( geoType, geoData ) {
    var newFeature = null;
    var values = null;

    if( geoType === 'point' )
    {
        values = geoData.split( ',' );

        var centerPt = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), this.map.getProjectionObject() );
        newFeature = new OpenLayers.Feature.Vector( centerPt );
        newFeature.geoType = 'point';
    }
    else if( geoType === 'line' )
    {
        var points = [];
        var pairs = geoData.split( ' ' );

        for( var iCtr = 0; iCtr < pairs.length; iCtr++ )
        {
            values = pairs[iCtr].split( ',' );
            var newPt = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), this.map.getProjectionObject() );
            points.push( newPt );
        }
        var geom = new OpenLayers.Geometry.LineString( points );
        newFeature = new OpenLayers.Feature.Vector( geom );
        newFeature.geoType = 'line';
    }
    else if( geoType === 'polygon' )
    {
        var points = [];
        var pairs = geoData.split( ' ' );

        for( var iCtr = 0; iCtr < pairs.length; iCtr++ )
        {
            values = pairs[iCtr].split( ',' );
            var newPt = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), this.map.getProjectionObject() );
            points.push( newPt );
        }
        var linRing = new OpenLayers.Geometry.LinearRing( points );
        var geom = new OpenLayers.Geometry.Polygon( [linRing] );
        newFeature = new OpenLayers.Feature.Vector( geom );

        newFeature.geoType = 'polygon';
    }
    else if( geoType === 'circle' )
    {
        var data = geoData.split( ' ' );
        var values = data[0].split( ',' );
        var centerPt = new OpenLayers.Geometry.Point( values[1], values[0] ).transform( new OpenLayers.Projection('EPSG:4326'), this.map.getProjectionObject() );

        var radius = data[1] * 1000; // WGS84 radius is in kilometers.
        var geom = new OpenLayers.Geometry.Polygon.createRegularPolygon( centerPt, radius, 64 );
        newFeature = new OpenLayers.Feature.Vector( geom );

        newFeature.radius = radius;
        newFeature.geoType = 'circle';
    }

    this._setNewFeature( newFeature );
};

mApp.LocationPanel.prototype.setFromGeoJSON = function( geoJSON ) {
    var newFeature = null;

    // TODO - IMPLEMENT THIS!

    this._setNewFeature( newFeature );
};

mApp.LocationPanel.prototype.setFromGeoRSS = function( geoType, geoRSS ) {
    var newFeature = null;

    // TODO - IMPLEMENT THIS!

    this._setNewFeature( newFeature );
};

Ext.reg( 'MASAS_locationPanel', mApp.LocationPanel );