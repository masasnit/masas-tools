mApp.EntryAttachmentPanel = Ext.extend( Ext.Panel , {

    initComponent: function() {

        this._origAttachmentCount = 0;

        this._attachmentStore = new Ext.data.ArrayStore({
            fields: [ 'title', 'fileName', 'type', 'fileObject' ],
            data: []
        });


        Ext.apply( this, {
            title: 'Attachments',
            layout: 'vbox',
            padding: 10,
            autoScroll: true,
            border: false,
            height: '100%',
            defaultType: 'textfield',

            items: [
                {
                    xtype: 'displayfield',
                    value: 'Add up to five (5) attachments to this Entry.  The maximum size of all attachments cannot exceed 10 MB.',
                    margins: { top:0, right:0, bottom:5, left:0 }
                },
                {
                    xtype: 'panel',
                    layout: {
                        type: 'table'
                    },
                    width: '100%',
                    padding: 5,
                    plain: true,
                    border: false,
                    items: [
                        {
                            xtype: 'button',
                            ref: '../btnAdd',
                            padding: 5,
                            width: 75,
                            text: 'Add',
                            tooltip: 'Add an attachment to this Entry',
                            scope: this,
                            handler: this._addAttachment
                        },
                        {
                            xtype: 'displayfield',
                            width: 5
                        },
                        {
                            xtype: 'button',
                            ref: '../btnRemoveAll',
                            width: 75,
                            text: 'Remove All',
                            tooltip: 'Remove All the associated attachments.',
                            scope: this,
                            handler: this._removeAllAttachments
                        }
                    ]
                },
                {
                    xtype: 'grid',
                    ref: 'gridAttachments',
                    fieldLabel: 'Attachments',
                    width: '100%',
                    flex: 1,
                    allowBlank: true,
                    autoScroll: true,
                    stripeRows: true,
                    columnLines: true,
                    header: false,
                    store: this._attachmentStore,
                    autoExpandColumn: '1',
                    viewConfig: {
                        headersDisabled: true,
                        hideSortIcons: true
                    },
                    columns: [
                        { header: 'Title', dataIndex: 'title', width: 200 },
                        { header: 'File Name', dataIndex: 'fileName' },
                        { header: 'Type', dataIndex: 'type' },
                        {
                            xtype: 'actioncolumn',
                            width: 28,
                            items: [
                                {
                                    iconCls: 'btnDeleteRelatedLinkIcon',
                                    tooltip: 'Delete Attachment',
                                    scope: this,
                                    handler: this._removeAttachment
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        mApp.EntryAttachmentPanel.superclass.initComponent.call( this );
    }

});

mApp.EntryAttachmentPanel.prototype._addAttachment = function() {
    if( this._attachmentStore.getCount() < 5 )
    {
        this.attachWnd = new mApp.AttachmentWindow();
        this.attachWnd.on( 'close', this._onAttachmentWindowClose, this );

        this.attachWnd.show();
    }
    else
    {
        Ext.Msg.alert( 'Info', 'The maximum number of attachment has been reached!' );
    }
};

mApp.EntryAttachmentPanel.prototype._onAttachmentWindowClose = function( panel ) {
    if( !panel.cancelled )
    {
        var newRecord = new this._attachmentStore.recordType( {
            title: panel.fileTitle,
            fileName: panel.fileName,
            fileObject: panel.fileObject,
            type: panel.fileObject.type
        });

        this._attachmentStore.add( newRecord );
        this._attachmentStore.commitChanges();
    }

    this.attachWnd.destroy();
    this.attachWnd = null;
}

mApp.EntryAttachmentPanel.prototype._removeAttachment = function( grid, rowIndex, colIndex, item, event ) {
    this._attachmentStore.removeAt( rowIndex );
    this._attachmentStore.commitChanges();
};

mApp.EntryAttachmentPanel.prototype._removeAllAttachments = function() {
    this._attachmentStore.removeAll();
    this.btnAdd.setDisabled( false );
    this.gridAttachments.setDisabled( false );
};

mApp.EntryAttachmentPanel.prototype.resetPanel = function() {
    var form = this.getForm();
    form.reset();
};

mApp.EntryAttachmentPanel.prototype._populateForm = function( entry ) {
    // Links...
    for( var linksCtr = 0; linksCtr < entry.link.length; linksCtr++ )
    {
        var curLink = entry.link[linksCtr];
        if( curLink['@rel'] === 'enclosure' )
        {
            var newRecord = new this._attachmentStore.recordType( {
                title: curLink['@title'],
                type: curLink['@type'],
                url: curLink['@href']
            });

            this._attachmentStore.add( newRecord );
            this._attachmentStore.commitChanges();

            this.btnAdd.setDisabled( true );
            this.gridAttachments.setDisabled( true );
        }
    }

    this._origAttachmentCount = this._attachmentStore.getCount();
};
