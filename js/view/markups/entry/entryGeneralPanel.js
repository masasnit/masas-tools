mApp.EntryGeneralPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        var pnlEntryGeneral = this;
        this._seviceSymbols = App.settings.appSettings.serviceSymbols;
        this._htmlPreviewTemplate = '<div class="symbolPreviewBox_48"><img class="symbolPreviewImage" src="{0}" alt="Symbol not available."></div>';
        var htmlPreview = this._htmlPreviewTemplate.replace( '{0}', this._seviceSymbols + "/other/large.png" );
        this._entrySymbol = "other";
        this._titleModified = false;

        this.relatedLinkStore = new Ext.data.ArrayStore({
            fields: ['title', 'url', 'type'],
            data: []
        });

        // Added this for now, clean up later...
        this.on( 'beforeadd', this._onValidateRequired );

        Ext.apply( this, {
            title: 'General',
            padding: 10,
            autoScroll: true,
            border:false,
            defaultType: 'textfield',

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 75,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },

            items: [
                {
                    xtype: 'button',
                    ref: 'btnToggleFrench',
                    text: 'Enable FR-CA',
                    enableToggle: true,
                    style: '{ position: absolute; right: 10px; top: 6px; z-index:10; }',
                    scope: this,
                    handler: this._onToggleFrenchTab
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Entry Type',
                    height: 56,
                    items: [
                        {
                            xtype: 'button',
                            text: 'Select...',
                            width: 56,
                            height: '100%',
                            scope: this,
                            handler: this._onIconSelectClicked
                        },
                        {
                            xtype: 'displayfield',
                            ref: '../imagePreview',
                            height: 56,
                            html: htmlPreview
                        },
                        {
                            xtype: 'container',
                            height: 48,
                            flex: 1,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    ref: '../../../lblSymbolDesc',
                                    value: 'Other'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'tabpanel',
                    ref: 'tabsLangContent',
                    plain: true,
                    padding: 10,
                    activeTab: 0,
                    deferredRender: false,
                    style: '{margin-bottom:10px;}',
                    defaults:{
                        bodyStyle:'padding:10px',
                        autoHeight: true
                    },

                    labelWidth: 65,
                    labelPad: 5,
                    defaults: {
                        labelSeparator: ':',
                        msgTarget: 'side'
                    },

                    items: [
                        {
                            title:'EN-CA',
                            ref: 'tabEngInfo',
                            layout:'form',
                            defaultType: 'textfield',
                            labelWidth: 65,
                            labelPad: 5,
                            height: 105,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },

                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },

                            items: [
                                {
                                    fieldLabel: 'Title',
                                    ref: '../../txtTitleEn',
                                    width: '95%',
                                    minLength: 5,
                                    minLengthText: 'A good title should say what and where',
                                    maxLength: 250,
                                    maxLengthText: 'Titles cannot be longer than 250 characters',
                                    blankText: 'MASAS requires a title value',
                                    allowBlank: false,
                                    listeners: {
                                        scope: this,
                                        'change': this._onTitleValueChanged
                                    }
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Content',
                                    ref: '../../txtContentEn',
                                    height: 80,
                                    width: '95%',
                                    allowBlank: true,
                                    flex: 1
                                }
                            ]
                        },
                        {
                            title:'FR-CA',
                            ref: 'tabFrenchInfo',
                            layout:'form',
                            defaultType: 'textfield',
                            disabled: true,
                            labelWidth: 65,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },

                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },

                            items: [
                                {
                                    fieldLabel: 'Titre',
                                    ref: '../../txtTitleFr',
                                    disabled: true,
                                    width: '95%',
                                    minLengthText: 'Un bon titre devrait contenir quoi et où.',
                                    maxLength: 250,
                                    maxLengthText: 'Un titre ne peut que contenir 250 caractères.',
                                    blankText: 'Un titre est requis',
                                    allowBlank: false
                                },
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Contenu',
                                    ref: '../../txtContentFr',
                                    disabled: true,
                                    height: 80,
                                    width: '95%',
                                    allowBlank: true
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Category',
                    ref: 'cboCategory',
                    allowBlank: true,
                    editable: false,
                    triggerAction: 'all',
                    mode: 'local',
                    displayField: 'e_name',
                    valueField: 'value',
                    tpl: '<tpl for="."><div ext:qtip="{e_tip}" class="x-combo-list-item">{e_name}&nbsp;</div></tpl>',
                    store: Ext.StoreMgr.get( 'categoryLookups' )
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Effective',
                    height: 22,
                    anchor: '80%',
                    items: [
                        {
                            xtype: 'radio',
                            ref: '../rdoEffectiveNow',
                            itemId: 'rdoEffectiveNow',
                            boxLabel: 'Immediately',
                            checked: true,
                            name: 'frmEntryGeneral_grpEffective',
                            width: 114,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onEffectiveChanged
                            }
                        },
                        {
                            xtype: 'radio',
                            ref: '../rdoEffectiveAt',
                            itemId: 'rdoEffectiveAt',
                            boxLabel: 'At',
                            name: 'frmEntryGeneral_grpEffective',
                            width: 32,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onEffectiveChanged
                            }
                        },
                        {
                            xtype: 'datefield',
                            ref: '../dateEffective',
                            disabled: true,
                            width: 95,
                            format: 'Y-m-d',
                            minValue: new Date(),
                            pnlEntryGeneral: this,
                            validator: this._onValidateEffectiveDT
                        },
                        {
                            xtype: 'timefield',
                            ref: '../timeEffective',
                            disabled: true,
                            width: 70,
                            format: 'H:i',
                            increment: 60,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            validator: this._onValidateEffectiveDT
                        }
                    ]
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Expires',
                    height: 22,
                    anchor: '80%',
                    items: [
                        {
                            xtype: 'radio',
                            ref: '../rdoExpiresIn',
                            itemId: 'rdoExpiresIn',
                            boxLabel: 'In',
                            checked: true,
                            name: 'frmEntryGeneral_grpExpires',
                            width: 28,
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onExpiresChanged
                            }
                        },
                        {
                            xtype: 'combo',
                            ref: '../cboExpiresInt',
                            allowBlank: true,
                            editable: false,
                            width: 75,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            value: 48,
                            valueField: 'interval',
                            tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
                            store: new Ext.data.ArrayStore({
                                fields: ['interval', 'name', 'tip'],
                                data: [ [null, 'N/A', 'Do not set expires'],
                                    [1, '1 Hour', 'Expires in 1 hour'],
                                    [6, '6 Hours', 'Expires in 6 hours'],
                                    [12, '12 Hours', 'Expires in 12 hours'],
                                    [24, '24 Hours', 'Expires in 1 day'],
                                    [48, '48 Hours', 'Expires in 2 days'] ]
                            })
                        },
                        {
                            xtype: 'radio',
                            ref: '../rdoExpiresOn',
                            itemId: 'rdoExpiresOn',
                            boxLabel: 'On',
                            name: 'frmEntryGeneral_grpExpires',
                            width: 38,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            listeners: {
                                'check': this._onExpiresChanged
                            }
                        },
                        {
                            xtype: 'datefield',
                            ref: '../dateExpires',
                            disabled: true,
                            width: 95,
                            format: 'Y-m-d',
                            minValue: new Date(),
                            pnlEntryGeneral: this,
                            validator: this._onValidateExpiresDT
                        },
                        {
                            xtype: 'timefield',
                            ref: '../timeExpires',
                            disabled: true,
                            width: 70,
                            format: 'H:i',
                            increment: 60,
                            margins: { top:0, right:0, bottom:0, left:5 },
                            pnlEntryGeneral: this,
                            validator: this._onValidateExpiresDT
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    title: 'Advanced Options',
                    width: '100%',
                    collapsible: true,
                    collapsed: true,
                    titleCollapse: true,
                    labelWidth: 65,
                    labelPad: 5,
                    defaults: {
                        labelSeparator: ':',
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: 'combo',
                            fieldLabel: 'Status',
                            ref: '../cboStatus',
                            allowBlank: false,
                            editable: false,
                            width: 108,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            valueField: 'name',
                            value: 'Actual',
                            tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}</div></tpl>',
                            store: new Ext.data.ArrayStore({
                                fields: ['name', 'tip'],
                                data: [
                                    ['Actual', 'Actual Entries'],
                                    ['Draft', 'Entries which cannot be seen by other users']
                                ]
                            })
                        },
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'Certainty',
                            ref: '../cboCertainty',
                            allowBlank: true,
                            items: [
                                {boxLabel: 'Unknown', name: 'entry-certainty', inputValue: 'Unknown'},
                                {boxLabel: 'Unlikely', name: 'entry-certainty', inputValue: 'Unlikely'},
                                {boxLabel: 'Possible', name: 'entry-certainty', inputValue: 'Possible'},
                                {boxLabel: 'Likely', name: 'entry-certainty', inputValue: 'Likely'},
                                {boxLabel: 'Observed', name: 'entry-certainty', inputValue: 'Observed'}
                            ]
                        },
                        {
                            xtype: 'radiogroup',
                            fieldLabel: 'Severity',
                            ref: '../cboSeverity',
                            allowBlank: true,
                            width: '95%',
                            items: [
                                { boxLabel: 'Unknown', name: 'entry-severity', inputValue: 'Unknown' },
                                { boxLabel: 'Minor', name: 'entry-severity', inputValue: 'Minor' },
                                { boxLabel: 'Moderate', name: 'entry-severity', inputValue: 'Moderate' },
                                { boxLabel: 'Severe', name: 'entry-severity', inputValue: 'Severe' },
                                { boxLabel: 'Extreme', name: 'entry-severity', inputValue: 'Extreme' }
                            ]
                        }
                    ],
                    scope: this,
                    listeners: {
                        'beforeadd': this._onValidateRequired
                    }
                },
                {
                    xtype: 'fieldset',
                    title: 'Related Links',
                    autoHeight: true,
                    collapsible: true,
                    collapsed: true,
                    titleCollapse: true,
                    items: [
                        {
                            xtype: 'panel',
                            anchor: '100%',
                            layout: 'vbox',
                            height: 150,
                            plain: true,
                            frame: false,
                            border: false,
                            items: [
                                {
                                    xtype: 'displayfield',
                                    height: 24,
                                    value: 'Link(s) to information related to this Entry.'
                                },
                                {
                                    xtype: 'button',
                                    width: 75,
                                    text: 'Add',
                                    tooltip: 'Add related links to this Entry',
                                    margins: { top:0, right:0, bottom:5, left:0 },
                                    scope: this,
                                    handler: this._addRelatedLink
                                },
                                {
                                    xtype: 'grid',
                                    fieldLabel: 'Links',
                                    width: '100%',
                                    flex: 1,
                                    allowBlank: true,
                                    autoScroll: true,
                                    stripeRows: true,
                                    columnLines: true,
                                    header: false,
                                    hideHeaders: true,
                                    store: this.relatedLinkStore,
                                    autoExpandColumn: '2',
                                    columns: [
                                        { dataIndex: 'title', resizable: true, width: 100 },
                                        { dataIndex: 'url', resizable: true, width: 200 },
                                        { dataIndex: 'type', resizable: true },
                                        {
                                            xtype: 'actioncolumn',
                                            width: 24,
                                            items: [
                                                {
                                                    iconCls: 'btnDeleteRelatedLinkIcon',
                                                    tooltip: 'Delete Link',
                                                    scope: this,
                                                    handler: this._removeRelatedLink
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]

                        }
                    ]

                }
            ]
        });

        mApp.EntryGeneralPanel.superclass.initComponent.call( this );
    }

});

mApp.EntryGeneralPanel.prototype._onTitleValueChanged = function( control, newValue, oldValue ) {
    this._titleModified = true;
    if( newValue.length == 0 ) {
        this._titleModified = false;
    }
};

mApp.EntryGeneralPanel.prototype._addRelatedLink = function() {
    this.linkWnd = new mApp.LinkWindow();
    this.linkWnd.on( 'close', this._onLinkWindowClose, this );

    this.linkWnd.show();
};

mApp.EntryGeneralPanel.prototype._onLinkWindowClose = function( panel ) {
    if( !panel.cancelled )
    {
        var newRecord = new this.relatedLinkStore.recordType( {
            title: panel.linkTitle,
            type: panel.linkContentType,
            url: panel.linkUrl
        });

        this.relatedLinkStore.add( newRecord );
        this.relatedLinkStore.commitChanges();
    }

    this.linkWnd.destroy();
    this.linkWnd = null;
};

mApp.EntryGeneralPanel.prototype._removeRelatedLink = function( grid, rowIndex, colIndex, item, event ) {
    this.relatedLinkStore.removeAt( rowIndex );
    this.relatedLinkStore.commitChanges();
};

mApp.EntryGeneralPanel.prototype.resetPanel = function() {
    var form = this.getForm();
    form.reset();
};

mApp.EntryGeneralPanel.prototype._onIconSelectClicked = function() {
    this.selectWnd = new mApp.SymbolSelectWindow();
    this.selectWnd.curSelectionSymbol = this._entrySymbol;
    this.selectWnd.on( 'close', this._onSymbolSelectWindowClose, this );

    this.selectWnd.show();
};

mApp.EntryGeneralPanel.prototype._onSymbolSelectWindowClose = function( panel ) {
    if( panel.curSelectionSymbol !== undefined &&  panel.curSelectionSymbol !== '' ) {
        this._setSymbol( panel.curSelectionSymbol, panel.curSelectionSymbolText );
    }

    // simple pre-population of the title value
    if( this._titleModified == false )
    {
        var titleValue = this.txtTitleEn.getValue();

        // Update the value with the generated one. Don't trigger an event.
        this.txtTitleEn.setRawValue( panel.curSelectionSymbolText );
    }

    this.selectWnd.destroy();
    this.selectWnd = null;
};

mApp.EntryGeneralPanel.prototype._setSymbol = function( symbol, symbolText ) {
    var foundText = symbolText;
    this._entrySymbol = symbol;
    var symbol_url = this._seviceSymbols + "/" + symbol + "/large.png";
    this.imagePreview.setValue( this._htmlPreviewTemplate.replace( '{0}', symbol_url ) );

    if( symbolText === undefined || symbolText == null ) {
        foundNode = App.dataStoreMgr.entrySymbolStore.findNodeByTerm( symbol );
        if( foundNode !== undefined && foundNode != null ) {
            foundText = foundNode.text;
        }
    }

    this.lblSymbolDesc.setValue( foundText );
};

mApp.EntryGeneralPanel.prototype._onEffectiveChanged = function( object, checked ) {
    if( checked )
    {
        var effectiveDT = this.pnlEntryGeneral.dateEffective;
        var effectiveTM = this.pnlEntryGeneral.timeEffective;

        if( object.itemId === "rdoEffectiveNow" ) {
            effectiveDT.setDisabled( true );
            effectiveTM.setDisabled( true );

            effectiveDT.clearInvalid();
            effectiveTM.clearInvalid();
        }
        else {
            effectiveDT.setDisabled( false );
            effectiveTM.setDisabled( false );
        }
    }
};

mApp.EntryGeneralPanel.prototype._onValidateEffectiveDT = function( val ) {
    if (!val || val.length === 0) {
        return false;
    }

    var effectiveDT = this.pnlEntryGeneral.dateEffective;
    var effectiveTM = this.pnlEntryGeneral.timeEffective;

    var isValid = this.pnlEntryGeneral._check_future_time( effectiveDT.getValue(), effectiveTM.getValue() );
    if( isValid ) {
        effectiveDT.clearInvalid();
        effectiveTM.clearInvalid();
    }

    return isValid;
};

mApp.EntryGeneralPanel.prototype._onExpiresChanged = function( object, checked ) {
    if( checked )
    {
        var expiresDT = this.pnlEntryGeneral.dateExpires;
        var expiresTM = this.pnlEntryGeneral.timeExpires;
        var expiresInt = this.pnlEntryGeneral.cboExpiresInt;

        if( object.itemId === "rdoExpiresIn" ) {
            expiresDT.setDisabled( true );
            expiresTM.setDisabled( true );
            expiresInt.setDisabled( false );

            expiresDT.clearInvalid();
            expiresTM.clearInvalid();
        }
        else {
            expiresDT.setDisabled( false );
            expiresTM.setDisabled( false );
            expiresInt.setDisabled( true );
        }
    }
};

mApp.EntryGeneralPanel.prototype._onValidateExpiresDT = function( val ) {
    if (!val || val.length === 0) {
        return false;
    }

    var expiresDT = this.pnlEntryGeneral.dateExpires;
    var expiresTM = this.pnlEntryGeneral.timeExpires;

    var isValid = this.pnlEntryGeneral._check_future_time( expiresDT.getValue(), expiresTM.getValue() );
    if( isValid ) {
        expiresDT.clearInvalid();
        expiresTM.clearInvalid();
    }

    return isValid;
};

mApp.EntryGeneralPanel.prototype._onToggleFrenchTab = function() {

    this.tabsLangContent.tabFrenchInfo.setDisabled( !this.btnToggleFrench.pressed );
    this.txtTitleFr.setDisabled( !this.btnToggleFrench.pressed );
    this.txtContentFr.setDisabled( !this.btnToggleFrench.pressed );

    if( this.tabsLangContent.tabFrenchInfo.disabled ) {
        this.tabsLangContent.setActiveTab( this.tabsLangContent.tabEngInfo );
    }

};

mApp.EntryGeneralPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.EntryGeneralPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    this.cascade( function( item ) {
        if( item.isFormField && !item.disabled )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

mApp.EntryGeneralPanel.prototype._populateForm = function( entry ) {
    // Utils object...
    var commonUtils = mApp.CommonUtils.getInstance();

    // Categories...
    for( var catCtr = 0; catCtr < entry.category.length; catCtr++ )
    {
        var curCat = entry.category[catCtr];

        switch( curCat['@scheme'] ) {
            case 'masas:category:category':
                this.cboCategory.setValue( curCat['@term'] );
                break;
            case 'masas:category:severity':
                this.cboSeverity.setValue( curCat['@term'] );
                break;
            case 'masas:category:certainty':
                this.cboCertainty.setValue( curCat['@term'] );
                break;
            case 'masas:category:status':
                this.cboStatus.setValue( curCat['@term'] );
                break;
            case 'masas:category:icon':
                this._setSymbol( curCat['@term'] );
                break;
            case 'masas:category:colour':
                break;
        }
    }

    // Title...
    if( entry.title.div.div instanceof Array )
    {
        for( var titleCtr = 0; titleCtr < entry.title.div.div.length; titleCtr++ )
        {
            var curTitle = entry.title.div.div[titleCtr];
            switch( curTitle['@xml:lang'] ) {
                case 'en':
                    this.txtTitleEn.setValue( commonUtils.decodeXML( curTitle['#text'] ) );
                    break;
                case 'fr':
                    this.txtTitleFr.setValue( commonUtils.decodeXML( curTitle['#text'] ) );
                    this.btnToggleFrench.toggle( true );
                    this._onToggleFrenchTab();
                    break;
            }
        }
    }
    else {
        this.txtTitleEn.setValue( commonUtils.decodeXML( entry.title.div.div['#text'] ) );
    }

    // Content...
    if( entry.content.div.div instanceof Array )
    {
        for( var contentCtr = 0; contentCtr < entry.content.div.div.length; contentCtr++ )
        {
            var curContent = entry.content.div.div[contentCtr];
            switch( curContent['@xml:lang'] ) {
                case 'en':
                    this.txtContentEn.setValue( commonUtils.decodeXML( curContent['#text'] ) );
                    break;
                case 'fr':
                    this.txtContentFr.setValue( commonUtils.decodeXML( curContent['#text'] ) );
                    break;
            }
        }
    }
    else {
        this.txtContentEn.setValue( commonUtils.decodeXML( entry.content.div.div['#text'] ) );
    }

    // Links...
    for( var linksCtr = 0; linksCtr < entry.link.length; linksCtr++ )
    {
        var curLink = entry.link[linksCtr];
        if( curLink['@rel'] === 'related' )
        {
            var newRecord = new this.relatedLinkStore.recordType( {
                title: curLink['@title'],
                type: curLink['@type'],
                url: curLink['@href']
            });

            this.relatedLinkStore.add( newRecord );
            this.relatedLinkStore.commitChanges();
        }
    }

    var nowEpoch = new Date().getTime();

    // Effective Date/Time...
    if( entry['met:effective'] )
    {
        var effectiveDate = Date.parseDate( entry['met:effective'], 'Y-m-d\\TH:i:s\\Z' );
        effectiveDate = commonUtils.adjustTime( effectiveDate, commonUtils.UTC_Local_Offset );

        // if the entry has already become effective, don't fill in the previous
        // date as any update should need a new effective time
        var effectiveEpoch = effectiveDate.format('U') * 1000;
        if( effectiveEpoch >= nowEpoch )
        {
            this.rdoEffectiveAt.setValue( true );
            this.dateEffective.setValue( effectiveDate );
            this.timeEffective.setValue( effectiveDate );
        }
    }

    // expires Date/Time...
    if( entry['age:expires'] )
    {
        var expiresDate = Date.parseDate( entry['age:expires'], 'Y-m-d\\TH:i:s\\Z' );
        expiresDate = commonUtils.adjustTime( expiresDate, commonUtils.UTC_Local_Offset );

        var expiresEpoch = expiresDate.format('U') * 1000;
        if( expiresEpoch >= nowEpoch )
        {
            this.rdoExpiresOn.setValue( true );
            this.dateExpires.setValue( expiresDate );
            this.timeExpires.setValue( expiresDate );
        }
    }

};

/**
 Validates that a time value is in the future, the format of the time should
 already have been checked using the existing Ext validator.  Assuming local
 time for comparison.

 @param {String} - a date value 'YYYY-MM-DD', optional
 @param {String} - a time value 'H:S'
 @return {Boolean} - {Boolean} for a valid time
 */
mApp.EntryGeneralPanel.prototype._check_future_time = function (d_val, t_val) {
    var current = new Date();
    // assumes that minValue set for the date input field has already validated
    // that the date is either at least for today or in the future.  Is optional
    // because the user may start entering the time first.
    if (d_val) {
        var c_date = current.format('Y-m-d');
        if (d_val !== c_date) {
            return true;
        }
    }
    // ignore empty values
    if (!t_val || t_val.length === 0) {
        return true;
    }
    // compare to current
    var c_hour = parseInt(current.format('H'), 10);
    var c_min = parseInt(current.format('i'), 10);
    var t_vals = t_val.split(':');
    var t_hour = parseInt(t_vals[0], 10);
    var t_min = parseInt(t_vals[1], 10);
    if (t_hour < c_hour) {
        return 'Must be a time value that is in the future';
    } else if (t_hour === c_hour) {
        if (t_min <= c_min) {
            return 'Must be a time value that is in the future';
        }
    }

    return true;
};