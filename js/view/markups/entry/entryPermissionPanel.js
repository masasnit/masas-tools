mApp.EntryPermissionPanel = Ext.extend( Ext.Panel , {

    initComponent: function() {

        Ext.apply( this, {
            title: 'Permissions',
            padding: 10,
            //disabled:true, // TEMPORARY UNTIL THINGS ARE FIXED ON THE PERMISSION ISSUES.
            autoScroll: true,
            border: false,
            items: [
                {
                    xtype: 'displayfield',
                    value: 'The default is that only your account can update an ' +
                        'Entry that you create.  However you can allow other ' +
                        'accounts to update Entries if you want them to share any ' +
                        'information that they can contribute.  Once you or another ' +
                        'account updates your Entry though, they can change the ' +
                        'Update Permissions back to a single account only if necessary.'
                },
                {
                    xtype: 'radiogroup',
                    ref: 'grpPermissions',
                    autoHeight: true,
                    style: '{ margin-top: 10px; }',
                    allowBlank: true,
                    value: 'user',
                    columns: 1,
                    items: [
                        {
                            boxLabel: 'Only My Account May Update',
                            name: 'entry-update-control',
                            inputValue: 'user'
                        },
                        {
                            boxLabel: 'Only Accounts in my Organization May Update',
                            name: 'entry-update-control',
                            inputValue: 'group'
                        },
                        {
                            boxLabel: 'Any MASAS Participant May Update',
                            name: 'entry-update-control',
                            inputValue: 'all'
                        }
                    ]
                }

            ]

        });

        mApp.EntryPermissionPanel.superclass.initComponent.call( this );
    }

});

mApp.EntryPermissionPanel.prototype.resetPanel = function() {
    this.grpPermissions.setValue( 'user' );
};

mApp.EntryPermissionPanel.prototype._populateForm = function( entry ) {
    var currentUser = App.masasHub.userData;
    var permissionValue = 'user';

    // Get the app control if there is one
    if( entry['app:control'] && entry['app:control']['mec:update']  )
    {
        var permissionString = entry['app:control']['mec:update'];


        if( permissionString.indexOf( "all" ) > -1 ) {
            // All permission...
            permissionValue = 'all';
        }
        else if( permissionString.indexOf( currentUser.organizationID ) > -1 ) {
            // Group Permission...
            permissionValue = 'group';
        }
        else if( permissionString.indexOf( currentUser.id ) > -1 ) {
            // Individual user permission (not owner)...
            // TODO - THIS CASE IS CURRENTLY NOT SUPPORTED... Default to org...
            permissionValue = 'group';
        }
    }

    this.grpPermissions.setValue( permissionValue );
};

mApp.EntryPermissionPanel.prototype._saveData = function() {

};

