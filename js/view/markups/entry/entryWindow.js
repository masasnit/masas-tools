mApp.EntryWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        // Map accessor...
        this.map = App.mainView.mapPanel.map;

        // Original Entry...
        this._origEntry = null;

        // Our drawing components...
        this.drawLayer = null;
        this.drawPointController = null;

        // Create the Map Layer we will need to use...
        this._setupMapLayers();

        this.generalPanel = new mApp.EntryGeneralPanel( { title: 'General' } );
        this.locationPanel = new mApp.EntryLocationPanel( { title: 'Location', entryWindow: this } );
        this.permissionPanel = new mApp.EntryPermissionPanel( { title: 'Permissions' } );
        this.attachmentPanel = new mApp.EntryAttachmentPanel( { title: 'Attachments' } );

        // Create and setup the Map Controllers we will need...
        this._setupMapControllers();

        Ext.apply( this, {

            title: "Create a new Entry",
            layout: 'fit',
            width: 600,
            height: 500,
            minWidth: 300,
            minHeight: 200,
            closeAction: 'close',
            plain: true,

            collapsible: true,
            maximizable: true,

            items: {
                xtype: 'tabpanel',
                activeTab: 0,
                deferredRender: false,

                items: [
                    this.generalPanel,
                    this.locationPanel,
                    this.attachmentPanel,
                    this.permissionPanel
                ]
            },

            buttonAlign: 'right',

            buttons: [
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                },
                {
                    text: "Submit",
                    disabled: false,
                    scope: this,
                    handler: this._onSendClick
                }
            ],

            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            }
        });

        mApp.EntryWindow.superclass.initComponent.call( this );
    }

});

mApp.EntryWindow.prototype._beforeShow = function() {
    if( 'entryLink' in this )
    {
        this._origEntry = this._loadEntryFromHub( this.entryLink );
        this.setTitle( "Update Entry: " + this._origEntry.id );
        this._populateForm( this._origEntry );
    }
};

mApp.EntryWindow.prototype._beforeClose = function() {
    this._removeMapControllers();
    this._removeMapLayers();
};

mApp.EntryWindow.prototype._onSendClick = function() {
    var generalValid = this.generalPanel.validate();
    var locationValid = this.locationPanel.validate();

    if( generalValid.valid && locationValid.valid )
    {
        var entryWindow = this;
        this.myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});

        this.disable();

        this.myMask.show();

        // Gather the data...
        var masasEntry = this._generateEntry();

        // Gather the attachments...
        var attachments = [];

        if( !this.attachmentPanel.gridAttachments.disabled )
        {
            for( var attCtr = 0; attCtr < this.attachmentPanel._attachmentStore.getCount(); attCtr++ )
            {
                var curAttachment = this.attachmentPanel._attachmentStore.getAt( attCtr );
                var newAttachment = new MASAS.Attachment();
                newAttachment.uri = curAttachment.data.fileName;
                newAttachment.title = curAttachment.data.title;
                newAttachment.contentType = curAttachment.data.type;
                newAttachment.file = curAttachment.data.fileObject;

                attachments.push( newAttachment );
            }
        }

        var entry = {
            masasEntry: masasEntry,
            state: 'new',
            attachments: attachments,
            editUrl: '',
            deleteAttachments: false
        };

        if( this._origEntry != null ) {
            entry.state = 'update';
            entry.editUrl = this.entryLink;
            if( this.attachmentPanel._origAttachmentCount > 0 &&
                !this.attachmentPanel.gridAttachments.disabled &&
                entry.attachments.length == 0 )
            {
                entry.deleteAttachments = true;
            }
        }

        // Publish...
        App.masasPublisher.publishEntry(
            entry,
            function( msg, entry ) {
                // success...
                entryWindow.myMask.hide();
                entryWindow.close();
            },
            function( failureMsg ) {
                entryWindow.enable();
                // Failed...
                entryWindow.myMask.hide();
                Ext.Msg.alert( 'Publish failed', 'Publish failed: <br>' +  failureMsg );
            }
        );
    }
    else
    {
        var strMessages = generalValid.messages.join( '</br>' );
        strMessages += '</br>' + locationValid.messages.join( '</br>' );

        var message = "Some required fields are missing:</br></br>" + strMessages + "</br>";

        Ext.Msg.alert( 'Error', message );
    }

};

mApp.EntryWindow.prototype._onCancelClick = function() {
    this.close();
};

mApp.EntryWindow.prototype._populateForm = function( entry ) {
    this.generalPanel._populateForm( entry );
    this.locationPanel._populateForm( entry );
    this.attachmentPanel._populateForm( entry );
    this.permissionPanel._populateForm( entry );
};

mApp.EntryWindow.prototype._getBasePubEntry = function() {
    var basePubEntry = {
        '@xmlns': 'http://www.w3.org/2005/Atom',
        '@xmlns:georss': 'http://www.georss.org/georss',
        category: [{
            '@label': 'Status',
            '@scheme': 'masas:category:status',
            '@term': 'Test'
        },
            {
                '@label': 'Icon',
                '@scheme': 'masas:category:icon',
                '@term': 'other'
            }
        ],
        title: {'@type': 'xhtml', div: {'@xmlns': 'http://www.w3.org/1999/xhtml',
            div: [{'@xml:lang': 'en', '#text': ''},
                {'@xml:lang': 'fr', '#text': ''}]
        }
        },
        content: {'@type': 'xhtml', div: {'@xmlns': 'http://www.w3.org/1999/xhtml',
            div: [{'@xml:lang': 'en', '#text': ''},
                {'@xml:lang': 'fr', '#text': ''}]
        }
        }
    };

//    if( this._origEntry != null ) {
//        basePubEntry = this._origEntry;
//    }

    return basePubEntry;
}

mApp.EntryWindow.prototype._generateEntry = function () {
    // Utils object...
    var commonUtils = mApp.CommonUtils.getInstance();

    // initial Atom data to populate entry with, customize as necessary
    var entry = this._getBasePubEntry();

    // Category - Status...
    var catStatus = this.generalPanel.cboStatus.getValue();
    entry.category[0]['@term'] = catStatus;

    // Category - Icon...
    entry.category[1]['@term'] = this.generalPanel._entrySymbol;

    // Category - Certainty (optional)...
    var catCertainty = this.generalPanel.cboCertainty.getValue();
    if( catCertainty )
    {
        entry.category.splice( 0, 0, {
            '@label': 'Certainty',
            '@scheme': 'masas:category:certainty',
            '@term': catCertainty.getGroupValue()
        } );
    }

    // Category - Severity (optional)...
    var catSeverity = this.generalPanel.cboSeverity.getValue();
    if( catSeverity )
    {
        entry.category.splice( 0, 0, {
            '@label': 'Severity',
            '@scheme': 'masas:category:severity',
            '@term': catSeverity.getGroupValue()
        } );
    }

    // Category - Category (optional)...
    var catCategory = this.generalPanel.cboCategory.getValue();
    if( catCategory )
    {
        entry.category.splice(0, 0, {
            '@label': 'Category',
            '@scheme': 'masas:category:category',
            '@term': catCategory
        });
    }

    // Title (en)...
    var enTitleDiv = entry.title.div.div[0];
    enTitleDiv['#text'] = commonUtils.encodeXML( this.generalPanel.txtTitleEn.getValue() );

    // Content (en)...
    var enContentDiv = entry.content.div.div[0];
    var enContent = this.generalPanel.txtContentEn.getValue();
    if( enContent ) {
        enContentDiv['#text'] = commonUtils.encodeXML( enContent );
    }

    if( this.generalPanel.btnToggleFrench.pressed == false ) {
        // remove the french divs by converting from an array of two objects to just one
        entry.title.div.div = enTitleDiv;
        entry.content.div.div = enContentDiv;
    }
    else
    {
        // Title (fr)...
        var frTitleDiv = entry.title.div.div[1];
        frTitleDiv['#text'] = commonUtils.encodeXML( this.generalPanel.txtTitleFr.getValue() );

        // Content (fr)...
        var frContentDiv = entry.content.div.div[1];
        var frContent = this.generalPanel.txtContentFr.getValue();
        if( frContent ) {
            frContentDiv['#text'] = commonUtils.encodeXML( frContent );
        }
    }

    // Related links...
    var links = [];
    this.generalPanel.relatedLinkStore.each( function(rec) {
        links.push( {
            '@href': commonUtils.encodeXML( rec.data.url ),
            '@rel': 'related',
            '@title': commonUtils.encodeXML( rec.data.title ),
            '@type': rec.data.type
        } );
    } );

    if( links.length === 1 ) {
        entry.link = links[0];
    }
    else if( links.length > 1 ) {
        entry.link = links;
    }

    // Location...
    var geoType = this.locationPanel.getGeoRSS();
    entry['georss:' + geoType.type ] = geoType.location.replace(/,/g, ' ');

    // Effective Date/Time...
    if( this.generalPanel.rdoEffectiveNow.checked == false )
    {
        var effectiveDT = this.generalPanel.dateEffective.getValue();
        var effectiveTM = this.generalPanel.timeEffective.getValue();

        var strTime = effectiveTM.split(":");
        var effectiveDate = new Date( effectiveDT.getFullYear(), effectiveDT.getMonth(), effectiveDT.getDate(), strTime[0], strTime[1] );
        entry['met:effective'] = effectiveDate.toISOString();
        entry['@xmlns:met'] = 'masas:experimental:time';
    }

    // Expires Date/Time...
    var expiresDate = null;

    if( this.generalPanel.rdoExpiresIn.checked )
    {
        var expiresInt = this.generalPanel.cboExpiresInt.getValue();
        expiresDate = new Date();
        expiresDate.setHours( expiresDate.getHours() + expiresInt );
    }
    else
    {
        var expiresDT = this.generalPanel.dateExpires.getValue();
        var expiresTM = this.generalPanel.timeExpires.getValue();

        var strTime = expiresTM.split(":");
        expiresDate = new Date( expiresDT.getFullYear(), expiresDT.getMonth(), expiresDT.getDate(), strTime[0], strTime[1] );
    }

    entry['age:expires'] = expiresDate.toISOString();
    // add age namespace
    entry['@xmlns:age'] = 'http://purl.org/atompub/age/1.0';

    // Permissions...
    var permissionValue = this.permissionPanel.grpPermissions.getValue();
    var permGroupValue = permissionValue.getGroupValue();

    if( permGroupValue !== 'user' )
    {
        entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        entry['@xmlns:mec'] = 'masas:extension:control';
    }

    switch( permGroupValue )
    {
        case 'group':
            entry['app:control'] = { 'mec:update': App.masasHub.userData.organizationID };
            break;
        case 'all':
            entry['app:control'] = {'mec:update': 'all'};
            break;
    }

    // Status = Draft...
    if( catStatus === 'Draft')
    {
        if( entry['app:control'] )
        {
            entry['app:control']['app:draft'] = 'yes';
        }
        else
        {
            entry['app:control'] = {'app:draft': 'yes'};
            entry['@xmlns:app'] = 'http://www.w3.org/2007/app';
        }
    }

    return entry;
};

mApp.EntryWindow.prototype._setupMapLayers = function() {
//    var defaultMapStyle = new OpenLayers.Style({
//        pointRadius: 8,
//        fillOpacity: 0.5,
//        fillColor: '${myFillColor}',
//        strokeOpacity: 0.7,
//        strokeColor: '${myStrokeColor}',
//        strokeWidth: '${myStrokeWidth}'
//    }, {
//        context: {
//            myStrokeWidth: function (feature) {
//                if (feature.geometry &&
//                    feature.geometry instanceof OpenLayers.Geometry.LineString) {
//                    // make lines easier to see by being thicker
//                    return 6;
//                }
//                return 2;
//            },
//            myStrokeColor: function (feature) {
//                if (feature.geometry &&
//                    feature.geometry instanceof OpenLayers.Geometry.LineString) {
//                    // lines are shown in their respective colour
//                    if (feature.attributes.colour) {
//                        var colour = feature.attributes.colour.toLowerCase();
//                        if (POST.Map.category_colour_map[colour]) {
//                            return POST.Map.category_colour_map[colour];
//                        }
//                    }
//                }
//                // black is the default for all other geometries as its
//                // used to outline the fill colour
//                return '#000000';
//            },
//            myFillColor: function (feature) {
//                if (feature.attributes.colour) {
//                    var colour = feature.attributes.colour.toLowerCase();
//                    if (POST.Map.category_colour_map[colour]) {
//                        return POST.Map.category_colour_map[colour];
//                    }
//                }
//                // OpenLayers default fill colour
//                return '#ee9900';
//            }
//        }
//    });

    // Create our drawing layer...
    this.drawLayer = new OpenLayers.Layer.Vector( 'DrawGeometry', {
        isBaseLayer: false,
        visibility: true
//        styleMap: new OpenLayers.StyleMap({
//            'default': defaultMapStyle
//        })
    });

    // Add the layer...
    this.map.addLayer( this.drawLayer );
};

mApp.EntryWindow.prototype._removeMapLayers = function() {
    if( this.drawLayer != null ) {
        this.drawLayer.destroy();
    }
};

mApp.EntryWindow.prototype._setupMapControllers = function() {

    // Draw Point Controller...
    this.drawPointController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Point,
        {
            geoType: 'point',
            entryWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    // Draw Line Controller...
    this.drawLineController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Path,
        {
            geoType: 'line',
            entryWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    // Draw Polygon Controller...
    this.drawPolygonController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Polygon,
        {
            geoType: 'polygon',
            entryWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    this.drawCircleController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.RegularPolygon,
        {
            geoType: 'circle',
            entryWindow: this,
            featureAdded: this.locationPanel._onNewFeature,
            handlerOptions: { snapAngle: 5.625, sides: 64, irregular: false }
        }
    );

    // Draw Box Controller...
    this.drawBoxController  = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.RegularPolygon,
        {
            geoType: 'box',
            entryWindow: this,
            featureAdded: this.locationPanel._onNewFeature,
            handlerOptions: { sides: 4, irregular: true }
        }
    );

    // Modify Feature Controller...
    this.modifyFeatureController = new OpenLayers.Control.ModifyFeature( this.drawLayer );

    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.drawBoxController,
        this.modifyFeatureController
    ];

    this.map.addControls( controls );
};

mApp.EntryWindow.prototype._removeMapControllers = function() {
    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.drawBoxController,
        this.modifyFeatureController
    ];

    for( var index = 0; index < controls.length; index++ )
    {
        var curControl = controls[index];
        if( curControl.active ) {
            curControl.deactivate();
        }

        this.map.removeControl( curControl );
    }
};

mApp.EntryWindow.prototype._loadEntryFromHub = function( entryLink ) {
    var entry = undefined;

    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();

    // sync loading these urls
    var entryRequest = new OpenLayers.Request.GET({
        url: entryLink,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': App.settings.currentHub.accessCode,
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: App.settings.appSettings.proxyUrl
    });

    if (entryRequest.status !== 200) {
        mask.hide();
        alert('Unable to load Entry.');
        return entry;
    }
    var xml_doc = mApp.CommonUtils.getInstance().parseXML(entryRequest.responseText);
    if (!xml_doc) {
        mask.hide();
        alert('Unable to parse Entry.');
        return entry;
    }

    var jsonEntry = xmlJsonClass.xml2json(xml_doc, '  ');
    try {
        var jsonObj = JSON.parse( jsonEntry );
        entry = jsonObj.entry;
    } catch (err) {
        mask.hide();
        alert('Unable to parse Atom Entry.');
        return entry;
    }

    // advance to the next card
    mask.hide();

    return entry;
};
