mApp.AttachmentWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        this.cancelled = true;
        this.fileTitle = "";
        this.fileName = "";
        this.fileObject = null;

        var btnSelectFile = new Ext.ux.form.FileUploadField({
            buttonOnly: true,
            attachmentWindow: this,
            listeners: {
                'render': function( ct, position ) {
                    this.fileInput.on({
                        scope: this,
                        change: this.attachmentWindow._onFileSelected
                    });
                }
            }

        });

        Ext.apply( this, {

            title: 'Add Attachment',
            layout: 'fit',
            width: 500,
            height: 160,
            minWidth: 300,
            minHeight: 100,
            closeAction: 'close',
            plain: true,
            modal: true,

            items: [
                {
                    xtype: 'panel',
                    ref: 'pnlMain',
                    layout: 'form',
                    padding: 10,
                    plain: true,
                    border: false,
                    hideLabels: false,
                    labelAlign: 'right',
                    labelWidth: 65,
                    labelPad: 5,
                    defaults: {
                        labelSeparator: ':',
                        msgTarget: 'side'
                    },
                    items: [

                        {
                            xtype: 'textfield',
                            fieldLabel: 'Title',
                            ref: '../txtTitle',
                            anchor: '95%',
                            allowBlank: false,
                            minLength: 1,
                            minLengthText: 'A Title is required',
                            blankText: 'A Title is required'
                        },
                        {
                            xtype: 'compositefield',
                            fieldLabel: 'File Name',
                            anchor: '95%',
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'URL',
                                    ref: '../../txtFileName',
                                    flex: 1,
                                    allowBlank: false,
                                    readOnly: true
                                },
                                btnSelectFile
                            ]

                        }

                    ],
                    listeners: {
                        scope: this,
                        'beforeadd': this._onValidateRequired
                    }
                }
            ],

            buttons: [
                {

                    text: "OK",
                    disabled: false,
                    scope: this,
                    handler: this._onOkClick
                },
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                }
            ]
        });

        mApp.AttachmentWindow.superclass.initComponent.call( this );
    }

});

mApp.AttachmentWindow.prototype._onFileSelected = function( evt ) {
    this.attachmentWindow.fileObject = evt.target.files[0];
    this.attachmentWindow.txtFileName.setValue( evt.target.files[0].name );
};

mApp.AttachmentWindow.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.AttachmentWindow.prototype.validate = function() {
    var foundInvalidItem = false;
    this.cascade( function( item ) {
        if( item.isFormField && !item.disabled )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
            }
        }
    });

    return !foundInvalidItem;
};

mApp.AttachmentWindow.prototype._onOkClick = function() {
    if( this.validate() )
    {
        this.cancelled = false;
        this.fileTitle = this.txtTitle.getValue();
        this.fileName = this.txtFileName.getValue();

        this.close();
    }
};

mApp.AttachmentWindow.prototype._onCancelClick = function() {
    this.cancelled = true;
    this.fileTitle = "";
    this.fileName = "";
    this.fileObject = null;

    this.close();
};
