mApp.LinkWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        this.cancelled = true;
        this.linkTitle = "";
        this.linkUrl = "";
        this.linkContentType = "";

        Ext.apply( this, {

            title: 'Add Link',
            layout: 'fit',
            width: 500,
            height: 160,
            minWidth: 300,
            minHeight: 100,
            closeAction: 'close',
            plain: true,
            modal: true,

            items: [
                {
                    xtype: 'panel',
                    ref: 'pnlLink',
                    layout: 'form',
                    padding: 10,
                    plain: true,
                    border:false,
                    hideLabels: false,
                    labelAlign: 'right',
                    labelWidth: 40,
                    labelPad: 5,
                    defaults: {
                        labelSeparator: ':',
                        msgTarget: 'side'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Title',
                            ref: '../txtLinkTitle',
                            anchor: '95%',
                            allowBlank: false,
                            minLength: 3,
                            minLengthText: 'A Title is required',
                            blankText: 'A Title is required'
                        },
                        {
                            xtype: 'combo',
                            fieldLabel: 'Type',
                            ref: '../cboLinkType',
                            anchor: '95%',
                            allowBlank: false,
                            editable: false,
                            width: 200,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            valueField: 'value',
                            value: 'text/html',
                            store: new Ext.data.ArrayStore({
                                fields: ['name', 'value'],
                                data: [
                                    ['Web Page', 'text/html'],
                                    ['Web Map Service', 'application/vnd.ogc.wms'],
                                    ['Tile Map Service', 'application/vnd.osgeo.tms']
                                ]
                            })
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'URL',
                            ref: '../txtLinkUrl',
                            anchor: '95%',
                            allowBlank: false,
                            minLength: 5,
                            minLengthText: 'A URL is required',
                            blankText: 'A URL is required',
                            vtype: 'url',
                            validator: function (val) {
                                // the ExtJS URL validation doesn't check for spaces
                                if (val.search(/\s/) !== -1) {
                                    return 'Blank spaces are not allowed in a URL';
                                }
                                return true;
                            }
                        }
                    ],
                    listeners: {
                        scope: this,
                        'beforeadd': this._onValidateRequired
                    }
                }
            ],

            buttons: [
                {

                    text: "OK",
                    disabled: false,
                    scope: this,
                    handler: this._onOkClick
                },
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                }
            ],

            listeners: {
                scope: this,
                beforeshow: this._beforeShow
            }
        });

        mApp.LinkWindow.superclass.initComponent.call( this );
    }

});

mApp.LinkWindow.prototype._beforeShow = function() {
    if( 'requireContentType' in this )
    {
        this.cboLinkType.setVisible( false );
        this.cboLinkType.setDisabled( true );
    }
};

mApp.LinkWindow.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.LinkWindow.prototype.validate = function() {
    var foundInvalidItem = false;
    this.cascade( function( item ) {
        if( item.isFormField && !item.disabled )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
            }
        }
    });

    return !foundInvalidItem;
};

mApp.LinkWindow.prototype._onOkClick = function() {
    if( this.validate() )
    {
        this.cancelled = false;
        this.linkTitle = this.txtLinkTitle.getValue();
        this.linkUrl = this.txtLinkUrl.getValue();
        this.linkContentType = this.cboLinkType.getValue();

        this.close();
    }
};

mApp.LinkWindow.prototype._onCancelClick = function() {
    this.cancelled = true;
    this.linkTitle = "";
    this.linkUrl = "";
    this.linkContentType = "";

    this.close();
};
