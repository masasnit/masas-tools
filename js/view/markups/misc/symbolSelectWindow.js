mApp.SymbolSelectWindow = Ext.extend( Ext.Window , {

    initComponent: function() {
        selectWindow = this;

        this.curSelectionNode = null;
        this.curSelectionSymbol = "other";
        this.curSelectionSymbolValue = "other";
        this.curSelectionSymbolUrl = "";
        this.curSelectionSymbolText = "Please select an Entry Type";

        this._seviceSymbols = App.settings.appSettings.serviceSymbols;

        this._htmlPreviewTemplate = '<div class="symbolPreviewBox_64"><img class="symbolPreviewImage" src="{0}" alt="Symbol not available."></div>';
        var htmlPreview = this._htmlPreviewTemplate.replace( '{0}', this._seviceSymbols + "/other/large.png" );

        var symbolStore = null;
        if( this.symbolLibrary !== undefined && this.symbolLibrary === "alerts" ) {
            symbolStore = App.dataStoreMgr.alertSymbolStore.symbols
        }
        else {
            symbolStore = App.dataStoreMgr.entrySymbolStore.symbols
        }


        var entrySymbolTreeLoader = new Ext.tree.TreeLoader({
            preloadChildren: true,
            // fix a preloadChildren bug, was to be fixed in ExtJS 3.3.2 but
            // doesn't appear to be as of 3.4.0, old ticket URL not longer working
            // http://code.extjs.com:8080/ext/ticket/1430
            load : function (node, callback, scope) {
                if (this.clearOnLoad) {
                    while (node.firstChild) {
                        node.removeChild(node.firstChild);
                    }
                }
                if (this.doPreload(node)) { // preloaded json children
                    this.runCallback(callback, scope || node, [node]);
                } else if (this.directFn || this.dataUrl || this.url) {
                    // MB
                    if (this.preloadChildren) {
                        if (typeof(callback) !== 'function') {
                            callback = Ext.emptyFn;
                        }
                        callback = callback.createInterceptor(function (node) {
                            for (var i = 0; i < node.childNodes.length; i++) {
                                this.doPreload(node.childNodes[i]);
                            }
                        }, this);
                    }
                    // end-MB
                    this.requestData(node, callback, scope || node);
                }
            }
        });

        this._treeSymbols = new Ext.tree.TreePanel( {
            autoScroll: true,
            animate: true,
            border: true,
            width: '100%',
            flex: 1,
            loader: entrySymbolTreeLoader,
            root: {
                nodeType: 'async',
                text: 'Root Node',
                children: symbolStore
            },
            rootVisible: false,
            selectWindow: selectWindow,
            listeners: {
                click: selectWindow._onNodeClicked
            }
        });

        this._filter = new Ext.ux.tree.TreeFilterX( this._treeSymbols );

        Ext.apply( this, {
            title: "Select an Entry Type",
            layout: 'vbox',
            width: 400,
            height: 350,
            minWidth: 400,
            minHeight: 300,
            closeAction: 'close',
            plain: true,
            modal: true,
            tbar: [
                'Search:',
                {
                    xtype: 'trigger',
                    width: 200,
                    triggerClass: 'x-form-clear-trigger',
                    onTriggerClick: function () {
                        this.setValue('');
                        selectWindow._filter.clear();
                    },
                    enableKeyEvents: true,
                    scope: this,
                    listeners: {
                        keyup: {buffer: 150, fn: function (field, e) {
                            if (Ext.EventObject.ESC === e.getKey()) {
                                field.onTriggerClick();
                            } else {
                                var val = this.getRawValue();
                                var re = new RegExp('.*' + val + '.*', 'i');
                                selectWindow._filter.clear();
                                selectWindow._filter.filter(re, 'text');
                            }
                        }}
                    }
                },
                '->',
                {
                    xtype: 'button',
                    iconCls: 'iconTreeExpand',
                    text: 'Expand',
                    tooltip: 'Expand the entire tree',
                    handler: function () { selectWindow._treeSymbols.expandAll(); }
                },
                '-',
                {
                    xtype: 'button',
                    iconCls: 'iconTreeCollapse',
                    text: 'Collapse',
                    tooltip: 'Collapse the entire tree',
                    handler: function () { selectWindow._treeSymbols.collapseAll(); }
                }
            ],

            items: [
                {
                    title: 'Current Selection',
                    width: '100%',
                    layout: 'hbox',
                    plain: true,
                    items: [
                        {
                            xtype: 'displayfield',
                            ref: '../imagePreview',
                            height: 72,
                            html: htmlPreview
                        },
                        {
                            xtype: 'container',
                            height: 72,
                            flex: 1,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    ref: '../../txtSymbolDesc',
                                    value: this.curSelectionSymbolText
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Entry Types',
                    width: '100%',
                    layout: 'vbox',
                    flex: 1,
                    plain: true,
                    items: [ this._treeSymbols ]
                }
            ],

            buttons: [
                {
                    text: "OK",
                    ref: '../btnOK',
                    disabled: true,
                    scope: this,
                    handler: this._onOKClick
                },
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                }
            ],

            listeners: {
                scope: this,
                beforeshow: this._beforeShow
            }
        });

        mApp.SymbolSelectWindow.superclass.initComponent.call( this );
    }

});

mApp.SymbolSelectWindow.prototype._onNodeClicked = function( node ) {
    selectWindow.curSelectionSymbol = "other";
    selectWindow.curSelectionSymbolUrl = selectWindow._seviceSymbols + '/other/large.png';
    selectWindow.curSelectionSymbolText = "Please select an Entry Type";

    if( node.attributes.term !== undefined )
    {
        selectWindow.curSelectionNode = node;
        selectWindow.curSelectionSymbol = node.attributes.term;
        selectWindow.curSelectionSymbolUrl = selectWindow._seviceSymbols + '/' + node.attributes.term + '/large.png';
        selectWindow.curSelectionSymbolText = node.attributes.text;
        selectWindow.btnOK.setDisabled( false );
    }
    else {
        selectWindow.btnOK.setDisabled( true );
    }

    selectWindow.imagePreview.setValue( selectWindow._htmlPreviewTemplate.replace( '{0}', selectWindow.curSelectionSymbolUrl  ) );
    selectWindow.txtSymbolDesc.setValue( selectWindow.curSelectionSymbolText );

};

mApp.SymbolSelectWindow.prototype._beforeShow = function() {
    this._onTreeDataLoaded();
};

mApp.SymbolSelectWindow.prototype._onTreeDataLoaded = function() {

    var foundNode = null;

    // NOTE: the preloadChildren patch (in the TreeLoader) is currently needed for findChild() to work.
    if( this.symbolLibrary && this.symbolLibrary === 'alerts' ) {
        foundNode = this._treeSymbols.root.findChild( 'value', this.curSelectionSymbolValue, true );
    }
    else {
        foundNode = this._treeSymbols.root.findChild( 'term', this.curSelectionSymbol, true );
    }

    if( foundNode )
    {
        this.curSelectionSymbol = foundNode.attributes.term;
        this.curSelectionSymbolUrl = this._seviceSymbols + '/' + this.curSelectionSymbol + '/large.png';
        this._treeSymbols.expandPath( foundNode.getPath(), null,
            function( e_success, e_last ) {
                if( e_success ) {
                    e_last.select();
                }
            }
        );

        selectWindow.imagePreview.setValue( this._htmlPreviewTemplate.replace( '{0}', this.curSelectionSymbolUrl  ) );
        selectWindow.txtSymbolDesc.setValue( foundNode.attributes.text );
        selectWindow.btnOK.setDisabled( false );
    }
};

mApp.SymbolSelectWindow.prototype._onOKClick = function() {
    this.close();
};

mApp.SymbolSelectWindow.prototype._onCancelClick = function() {
    this.curSelectionSymbol = "";
    this.curSelectionSymbolText = "";

    this.close();
};
