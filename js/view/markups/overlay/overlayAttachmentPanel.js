mApp.OverlayAttachmentPanel = Ext.extend( Ext.Panel , {

    initComponent: function() {

        this._attachmentStore = new Ext.data.ArrayStore({
            fields: [ 'title', 'fileName', 'type', 'fileObject' ],
            data: []
        });


        Ext.apply( this, {
            title: 'Attachments',
            layout: 'vbox',
            padding: 10,
            autoScroll: true,
            border: false,
            height: '100%',
            defaultType: 'textfield',

            items: [
                {
                    xtype: 'displayfield',
                    value: 'Add up to five (5) attachments to this Entry.  The maximum size of all attachments cannot exceed 10 MB.',
                    margins: { top:0, right:0, bottom:5, left:0 }
                },
                {
                    xtype: 'button',
                    ref: 'btnAdd',
                    width: 75,
                    text: 'Add',
                    tooltip: 'Add an attachment to this Entry',
                    margins: { top:0, right:0, bottom:5, left:0 },
                    scope: this,
                    handler: this._addAttachment
                },
                {
                    xtype: 'grid',
                    ref: 'gridAttachments',
                    fieldLabel: 'Attachments',
                    width: '100%',
                    flex: 1,
                    allowBlank: true,
                    autoScroll: true,
                    stripeRows: true,
                    columnLines: true,
                    header: false,
                    store: this._attachmentStore,
                    autoExpandColumn: '1',
                    viewConfig: {
                        headersDisabled: true,
                        hideSortIcons: true
                    },
                    columns: [
                        { header: 'Title', dataIndex: 'title', width: 200 },
                        { header: 'File Name', dataIndex: 'fileName' },
                        { header: 'Type', dataIndex: 'type' },
                        {
                            xtype: 'actioncolumn',
                            width: 28,
                            items: [
                                {
                                    iconCls: 'btnDeleteRelatedLinkIcon',
                                    tooltip: 'Delete Attachment',
                                    scope: this,
                                    handler: this._removeAttachment
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        mApp.OverlayAttachmentPanel.superclass.initComponent.call( this );
    }

});

mApp.OverlayAttachmentPanel.prototype._addAttachment = function() {
    if( this._attachmentStore.getCount() < 5 )
    {
        this.attachWnd = new mApp.AttachmentWindow();
        this.attachWnd.on( 'close', this._onAttachmentWindowClose, this );

        this.attachWnd.show();
    }
    else
    {
        Ext.Msg.alert( 'Info', 'The maximum number of attachment has been reached!' );
    }
};

mApp.OverlayAttachmentPanel.prototype._onAttachmentWindowClose = function( panel ) {
    if( !panel.cancelled )
    {
        var newRecord = new this._attachmentStore.recordType( {
            title: panel.fileTitle,
            fileName: panel.fileName,
            fileObject: panel.fileObject,
            type: panel.fileObject.type
        });

        this._attachmentStore.add( newRecord );
        this._attachmentStore.commitChanges();
    }

    this.attachWnd.destroy();
    this.attachWnd = null;
}

mApp.OverlayAttachmentPanel.prototype._removeAttachment = function( grid, rowIndex, colIndex, item, event ) {
    this._attachmentStore.removeAt( rowIndex );
    this._attachmentStore.commitChanges();
};

mApp.OverlayAttachmentPanel.prototype.resetPanel = function() {
    var form = this.getForm();
    form.reset();
};

mApp.OverlayAttachmentPanel.prototype._populateForm = function( overlay ) {
    // TODO: load attachements...
};
