mApp.OverlayGeneralPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        this._seviceSymbols = App.settings.appSettings.serviceSymbols;
        this._htmlPreviewTemplate = '<div class="symbolPreviewBox_48"><img class="symbolPreviewImage" src="{0}" alt="Symbol not available."></div>';
        var htmlPreview = this._htmlPreviewTemplate.replace( '{0}', this._seviceSymbols + "/other/large.png" );

        // Clone the Overlay symbols store...
        var records = [];
        App.dataStoreMgr.overlaySymbolStore.each( function( r ) {
            records.push( r.copy() );
        });

        var symbolStore = new Ext.data.Store({
            recordType: App.dataStoreMgr.overlaySymbolStore.recordType
        });
        symbolStore.add( records );

        this.relatedLinkStore = new Ext.data.ArrayStore({
            fields: ['title', 'url'],
            data: []
        });

        // Added this for now, clean up later...
        this.on( 'beforeadd', this._onValidateRequired );

        Ext.apply( this, {
            title: 'General',
            padding: 10,
            autoScroll: true,
            border:false,
            defaultType: 'textfield',

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 75,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },

            items: [
                {
                    xtype: 'compositefield',
                    fieldLabel: '',
                    allowBlank: true,
                    height: 56,
                    items: [
                        {
                            xtype: 'displayfield',
                            ref: '../imagePreview',
                            height: 56,
                            html: htmlPreview
                        },
                        {
                            xtype: 'container',
                            height: 48,
                            flex: 1,
                            layout: 'hbox',
                            layoutConfig: {
                                align: 'middle'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    ref: '../../../lblSymbolDesc',
                                    value: 'Other'
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Collection',
                    ref: 'cboType',
                    allowBlank: false,
                    width: 200,
                    editable: false,
                    forceSelection: true,
                    triggerAction: 'all',
                    mode: 'local',
                    displayField: 'name',
                    value: 'hazard',
                    valueField: 'type',
                    tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
                    store: new Ext.data.ArrayStore({
                        fields: ['type', 'name', 'tip'],
                        data: [
                            [ 'hazard', 'Hazard', 'Hazard' ],
                            [ 'resource', 'Resource', 'Resource' ],
                            [ 'infrastructure', 'Infrastructure', 'Infrastructure' ]
                        ]
                    }),
                    overlayGeneralPanel: this,
                    listeners : {
                        'select': this._onCboTypeSelect
                    }
                },
                {
                    xtype: 'combo',
                    fieldLabel: 'Item',
                    ref: 'cboSubType',
                    allowBlank: false,
                    width: 200,
                    editable: false,
                    forceSelection: true,
                    triggerAction: 'all',
                    lastQuery: '', // NOTE: This is needed for the filter to work on load.
                    mode: 'local',
                    displayField: 'valueEn',
                    valueField: 'subType',
                    tpl: '<tpl for="."><div ext:qtip="{valueEn}" class="x-combo-list-item">{valueEn}&nbsp;</div></tpl>',
                    store: symbolStore,
                    overlayGeneralPanel: this,
                    listeners : {
                        'render': this._onCboSubTypeRender,
                        'select': this._onCboSubTypeSelect
                    }
                },
                {
                    xtype: 'checkbox',
                    fieldLabel: 'Active',
                    ref: 'chkActive',
                    hidden: true,
                    width: '95%'
                },
                {
                    fieldLabel: 'Title',
                    ref: 'txtTitleEn',
                    width: '95%',
                    minLength: 1,
                    minLengthText: 'A good title should say what and where',
                    blankText: 'An Overlay requires a title value',
                    allowBlank: false
                },
                {
                    xtype: 'textarea',
                    fieldLabel: 'Description',
                    ref: 'txtDescEn',
                    height: 80,
                    width: '95%',
                    allowBlank: true,
                    flex: 1
                },
                {
                    xtype: 'fieldset',
                    title: 'Related Links',
                    autoHeight: true,
                    collapsible: true,
                    collapsed: true,
                    titleCollapse: true,
                    items: [
                        {
                            xtype: 'panel',
                            anchor: '100%',
                            layout: 'vbox',
                            height: 150,
                            plain: true,
                            frame: false,
                            border: false,
                            items: [
                                {
                                    xtype: 'displayfield',
                                    height: 24,
                                    value: 'Link(s) to information related to this Overlay.'
                                },
                                {
                                    xtype: 'button',
                                    width: 75,
                                    text: 'Add',
                                    tooltip: 'Add related links to this Overlay',
                                    margins: { top:0, right:0, bottom:5, left:0 },
                                    scope: this,
                                    handler: this._addRelatedLink
                                },
                                {
                                    xtype: 'grid',
                                    fieldLabel: 'Links',
                                    width: '100%',
                                    flex: 1,
                                    allowBlank: true,
                                    autoScroll: true,
                                    stripeRows: true,
                                    columnLines: true,
                                    header: false,
                                    hideHeaders: true,
                                    store: this.relatedLinkStore,
                                    autoExpandColumn: '1',
                                    columns: [
                                        { dataIndex: 'title', resizable: true, width: 100 },
                                        { dataIndex: 'url', resizable: true },
                                        {
                                            xtype: 'actioncolumn',
                                            width: 24,
                                            items: [
                                                {
                                                    iconCls: 'btnDeleteRelatedLinkIcon',
                                                    tooltip: 'Delete Link',
                                                    scope: this,
                                                    handler: this._removeRelatedLink
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]

                        }
                    ]

                }
            ]
        });

        mApp.OverlayGeneralPanel.superclass.initComponent.call( this );
    }

});

mApp.OverlayGeneralPanel.prototype.resetPanel = function() {
    var form = this.getForm();
    form.reset();
};

mApp.OverlayGeneralPanel.prototype._onCboSubTypeRender = function() {
    // Fill in the sub type combo...
    var valueType = this.overlayGeneralPanel.cboType.getValue();
    this.overlayGeneralPanel._updateSubTypeCombo( valueType );
};

mApp.OverlayGeneralPanel.prototype._onCboTypeSelect = function( combo, record, index ) {
    // Fill in the sub type combo...
    combo.overlayGeneralPanel._updateSubTypeCombo( record.data.type );
};

mApp.OverlayGeneralPanel.prototype._onCboSubTypeSelect = function( combo, record, index ) {
    // Fill in the sub type combo...
    var valueType = combo.overlayGeneralPanel.cboType.getValue();
    combo.overlayGeneralPanel._setSymbol( valueType, record.data.subType );
};

mApp.OverlayGeneralPanel.prototype._updateSubTypeCombo = function( selectedType ) {

    // Apply the filter...
    this.cboSubType.store.filter( "type", selectedType );

    if( this.cboSubType.store.getCount() > 0 )
    {
        // Select the first item...
        var symbolData = this.cboSubType.store.getAt(0).data;
        this.cboSubType.setValue( symbolData.subType );

        // Update the symbol...
        this._setSymbol( selectedType, symbolData.subType );
    }
};

mApp.OverlayGeneralPanel.prototype._setSymbol = function( type, subType ) {
    var recordId = this.cboSubType.store.find( "subType", subType );
    if( recordId >= 0 )
    {
        var record = this.cboSubType.store.getAt( recordId );

        var symbol_url = record.data.symbolUrl;
        this.imagePreview.setValue( this._htmlPreviewTemplate.replace( '{0}', symbol_url ) );
        this.lblSymbolDesc.setValue( record.data.valueEn );
    }
};

mApp.OverlayGeneralPanel.prototype._addRelatedLink = function() {
    this.linkWnd = new mApp.LinkWindow( { requireContentType: false } );
    this.linkWnd.on( 'close', this._onLinkWindowClose, this );

    this.linkWnd.show();
};

mApp.OverlayGeneralPanel.prototype._onLinkWindowClose = function( panel ) {
    if( !panel.cancelled )
    {
        var newRecord = new this.relatedLinkStore.recordType( {
            title: panel.linkTitle,
            type: panel.linkContentType,
            url: panel.linkUrl
        });

        this.relatedLinkStore.add( newRecord );
        this.relatedLinkStore.commitChanges();
    }

    this.linkWnd.destroy();
    this.linkWnd = null;
};

mApp.OverlayGeneralPanel.prototype._removeRelatedLink = function( grid, rowIndex, colIndex, item, event ) {
    this.relatedLinkStore.removeAt( rowIndex );
    this.relatedLinkStore.commitChanges();
};

mApp.OverlayGeneralPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.OverlayGeneralPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    this.cascade( function( item ) {
        if( item.isFormField && !item.disabled )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

mApp.OverlayGeneralPanel.prototype._populateForm = function( overlay ) {

    // Title...
    this.txtTitleEn.setValue( overlay.title );

    // Description...
    this.txtDescEn.setValue( overlay.description );

    // Active...
    this.chkActive.setValue( overlay.active );

    // Symbol...
    // Check if the "type" is valid...
    var typeRecId = this.cboType.store.findExact( "type", overlay.type );
    if( typeRecId >= 0 )
    {
        this.cboType.setValue( overlay.type );
        this._updateSubTypeCombo( overlay.type );

        // Check if the "subtype" is valid...
        var subTypeRecId = this.cboSubType.store.findExact( "subType", overlay.subtype );
        if( subTypeRecId >= 0 )
        {
            this.cboSubType.setValue( overlay.subtype );
            this._setSymbol( overlay.type, overlay.subtype );
        }
    }

    // Links...
    if( overlay.links !== undefined )
    {
        for( var linksCtr = 0; linksCtr < overlay.links.length; linksCtr++ )
        {
            var curLink = overlay.links[linksCtr];

            var newRecord = new this.relatedLinkStore.recordType( {
                title: curLink.text,
                url: curLink.url
            });

            this.relatedLinkStore.add( newRecord );
            this.relatedLinkStore.commitChanges();
        }
    }

};