mApp.OverlayLocationPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        var locationPanel = this;
        this._activeControl = null;
        this._currentFeature = null;

        this.overlayWindow.drawLayer.events.on( {
            'featuremodified': function( object ) { locationPanel._onFeatureModified( locationPanel, object ); },
            'afterfeaturemodified': function( object ) { locationPanel._onAfterFeatureModified( locationPanel, object ); }
        });

        Ext.apply( this, {
            title: 'Location',
            padding: 10,
            autoScroll: true,
            border:false,

            hideLabels: false,
            labelAlign: 'right',
            labelWidth: 90,
            labelPad: 5,
            defaults: {
                labelSeparator: ':',
                msgTarget: 'side'
            },

            scope: this,
            listeners: {
                'beforeadd': this._onValidateRequired
            },

            items: [
                {
                    xtype: 'panel',
                    border: false,
                    autoHeight: true,
                    style: '{ margin-bottom: 10px; }',
                    items: [
                        {
                            xtype: 'displayfield',
                            ref: '../txtInstructionField',
                            bodyStyle: 'padding: 10px',
                            value: ''
                        }
                    ]
                },
                {
                    xtype: 'compositefield',
                    fieldLabel: 'Geometry',
                    ref: 'pnlButtons',
                    anchor: '100%',
                    plain: true,
                    border: false,
                    allowBlank: true,
                    items: [
                        {
                            xtype: 'combo',
                            fieldLabel: 'Geometry',
                            ref: '../cboGeometry',
                            width: 150,
                            editable: false,
                            forceSelection: true,
                            triggerAction: 'all',
                            mode: 'local',
                            displayField: 'name',
                            value: 'point',
                            valueField: 'type',
                            tpl: '<tpl for="."><div ext:qtip="{tip}" class="x-combo-list-item">{name}&nbsp;</div></tpl>',
                            store: new Ext.data.ArrayStore({
                                fields: ['type', 'name', 'tip'],
                                data: [
                                    [ 'point', 'Point', 'Draw a point' ]//,
                                    // TODO: Re-enable the other types once the issue with multi-points has been resolved.
//                                    [ 'line', 'Line', 'Draw a line' ],
//                                    [ 'polygon', 'Polygon', 'Draw a polygon' ],
//                                    [ 'circle', 'Circle', 'Draw a circle' ],
//                                    [ 'box', 'Box', 'Draw a box' ]
                                ]
                            }),
                            overlayLocationPanel: this,
                            listeners : {
                                'select': this._onCboGeometrySelect
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'New Drawing',
                            ref: '../btnStartDrawing',
                            scope: this,
                            handler: this._onBtnStartDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Stop Drawing',
                            ref: '../btnStopDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnStopDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Edit Drawing',
                            ref: '../btnEditDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnEditDrawingClick
                        },
                        {
                            xtype: 'button',
                            text: 'Clear Drawing',
                            ref: '../btnClearDrawing',
                            hidden: true,
                            scope: this,
                            handler: this._onBtnClearDrawingClick
                        }
                    ]
                },
                {
                    xtype: 'panel',
                    layout: 'card',
                    ref: 'pnlGeometry',
                    border: false,
                    activeItem: 0,
                    defaults: {
                        bodyStyle: 'padding:10px'
                    },
                    items: [
                        {
                            xtype: 'panel',
                            ref: 'pnlPoint',
                            title: 'Point',
                            layout: 'form',
                            autoHeight: true,
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to draw a new point.',
                                'edit': 'Click and move the selected point the the new location.'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtPointLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtPointLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlLine',
                            title: 'Line',
                            layout: 'form',
                            autoHeight: true,
                            anchor: '100% 100%',
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to start and add additional points.  Double-click the last point to stop drawing.',
                                'edit': 'Click and move the selected vertices to their new location.'
                            },
                            items: [
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Line',
                                    ref: 'txtLineGeometry',
                                    height: 240,
                                    anchor: '95%',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlPolygon',
                            title: 'Polygon',
                            layout: 'form',
                            autoHeight: true,
                            anchor: '100% 100%',
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map to start and add additional points.  Double-click the last point to stop drawing.',
                                'edit': 'Click and move the selected vertices to their new location.'
                            },
                            items: [
                                {
                                    xtype: 'textarea',
                                    fieldLabel: 'Polygon',
                                    ref: 'txtPolygonGeometry',
                                    height: 240,
                                    anchor: '95%',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlCircle',
                            title: 'Circle',
                            layout: 'form',
                            autoHeight: true,
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map and drag the mouse to create the circle.  The location of the click will be the center of the circle.',
                                'edit': 'Click and move the center to change to location.  Click and move the Radius handle to change the size.'
                            },
                            items: [
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtCircleLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtCircleLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Radius (km)',
                                    ref: 'txtCircleRadius',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: '0.0',
                                    regex: /^(?=.+)(?:[1-9]\d*|0)?(?:\.\d+)?$/
                                }
                            ]
                        },
                        {
                            xtype: 'panel',
                            ref: 'pnlBox',
                            title: 'Box',
                            layout: 'form',
                            autoHeight: true,
                            labelAlign: 'right',
                            labelWidth: 80,
                            labelPad: 5,
                            defaults: {
                                labelSeparator: ':',
                                msgTarget: 'side'
                            },
                            scope: this,
                            listeners: {
                                'beforeadd': this._onValidateRequired
                            },
                            instructions: {
                                'new': 'Click on the map and drag the mouse to create the box.',
                                'edit': 'Click and move the corners to modify the box.'
                            },
                            items: [
                                {
                                    xtype: 'displayfield',
                                    bodyStyle: 'padding: 10px',
                                    value: 'Lower Left Point'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtBoxLLLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtBoxLLLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                },
                                {
                                    xtype: 'displayfield',
                                    bodyStyle: 'padding: 10px',
                                    value: 'Upper Right Point'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Latitude',
                                    ref: 'txtBoxURLatitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A',
                                    validator: this._onValidateLatitude
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Longitude',
                                    ref: 'txtBoxURLongitude',
                                    allowBlank: false,
                                    readOnly: true,
                                    value: 'N/A' ,
                                    validator: this._onValidateLongitude
                                }
                            ]
                        }
                    ]
                }

            ]

        });

        mApp.OverlayLocationPanel.superclass.initComponent.call( this );

        this._updateInstructions( 'clear' );
    }

});

mApp.OverlayLocationPanel.prototype._onValidateRequired = function( me, field ) {
    if( !field.labelSeparator ) {
        field.labelSeparator = "";
    }

    if( !field.allowBlank ) {
        field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
    }
    else {
        field.labelSeparator += '<span style="padding-left: 9px;"></span>';
    }
};

mApp.OverlayLocationPanel.prototype._onValidateLatitude = function( value ) {
    if( value === 'N/A' ) {
        return false;
    }

    return true;
};

mApp.OverlayLocationPanel.prototype._onValidateLongitude = function( value ) {
    if( value === 'N/A' ) {
        return false;
    }

    return true;
};

mApp.OverlayLocationPanel.prototype.validate = function() {
    var messages = [];
    var foundInvalidItem = false;
    var title = this.title;

    this.cascade( function( item ) {
        if( item.isFormField && !item.ownerCt.hidden )
        {
            if( !item.validate() ) {
                foundInvalidItem = true;
                messages.push( title + " - " + item.fieldLabel );
            }
        }
    });

    return { valid: !foundInvalidItem, messages: messages };
};

mApp.OverlayLocationPanel.prototype._updateInstructions = function( action ) {
    var drawDoneInstructions = "Click 'Edit Drawing' to modify the geometry, or 'Clear Drawing' to start over.";
    var instructions = "Select a drawing tool to draw a new geometry. Click 'Start Drawing' to begin.";

    switch( action ) {
        case 'geometry':
            break;
        case 'start':
            instructions = this.pnlGeometry.layout.activeItem.instructions.new;
            break;
        case 'edit':
            instructions = this.pnlGeometry.layout.activeItem.instructions.edit;
            break;
        case 'stop':
            if( this._currentFeature != null )
            {
                instructions = drawDoneInstructions;
            }
            else {
                instructions = this.pnlGeometry.layout.activeItem.instructions.new;
            }
            break;
        case 'clear:':
            break;
        case 'drawDone':
            instructions = drawDoneInstructions;
            break;
    }

    this.txtInstructionField.setValue( instructions );
};

mApp.OverlayLocationPanel.prototype._onCboGeometrySelect = function( combo, record, index ) {
    var pnlGeometry = combo.overlayLocationPanel.pnlGeometry;

    switch( combo.getValue() )
    {
        case 'point':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPoint );
            break;
        case 'line':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlLine );
            break;
        case 'polygon':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPolygon );
            break;
        case 'box':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlBox );
            break;
        case 'circle':
            pnlGeometry.layout.setActiveItem( pnlGeometry.pnlCircle );
            break;
    }

    combo.overlayLocationPanel._updateInstructions( 'geometry' );
    combo.overlayLocationPanel.doLayout();
};

mApp.OverlayLocationPanel.prototype._onBtnStartDrawingClick = function() {
    this.btnStartDrawing.setVisible( false );
    this.btnStopDrawing.setVisible( true );
    this.btnClearDrawing.setVisible( true );
    this.cboGeometry.setDisabled( true );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'start' );

    this._clearFeature();

    switch( this.cboGeometry.getValue() )
    {
        case 'point':
            this._activateController( this.overlayWindow.drawPointController );
            break;
        case 'line':
            this._activateController( this.overlayWindow.drawLineController );
            break;
        case 'polygon':
            this._activateController( this.overlayWindow.drawPolygonController );
            break;
        case 'box':
            this._activateController( this.overlayWindow.drawBoxController );
            break;
        case 'circle':
            this._activateController( this.overlayWindow.drawCircleController );
            break;
    }

    this.overlayWindow.collapse();
};

mApp.OverlayLocationPanel.prototype._onBtnStopDrawingClick = function() {
    this.btnStopDrawing.setVisible( false );
    this.btnEditDrawing.setVisible( this._currentFeature != null );
    this.btnStartDrawing.setVisible( this._currentFeature == null );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'stop' );

    this._activateController( null );
};

mApp.OverlayLocationPanel.prototype._onBtnEditDrawingClick = function() {
    this.btnStopDrawing.setVisible( true );
    this.btnEditDrawing.setVisible( false );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'edit' );

    this._activateController( null );

    if( this._currentFeature != null )
    {

        var controller = this.overlayWindow.modifyFeatureController
        controller.standalone = true;
        controller.clickout = false;

        switch( this._currentFeature.geoType )
        {
            case 'point':
                controller.createVertices = false;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                controller.mode |= OpenLayers.Control.ModifyFeature.DRAG;
                break;
            case 'line':
                controller.createVertices = true;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                break;
            case 'polygon':
                controller.createVertices = true;
                controller.mode = OpenLayers.Control.ModifyFeature.RESHAPE;
                break;
            case 'box':
                controller.createVertices = false;
                controller.mode = OpenLayers.Control.ModifyFeature.RESIZE;
                controller.mode |= OpenLayers.Control.ModifyFeature.DRAG;
                break;
            case 'circle':
                controller.createVertices = false;
                controller.mode = OpenLayers.Control.ModifyFeature.RESIZE;
                controller.mode |= OpenLayers.Control.ModifyFeature.DRAG;
                break;
        }

        this._activateController( this.overlayWindow.modifyFeatureController );
        controller.selectFeature( this._currentFeature );
    }
};

mApp.OverlayLocationPanel.prototype._onBtnClearDrawingClick = function() {
    this._activateController( null );

    this._clearFeature();

    this.btnStartDrawing.setVisible( true );
    this.btnStopDrawing.setVisible( false );
    this.btnEditDrawing.setVisible( false );
    this.btnClearDrawing.setVisible( false );
    this.cboGeometry.setDisabled( false );
    this.pnlButtons.doLayout();
    this._updateInstructions( 'clear' );
};

mApp.OverlayLocationPanel.prototype._clearFeature = function() {

    if( this._currentFeature != null ) {
        this.overlayWindow.drawLayer.destroyFeatures( [ this._currentFeature ] );
        this._currentFeature = null;
    }

    this._resetDrawingControls();
};


mApp.OverlayLocationPanel.prototype._activateController = function( controller ) {
    var oldController = this._activeControl;
    this._activeControl = null;

    if( oldController != null ) {
        oldController.deactivate();
    }

    if( controller != null ) {
        controller.activate();
        this._activeControl = controller;
    }
};

mApp.OverlayLocationPanel.prototype._onNewFeature = function( feature ) {

    var locPanel = this.overlayWindow.locationPanel;

    // Stop the controller...
    if( locPanel._activeControl != null ) {
        locPanel._activeControl.deactivate();
    }

    if( this.overlayWindow.collapsed ) {
        this.overlayWindow.toggleCollapse();
    }


    if( locPanel._currentFeature != null ) {
        this.layer.destroyFeatures( [ locPanel._currentFeature ] );
    }

    if( this.geoType === 'circle' )
    {
        // Figure out the radius...
        locPanel._updateRadius( feature );
    }

    feature.geoType = this.geoType;

    locPanel._currentFeature = feature;

    // Update the panel with the new data...
    mApp.OverlayLocationPanel.prototype._updatePanel.call( locPanel, locPanel._currentFeature );

    locPanel.btnStopDrawing.setVisible( false );
    locPanel.btnEditDrawing.setVisible( true );
    locPanel.btnClearDrawing.setVisible( true );
    locPanel.pnlButtons.doLayout();

    mApp.OverlayLocationPanel.prototype._updateInstructions.call( locPanel, 'drawDone' );
};

mApp.OverlayLocationPanel.prototype._onFeatureModified = function( panel, event ) {
    panel._updateRadius( event.feature );
    mApp.OverlayLocationPanel.prototype._updatePanel.call( panel, event.feature );
};

mApp.OverlayLocationPanel.prototype._onAfterFeatureModified = function( panel, event ) {
    panel._updateRadius( event.feature );
    mApp.OverlayLocationPanel.prototype._updatePanel.call( panel, event.feature );
};

mApp.OverlayLocationPanel.prototype._updateRadius = function( feature ) {
    // Figure out the radius...
    var radius = Math.abs( (feature.geometry.bounds.left - feature.geometry.bounds.right) / 2 );
    feature.radius = radius;
};

mApp.OverlayLocationPanel.prototype._updatePanel = function( feature ) {
    this._resetDrawingControls();

    switch( feature.geoType )
    {
        case 'point':
            var latLonPt = new OpenLayers.LonLat( feature.geometry.x, feature.geometry.y );
            latLonPt = latLonPt.transform( feature.layer.map.projection, 'EPSG:4326' );
            this.pnlGeometry.pnlPoint.txtPointLatitude.setValue( latLonPt.lat );
            this.pnlGeometry.pnlPoint.txtPointLongitude.setValue( latLonPt.lon );
            break;
        case 'line':
            var strPoints = this._pointArrayToGeoRSS( feature.geometry.getVertices(), false );
            this.pnlGeometry.pnlLine.txtLineGeometry.setValue( strPoints );
            break;
        case 'polygon':
            var strPoints = this._pointArrayToGeoRSS( feature.geometry.getVertices(), true );
            this.pnlGeometry.pnlPolygon.txtPolygonGeometry.setValue( strPoints );
            break;
        case 'box':
            var lowerLeft = new OpenLayers.LonLat( feature.geometry.bounds.left, feature.geometry.bounds.bottom ).transform( feature.layer.map.projection, 'EPSG:4326' );
            var upperRight = new OpenLayers.LonLat( feature.geometry.bounds.right, feature.geometry.bounds.top ).transform( feature.layer.map.projection, 'EPSG:4326' );
            this.pnlGeometry.pnlBox.txtBoxLLLatitude.setValue( lowerLeft.lat );
            this.pnlGeometry.pnlBox.txtBoxLLLongitude.setValue( lowerLeft.lon );
            this.pnlGeometry.pnlBox.txtBoxURLatitude.setValue( upperRight.lat );
            this.pnlGeometry.pnlBox.txtBoxURLongitude.setValue( upperRight.lon );
            break;
        case 'circle':
            var centerLL = feature.geometry.bounds.getCenterLonLat();
            var latLonPt = new OpenLayers.LonLat( centerLL.lon, centerLL.lat );
            latLonPt = latLonPt.transform( feature.layer.map.projection, 'EPSG:4326' );
            this.pnlGeometry.pnlCircle.txtCircleLatitude.setValue( latLonPt.lat );
            this.pnlGeometry.pnlCircle.txtCircleLongitude.setValue( latLonPt.lon );
            this.pnlGeometry.pnlCircle.txtCircleRadius.setValue( feature.radius / 1000 );
            break;
    }
};

mApp.OverlayLocationPanel.prototype._pointArrayToGeoRSS = function( pointsArray, repeatFirstPoint ) {
    var strPoints = '';
    var curLatLonPoint = null;
    var firstPointStr = null;

    for( var index = 0; index < pointsArray.length; index++ )
    {
        if( index > 0 ) {
            strPoints += ' ';
        }

        curLatLonPoint = new OpenLayers.LonLat( pointsArray[index].x, pointsArray[index].y );
        curLatLonPoint = curLatLonPoint.transform( this.overlayWindow.drawLayer.map.projection, 'EPSG:4326' );
        if( index == 0 ) {
            firstPointStr = curLatLonPoint.lat + ' ' + curLatLonPoint.lon;
        }

        strPoints += curLatLonPoint.lat + ' ' + curLatLonPoint.lon;
    }

    if( repeatFirstPoint ) {
        strPoints += ' ' + firstPointStr;
    }

    return strPoints;
};

mApp.OverlayLocationPanel.prototype._resetDrawingControls = function() {

    // Reset the point panel...
    this.pnlGeometry.pnlPoint.txtPointLatitude.setValue( 'N/A' );
    this.pnlGeometry.pnlPoint.txtPointLongitude.setValue( 'N/A' );

    // Reset the circle panel...
    this.pnlGeometry.pnlCircle.txtCircleLatitude.setValue( 'N/A' );
    this.pnlGeometry.pnlCircle.txtCircleLongitude.setValue( 'N/A' );
    this.pnlGeometry.pnlCircle.txtCircleRadius.setValue( 0.0 );

    // Reset the box panel...
    this.pnlGeometry.pnlBox.txtBoxLLLatitude.setValue( 'N/A' );
    this.pnlGeometry.pnlBox.txtBoxLLLongitude.setValue( 'N/A' );
    this.pnlGeometry.pnlBox.txtBoxURLatitude.setValue( 'N/A' );
    this.pnlGeometry.pnlBox.txtBoxURLongitude.setValue( 'N/A' );

    // Reset the line panel...
    this.pnlGeometry.pnlLine.txtLineGeometry.setValue( 'N/A' );

    // Reset the polygon panel...
    this.pnlGeometry.pnlPolygon.txtPolygonGeometry.setValue( 'N/A' );
};

mApp.OverlayLocationPanel.prototype.getGeoJSON = function() {
//    var location = '';
//    var geoType = this._currentFeature.geoType.toLowerCase();
//    var pnlGeo = this.pnlGeometry;

    var formatGeo = new OpenLayers.Format.GeoJSON();
    var newGeometry = this._currentFeature.geometry.clone();

    // Convert the points...
    var vertices = newGeometry.getVertices();
    for( var i = 0; i < vertices.length; i++ ) {
        vertices[i].transform( App.mainView.mapPanel.map.getProjectionObject(), new OpenLayers.Projection('EPSG:4326') )
    }

    var geoStr = formatGeo.write( newGeometry );
    var geoObj = JSON.parse( geoStr );

//    switch( this._currentFeature.geoType )
//    {
//        case 'point':
//            location = pnlGeo.pnlPoint.txtPointLatitude.getValue() + ' ' + pnlGeo.pnlPoint.txtPointLongitude.getValue();
//            break;
//        case 'line':
//            location = pnlGeo.pnlLine.txtLineGeometry.getValue();
//            break;
//        case 'polygon':
//            location = pnlGeo.pnlPolygon.txtPolygonGeometry.getValue();
//            break;
//        case 'box':
//            location = pnlGeo.pnlBox.txtBoxLLLatitude.getValue() + ' ' + pnlGeo.pnlBox.txtBoxLLLongitude.getValue() + ' ' +
//                       pnlGeo.pnlBox.txtBoxURLatitude.getValue() + ' ' + pnlGeo.pnlBox.txtBoxURLongitude.getValue();
//            break;
//        case 'circle':
//            var radius = parseFloat( pnlGeo.pnlCircle.txtCircleRadius.getValue() ) * 1000;
//            location = pnlGeo.pnlCircle.txtCircleLatitude.getValue() + ' ' + pnlGeo.pnlCircle.txtCircleLongitude.getValue() + ' ' +
//                       radius.toString();
//            break;
//    }
//
//    return { type: geoType, location: location };
    return geoObj;
};


mApp.OverlayLocationPanel.prototype._populateForm = function( overlay ) {
    var newFeature = null;
    var pnlGeometry = this.pnlGeometry;

    // Geometry
    var geoJsonFormat = new OpenLayers.Format.GeoJSON();
    var geometry = geoJsonFormat.read( overlay.geometry, "Geometry" );

    // Convert the vertices...
    var vertices = geometry.getVertices();
    for( var i=0; i<vertices.length;i++ ) {
        vertices[i].transform( new OpenLayers.Projection('EPSG:4326'), App.mainView.mapPanel.map.getProjectionObject() )
    }

    // create the OpenLayers Feature for this Atom entry
    newFeature = new OpenLayers.Feature.Vector( geometry );

    if( geometry.CLASS_NAME === 'OpenLayers.Geometry.Point' )
    {
        newFeature.geoType = 'point';
        pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPoint );
    }
    else if( geometry.CLASS_NAME === 'OpenLayers.Geometry.LineString' )
    {
        newFeature.geoType = 'line';
        pnlGeometry.layout.setActiveItem( pnlGeometry.pnlLine );
    }
    else if( geometry.CLASS_NAME === 'OpenLayers.Geometry.Polygon' )
    {
        newFeature.geoType = 'polygon';
        pnlGeometry.layout.setActiveItem( pnlGeometry.pnlPolygon );
    }
//    else if( geometry.CLASS_NAME === 'OpenLayers.Geometry.Box' )
//    {
//        newFeature.geoType = 'box';
//        pnlGeometry.layout.setActiveItem( pnlGeometry.pnlBox );
//    }
//    else if( geometry.CLASS_NAME === 'OpenLayers.Geometry.Circle' )
//    {
//        newFeature.radius = values[2]; // GeoRSS radius is in meters
//        newFeature.geoType = 'circle';
//        pnlGeometry.layout.setActiveItem( pnlGeometry.pnlCircle );
//    }

    // Add the feature...
    this.overlayWindow.drawLayer.addFeatures( [newFeature] );
    this._currentFeature = newFeature;

    // Set the combo value...
    this.cboGeometry.setValue( this._currentFeature.geoType );

    // Update the panel with the new data...
    this._updatePanel( this._currentFeature );

    // Show the Edit button...
    this.btnEditDrawing.setVisible( true );
    this.btnStartDrawing.setVisible( false );
    this.btnClearDrawing.setVisible( true );
    this.pnlButtons.doLayout();

    this.cboGeometry.setDisabled( true );

    this._updateInstructions( 'drawDone' );

    this.doLayout();
};
