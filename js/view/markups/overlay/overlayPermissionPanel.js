mApp.OverlayPermissionPanel = Ext.extend( Ext.Panel , {

    initComponent: function() {

        Ext.apply( this, {
            title: 'Permissions',
            padding: 10,
            autoScroll: true,
            border: false,
            items: [
                {
                    xtype: 'displayfield',
                    value: 'The default is that only your account can update an ' +
                        'Entry that you create.  However you can allow other ' +
                        'accounts to update Entries if you want them to share any ' +
                        'information that they can contribute.  Once you or another ' +
                        'account updates your Entry though, they can change the ' +
                        'Update Permissions back to a single account only if necessary.'
                },
                {
                    xtype: 'radiogroup',
                    ref: 'grpPermissions',
                    autoHeight: true,
                    style: '{ margin-top: 10px; }',
                    allowBlank: true,
                    value: 'owner',
                    columns: 1,
                    items: [
                        {
                            boxLabel: 'Only My Account May Update',
                            name: 'entry-update-control',
                            inputValue: 'owner'
                        },
                        {
                            boxLabel: 'Only Accounts in my Organization May Update',
                            name: 'entry-update-control',
                            inputValue: 'org'
                        },
                        {
                            boxLabel: 'Any MASAS Participant May Update',
                            name: 'entry-update-control',
                            inputValue: 'any'
                        }
                    ]
                }

            ]

        });

        mApp.OverlayPermissionPanel.superclass.initComponent.call( this );
    }

});

mApp.OverlayPermissionPanel.prototype.resetPanel = function() {
    this.grpPermissions.setValue( 'owner' );
};

mApp.OverlayPermissionPanel.prototype._populateForm = function( overlay ) {
    this.resetPanel();

    if( overlay.updateAllowed ) {
        this.grpPermissions.setValue( overlay.updateAllowed );
    }
};
