mApp.OverlayViewWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        // Map accessor...
        this.map = App.mainView.mapPanel.map;

        // Original Overlay...
        this._origOverlay = null;

        Ext.apply( this, {

            title: "Overlay",
            layout: 'fit',
            width: 400,
            height: 340,
            minWidth: 300,
            minHeight: 200,
            closeAction: 'close',
            plain: true,

            collapsible: true,
            maximizable: true,

            items: [
                {
                    xtype: 'panel',
                    layout: 'form',
                    padding: 10,
                    autoScroll: true,
                    border:false,

                    hideLabels: false,
                    labelAlign: 'right',
                    labelWidth: 75,
                    labelPad: 5,
                    defaults: {
                        labelSeparator: ':'
                    },

                    items: [
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Collection',
                            ref: '../lblType'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Item',
                            ref: '../lblSubType'
                        },
                        {
                            xtype: 'checkbox',
                            fieldLabel: 'Active',
                            ref: '../chkActive',
                            hidden: true,
                            disabled: true,
                            width: '95%'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Title',
                            ref: '../lblTitleEn',
                            width: '95%'
                        },
                        {
                            xtype: 'textarea',
                            fieldLabel: 'Description',
                            ref: '../lblDescEn',
                            disabled: true,
                            height: 120,
                            width: '95%',
                            flex: 1
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Owner',
                            ref: '../lblOwner'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Owner ID',
                            ref: '../lblOwnerID'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Organization',
                            ref: '../lblOrgID'
                        },
                        {
                            xtype: 'displayfield',
                            fieldLabel: 'Links',
                            ref: '../lblLinks',
                            height: '100%',
                            html: ''
                        }
                    ]
                }
            ],

            buttonAlign: 'left',

            buttons: [
                {
                    text: 'Edit...',
                    ref: '../btnEdit',
                    cls: 'x-btn-left',
                    ctCls: 'width_100',
                    iconCls: 'btnEditItemIcon',
                    hidden: true,
                    scope: this,
                    handler: this._onEditClick
                },
                {
                    text: "Close",
                    scope: this,
                    handler: this._onCloseClick
                }
            ],

            listeners: {
                scope: this,
                beforeshow: this._beforeShow,
                show: this._show
            }
        });

        mApp.OverlayViewWindow.superclass.initComponent.call( this );
    }

});

mApp.OverlayViewWindow.prototype.loadOverlay = function( overlayId, updateAllowed ) {
    this.overlayId = overlayId;
    this.updateAllowed = updateAllowed;
    this._origOverlay = this._loadOverlayFromHub( this.overlayId );
    this.setTitle( "Overlay: " + this._origOverlay.id );
    this._populateForm( this._origOverlay );
};

mApp.OverlayViewWindow.prototype._beforeShow = function() {
    if( 'overlayId' in this ) {
        this.loadOverlay( this.overlayId, this.updateAllowed );
    }
};

mApp.OverlayViewWindow.prototype._show = function() {
    // Move the window to the top right corner...
    this.anchorTo( Ext.getBody(), 'tr-tr', [-5, 65], true );
};

mApp.OverlayViewWindow.prototype._onCloseClick = function() {
    this.close();
};

mApp.OverlayViewWindow.prototype._onEditClick = function() {
    App.mainView.editOverlay( this.overlayId );
};

mApp.OverlayViewWindow.prototype._populateForm = function( overlay ) {
    this.lblTitleEn.setValue( overlay.title );
    this.lblDescEn.setValue( overlay.description );
    this.chkActive.setValue( overlay.active );
    this.lblType.setValue( overlay.type );
    this.lblSubType.setValue( overlay.subtype );
    this.lblOwner.setValue( overlay.ownerName );
    this.lblOwnerID.setValue( overlay.ownerid );
    this.lblOrgID.setValue( overlay.organizationid );

    // Links...
    var html = "";
    if( overlay.links !== undefined )
    {
        for( var linkCtr = 0; linkCtr < overlay.links.length; linkCtr++ )
        {
            var curLink = overlay.links[linkCtr];
            html += '<a href="' + curLink.url + '" target="_blank" >' + curLink.text + '</a><br>';
        }
    }
    this.lblLinks.setValue( html );

    this.btnEdit.setVisible( this.updateAllowed );
};

mApp.OverlayViewWindow.prototype._loadOverlayFromHub = function( overlayId ) {
    var overlay = undefined;

    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();

    var ovlLink = App.dataStoreMgr.masasOverlayFeed._url + "/" + overlayId;

    var ovlRequest = new OpenLayers.Request.GET({
        url: ovlLink,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': App.settings.currentHub.accessCode,
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: App.settings.appSettings.proxyUrl
    });

    if (ovlRequest.status !== 200) {
        mask.hide();
        alert('Unable to load Overlay.');
        return overlay;
    }

    try {
        var overlays = JSON.parse( ovlRequest.responseText );
        overlay = overlays[0];
    }
    catch( err ) {
        mask.hide();
        alert( 'Unable to parse Overlay.' );
        return overlay;
    }

    // advance to the next card
    mask.hide();

    return overlay;
};
