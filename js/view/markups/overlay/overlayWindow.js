mApp.OverlayWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        // Map accessor...
        this.map = App.mainView.mapPanel.map;

        // Original Overlay...
        this._origOverlay = null;

        // Our drawing components...
        this.drawLayer = null;
        this.drawPointController = null;

        // Create the Map Layer we will need to use...
        this._setupMapLayers();

        this.generalPanel = new mApp.OverlayGeneralPanel( { title: 'General' } );
        this.locationPanel = new mApp.OverlayLocationPanel( { title: 'Location', overlayWindow: this } );
        this.permissionPanel = new mApp.OverlayPermissionPanel( { title: 'Permissions' } );
//        this.attachmentPanel = new mApp.OverlayAttachmentPanel( { title: 'Attachments' } );

        // Create and setup the Map Controllers we will need...
        this._setupMapControllers();

        Ext.apply( this, {

            title: "Create a new Overlay Item",
            layout: 'fit',
            width: 512,
            height: 460,
            minWidth: 300,
            minHeight: 200,
            closeAction: 'close',
            plain: true,

            collapsible: true,
            maximizable: true,

            items: {
                xtype: 'tabpanel',
                activeTab: 0,
                deferredRender: false,

                items: [
                    this.generalPanel,
                    this.locationPanel,
//                    this.attachmentPanel,
                    this.permissionPanel
                ]
            },

            buttonAlign: 'left',

            buttons: [
                {
                    text: "Delete",
                    cls: 'x-btn-left',
                    ctCls: 'width_100',
                    ref: "../btnDelete",
                    hidden: true,
                    scope: this,
                    handler: this._onDeleteClick
                },
                {
                    xtype: 'displayfield',
                    ctCls: 'width_100'
                },
                {
                    text: "Cancel",
                    scope: this,
                    handler: this._onCancelClick
                },
                {
                    text: "Submit",
                    disabled: false,
                    scope: this,
                    handler: this._onSendClick
                }
            ],

            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            }
        });

        mApp.OverlayWindow.superclass.initComponent.call( this );
    }

});

mApp.OverlayWindow.prototype._beforeShow = function() {
    if( 'overlayId' in this )
    {
        this.btnDelete.setVisible( true );

        this._origOverlay = this._loadOverlayFromHub( this.overlayId );
        this.setTitle( "Update Overlay: " + this._origOverlay.id );
        this._populateForm( this._origOverlay );
    }
};

mApp.OverlayWindow.prototype._beforeClose = function() {
    this._removeMapControllers();
    this._removeMapLayers();
};

mApp.OverlayWindow.prototype._onSendClick = function() {
    var generalValid = this.generalPanel.validate();
    var locationValid = this.locationPanel.validate();

    if( generalValid.valid && locationValid.valid )
    {
        var overlayWindow = this;
        this.myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});

        this.disable();

        this.myMask.show();

        // Gather the data...
        var masasOverlay = this._generateOverlay();

        // Gather the attachments...
        var attachments = [];

//        for( var attCtr = 0; attCtr < this.attachmentPanel._attachmentStore.getCount(); attCtr++ )
//        {
//            var curAttachment = this.attachmentPanel._attachmentStore.getAt( attCtr );
//            var newAttachment = new MASAS.Attachment();
//            newAttachment.uri = curAttachment.data.fileName;
//            newAttachment.title = curAttachment.data.title;
//            newAttachment.contentType = curAttachment.data.type;
//            newAttachment.file = curAttachment.data.fileObject;
//
//            attachments.push( newAttachment );
//        }

        var overlay = {
            masasOverlay: masasOverlay,
            state: 'new',
            attachments: attachments,
            editUrl: ''
        };

        if( this._origOverlay != null ) {
            overlay.state = 'update';
            overlay.editUrl = App.dataStoreMgr.masasOverlayFeed._url + "/" + this._origOverlay.id;
        }

        // Publish...
        App.masasPublisher.publishOverlay(
            overlay,
            function( msg, overlay ) {
                // success...
                overlayWindow.myMask.hide();
                overlayWindow.close();
            },
            function( failureMsg ) {
                overlayWindow.enable();
                // Failed...
                overlayWindow.myMask.hide();
                alert('Publish failed!');
            }
        );
    }
    else
    {
        var strMessages = generalValid.messages.join( '</br>' );
        strMessages += '</br>' + locationValid.messages.join( '</br>' );

        var message = "Some required fields are missing:</br></br>" + strMessages + "</br>";

        Ext.Msg.alert( 'Error', message );
    }

};

mApp.OverlayWindow.prototype._onCancelClick = function() {
    this.close();
};

mApp.OverlayWindow.prototype._onDeleteClick = function() {
    var overlayWindow = this;

    // Are you sure...
    Ext.Msg.confirm( "Delete Overlay", "Are you sure you want to delete this overlay?", function( buttonId ){
        if( buttonId == "yes" )
        {
            this.myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Please wait..."});
            this.disable();
            this.myMask.show();

            var overlayUrl = App.dataStoreMgr.masasOverlayFeed._url + "/" + this._origOverlay.id;

            App.masasPublisher.deleteOverlay(
                this._origOverlay.id,
                function( msg ) {
                    // success...
                    overlayWindow.myMask.hide();
                    overlayWindow.close();
                },
                function( failureMsg ) {
                    overlayWindow.enable();
                    // Failed...
                    overlayWindow.myMask.hide();
                    Ext.Msg.alert( 'Delete failed', 'Delete failed: <br>' +  failureMsg );
                }
            )
        }
    }, this );
};

mApp.OverlayWindow.prototype._populateForm = function( overlay ) {
    this.generalPanel._populateForm( overlay );
    this.locationPanel._populateForm( overlay );
//    this.attachmentPanel._populateForm( overlay );
    this.permissionPanel._populateForm( overlay );
};

mApp.OverlayWindow.prototype._getBaseOverlay = function() {
    var baseOverlay = {
        title: "",
        description: "",
        type: "",
        subtype: "",
        active: false,
        updateAllowed: "",
        links: []
    };

    return baseOverlay;
}

mApp.OverlayWindow.prototype._generateOverlay = function () {
    // Utils object...
    var commonUtils = mApp.CommonUtils.getInstance();

    // initial Atom data to populate entry with, customize as necessary
    var overlay = this._getBaseOverlay();

    // Category - Icon...
    overlay.type = this.generalPanel.cboType.getValue();
    overlay.subtype = this.generalPanel.cboSubType.getValue();

    overlay.active = this.generalPanel.chkActive.getValue();
    overlay.title = commonUtils.encodeXML( this.generalPanel.txtTitleEn.getValue() );
    overlay.description = commonUtils.encodeXML( this.generalPanel.txtDescEn.getValue() );

    // Related links...
    overlay.links = [];
    this.generalPanel.relatedLinkStore.each( function( rec ) {
        overlay.links.push( {
            'text': rec.data.title,
            'url': rec.data.url
        } );
    } );

    // Location...
    var geoJSON = this.locationPanel.getGeoJSON();
    overlay.geometry = geoJSON;

    // Permissions...
    var permissionValue = this.permissionPanel.grpPermissions.getValue();
    overlay.updateAllowed = permissionValue.getGroupValue();

    return overlay;
};

mApp.OverlayWindow.prototype._setupMapLayers = function() {
//    var defaultMapStyle = new OpenLayers.Style({
//        pointRadius: 8,
//        fillOpacity: 0.5,
//        fillColor: '${myFillColor}',
//        strokeOpacity: 0.7,
//        strokeColor: '${myStrokeColor}',
//        strokeWidth: '${myStrokeWidth}'
//    }, {
//        context: {
//            myStrokeWidth: function (feature) {
//                if (feature.geometry &&
//                    feature.geometry instanceof OpenLayers.Geometry.LineString) {
//                    // make lines easier to see by being thicker
//                    return 6;
//                }
//                return 2;
//            },
//            myStrokeColor: function (feature) {
//                if (feature.geometry &&
//                    feature.geometry instanceof OpenLayers.Geometry.LineString) {
//                    // lines are shown in their respective colour
//                    if (feature.attributes.colour) {
//                        var colour = feature.attributes.colour.toLowerCase();
//                        if (POST.Map.category_colour_map[colour]) {
//                            return POST.Map.category_colour_map[colour];
//                        }
//                    }
//                }
//                // black is the default for all other geometries as its
//                // used to outline the fill colour
//                return '#000000';
//            },
//            myFillColor: function (feature) {
//                if (feature.attributes.colour) {
//                    var colour = feature.attributes.colour.toLowerCase();
//                    if (POST.Map.category_colour_map[colour]) {
//                        return POST.Map.category_colour_map[colour];
//                    }
//                }
//                // OpenLayers default fill colour
//                return '#ee9900';
//            }
//        }
//    });

    // Create our drawing layer...
    this.drawLayer = new OpenLayers.Layer.Vector( 'DrawGeometry', {
        isBaseLayer: false,
        visibility: true
//        styleMap: new OpenLayers.StyleMap({
//            'default': defaultMapStyle
//        })
    });

    // Add the layer...
    this.map.addLayer( this.drawLayer );
};

mApp.OverlayWindow.prototype._removeMapLayers = function() {
    if( this.drawLayer != null ) {
        this.drawLayer.destroy();
    }
};

mApp.OverlayWindow.prototype._setupMapControllers = function() {

    // Draw Point Controller...
    this.drawPointController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Point,
        {
            geoType: 'point',
            overlayWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    // Draw Line Controller...
    this.drawLineController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Path,
        {
            geoType: 'line',
            overlayWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    // Draw Polygon Controller...
    this.drawPolygonController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.Polygon,
        {
            geoType: 'polygon',
            overlayWindow: this,
            featureAdded: this.locationPanel._onNewFeature
        }
    );

    this.drawCircleController = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.RegularPolygon,
        {
            geoType: 'circle',
            overlayWindow: this,
            featureAdded: this.locationPanel._onNewFeature,
            handlerOptions: { snapAngle: 5.625, sides: 64, irregular: false }
        }
    );

    // Draw Box Controller...
    this.drawBoxController  = new OpenLayers.Control.DrawFeature(
        this.drawLayer,
        OpenLayers.Handler.RegularPolygon,
        {
            geoType: 'box',
            overlayWindow: this,
            featureAdded: this.locationPanel._onNewFeature,
            handlerOptions: { sides: 4, irregular: true }
        }
    );

    // Modify Feature Controller...
    this.modifyFeatureController = new OpenLayers.Control.ModifyFeature( this.drawLayer );

    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.drawBoxController,
        this.modifyFeatureController
    ];

    this.map.addControls( controls );
};

mApp.OverlayWindow.prototype._removeMapControllers = function() {
    var controls = [
        this.drawPointController,
        this.drawLineController,
        this.drawPolygonController,
        this.drawCircleController,
        this.drawBoxController,
        this.modifyFeatureController
    ];

    for( var index = 0; index < controls.length; index++ )
    {
        var curControl = controls[index];
        if( curControl.active ) {
            curControl.deactivate();
        }

        this.map.removeControl( curControl );
    }
};

mApp.OverlayWindow.prototype._loadOverlayFromHub = function( overlayId ) {
    var overlay = undefined;

    // mask to help indicate loading delays due to large Entries
    var mask = new Ext.LoadMask(Ext.getBody(), {msg:"Loading, Please Wait..."});
    mask.show();

    var ovlLink = App.dataStoreMgr.masasOverlayFeed._url + "/" + overlayId;

    var ovlRequest = new OpenLayers.Request.GET({
        url: ovlLink,
        // use a secret on request line if needed for this atom_link along with
        // cache busting parameter
        params: {
            'secret': App.settings.currentHub.accessCode,
            '_dc': new Date().getTime()
        },
        async: false,
        proxy: App.settings.appSettings.proxyUrl
    });

    if (ovlRequest.status !== 200) {
        mask.hide();
        alert('Unable to load Overlay.');
        return overlay;
    }

    try {
        var overlays = JSON.parse( ovlRequest.responseText );
        overlay = overlays[0];
    }
    catch( err ) {
        mask.hide();
        alert( 'Unable to parse Overlay.' );
        return overlay;
    }

    // advance to the next card
    mask.hide();

    return overlay;
};
