mApp.SettingsGeneralPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {
        this._defaultViewExtent = "";

        this.ref = "tabGeneral";
        this.title = "General";
        this.layout = "form";
        this.labelAlign = "right";
        this.labelWidth = 130;

        this.cboRefreshRate = new Ext.form.ComboBox({
            name: 'cboRefreshRate',
            fieldLabel: 'Data Refresh Rate',
            allowBlank: false,
            editable: false,
            anchor: '100%',
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'name',
            valueField: 'interval',
            value: '5 minutes',
            store: new Ext.data.ArrayStore({
                id: 0,
                fields: [ 'name', 'interval' ],
                data: [ [ 'Disabled', 0 ],
                        [ '5 minutes', 5 ],
                        [ '15 minutes', 15 ],
                        [ '30 minutes', 30 ],
                        [ '1 hour', 60 ] ]
            }),
            listeners: {
                scope: this,
                'select': this._on_cboRefreshRate_changed
            }
        });

        this.cboEntryLimit = new Ext.form.ComboBox({
            name: 'cboEntryLimit',
            fieldLabel: 'MASAS Entry Limit',
            allowBlank: false,
            editable: false,
            anchor: '100%',
            forceSelection: true,
            triggerAction: 'all',
            mode: 'local',
            displayField: 'name',
            valueField: 'limit',
            value: 'Max',
            store: new Ext.data.ArrayStore({
                id: 0,
                fields: [ 'name', 'limit' ],
                data: [
                    [ '100', 100 ],
                    [ '200', 200 ],
                    [ '400', 400 ],
                    [ '600', 600 ],
                    [ '800', 800 ],
                    [ 'Max', 0 ]
                ]
            }),
            listeners: {
                scope: this,
                'select': this._on_cboEntryLimit_changed
            }
        });

        this.cfDefMapExtent = new Ext.Container( {
            name: 'cfMapExtent',
            layout: 'column',
            fieldLabel: 'Starting View Extent',
            autoWidth: true,
            defaults: {
                // implicitly create Container by specifying xtype
                xtype: 'container',
                autoEl: 'div', // This is the default.
                layout: 'form',
                columnWidth: 0.5
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Set',
                    tooltip: 'Set the your start extent with the current view extent.',
                    ref: 'btnSetDefaultMapExtent',
                    style: {
                        margin: '0px 2px 0px 0px'
                    },
                    scope: this,
                    handler: this._on_btnSetDefaultMapExtent_click
                },
                {
                    xtype: 'button',
                    text: 'Reset',
                    tooltip: 'Reset the start extent to the default value.',
                    ref: 'btnResetDefaultMapExtent',
                    style: {
                        margin: '0px 0px 0px 2px'
                    },
                    scope: this,
                    handler: this._on_btnResetDefaultMapExtent_click
                }
            ]
        } );

        this.chkUserOverlays = new Ext.form.Checkbox({
            fieldLabel: 'My Overlays only',
            anchor: '100%',
            listeners: {
                scope: this,
                'change': this._on_chkUserOverlays_changed
            }
        });

        this.chkFilterUSAlerts = new Ext.form.Checkbox({
            fieldLabel: 'No US Public Alerts',
            anchor: '100%',
            listeners: {
                scope: this,
                'change': this._on_chkFilterUSAlerts_changed
            }
        });

        this.chkFilterTestMsgs = new Ext.form.Checkbox({
            fieldLabel: 'Show Test Messages',
            anchor: '100%',
            listeners: {
                scope: this,
                'change': this._on_chkFilterTestMsgs_changed
            }
        });

        this.items = [
            this.cboRefreshRate,
            this.cboEntryLimit,
            this.chkFilterUSAlerts,
            this.cfDefMapExtent,
            {
                xtype: 'displayfield',
                html: '<hr>'
            },
            this.chkUserOverlays,
            this.chkFilterTestMsgs
        ];

        mApp.SettingsGeneralPanel.superclass.initComponent.call( this );
    }

});

mApp.SettingsGeneralPanel.prototype._populateForm = function() {
    // Refresh rate...
    this.cboRefreshRate.setValue( App.settings.userSettings.refreshRate );

    // Entry Limit...
    this.cboEntryLimit.setValue( App.settings.userSettings.masasEntryLimit );

    // Default View Extent...
    this._defaultViewExtent = App.settings.userSettings.defaultViewExtent;

    // Filter US Alerts...
    this.chkFilterUSAlerts.setValue( App.settings.userSettings.filterUSAlerts );

    // My Overlays check...
    this.chkUserOverlays.setValue( App.settings.userSettings.overlayShowOnlyMine );

    // Filter Test Messages...
    this.chkFilterTestMsgs.setValue( !App.settings.userSettings.filterTestEntries );
};

mApp.SettingsGeneralPanel.prototype._saveData = function() {
    App.settings.userSettings.refreshRate = this.cboRefreshRate.getValue();
    App.settings.userSettings.enableAutoRefresh = this.cboRefreshRate.getValue() > 0;
    App.settings.userSettings.masasEntryLimit = this.cboEntryLimit.getValue();
    App.settings.userSettings.defaultViewExtent = this._defaultViewExtent;
    App.settings.userSettings.filterUSAlerts = this.chkFilterUSAlerts.getValue();
    App.settings.userSettings.overlayShowOnlyMine = this.chkUserOverlays.getValue();
    App.settings.userSettings.filterTestEntries = !this.chkFilterTestMsgs.getValue();
}

mApp.SettingsGeneralPanel.prototype._on_cboRefreshRate_changed = function() {
    this.settingsWindow.refreshData();
};

mApp.SettingsGeneralPanel.prototype._on_cboEntryLimit_changed = function() {
    this.settingsWindow.refreshData();
};

mApp.SettingsGeneralPanel.prototype._on_chkUserOverlays_changed = function() {
    this.settingsWindow.refreshData();
};

mApp.SettingsGeneralPanel.prototype._on_chkFilterUSAlerts_changed = function() {
    this.settingsWindow.refreshData();
};

mApp.SettingsGeneralPanel.prototype._on_chkFilterTestMsgs_changed = function() {
    this.settingsWindow.refreshData();
};

mApp.SettingsGeneralPanel.prototype._on_btnSetDefaultMapExtent_click = function() {
    var viewExtent = App.mainView.mapPanel.map.getExtent()
    this._defaultViewExtent = viewExtent.transform( VIEW.mapOptions.projection, 'EPSG:4326' ).toBBOX();
};

mApp.SettingsGeneralPanel.prototype._on_btnResetDefaultMapExtent_click = function() {
    this._defaultViewExtent = App.settings.appSettings.mapDefaultViewExtent;
};