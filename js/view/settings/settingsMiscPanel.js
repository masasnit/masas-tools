mApp.SettingsMiscPanel = Ext.extend( Ext.FormPanel , {

    initComponent: function() {

        this.ref = "tabMisc";
        this.title = "Misc.";
        this.layout = "form";

        this.items = [{
            ref: "chkAdjustPolygonMarkup",
            hideLabel: true,
            boxLabel: '<span title="Adjusts icon positions so they remain visible">Auto Adjust Entry Icons</span>',
            xtype: 'checkbox',
            checked: true
        }]

        mApp.SettingsMiscPanel.superclass.initComponent.call( this );
    }

});

mApp.SettingsMiscPanel.prototype._populateForm = function() {
    this.chkAdjustPolygonMarkup.setValue( App.settings.userSettings.autoCenterPolygonMarkup );
};

mApp.SettingsMiscPanel.prototype._saveData = function() {
    App.settings.userSettings.autoCenterPolygonMarkup = this.chkAdjustPolygonMarkup.getValue();
};