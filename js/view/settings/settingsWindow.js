mApp.SettingsWindow = Ext.extend( Ext.Window , {

    initComponent: function() {
        this._refreshData = false;

        this.title = "Settings";
        this.layout = 'fit';
        this.width = 350;
        this.height = 300;
        this.minWidth = 300;
        this.minHeight = 200;
        this.closeAction = 'close';
        this.plain = true;

        // Pre-create the panels...
        this.tabGeneral = new mApp.SettingsGeneralPanel( { settingsWindow: this } );
//        this.tabMisc = new mApp.SettingsMiscPanel( { settingsWindow: this } );

        this.items = {
            ref: "tabsMain",
            xtype: "tabpanel",
            border: false,
            activeTab: 0,
            defaults: { autoHeight: true, bodyStyle: 'padding:10px' },
            items:[
                this.tabGeneral//,
//                this.tabMisc
            ]
        };

        this.buttons = [{
            text: "Apply",
            disabled: false,
            scope: this,
            handler: this._onOKClick
        }, {
            text: "Cancel",
            scope: this,
            handler: this._onCloseClick
        }];

        this.listeners = {
            scope: this,
            beforeclose: this._beforeClose,
            beforeshow: this._beforeShow
        };

        mApp.SettingsWindow.superclass.initComponent.call( this );
    }

});

mApp.SettingsWindow.prototype._populateForm = function() {
    this.tabGeneral._populateForm();
//    this.tabMisc._populateForm();
};

mApp.SettingsWindow.prototype._saveData = function() {
    this.tabGeneral._saveData();
//    this.tabMisc._saveData();

    // Save the data...
    App.settings.saveUserSettings();

    // Force a reload of the MASAS data...
    if( this._refreshData ) {
        App.dataStoreMgr.refreshAll();
        App.dataStoreMgr.updateAutoRefresh();
    }
};

mApp.SettingsWindow.prototype._beforeShow = function() {
    this._populateForm();
};

mApp.SettingsWindow.prototype._beforeClose = function() {
    // Cancel the close event if needed...
    //return false;
};

mApp.SettingsWindow.prototype._onApplyClick = function() {
    this._saveData();
};

mApp.SettingsWindow.prototype._onOKClick = function() {
    this._onApplyClick();
    this.close();
};

mApp.SettingsWindow.prototype._onCloseClick = function() {
    this.close();
};

mApp.SettingsWindow.prototype.refreshData = function() {
    this._refreshData = true;
};