mApp.MainStatusBar = Ext.extend( Ext.ux.StatusBar , {

    initComponent: function() {

        this.tbLastUpdated = new Ext.Toolbar.TextItem( 'MMM DD, HH:MM:SS' );
        this.tbEntryCount = new Ext.Toolbar.TextItem( '0' );
        this.tbPermissions = new Ext.Toolbar.TextItem( 'Read-Only' );

        // Feed Update display
        var entryViewStore = this.mainView.entryPanel.store;
        entryViewStore.on( 'load', this._onUpdateFeedDataDisplay, this );

        var draftViewStore = this.mainView.draftPanel.store;
        draftViewStore.on( 'load', this._onUpdateFeedDataDisplay, this );

        // Apply the basic configurations...
        Ext.apply( this, {
            statusAlign: 'right', // the magic config
            items: [
                'Hub: ' + App.settings.currentHub.name,
                '-',
                'User: ' + App.masasHub.userData.name,
                '-',
                this.tbPermissions,
                '-',
                'Last Updated:',
                this.tbLastUpdated,
                '-',
                'Total Entries:',
                this.tbEntryCount,
                '-',
                ' '
            ],

            listeners: {
                scope: this,
                render: this._onRender
            }
        });

        mApp.MainStatusBar.superclass.initComponent.call( this );
    }

});

mApp.MainStatusBar.prototype._onRender = function() {
    this.updatePermissions();
};

mApp.MainStatusBar.prototype._onUpdateFeedDataDisplay = function( store, records ) {
    if( records.length > 0 )
    {
        var entryViewStore = App.mainView.entryPanel.store;
        var draftViewStore = App.mainView.draftPanel.store;

        this.tbLastUpdated.setText( new Date().format( 'M j H:i:s' ) );
        var count = entryViewStore.getCount() + draftViewStore.getCount();
        this.tbEntryCount.setText( count.toString() );
    }
};

mApp.MainStatusBar.prototype.showLoadingFeeds = function() {
    this.showBusy( "Loading Feeds..." );
};

mApp.MainStatusBar.prototype.doneLoadingFeeds = function() {
    this.clearStatus();
};

mApp.MainStatusBar.prototype.updatePermissions = function() {
    if( App.masasHub.CanPost() ) {
        this.tbPermissions.setText( "Read/Write" );
    }
    else {
        this.tbPermissions.setText( "Read-Only" );
    }
};
