mApp.MainToolBar = Ext.extend( Ext.Toolbar , {

    initComponent: function() {

        // Apply the basic configurations...
        Ext.apply( this, {
            defaults: {
                scale: "medium",
                iconAlign: "top"
            }

        });

        // Create any tools we may need...

        var add_menu = new Ext.menu.Menu({
            ignoreParentClicks: true,
            plain: true,
            items: [
                {
                    text: "Add Entry...",
                    tooltip: "Add an Entry",
                    handler: function() {
                        var entryWin = new mApp.EntryWindow();
                        entryWin.show( this );
                    }
                },
                {
                    text: "Add Alert...",
                    tooltip: "Add an Alert",
                    handler: function() {
                        var alertWin = new mApp.AlertWindow();
                        alertWin.show( this );
                    }
                },
                {
                    text: "Add to Overlays...",
                    tooltip: "Add a new item to the Overlays",
                    handler: function() {
                        var overlayWin = new mApp.OverlayWindow();
                        overlayWin.show( this );
                    }
                }
            ]
        });

        // TODO - clean these up
        // Tools menu items
        var tools_menu = [{
            text: 'Zoom to Load Feed Extent',
            handler: VIEW.MapTools.zoom_to_load_extent
        }, {
            xtype: 'menucheckitem',
            text: 'Show All Entry Geometry',
            listeners: {
                'checkchange': VIEW.MapTools.all_geometry_toggle
            }
        }, {
            text: 'Measure',
            menu: {
                plain: true,
                items: [
                    '&nbsp;&nbsp;<b>Measuring Tools</b>',
                    {
                        text: 'Point Measure',
                        measureType: 'MeasurePoint',
                        handler: VIEW.MapTools.measurement_toggle
                    }, {
                        text: 'Length Measure',
                        measureType: 'MeasureLength',
                        handler: VIEW.MapTools.measurement_toggle
                    }, {
                        text: 'Area Measure',
                        measureType: 'MeasureArea',
                        handler: VIEW.MapTools.measurement_toggle
                    }]
            }
        }, {
            xtype: 'menucheckitem',
            text: 'Scale Line',
            listeners: {
                'checkchange': VIEW.MapTools.scale_line_toggle
            }
        }, {
            xtype: 'menucheckitem',
            text: 'Overview Map',
            listeners: {
                'checkchange': VIEW.MapTools.overview_map_toggle
            }
        }];
        // streetview is optional
        if ('google' in window) {
            tools_menu.push({
                text: 'Google Street View',
                handler: VIEW.MapTools.streetview_activate
            });
        }
        tools_menu.push({
            text: 'Reset Layer Loading Indicator',
            handler: function () {
                // the layer loading indicator can get stuck in place sometimes
                // while waiting for a slow layer to load, this will reset it
                VIEW.mapControls.loadingControl.counter = 0;
                VIEW.mapControls.loadingControl.minimizeControl();
            }
        });

         VIEW.MapTools.locationClearBtn = new Ext.Button({
            tooltip: 'Clear address locations',
            iconCls: 'btnClearSearchIcon',
            scale: "medium",
            iconAlign: "top",
            arrowAlign: "bottom",
            handler: function (btn, evt) {
                VIEW.mapLayers.location.destroyFeatures();
            }
        });

        this.addressSearchCombo = new VIEW.AddressSearchCombo({
            'url': App.settings.appSettings.serviceTool_addressSearchUrl,
            'layer': VIEW.mapLayers.location,
            'clearBtn': VIEW.MapTools.locationClearBtn
        });

        // OpenLayers Nav History, VIEW.mapControls.navHistory must already have
        // been created in order to associate the control
        var navPrevious = new GeoExt.Action({
            text: 'Previous',
            control: VIEW.mapControls.navHistory.previous,
            disabled: true,
            tooltip: 'Zoom to previous view from history',
            iconCls: 'btnLeftArrow'
        });
        var navNext = new GeoExt.Action({
            text: 'Next',
            control: VIEW.mapControls.navHistory.next,
            disabled: true,
            tooltip: 'Zoom to next view in history',
            iconCls: 'btnRightArrow'
        });
        // TODO - DONE

        /* Layer menus */
        this._setupLayerMenus();

        this.items = [
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "bottom"
                },
                items: [
                    {
                        iconCls: "btnMenuIcon",
                        menu: {
                            ignoreParentClicks: true,
                            plain: true,
                            items: [
                                {
                                    text: "Tools",
                                    menu: tools_menu
                                },'-',
                                {
                                    text: "Settings...",
                                    scope: this.mainView,
                                    handler: this.mainView.openSettingsWindow
                                },'-',
                                {
                                    text: "About...",
                                    scope: this,
                                    handler: this._onAboutClick
                                }
                            ]
                        }
                    }
                ]
            },
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "bottom"
                },
                items: [
                    {
                        iconCls: "btnHomeIcon",
                        xtype: 'button',
                        text: 'Home',
                        tooltip: 'Zoom to your Home extent',
                        handler: function() { App.mainView.mapPanel.zoomToDefaultExtent(); }
                    },
                    {
                        xtype: 'tbseparator',
                        width: 10
                    },
                    navPrevious,
                    navNext
                ]
            },
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "right"
                },
                items: [
                    {
                        ref: "../btnAdd",
                        text: "Add",
                        iconCls: "btnToolsAddIcon",
                        disabled: true,
                        menu: add_menu
                    }
                ]
            },
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "right"
                },
                items: [
                    {
                        xtype: 'button',
                        text: "Refresh",
                        iconCls: "btnRefresh",
                        scope: this,
                        handler: this._onRefresh_click
                    },
                    {
                        xtype: 'tbseparator',
                        width: 10
                    },
                    {
                        xtype: 'button',
                        text: "Time",
                        itemCls: "btnRefresh",
                        iconCls: "btnTimeFilter",
                        scope: this.mainView,
                        handler: this.mainView.openTimeFilterWindow
                    },
                    {
                        xtype: 'tbseparator',
                        width: 10
                    },
                    {
                        xtype: 'button',
                        text: "View",
                        iconCls: "btnViewIcon",
                        menu: {
                            ignoreParentClicks: true,
                            plain: true,
                            items: [{
                                text: "Map View",
                                tooltip: "Show only the map",
                                scope: this,
                                handler: this._onMapView
                            },
                                {
                                    text: "Split View",
                                    tooltip: "Show the map and the grid",
                                    scope: this,
                                    handler: this._onSplitView
                                },
                                {
                                    text: "Grid View",
                                    tooltip: "Show only the grid",
                                    scope: this,
                                    handler: this._onGridView
                                }]
                        }
                    },
                    {
                        xtype: 'tbseparator',
                        width: 10
                    },
                    {
                        text: "Base",
                        iconCls: "btnBaseLayersIcon",
                        menu: this.menuBaseLayers
                    },
                    {
                        text: "More",
                        iconCls: "btnOverlayLayersIcon",
                        menu: this.menuOverlayLayers
                    }
                ]
            },
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "bottom"
                },
                items: [
                    {
                        text: 'My Location',
                        iconCls: 'btnGeolocate',
                        tooltip: 'Zoom to your current location (GPS) on the map',
                        handler: VIEW.MapTools.zoom_to_geolocation
                    },
                    {
                        xtype: 'tbseparator',
                        width: 20
                    },
                    this.addressSearchCombo,
                    {
                        xtype: 'tbspacer',
                        width: 5
                    },
                    VIEW.MapTools.locationClearBtn
                ]
            },
            {
                xtype: "buttongroup",
                defaults: {
                    scale: "medium",
                    iconAlign: "top",
                    arrowAlign: "bottom"
                },
                items: [
                    {
                        iconCls: "btnLogoutIcon",
                        text: "Logout",
                        scope: this,
                        handler: App.logout
                    }
                ]
            }
        ];

        this.listeners = {
            scope: this,
            render: this._onRender
        }

        mApp.MainToolBar.superclass.initComponent.call( this );
    }

});

mApp.MainToolBar.prototype._onRender = function() {
    if( App.masasHub.CanPost() ) {
        this.btnAdd.setDisabled( false );
    }
};

mApp.MainToolBar.prototype._setupLayerMenus = function() {

    this.menuBaseLayers = new VIEW.Layers.LayerMenu({
        layers: VIEW.MapLayers.baseLayers
    });

    this.menuOverlayLayers = new VIEW.Layers.LayerMenu({
        layers: VIEW.MapLayers.overlayLayers
    });

    // Insert a separator...
    this.menuOverlayLayers.insert( 0, '-' );

    this.menuOverlayLayers.insert( 0, {
        xtype: 'menucheckitem',
        text: 'Infrastructure Overlay Collection',
        overlayType: 'infrastructure',
        listeners : {
            'checkchange': this._onMASASOverlayInfraCheckChange
        }
    } );

    this.menuOverlayLayers.insert( 0, {
        xtype: 'menucheckitem',
        text: 'Resource Overlay Collection',
        overlayType: 'resource',
        listeners : {
            'checkchange': this._onMASASOverlayResCheckChange
        }
    } );

    this.menuOverlayLayers.insert( 0, {
        xtype: 'menucheckitem',
        text: 'Hazard Overlay Collection',
        overlayType: 'hazard',
        listeners : {
            'checkchange': this._onMASASOverlayHazardsCheckChange
        }
    } );

    // Insert a separator...
    this.menuOverlayLayers.insert( 0, '-' );

    // Insert Draft...
    this.menuOverlayLayers.insert( 0, {
        xtype: 'menucheckitem',
        text: 'Draft Entries',
        checked: false,
        listeners : {
            'checkchange': this._onMASASDraftEntriesCheckChange
        }
    } );

};

mApp.MainToolBar.prototype._onRefresh_click = function() {
    // Update the extent in the Main View...
    App.mainView.updateViewFilterExtent();

    // Refresh the data stores...
    App.dataStoreMgr.refreshAll();
};

mApp.MainToolBar.prototype._onMapView = function() {
    App.mainView.toggleMapView();
};

mApp.MainToolBar.prototype._onSplitView = function() {
    App.mainView.toggleSplitView();
};

mApp.MainToolBar.prototype._onGridView = function() {
    App.mainView.toggleGridView();
};

mApp.MainToolBar.prototype._onAboutClick = function() {
    // Temporary about box...
    var aboutMsg = "";
    aboutMsg += "<div>";
    aboutMsg += "  <div style='height: 40px; display: flex; justify-content: center;'>"
    aboutMsg += "    <div style='margin:auto auto;'>"
    aboutMsg += '      <h1><a target="_blank" href="' + App.settings.appSettings.masasSiteLink + '">' + App.settings.appSettings.masasSiteLabel + '</a></h1>';
    aboutMsg += "    </div>"
    aboutMsg += "  </div>"
    aboutMsg += "  <div style='height: 40px;display: flex; justify-content: right;'><div style='margin:auto auto;'><div>MASAS Tools version: " + App.version + "</div></div></div>";
    aboutMsg += "  <div style='height: 40px; display: flex; justify-content: center;'>"
    aboutMsg += "    <div style='margin:auto auto;'>"
    aboutMsg += "      <div>Support Resources:<br>" + '<a target="_blank" href="' + App.settings.appSettings.supportLink + '">' + App.settings.appSettings.supportLink + '</a></div>';
    aboutMsg += "    </div>"
    aboutMsg += "  </div>"
    aboutMsg += "</div>";

    Ext.MessageBox.minWidth = 350;
    var msgBox = Ext.MessageBox.alert( "About", aboutMsg );
    //msgBox.minWidth = 300;
};

mApp.MainToolBar.prototype._onMASASDraftEntriesCheckChange = function( item, checked ) {
    App.mainView.setDraftVisibility( checked );
};

mApp.MainToolBar.prototype._onMASASOverlayInfraCheckChange = function( item, checked ) {
    App.dataStoreMgr.masasOverlayFeed.filterInfra = !checked;
    App.dataStoreMgr.refreshAll();
};

mApp.MainToolBar.prototype._onMASASOverlayResCheckChange = function( item, checked ) {
    App.dataStoreMgr.masasOverlayFeed.filterResources = !checked;
    App.dataStoreMgr.refreshAll();
};

mApp.MainToolBar.prototype._onMASASOverlayHazardsCheckChange = function( item, checked ) {
    App.dataStoreMgr.masasOverlayFeed.filterHazards = !checked;
    App.dataStoreMgr.refreshAll();
};

