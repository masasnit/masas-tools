mApp.TimeFilterWindow = Ext.extend( Ext.Window , {

    initComponent: function() {

        Ext.apply( this, {
            title: "Time",
            layout: 'fit',
            width: 350,
            height: 320,
            minWidth: 300,
            minHeight: 200,
            closeAction: 'close',
            plain: true,

            listeners: {
                scope: this,
                beforeclose: this._beforeClose,
                beforeshow: this._beforeShow
            },

            items: [
                {
                    xtype: 'panel',
                    layout: 'form',
                    autoScroll: true,
                    labelWidth: 15,
                    bodyStyle: 'padding: 10px;',
                    defaults: { msgTarget: 'side' },
                    items: [
                        {
                            xtype: 'radio',
                            name: 'timeSelect',
                            ref: '../rdoCurrent',
                            hideLabel: true,
                            boxLabel: 'Effective now',
                            inputValue: 'current',
                            checked: true
                        },
                        {
                            xtype: 'displayfield',
                            html: '<hr>'
                        },
                        {
                            xtype: 'radio',
                            name: 'timeSelect',
                            ref: '../rdoInstance',
                            hideLabel: true,
                            boxLabel: 'Effective at',
                            inputValue: 'instance'
                        },
                        {
                            xtype: 'compositefield',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    width: 35
                                },
                                {
                                    xtype: 'datefield',
                                    width: 95,
                                    format: 'Y-m-d',
                                    value: new Date(),
                                    ref: '../../dateInstance'
                                }, {
                                    xtype: 'timefield',
                                    width: 60,
                                    format: 'H:i',
                                    increment: 60,
                                    value: '00:00',
                                    ref: '../../timeInstance'
                                }
                            ]
                        },
                        {
                            xtype: 'displayfield',
                            html: '<hr>'
                        },
                        {
                            xtype: 'radio',
                            name: 'timeSelect',
                            ref: '../rdoRange',
                            hideLabel: true,
                            boxLabel: 'Effective during',
                            inputValue: 'range'
                        },
                        {
                            xtype: 'compositefield',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    width: 35,
                                    html: 'Start:'
                                },
                                {
                                    xtype: 'datefield',
                                    id: 'startDT',
                                    name: 'startDT',
                                    width: 95,
                                    format: 'Y-m-d',
                                    vtype: 'daterange',
                                    // ID of the end date field used for validation
                                    endDateField: 'endDT',
                                    ref: '../../dateStart'
                                },
                                {
                                    xtype: 'timefield',
                                    id: 'startTM',
                                    name: 'startTM',
                                    width: 60,
                                    format: 'H:i',
                                    increment: 60,
                                    value: '00:00',
                                    ref: '../../timeStart'
                                }
                            ]
                        },
                        {
                            xtype: 'compositefield',
                            items: [
                                {
                                    xtype: 'displayfield',
                                    width: 35,
                                    html: 'End:'
                                },
                                {
                                    xtype: 'datefield',
                                    id: 'endDT',
                                    name: 'endDT',
                                    width: 95,
                                    format: 'Y-m-d',
                                    vtype: 'daterange',
                                    startDateField: 'startDT',
                                    ref: '../../dateEnd'
                                },
                                {
                                    xtype: 'timefield',
                                    id: 'endTM',
                                    name: 'endTM',
                                    width: 60,
                                    format: 'H:i',
                                    increment: 60,
                                    value: '00:00',
                                    ref: '../../timeEnd'
                                }
                            ]
                        },
                        {
                            xtype: 'displayfield',
                            html: '<hr>'
                        },
                        {
                            hideLabel: true,
                            boxLabel: 'Include future effective',
                            xtype: 'checkbox',
                            checked: false,
                            ref: '../chkFilterEffective'
                        },
                    ]
                }
            ],

            buttons: [
                {
                    text: "OK",
                    disabled: false,
                    scope: this,
                    handler: this._onOKClick
                },
                {
                    text: "Apply",
                    disabled: false,
                    scope: this,
                    handler: this._onApplyClick
                },
                {
                    text: "Close",
                    scope: this,
                    handler: this._onCloseClick
                }
            ]
        });

        mApp.TimeFilterWindow.superclass.initComponent.call( this );
    }

});

mApp.TimeFilterWindow.prototype._populateForm = function() {
    var filterData = App.settings.userSettings.timeFilter;

    this.rdoCurrent.setValue( filterData.type );

    switch( filterData.type ) {
        case 'instance':
            this.dateInstance.setValue( filterData.startDate );
            this.timeInstance.setValue( filterData.startDate );
            break;
        case 'range':
            this.dateStart.setValue( filterData.startDate );
            this.timeStart.setValue( filterData.startDate );
            this.dateEnd.setValue( filterData.endDate );
            this.timeEnd.setValue( filterData.endDate );
            break;
    }

    // Update the filter Effective value...
    var filterEffective = false;
    if( App.settings.userSettings.effectiveOnly ) {
        filterEffective = App.settings.userSettings.effectiveOnly;
    }

    this.chkFilterEffective.setValue( !filterEffective );
};

mApp.TimeFilterWindow.prototype._saveData = function() {
    var filterData = App.settings.userSettings.timeFilter;

    filterData.type = this.rdoCurrent.getGroupValue();
    filterData.startDate = new Date();
    filterData.endDate = new Date();

    switch( filterData.type ) {
        case 'instance':
            filterData.startDate = this.dateInstance.getValue();
            this._setTime( filterData.startDate, this.timeInstance.getValue() );
            break;
        case 'range':
            filterData.startDate = this.dateStart.getValue();
            this._setTime( filterData.startDate, this.timeStart.getValue() );

            filterData.endDate = this.dateEnd.getValue();
            this._setTime( filterData.endDate, this.timeEnd.getValue() );
            break;
    }

    // Effective Filter...
    App.settings.userSettings.effectiveOnly = !this.chkFilterEffective.getValue();

    // Save the data...
    App.settings.saveUserSettings();

    // Force a reload of the MASAS data...
    App.dataStoreMgr.refreshAll();
};

mApp.TimeFilterWindow.prototype._setTime = function( date, timeStr ) {
    var timeValues = timeStr.split( ':' );
    date.setHours( timeValues[0] );
    date.setMinutes( timeValues[1] );
};

mApp.TimeFilterWindow.prototype._beforeShow = function() {
    this._populateForm();
};

mApp.TimeFilterWindow.prototype._beforeClose = function() {

};

mApp.TimeFilterWindow.prototype._onApplyClick = function() {
    this._saveData();
};

mApp.TimeFilterWindow.prototype._onOKClick = function() {
    this._onApplyClick();
    this.close();
};

mApp.TimeFilterWindow.prototype._onCloseClick = function() {
    this.close();
};