/**
 * Copyright (c) 2008-2010 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */
/*global Ext,OpenLayers,GeoExt */

/** api: (define)
 *  module = GeoExt.grid
 *  class = ClusterSelectionModel
 *  base_link = `Ext.grid.RowSelectionModel <http://dev.sencha.com/deploy/dev/docs/?class=Ext.grid.RowSelectionModel>`_
 */

Ext.namespace('GeoExt.grid');

/** api: constructor
 *  .. class:: ClusterSelectionModel
 *
 *      A row selection model which enables automatic selection of features
 *      in the map when rows are selected in the grid and vice-versa.
 */

/** api: example
 *  Sample code to create a feature grid with a feature selection model:
 *
*  .. code-block:: javascript
*
*       var gridPanel = new Ext.grid.GridPanel({
*          title: "Feature Grid",
*          region: "east",
*          store: store,
*          width: 320,
*          columns: [{
*              header: "Name",
*              width: 200,
*              dataIndex: "name"
*          }, {
*              header: "Elevation",
*              width: 100,
*              dataIndex: "elevation"
*          }],
*          sm: new GeoExt.grid.FeatureSelectionModel()
*      });
*/

GeoExt.grid.ClusterSelectionModelMixin = function () {
    return {

        /** api: config[layerFromStore]
         *  ``Boolean`` If true, and if the constructor is passed neither a
         *  layer nor a select feature control, a select feature control is
         *  created using the layer found in the grid's store. Set it to
         *  false if you want to manually bind the selection model to a
         *  layer. Defaults to true.
         */
        layerFromStore: true,

        /** api: config[layer]
         *  ``OpenLayers.Layer.Vector`` The vector layer used for the creation of
         *  the select feature control, it must already be added to the map. If not
         *  provided, the layer bound to the grid's store, if any, will be used.
         */

        /** private: property[bound]
         *  ``Boolean`` Flag indicating if the selection model is bound.
         */
        bound: false,

        /** private: property[superclass]
         *  ``Ext.grid.AbstractSelectionModel`` Our superclass.
         */
        superclass: null,

        /** private */
        constructor: function (config) {
            config = config || {};
            this.superclass = arguments.callee.superclass;
            this.superclass.constructor.call(this, config);
        },

        /** private: method[initEvents]
         *
         *  Called after this.grid is defined
         */
        initEvents: function () {
            this.superclass.initEvents.call(this);
            if (this.layerFromStore) {
                var layer = this.grid.getStore() && this.grid.getStore().layer;
            }
            this.bind(layer);
        },

        /** api: method[bind]
         *
         *  :param layer: ``OpenLayers.Layer.Vector`` The vector layer this selection
         *      model should be bound to.
         *  Bind the selection model to a layer.
         */
        bind: function (layer) {
            if (!this.bound && layer) {
                this.layer = layer;
                this.on("rowselect", this.rowSelected, this);
                this.on("rowdeselect", this.rowDeselected, this);
                this.bound = true;
            }
        },

        /** api: method[unbind]
         *  Unbind the selection model from the layer.
         */
        unbind: function () {
            var layer = this.layer;
            if (this.bound) {
                this.un("rowselect", this.rowSelected, this);
                this.un("rowdeselect", this.rowDeselected, this);
                this.bound = false;
            }
        },

        /** private: method[rowSelected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowSelected: function (model, row, record) {
            var feature = record.getFeature();
            var layer = this.layer;
            if (!this._selecting && feature) {
                if (layer.selectedFeatures.indexOf(feature) == -1) {
                    this._selecting = true;
                    //find feature within a cluster
                    var cluster = this.findClusterByFeature(layer, feature);
                    if (cluster) {
                        if (layer.events.triggerEvent("beforefeatureselected", {feature: cluster}) !== false) {
                            layer.events.triggerEvent("featureselected", {
                                feature: cluster,
                                targetFeature: feature
                            });
                        }
                    }
                    this._selecting = false;
                }
            }
        },

        /** private: method[rowDeselected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowDeselected: function (model, row, record) {
            var feature = record.getFeature();
            if (!this._selecting && feature) {
                var layer = this.layer;
                //find feature within a cluster
                var cluster = this.findClusterByFeature(layer, feature);
                if (cluster) {
                    this._selecting = true;
                    layer.events.triggerEvent("featureunselected", {
                        feature: cluster,
                        targetFeature: feature
                    });
                    this._selecting = false;
                }
            }
        },

        /** private: method[findClusterByFeature]
         *  :param layer: ``OpenLayers.Layer.Vector``
         *  :param feature: ``OpenLayers.Feature.Vector``
         *  :return: ``OpenLayers.Feature.Vector`` The clustered feature containing the specified feature
         *  Return the clustered feature containing the specified feature
         */
        findClusterByFeature: function (layer, feature) {
            for (var i = 0, len = layer.features.length; i < len; i++) {
                var cfeat = layer.features[i];
                if (cfeat == feature) {
                    return cfeat;
                }
                else if (cfeat.cluster) {
                    if (cfeat.cluster.length == 1 && cfeat.cluster[0] == feature) {
                        return cfeat;
                    }
                    else {
                        for (var j = 0, clen = cfeat.cluster.length; j < clen; j++) {
                            if (cfeat.cluster[j] == feature) {
                                return cfeat;
                            }
                        }
                    }
                }
            }
        }
    };
};

GeoExt.grid.ClusterSelectionModel = Ext.extend(Ext.grid.RowSelectionModel,
    new GeoExt.grid.ClusterSelectionModelMixin());
