/**
 * Copyright (c) 2008-2011 The Open Planning Project
 *
 * JPW: Modified to use GeoExt.ux namespace instead of gxp and to turn
 *      control off
 * 
 * Published under the GPL license.
 * See https://github.com/opengeo/gxp/raw/master/license.txt for the full text
 * of the license.
 */
/*global Ext,OpenLayers,GeoExt,google */

Ext.namespace("GeoExt.ux");

GeoExt.ux.GoogleStreetViewPanel = Ext.extend(Ext.Panel, {

    /** private: property[panorama]
     *  ``google.maps.StreetViewPanorama``  The panorama object.
     */
    panorama: null,

    /** api: config[heading]
     *  ``Number``  The camera heading in degrees relative to true north. True north 
     *  is 0 degrees, east is 90 degrees, south is 180 degrees, west is 270 
     *  degrees.
     */
    /** private: property[heading]
     *  ``Number``  Camera heading.
     */
    heading: 0,

    /** api: config[pitch]
     *  ``Number``  The camera pitch in degrees, relative to the street view 
     *  vehicle. Ranges from 90 degrees (directly upwards) to -90 degrees 
     *  (directly downwards).
     */
    /** private: property[pitch]
     *  ``Number``  Camery pitch
     */
    pitch: 0,

    /** api: config[zoom]
     *  ``Number``  The zoom level. Fully zoomed-out is level 0, zooming in 
     *  increases the zoom level.
     */
    /** private: property[zoom]
     *  ``Number``  Panorama zoom level
     */
    zoom: 0,

    /** api: config[location]
     *  ``OpenLayers.LonLat``  The panorama location
     */
    /** private: property[location]
     *  ``OpenLayers.LonLat``  Panorama location
     */
    location: null,

    /** private: method[initComponent]
     *  Private initComponent override.
     */
    initComponent : function() {
        var defConfig = {
            plain: true,
            border: false
        };

        Ext.applyIf(this, defConfig);
        return GeoExt.ux.GoogleStreetViewPanel.superclass.initComponent.call(this);
    },

    /** private: method[afterRender]
     *  Private afterRender override.
     */
    afterRender : function() {
        var owner = this.ownerCt;
        if (owner) {
            var size = owner.getSize();
            Ext.applyIf(this, size);
            if (!this.location) {
                // try to derive location from owner (e.g. popup)
                if (GeoExt.Popup) {
                    this.bubble(function(cmp) {
                        if (cmp instanceof GeoExt.Popup) {
                            this.location = cmp.location.clone().transform(
                                cmp.map.getProjectionObject(),
                                new OpenLayers.Projection("EPSG:4326")
                            );
                            return false;
                        }
                    }, this);
                }
            }
        }
        GeoExt.ux.GoogleStreetViewPanel.superclass.afterRender.call(this);
        
        // Configure panorama and associate methods and parameters to it
        var options = {
            position: new google.maps.LatLng(this.location.lat, this.location.lon),
            pov: {
                heading: this.heading,
                pitch: this.pitch,
                zoom: this.zoom
            }
        };
        this.panorama = new google.maps.StreetViewPanorama(this.body.dom, options);
    },

    /** private: method[beforeDestroy]
     *  Destroy Street View Panorama instance and navigation tools
     *
     */
    beforeDestroy: function() {
        delete this.panorama;
        GeoExt.ux.GoogleStreetViewPanel.superclass.beforeDestroy.apply(this, arguments);
    },

    /** private: method[onResize]
     *  Resize Street View Panorama
     *  :param w: ``Number`` Width
     *  :param h: ``Number`` Height
     */
    onResize : function(w, h) {
        GeoExt.ux.GoogleStreetViewPanel.superclass.onResize.apply(this, arguments);
        if (this.panorama) {
            if (typeof this.panorama === "object") {
                google.maps.event.trigger(this.panorama, "resize");
            }
        }
    },

    /** private: method[setSize]
     *  Set size of Street View Panorama
     */
    setSize : function(width, height, animate) {
        GeoExt.ux.GoogleStreetViewPanel.superclass.setSize.apply(this, arguments);
        if (this.panorama) {
            if (typeof this.panorama === "object") {
                google.maps.event.trigger(this.panorama, "resize");
            }
        }
    }
});

Ext.reg("gx_googlestreetviewpanel", GeoExt.ux.GoogleStreetViewPanel);


/**
 * Google Street View Control
 */
GeoExt.ux.GoogleStreetView = OpenLayers.Class(OpenLayers.Control, {
    
    /**
     * Attributes
     */
    CLASS_NAME: 'OpenLayers.Control.GoogleStreetView',
    popup: null,
    
    /**
     * Control setup
     */
    initialize: function(options) {
        OpenLayers.Control.prototype.initialize.apply(this, arguments);
        this.handler = new OpenLayers.Handler.Point(this, {done: this.trigger});
    },
    
    /**
     * Handler for the Point, shows a popup in that location
     */
    trigger: function(point) {
        if (this.popup) {
            this.popup.close();
        }
        if (!point) {
            return;
        }
        var popup_title = 'Street View';
        this.popup = new GeoExt.Popup({
            title: popup_title,
            location: point,
            width: 300,
            height: 300,
            collapsible: true,
            map: this.map,
            items: [new GeoExt.ux.GoogleStreetViewPanel()]
        });
        // for mobile clients the normal close button is very small
        // and hard to use.  To modify it to make it bigger takes
        // a lot of overriding, so this hackish method provides
        // an easier way for touch based clients to close windows
        popup_title += ' <a class="titleClose" href="#" ' +
            'onclick="Ext.getCmp(\'' + this.popup.getId() +
            '\').close(); return false;">Close</a>';
        this.popup.setTitle(popup_title);
        this.popup.show();
        // reduce the user confusion of how to turn the control off
        this.deactivate();
    }
});
