
/*global Ext,OpenLayers,GeoExt */

Ext.namespace('GeoExt.ux');

GeoExt.ux.OverviewMapPanel = Ext.extend(Ext.Panel, {
    map: null,
    floating: true,
    hideMode: 'offsets', 
    ovMapAlign: 'br-br',
    inMapPanel: true,
    //layout: 'fit',
    ctrlOptions: null,
    mapOptions: null,
    layers: null,
    control: null,
    
    initComponent: function () {
        var config = Ext.apply({}, this.initialConfig);
        // create the control
        this.control = this.createControl(config);
        // var ctrlElem = new Ext.Element(this.control.div).wrap();
        var node = Ext.DomQuery.selectNode('#ovwrap .olMap') ||
            Ext.DomQuery.selectNode('#ovwrap .olControlOverviewMapElement');
        this.controlElem = new Ext.Element(node).wrap();
        this.items = [new Ext.BoxComponent(this.controlElem)];
        if (this.inMapPanel && config.hidden !== false) {
            config.hidden = true;
        }
        GeoExt.ux.OverviewMapPanel.superclass.initComponent.apply(this, arguments);
        this.on({
            'show': function (cmp) {
                if (this.inMapPanel) {
                    this.getEl().anchorTo(this.map.viewPortDiv, this.ovMapAlign);
                }
            },
            scope: this
        });
    },
    
    afterRender: function () {
        GeoExt.ux.OverviewMapPanel.superclass.afterRender.apply(this, arguments);
        if (this.autoShow) {
            this.show();
        }
        // force resize of overview map
        /*
        var w = this.getInnerWidth()-this.getFrameWidth();
        var headerHt = (this.header) ? this.header.getHeight() : 0;
        var h=this.getInnerHeight()-this.getFrameHeight()- headerHt;
        this.controlElem.setSize(w,h);
        */
        if (this.floating & this.inMapPanel) {
            this.getEl().anchorTo(this.map.viewPortDiv, this.ovMapAlign);
        }
        Ext.removeNode(Ext.getDom('ovwrap')); //nnn
    },
    
    beforeDestroy: function () {
        if (this.control) {
            this.control.destroy();
            this.control = null;
        }
        GeoExt.ux.OverviewMapPanel.superclass.beforeDestroy.call(this);
    },
    
    createControl: function (opts) {
        // short circuit if someone supplies the control
        if (this.control) {
            return this.control;
        }
        // regular path
        opts.ctrlOptions = opts.ctrlOptions || {};
        // move mapOptions to the ctrlOptions if needed
        if (opts.mapOptions) {
            opts.ctrlOptions.mapOptions = opts.mapOptions;
            delete opts.mapOptions;
        }
        // move layers to the ctrlOptions if needed
        if (opts.layers) {
            opts.ctrlOptions.layers = opts.layers;
            delete opts.layers;
        }
        // infer the map if it is not given
        if (!opts.map) {
            opts.map = GeoExt.MapPanel.guess().map;
        }
        // assign the actual OL Map if a MapPanel was passed
        if (opts.map instanceof GeoExt.MapPanel) {
            opts.map = opts.map.map;
        }
        // use the size of this container unless a size was specifically given
        // specifically set a div if one hasn't been set to make 'outsideViewport' = true
        Ext.applyIf(opts.ctrlOptions,
            {size: new OpenLayers.Size(opts.width, opts.height),
            div: Ext.DomHelper.append(Ext.getBody().dom, {tag: 'div', id: 'ovwrap'})
        });
        var control = new OpenLayers.Control.OverviewMap(opts.ctrlOptions);
        opts.map.addControl(control);
        return control;
    },
    
    xtype: 'gx_overviewmap'
});

Ext.reg('gx_overviewmap', GeoExt.ux.OverviewMapPanel);
