MASAS Tools - Proxy

For more information visit http://www.masas.ca

--
ems-icons.zip contains all of the EM symbology set icons 
http://emsymbology.org/EMS/index.html that are used by MASAS tools and
hosted at http://icon.masas-sics.ca

To use these icons locally instead, unzip, place in an accessible location, and
change to the new location in the included index.html
