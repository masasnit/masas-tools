#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Proxy
Author:         MASAS Dev Team
Created:        Jan 16, 2015
Updated:        Jan 16, 2015
Copyright:      Independent Joint Copyright (c) 2011-2015 MASAS Contributors
License:        Published under the Modified BSD license.  See license.txt for 
                the full text of the license.
Description:    This application serves as a proxy for the MASAS Tools.
"""

import os, sys
import urllib2
import cherrypy
import argparse

# get absolute path name from executable
sys.stdout = sys.stderr
server_pathname = os.path.dirname("/usr/share/masas-tools/proxy/")
# change directory to absolute path name
import site
site.addsitedir(server_pathname)

#
def noBodyProcess():
    """Sets cherrypy.request.process_request_body = False, giving
    us direct control of the body payload."""
    cherrypy.request.process_request_body = False

cherrypy.tools.noBodyProcess = cherrypy.Tool('before_request_body', noBodyProcess)

class RootPage(object):

    def __init__(self):
        """ initial setup """
        return

    @cherrypy.expose
    @cherrypy.tools.noBodyProcess()
    def go(self, *args, **kwargs):
        """ Proxy for cross-domain javascript """
        
        if "url" not in kwargs:
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 3"
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 4"
        # default is 30 seconds which should be long enough to fetch a Feed
        # or an Entry, any longer and the user will wonder what is happening.
        # For POST or GET of other content, supplying a longer timeout
        # is possible because those operations will take longer with large
        # files, ie 3 minutes for 10 MB on most DSL service
        request_timeout = 300
        if "timeout" in kwargs:
            try:
                request_timeout = int(kwargs["timeout"])
                # cherrypy default is 5 mins max for a request
                if request_timeout > 300:
                    raise ValueError
            except:
                cherrypy.response.headers["Content-Type"] = "text/plain"
                return "\nIllegal Request, Error: 5"
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Tools Proxy"}
            if "Authorization" in cherrypy.request.headers:
                new_headers["Authorization"] = cherrypy.request.headers["Authorization"]
            if "Content-Transfer-Encoding" in cherrypy.request.headers:
                new_headers["Content-Transfer-Encoding"] = cherrypy.request.headers["Content-Transfer-Encoding"]
            if "Content-Disposition" in cherrypy.request.headers:
                new_headers["Content-Disposition"] = cherrypy.request.headers["Content-Disposition"]
            if "Slug" in cherrypy.request.headers:
                new_headers["Slug"] = cherrypy.request.headers["Slug"]

            if cherrypy.request.method == "POST":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.rfile.read( original_length )
                new_request = urllib2.Request(url, original_body, new_headers)
                # There is a bug in older python versions < 2.5 where only 200
                # is an acceptable response vs 201
            elif cherrypy.request.method == "PUT":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.rfile.read( original_length )
                new_request = urllib2.Request(url, original_body, new_headers)
                # consider using httplib2 which has builtin PUT method instead
                new_request.get_method = lambda: 'PUT'
            elif cherrypy.request.method == "DELETE":
                new_request = urllib2.Request(url, headers=new_headers)
                new_request.get_method = lambda: 'DELETE'
            elif cherrypy.request.method == "OPTIONS":
                return
            else:
                new_request = urllib2.Request(url, headers=new_headers)
            new_connection = urllib2.urlopen(new_request, timeout=request_timeout)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            return new_data
        except Exception, err:
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    # end go()


def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Headers"] = "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Headers, Authorization, Content-Transfer-Encoding, Slug, Content-Description"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS"
    return

def run( args ):
    """ Run method for standalone server"""

    server_config = {"server.socket_host": "0.0.0.0", "server.socket_port": args.serverPort,
        # set to 10MB to prevent oversize uploads
        "server.max_request_body_size": 10485760
    }

    app_config = {
        "/proxy/": {
            "tools.staticdir.root": os.path.abspath(server_pathname),
            "tools.staticdir.on": True,
            "tools.staticdir.dir": "."
        }
    }

    cherrypy.config.update(server_config)
    cherrypy.tree.mount(RootPage(), "/", app_config)

    # Enable Cross Origin Resource Sharing (aka Cross Domain requests)...
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)
    cherrypy.config.update( { "tools.CORS.on": args.enableCORS } )

    cherrypy.engine.start()
    cherrypy.engine.block()

def application(environ,start_response):
    cherrypy.config.update({'environment': 'embedded'})
    cherrypy.tree.mount(RootPage(), "/proxy/")

    # Enable Cross Origin Resource Sharing (aka Cross Domain requests)...
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', CORS)
    return cherrypy.tree(environ, start_response)

if __name__ == '__main__':
    # root = Root()
    # cherrypy.quickstart(root,"/",config = None)
    parser = argparse.ArgumentParser( description='MASAS Tools Proxy' )
    parser.add_argument( '--enableCORS', action='store_true', help='Enable CORS (default: disabled)' )
    parser.add_argument( '--serverPort', help='Server Port (default: 8181)', default=8181, type=int )

    args = parser.parse_args()

    run( args )
# end main
