# SMTP server for forwarded emails
smpt_server = "localhost:25"
# Return address for forwarded emails
return_address = "test@example.com"

db_info = {"host": "localhost",
        "username": "",
        "password": "",
        "database": "sgc_zones",
        "cd_table": "cd",
        "csd_table": "csd",
        "other_table": "other",
    }