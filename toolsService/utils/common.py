#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 - Common
Author:         Jacob Westfall
Created:        Nov 01, 2011
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2011-2014 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Description:    Common standalone functions.
"""

import mimetools
from cStringIO import StringIO
try:
    from xml.etree import cElementTree as ElementTree
except ImportError:
    try:
        import cElementTree as ElementTree
    except ImportError:
        from elementtree import ElementTree
import smtplib
import urllib2


def addslashes(s):
    """ Escape characters that javascript will have a problem with in 
    the templates. Modelled after PHP addslashes function.  Primarily names
    with ' in them. """
    
    d = {"'":"\\'", "\\":"\\\\"}
    
    return ''.join(d.get(c, c) for c in s)


def unicode_convert(s):
    """ Ensure that an input string is in unicode, decoding using select
    types if necessary """
    
    if not isinstance(s, basestring):
        raise ValueError("Not a string")
    if not isinstance(s, unicode):
        try:
            s = unicode(s, "utf-8")
        except UnicodeDecodeError:
            try:
                s = unicode(s, "iso-8859-1")
            except UnicodeDecodeError:
                raise
    
    return s


def multipart_encode(files):
    """ Combine files for a multipart message POST/PUT """
    
    boundary = mimetools.choose_boundary()
    buffer = StringIO()
    for file_name, file_data, file_type, file_desc, file_size in files:
        buffer.write('--%s\r\n' % boundary)
        buffer.write('Content-Disposition: form-data; name="%s"; filename="%s"\r\n' \
            %(file_name.encode("iso-8859-1"), file_name.encode("iso-8859-1")))
        buffer.write('Content-Type: %s\r\n' %file_type)
        if file_desc:
            buffer.write('Content-Description: %s\r\n' \
                %file_desc.encode("iso-8859-1"))
        buffer.write('\r\n' + file_data + '\r\n')
    buffer.write('--' + boundary + '--\r\n\r\n')
    result = buffer.getvalue()
    
    return boundary, result


def find_acceptable_types(service_doc):
    """ Determine the mimetypes accepted by a Hub from its service document """
    
    #NOTE: assuming single collection "/feed" for now
    
    service_tree = ElementTree.XML(service_doc)
    accept_types = {}
    for accept in service_tree.findall(".//{http://www.w3.org/2007/app}accept"):
        size = accept.attrib.get("maxsize", "1048576")
        type = accept.text.lower().replace(" ", "")
        accept_types[type] = int(size)
    
    if not accept_types:
        raise ValueError("No accept types found")
    else:
        return accept_types


def parse_js_stacktrace(stacktrace):
    """ Parse a Javascript stacktrace string to create a Python version.
    
    According to the traceback module documentation:
    A “pre-processed” stack trace entry is a quadruple (filename,
    line number, function name, text) representing the information that is
    usually printed for a stack trace. The text is a string with leading and
    trailing whitespace stripped; if the source is not available it is None.
    """
    
    result = []
    # all browsers should use \n to delineate trace lines
    trace_lines = stacktrace.split("\n")
    for each_line in trace_lines:
        # ignore blank lines for now, may need better handling later
        if not each_line:
            continue
        # use defaults here and replace with actual values if available
        new_trace = {"filename": "(unknown source)", "lineno": 0}
        # firefox and safari use @ to provide additional info
        if "@" in each_line:
            line_details = each_line.split("@", 1)
            new_trace["function"] = line_details[0]
            file_info = line_details[1]
        else:
            file_info = each_line
        try:
            file_details = file_info.split(":")
            # there could be a : in http:// or : in :8080 so for now just
            # assuming the last part of the line will have lineno
            new_trace["lineno"] = int(file_details.pop())
            if len(file_details) == 1:
                new_trace["filename"] = file_details[0]
            else:
                new_trace["filename"] = ":".join(file_details)
        except:
            new_trace["filename"] = file_info
        # Javascript doesn't provide the 'text' from each line
        result.append((new_trace["filename"], new_trace["lineno"],
            new_trace.get("function", None) , None))
    
    return result


def send_message(server, msg_to, msg_from, msg_txt):
    """ Send a message to an SMTP server """
    
    smtp_connect = smtplib.SMTP(server)
    smtp_connect.sendmail(from_addr=msg_from, to_addrs=msg_to, msg=msg_txt)
    smtp_connect.quit()


def url_download(url, request_timeout):
    """ Download a provided URL """
    
    connection = None
    try:
        proxy_headers = {"User-Agent": "MASAS Portal Proxy"}
        request = urllib2.Request(url, headers=proxy_headers)
        connection = urllib2.urlopen(request, timeout=request_timeout)
        # 10 MB max size on most Hubs
        data = connection.read(10485760)
        return data
    finally:
        if connection:
            connection.close()