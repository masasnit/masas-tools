#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Hub v2.2 - MySQL DB Utils
Author:         Jacob Westfall
Created:        Mar 01, 2011
Updated:        Apr 25, 2011
Copyright:      Copyright (c) 2011
License:        Internal Use Only, No Redistribution Permitted
Description:    Database methods and utilites.
"""

# import MySQLdb
import psycopg2
from psycopg2 import extras

class DBAccess(object):
    """ Database access methods for Hub """
    
    def __init__(self, db_info):
        """ Make initial connection and establish a db cursor """
        
        if "port" in db_info:
            self.db_connect = psycopg2.connect(host=db_info["host"],
                port=db_info["port"], user=db_info["username"],
                passwd=db_info["password"], db=db_info["database"])
        else:
            self.db_connect = psycopg2.connect(host=db_info["host"],
                user=db_info["username"], password=db_info["password"],
                dbname=db_info["database"])
        self.db_cursor = self.db_connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    
    
    def close(self):
        """ Close the DB connection """
        
        try:
            self.db_connect.close()
        except:
            pass
    
    
    def __del__(self):
        """ Clean up the class instance """
        
        self.close()
    
    
    def get_table_info(self, table):
        """ Show the column info for a table """
        
        sql_string = 'SHOW COLUMNS FROM "%s"' %table
        result = self.db_cursor.execute(sql_string)
        return result
    
    
    def get_all(self, table, sort=None, reverse=False, join=None):
        """ Return all values in a table """
        
        #TODO: could combine this method with search since they are similar but
        #      for now keeping seperate to make clear its getting all records
        
        sql_cmds = []
        if join:
            join_selects = []
            join_statements = []
            for join_data in join:
                # alias the table name, alias.field will be in the results
                if not join_data["fields"]:
                    raise ValueError("Join fields blank")
                for j_field in join_data["fields"]:
                    join_selects.append("%s.%s AS '%s.%s'" %(join_data["table"],
                        j_field, join_data["alias"], j_field))
                # joins can match on different field names, no match is okay
                join_statements.append("LEFT JOIN %s ON %s.%s = %s.%s" \
                    %(join_data["table"], table, join_data["lfield"],
                      join_data["table"], join_data["rfield"]))
            sql_cmds.append('SELECT %s.*,%s FROM "%s" "%s"' %(table,
                ",".join(join_selects), table, " ".join(join_statements)))
        else:
            # prefixing everything else with table.fields so that it works
            # properly with or without a join being used
            sql_cmds.append('SELECT %s.* FROM "%s"' %(table, table))
        
        # check for sorting
        if sort:
            sql_cmds.append("ORDER BY %s.%s" %(table, sort))
            if reverse:
                sql_cmds.append("DESC")
        
        self.db_cursor.execute(" ".join(sql_cmds))
        result = self.db_cursor.fetchall()
        return result
    
    
    def search(self, table, field, value, time, match=None, sort=None, reverse=False,
        join=None):
        """ Search the db and return results """
        
        sql_cmds = []
        if join:
            join_selects = []
            join_statements = []
            for join_data in join:
                # alias the table name, alias.field will be in the results
                if not join_data["fields"]:
                    raise ValueError("Join fields blank")
                for j_field in join_data["fields"]:
                    join_selects.append('"%s".%s' %(join_data["table"], j_field))
                # joins can match on different field names, no match is okay
                join_statements.append('LEFT JOIN "%s" ON "%s"."%s" = "%s"."%s"' \
                    %(join_data["table"], table, join_data["lfield"],
                      join_data["table"], join_data["rfield"]))
            sql_cmds.append('SELECT "%s".*,%s FROM "%s" %s' %(table,
                ",".join(join_selects), table, " ".join(join_statements)))
        else:
            # prefixing everything else with table.fields so that it works
            # properly with or without a join being used
            sql_cmds.append('SELECT "%s".* FROM "%s"' %(table, table))
        # always leave the search value quoted for security
        sql_cmds.append('WHERE "%s".%s = %%s' %(table, field))
        sql_data = [value]
        sql_cmds.append('AND "%s"."ActiveRange" @> \'%s\'::timestamp' %(table, time))
        if join:
            for join_data in join:
                sql_cmds.append('AND "%s"."ActiveRange" @> \'%s\'::timestamp' %(join_data["table"], time))
        # check for extra match data
        if match:
            for match_val in match:
                sql_cmds.append('AND "%s".%s = %%s' %(table, match_val))
                sql_data.append(match[match_val])
        # check for sorting
        if sort:
            sql_cmds.append('ORDER BY "%s".%s' %(table, sort))
            if reverse:
                sql_cmds.append("DESC")
#         print " ".join(sql_cmds) % tuple(sql_data)
        self.db_cursor.execute(" ".join(sql_cmds), tuple(sql_data))
        result = self.db_cursor.fetchall()
        
        return result
    
    
    def search_single(self, table, field, value, join=None):
        """ Search the db and return results """
        
        sql_cmds = []
        if join:
            join_selects = []
            join_statements = []
            for join_data in join:
                # alias the table name, alias.field will be in the results
                if not join_data["fields"]:
                    raise ValueError("Join fields blank")
                for j_field in join_data["fields"]:
                    join_selects.append('"%s".%s' %(join_data["table"], j_field))
                # joins can match on different field names, no match is okay
                join_statements.append('LEFT JOIN "%s" ON "%s"."%s" = "%s"."%s"' \
                    %(join_data["table"], table, join_data["lfield"],
                      join_data["table"], join_data["rfield"]))
            sql_cmds.append('SELECT DISTINCT ON (%s) "%s".*,%s FROM "%s" %s' %('"'+ table + '".' + field ,table,
                ",".join(join_selects), table, " ".join(join_statements)))
        else:
            # prefixing everything else with table.fields so that it works
            # properly with or without a join being used
            sql_cmds.append('SELECT "%s".* FROM "%s"' %(table, table))
        # always leave the search value quoted for security
        sql_cmds.append('WHERE "%s".%s = %%s' %(table, field))
        sql_data = [value]

        sql_cmds.append('ORDER BY "%s".%s, "%s".%s ' %(table, field, table, '"ActiveRange"'))

        sql_cmds.append("DESC")
#         print " ".join(sql_cmds) % tuple(sql_data)
        self.db_cursor.execute(" ".join(sql_cmds), tuple(sql_data))
        result = self.db_cursor.fetchall()
        
        return result
    
    def search_norange(self, table, field, value, match=None, sort=None, reverse=False,join=None):
        """ Search the db and return results """
        
        sql_cmds = []
        if join:
            join_selects = []
            join_statements = []
            for join_data in join:
                # alias the table name, alias.field will be in the results
                if not join_data["fields"]:
                    raise ValueError("Join fields blank")
                for j_field in join_data["fields"]:
                    join_selects.append('"%s".%s' %(join_data["table"], j_field))
                # joins can match on different field names, no match is okay
                join_statements.append('LEFT JOIN "%s" ON "%s"."%s" = "%s"."%s"' \
                    %(join_data["table"], table, join_data["lfield"],
                      join_data["table"], join_data["rfield"]))
            sql_cmds.append('SELECT "%s".*,%s FROM "%s" %s' %(table,
                ",".join(join_selects), table, " ".join(join_statements)))
        else:
            # prefixing everything else with table.fields so that it works
            # properly with or without a join being used
            sql_cmds.append('SELECT "%s".* FROM "%s"' %(table, table))
        # always leave the search value quoted for security
        sql_cmds.append('WHERE "%s".%s = %%s' %(table, field))
        sql_data = [value]

        # check for extra match data
        if match:
            for match_val in match:
                sql_cmds.append('AND "%s".%s = %%s' %(table, match_val))
                sql_data.append(match[match_val])
        # check for sorting
        if sort:
            sql_cmds.append('ORDER BY "%s".%s' %(table, sort))
            if reverse:
                sql_cmds.append("DESC")
#         print " ".join(sql_cmds) % tuple(sql_data)
        self.db_cursor.execute(" ".join(sql_cmds), tuple(sql_data))
        result = self.db_cursor.fetchall()
        
        return result
    
    def insert_row(self, table, data, geo=None):
        """ Add a row to the db using an input data dict """
        
        # the db field names are the data keys
        if len(data) > 1:
            fields = ', '.join(data.keys())
        else:
            fields = data.keys()[0]
        # create the correct number of %s placeholders for
        # the insert values
        if len(data) > 1:
            input = ', '.join(["%s"] * len(data.values()))
        else:
            input = "%s"
        if geo:
            # a geo value can't be the only value being inserted
            fields += ", %s" %geo[0]
            if geo[1] == "line"  or geo[1] == "Line":
                input += ", ST_GeomFromText('SRID=4326;LineString(%s)')" %geo[2]
            elif geo[1] == "polygon"  or geo[1] == "Polygon":
                input += ", ST_GeomFromText('SRID=4326;Polygon((%s))')" %geo[2]
            else:
                input += ", ST_GeomFromText('SRID=4326;Point(%s)')" %geo[2]
        # create the sql string with fields and placeholders
        sql_string = 'INSERT INTO "%s" (%s) VALUES (%s)' %(table, fields, input)
#         print sql_string %tuple(data.values())
        self.db_cursor.execute(sql_string, tuple(data.values()))
        self.db_connect.commit()
        # if auto-increment, return the new id
        
        return self.db_cursor.lastrowid
    
    
    def update_row(self, table, field, value, data, match={}, geo=None):
        """ Update a specified row in the db using an input data dict """
        
        # ensure that the search field is not in the update data
        if field in data:
            raise ValueError("Search field in update data")
        # the update field names are the data keys and we use
        # %s placeholders for the update values
        input = []
        for key in data.keys():
            input.append("%s = %%s" %key)
        if len(input) > 1:
            items = ', '.join(input)
        elif len(input) == 0:
            items = ""
        else:
            items = input[0]
        if geo:
            # a geo value can't be the only value updated right now
            if geo[1] == "line":
                items += " %s = ST_GeomFromText('SRID=4326;LineString(%s)')" %(geo[0], geo[2])
            elif geo[1] == "polygon":
                items += " %s = ST_GeomFromText('SRID=4326;Polygon((%s))')" %(geo[0], geo[2])
            else:
                items += " %s = ST_GeomFromText('SRID=4326;Point(%s)')" %(geo[0], geo[2])
        # create the sql string with fields and placeholders and sql data
        sql_string = 'UPDATE "%s" SET %s WHERE "%s" = %%s' %(table, items, field)
        sql_data = data.values()+[value]
        # check for extra match data
        if match:
            for match_val in match:
                sql_string += " AND %s = %%s" %match_val
                sql_data.append(match[match_val])
#         print sql_string %tuple(sql_data)
        self.db_cursor.execute(sql_string, tuple(sql_data))
        self.db_connect.commit()
    
    def delete_row(self, table, field, value, match={}):
        """ Delete the selected row """
        
        # insert db specific strings, but leave the value to search
        # for quoted for the execute instead
        sql_string = 'DELETE FROM "%s" WHERE %s = %%s' %(table, field)
        sql_data = [value]
        # check for extra match data
        if match:
            for match_val in match:
                sql_string += " AND %s = %%s" %match_val
                sql_data.append(match[match_val])
        
        self.db_cursor.execute(sql_string, tuple(sql_data))
        self.db_connect.commit()